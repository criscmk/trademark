# WiT Backend API

Backend application for Women In Trade Platform

## Pre-requisites

You will need the following installed on your local development machine for this to work:

1. [Gradle](https://gradle.org/)

    This is a build & dependency system for JVM languages. No installation is required due to the availability of
    the [gradle wrapper](./gradlew). However, installation of gradle can be found in the link provided

2. [Docker](https://www.docker.com/) & [Docker compose](https://docs.docker.com/compose/)

    Even though not required, you can use these 2 tools for spinning up a running instance of dependent
    services such as a running DB or an external API. This has been setup for convenience & reproducability allowing
    the environment for the application to be replicated easily. Dependent services can be added to the
    [docker-compose file](./docker-compose.yml) which can then be spun up with:

    ```bash
    docker-compse up
    ```
   
   And that should setup the services needed by the application without the hustle of installing these services on your
   local running development environment
   
3. [Groovy SDK](https://groovy-lang.org/)

   Parts of this application have been written in Groovy & installation guide can be found in the provided link

4. [Kotlin SDK](https://kotlinlang.org/)
    
    Parts of this application have been written in Kotlin & installation guides can be found in the provided link

For the above 2 however, [sdkman](https://sdkman.io/) can be used to easily install & set it up.

## Running the application

You can run the application with the IDE of your choice setting up necessary configurations as needed or you can use
gradle to do that:

``` bash
docker-compose up
```

> This starts up services this application will be communicating with, e.g. postgresql

```bash
./gradlew bootRun
```

> This starts up the running application in `default` profile. 

If you want to run it in any other profile e.g. uat, test, dev, live/production etc:

``` bash
export SPRING_PROFILES_ACTIVE=dev
./gradlew bootRun
```

> This will start it up in the `dev` profile

A list of other environment variables can be found [here](.env.example).



