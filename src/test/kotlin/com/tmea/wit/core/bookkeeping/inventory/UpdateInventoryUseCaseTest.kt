package com.tmea.wit.core.bookkeeping.inventory

import com.tmea.wit.core.bookkeeping.inventory.exceptions.InventoryException
import com.tmea.wit.core.bookkeeping.inventory.usecases.UpdateInventoryUseCase
import io.mockk.every
import io.mockk.mockk
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import java.lang.IllegalArgumentException

class UpdateInventoryUseCaseTest {
    private val mockInventoryDataStore = mockk<InventoryDataStore>()
    private val updateInventoryUseCase by lazy {
        UpdateInventoryUseCase(mockInventoryDataStore)
    }

    @Test
    fun `Should throw IllegalArgumentException when null identifier is passed`() {
        val expectedError = "Must pass in valid params to update an inventory item"
        val actual = Assertions.assertThrows(IllegalArgumentException::class.java) {
            updateInventoryUseCase.execute()
        }
        Assertions.assertEquals(expectedError, actual.message)
    }

    @Test
    fun `Should catch exception thrown by inventory data store & return false`() {
        every {
            mockInventoryDataStore.update("identifier", "ugali", 1, 350.00)
        } throws InventoryException("DB Exception")

        val actual = updateInventoryUseCase.execute(
            UpdateInventoryUseCase.Params(
                "identifier", "ugali", 1, 350.00
            )
        )

        Assertions.assertFalse(actual)
    }

    @Test
    fun `Should return true when inventory datastore correctly updates inventory item`() {
        every {
            mockInventoryDataStore.update("identifier", "ugali", 1, 350.00)
        } returns true

        val actual = updateInventoryUseCase.execute(
            UpdateInventoryUseCase.Params(
                "identifier", "ugali", 1, 350.00
            )
        )

        Assertions.assertTrue(actual)
    }
}
