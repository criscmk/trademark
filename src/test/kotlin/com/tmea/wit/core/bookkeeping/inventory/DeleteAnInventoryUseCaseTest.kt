package com.tmea.wit.core.bookkeeping.inventory

import com.tmea.wit.core.bookkeeping.inventory.exceptions.InventoryException
import com.tmea.wit.core.bookkeeping.inventory.usecases.DeleteAnInventoryUseCase
import io.mockk.every
import io.mockk.mockk
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class DeleteAnInventoryUseCaseTest {
    private val mockInventoryDataStore = mockk<InventoryDataStore>()
    private val deleteAnInventoryUseCase by lazy {
        DeleteAnInventoryUseCase(mockInventoryDataStore)
    }

    @Test
    fun `Should throw IllegalArgumentException when null identifier is passed`() {
        val expectedError = "Must pass in valid inventory identifier"
        val actual = Assertions.assertThrows(IllegalArgumentException::class.java) {
            deleteAnInventoryUseCase.execute()
        }
        Assertions.assertEquals(expectedError, actual.message)
    }

    @Test
    fun `Should catch exception thrown by inventory data store & return false`() {
        every {
            mockInventoryDataStore.delete("identifier")
        } throws InventoryException("DB Exception")

        val actual = deleteAnInventoryUseCase.execute("identifier")

        Assertions.assertFalse(actual)
    }

    @Test
    fun `Should return true when inventory datastore correctly updates inventory item`() {
        every {
            mockInventoryDataStore.delete("identifier")
        } returns true

        val actual = deleteAnInventoryUseCase.execute("identifier")

        Assertions.assertTrue(actual)
    }
}
