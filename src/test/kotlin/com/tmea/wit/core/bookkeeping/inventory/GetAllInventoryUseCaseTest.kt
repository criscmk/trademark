package com.tmea.wit.core.bookkeeping.inventory

import com.tmea.wit.core.bookkeeping.inventory.exceptions.InventoryException
import com.tmea.wit.core.bookkeeping.inventory.models.Inventory
import com.tmea.wit.core.bookkeeping.inventory.usecases.GetAllInventoryUseCase
import io.mockk.every
import io.mockk.mockk
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.time.LocalDateTime

class GetAllInventoryUseCaseTest {

    private val mockInventoryDataStore = mockk<InventoryDataStore>()
    private lateinit var getAllInventoryUseCase: GetAllInventoryUseCase

    @BeforeEach
    fun setUp() {
        getAllInventoryUseCase = GetAllInventoryUseCase(mockInventoryDataStore)
    }

    @Test
    fun `Should get all Inventory Items when data store returns items`() {
        val inventory = Inventory(
            identifier = "identifier",
            name = "Rice",
            quantity = 8,
            pricePerUnit = 500.00,
            dateAdded = LocalDateTime.of(2020, 1, 1, 1, 1)
        )
        val expected = setOf(inventory)
        every {
            mockInventoryDataStore.getAll()
        } returns expected

        val actual = getAllInventoryUseCase.execute()

        Assertions.assertEquals(expected, actual)
    }

    @Test
    fun `Should return an empty list data store throws an exception`() {
        every {
            mockInventoryDataStore.getAll()
        } throws InventoryException("Failed to get inventory items")

        val actual = getAllInventoryUseCase.execute()

        Assertions.assertEquals(0, actual.size)
    }
}
