package com.tmea.wit.core.bookkeeping.inventory

import com.tmea.wit.core.bookkeeping.inventory.exceptions.InventoryException
import com.tmea.wit.core.bookkeeping.inventory.models.Inventory
import com.tmea.wit.core.bookkeeping.inventory.usecases.GetAnInventoryItemUseCase
import io.mockk.every
import io.mockk.mockk
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import java.lang.IllegalArgumentException
import java.time.LocalDateTime

class GetAnInventoryItemUseCaseTest {

    private val mockInventoryDataStore = mockk<InventoryDataStore>()
    private val getAnInventoryItemUseCase by lazy {
        GetAnInventoryItemUseCase(mockInventoryDataStore)
    }

    @Test
    fun `Should get an Inventory item when data store returns an item by identifier`() {
        val identifier = "identifier"
        val inventory = Inventory(
            identifier,
            name = "Rice",
            quantity = 8,
            pricePerUnit = 500.00,
            dateAdded = LocalDateTime.of(2020, 1, 1, 1, 1)
        )
        every {
            mockInventoryDataStore.getAnInventoryItem(identifier)
        } returns inventory

        val actual = getAnInventoryItemUseCase.execute(identifier)

        Assertions.assertEquals(inventory, actual)
    }

    @Test
    fun `Should return null when an exception is thrown by data store`() {
        every {
            mockInventoryDataStore.getAnInventoryItem("identifier")
        } throws InventoryException("Failed to get an inventory item")

        val actual = getAnInventoryItemUseCase.execute("identifier")

        Assertions.assertNull(actual)
    }

    @Test
    fun `Should throw IllegalArgumentException when null params are passed to usecase`() {
        val expectedErr = "Must have an identifier to get an Inventory item"
        val actual = Assertions.assertThrows(IllegalArgumentException::class.java) {
            getAnInventoryItemUseCase.execute()
        }

        Assertions.assertEquals(expectedErr, actual.message)
    }
}

