package com.tmea.wit.core.bookkeeping.inventory

import com.tmea.wit.core.bookkeeping.inventory.exceptions.InventoryException
import com.tmea.wit.core.bookkeeping.inventory.usecases.CreateInventoryUseCase
import io.mockk.every
import io.mockk.mockk
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.lang.IllegalArgumentException

class CreateInventoryUseCaseTest {
    private val mockInventoryDataStore = mockk<InventoryDataStore>()
    private lateinit var createInventoryUseCase: CreateInventoryUseCase

    @BeforeEach
    fun setUp() {
        createInventoryUseCase = CreateInventoryUseCase(mockInventoryDataStore)
    }

    @Test
    fun `Should throw IllegalArgumentException when executed with null params`() {
        val expectedErr = "Must pass in an inventory item"
        val actual = Assertions.assertThrows(IllegalArgumentException::class.java) {
            createInventoryUseCase.execute()
        }

        Assertions.assertEquals(expectedErr, actual.message)
    }

    @Test
    fun `Should return an inventory identifier when successfully saved on inventory database`() {
        val identifier = "identifier"
        every {
            mockInventoryDataStore.create(
                name = "Rice",
                pricePerUnit = 400.00,
                quantity = 1
            )
        } returns identifier

        val actual = createInventoryUseCase.execute(
            CreateInventoryUseCase.Params(
                name = "Rice",
                pricePerUnit = 400.00,
                quantity = 1
            )
        )

        Assertions.assertEquals(identifier, actual)
    }

    @Test
    fun `Should return null when inventory datastore throws error`() {
        every {
            mockInventoryDataStore.create(
                name = "Rice",
                pricePerUnit = 400.00,
                quantity = 1
            )
        } throws InventoryException("Failed to save inventory")

        val actual = createInventoryUseCase.execute(
            CreateInventoryUseCase.Params(
                name = "Rice",
                pricePerUnit = 400.00,
                quantity = 1
            )
        )

        Assertions.assertNull(actual)
    }
}
