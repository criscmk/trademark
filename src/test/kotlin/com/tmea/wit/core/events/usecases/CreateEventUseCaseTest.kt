package com.tmea.wit.core.events.usecases

import com.tmea.wit.core.events.models.Ticket
import com.tmea.wit.core.events.models.TicketType
import com.tmea.wit.core.events.usecases.exceptions.EventsException
import com.tmea.wit.core.events.EventsDataStore
import com.tmea.wit.core.storage.PolicyTypes
import com.tmea.wit.core.storage.StorageApi
import io.mockk.every
import io.mockk.mockk
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.lang.IllegalArgumentException
import java.time.LocalDateTime
import java.time.LocalTime

class CreateEventUseCaseTest {
    private val mockEventsDataStore = mockk<EventsDataStore>()
    private val mockStorageApi = mockk<StorageApi>()
    private val createEventUseCase by lazy {
        CreateEventUseCase(
                mockEventsDataStore,
                mockStorageApi
        )
    }

    private val bucket = "events"
    private val someImageUrl = "someurl"

    @Test
    fun `Should throw illegal argument exception when executed with null params`() {
        val expectedErrorMsg = "Must pass in an event item"
        val actual = Assertions.assertThrows(IllegalArgumentException::class.java) {
            createEventUseCase.execute()
        }

        Assertions.assertEquals(expectedErrorMsg, actual.message)
    }

    @Test
    fun `Should return null when Events Exception is thrown by data store`() {
        val tickets = listOf(
                Ticket(
                        type = TicketType.FULL_PASS,
                        description = "Full meals",
                        price = "200.00"
                )
        )
        val description = "Description"
        val organization = "TMEA"
        val type = "Workshop"
        val date = LocalDateTime.of(2020, 1, 1, 1, 1)
        val startTime = LocalTime.of(2, 1, 1)
        val endTime = LocalTime.of(4, 1, 1)
        val title = "Biggest Event Ever!"
        val location = "Kigali, Rwanda"
        val bannerImageUrl = "bannerimage url"
        val imageUrl = "image url"

        every {
            mockStorageApi.saveObjectData(bucket, "$title-$organization-banner-image", bannerImageUrl, PolicyTypes.READ_ONLY)
        } returns someImageUrl

        every {
            mockStorageApi.saveObjectData(bucket, "$title-$organization-image", imageUrl, PolicyTypes.READ_ONLY)
        } returns someImageUrl

        every {
            mockEventsDataStore.create(
                    organization,
                    type,
                    date,
                    startTime,
                    endTime,
                    title,
                    description,
                    location,
                    someImageUrl,
                    someImageUrl,
                    tickets
            )
        } throws EventsException(
                "Failed to create event"
        )

        val actual = createEventUseCase.execute(
                CreateEventUseCase.Params(
                        organization,
                        type,
                        date,
                        startTime,
                        endTime,
                        title,
                        description,
                        location,
                        bannerImageUrl,
                        imageUrl,
                        tickets
                )
        )

        Assertions.assertNull(actual)
    }

    @Test
    fun `Should return event identifier when an event is successfully created by data store`() {
        val tickets = listOf(Ticket(
                        type = TicketType.FULL_PASS,
                        description = "Full meals",
                        price = "200.00"
                )
        )
        val description = "Description"
        val organization = "TMEA"
        val type = "Workshop"
        val date = LocalDateTime.of(2020, 1, 1, 1, 1)
        val startTime = LocalTime.of(2, 1, 1)
        val endTime = LocalTime.of(4, 1, 1)
        val title = "Biggest Event Ever!"
        val location = "Kigali, Rwanda"
        val bannerImageUrl = "bannerimage url"
        val imageUrl = "image url"

        every {
            mockStorageApi.saveObjectData(bucket, "$title-$organization-banner-image", bannerImageUrl, PolicyTypes.READ_ONLY)
        } returns someImageUrl

        every {
            mockStorageApi.saveObjectData(bucket, "$title-$organization-image", imageUrl, PolicyTypes.READ_ONLY)
        } returns someImageUrl

        every {
            mockEventsDataStore.create(
                    organization,
                    type,
                    date,
                    startTime,
                    endTime,
                    title,
                    description,
                    location,
                    someImageUrl,
                    someImageUrl,
                    tickets
            )
        } returns "identifier"

        val actual = createEventUseCase.execute(
                CreateEventUseCase.Params(
                        organization,
                        type,
                        date,
                        startTime,
                        endTime,
                        title,
                        description,
                        location,
                        bannerImageUrl,
                        imageUrl,
                        tickets
                )
        )

        Assertions.assertEquals("identifier", actual)
    }
}
