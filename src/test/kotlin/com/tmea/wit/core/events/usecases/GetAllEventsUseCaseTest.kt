package com.tmea.wit.core.events.usecases

import com.tmea.wit.core.events.models.Event
import com.tmea.wit.core.events.models.Ticket
import com.tmea.wit.core.events.models.TicketType
import com.tmea.wit.core.events.usecases.exceptions.EventsException
import com.tmea.wit.core.events.EventsDataStore
import io.mockk.every
import io.mockk.mockk
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.time.LocalDateTime
import java.time.LocalTime

class GetAllEventsUseCaseTest {

    private val mockEventsDataStore = mockk<EventsDataStore>()
    private lateinit var getAllEventsUseCase: GetAllEventsUseCase

    @BeforeEach
    fun setUp() {
        getAllEventsUseCase =
                GetAllEventsUseCase(
                        mockEventsDataStore
                )
    }

    @Test
    fun `Should return an empty list when EventNotFoundException is thrown by data store`() {
        every {
            mockEventsDataStore.getAll()
        } throws EventsException(
                "DB error"
        )

        val actual = getAllEventsUseCase.execute()

        Assertions.assertEquals(0, actual.size)
    }

    @Test
    fun `Should return all events when data store retrieves all events`() {
        val ticket = Ticket(
                identifier = "identifier",
                type = TicketType.FULL_PASS,
                price = "TZSH 2500",
                description = "Full meals"
        )
        val event = Event(
                identifier = "identifier",
                organization = "TMEA",
                type = "Workshop",
                date = LocalDateTime.now(),
                startTime = LocalTime.now(),
                endTime = LocalTime.now().plusHours(2),
                title = "Largest Biz Conference in East Africa",
                description = "Long description",
                location = "Kigali, Rwanda",
                bannerImage = "https://image.png",
                image = "https://image.png",
                tickets = mutableSetOf(ticket)
        )

        every {
            mockEventsDataStore.getAll()
        } returns mutableSetOf(event)

        val actualResponse = getAllEventsUseCase.execute()

        Assertions.assertEquals(mutableSetOf(event), actualResponse)
    }
}
