package com.tmea.wit.api.rest.bookkeeping.inventory

import com.tmea.wit.api.rest.bookkeeping.inventory.dto.InventoryRequestDto
import com.tmea.wit.api.rest.bookkeeping.inventory.dto.InventoryResponseDto
import com.tmea.wit.core.bookkeeping.inventory.models.Inventory
import com.tmea.wit.core.bookkeeping.inventory.usecases.CreateInventoryUseCase
import com.tmea.wit.core.bookkeeping.inventory.usecases.DeleteAnInventoryUseCase
import com.tmea.wit.core.bookkeeping.inventory.usecases.GetAllInventoryUseCase
import com.tmea.wit.core.bookkeeping.inventory.usecases.GetAnInventoryItemUseCase
import com.tmea.wit.core.bookkeeping.inventory.usecases.UpdateInventoryUseCase
import io.mockk.every
import io.mockk.mockk
import org.junit.Test
import org.junit.jupiter.api.Assertions
import java.time.LocalDateTime

class InventoryServiceTest {
    private val mockGetAllInventoryUseCase = mockk<GetAllInventoryUseCase>()
    private val mockGetAnInventoryUseCase = mockk<GetAnInventoryItemUseCase>()
    private val mockCreateInventoryUseCase = mockk<CreateInventoryUseCase>()
    private val mockUpdateInventoryUseCase = mockk<UpdateInventoryUseCase>()
    private val mockDeleteAnInventoryUseCase = mockk<DeleteAnInventoryUseCase>()

    private val inventoryService by lazy {
        InventoryService(
                mockGetAllInventoryUseCase,
                mockCreateInventoryUseCase,
                mockUpdateInventoryUseCase,
                mockGetAnInventoryUseCase,
                mockDeleteAnInventoryUseCase
        )
    }

    @Test
    fun `Should return a collection of inventory items`() {
        val inventories = mutableSetOf(
                Inventory(
                        identifier = "14124142",
                        name = "Rice",
                        quantity = 1,
                        pricePerUnit = 450.00,
                        dateAdded = LocalDateTime.of(2020, 1, 1, 1, 1)
                )
        )

        val inventoriesDto = arrayListOf(
                InventoryResponseDto(
                        identifier = "14124142",
                        name = "Rice",
                        quantity = 1,
                        pricePerUnit = 450.00,
                        dateAdded = LocalDateTime.of(2020, 1, 1, 1, 1)
                )
        )

        every {
            mockGetAllInventoryUseCase.execute()
        } returns inventories

        val actualInventories = inventoryService.getAll()
        Assertions.assertEquals(inventoriesDto, actualInventories)
    }

    @Test
    fun `Should return an inventory item when an inventory item exists`() {
        val identifier = "14124142"
        val inventory = Inventory(
                identifier,
                name = "Rice",
                quantity = 1,
                pricePerUnit = 450.00,
                dateAdded = LocalDateTime.of(2020, 1, 1, 1, 1)
        )

        val inventoryDto = InventoryResponseDto(
                identifier,
                name = "Rice",
                quantity = 1,
                pricePerUnit = 450.00,
                dateAdded = LocalDateTime.of(2020, 1, 1, 1, 1)
        )

        every {
            mockGetAnInventoryUseCase.execute(identifier)
        } returns inventory

        val actualInventory = inventoryService.getAnInventoryItem(identifier)
        Assertions.assertEquals(inventoryDto, actualInventory)
    }

    @Test
    fun `Should return null when inventory item does not exist`() {
        val identifier = "14124142"
        every {
            mockGetAnInventoryUseCase.execute(identifier)
        } returns null

        val actualInventory = inventoryService.getAnInventoryItem(identifier)
        Assertions.assertNull(actualInventory)
    }

    @Test
    fun `Should return identifier if there is successful creation of inventory item`() {
        val identifier = "identifier"
        val inventoryRequest =
                InventoryRequestDto(
                        name = "Rice",
                        quantity = 1,
                        pricePerUnit = 450.00
                )

        every {
            mockCreateInventoryUseCase.execute(
                    CreateInventoryUseCase.Params(
                            name = inventoryRequest.name,
                            quantity = inventoryRequest.quantity,
                            pricePerUnit = inventoryRequest.pricePerUnit
                    )
            )
        } returns identifier

        val actualIdentifier = inventoryService.create(inventoryRequest)
        Assertions.assertEquals(identifier, actualIdentifier)
    }

    @Test
    fun `Should return boolean value if there is successful or failure to update inventory item usecase`() {
        val identifier = "identifier"
        val inventoryRequest =
                InventoryRequestDto(
                        name = "Rice",
                        quantity = 1,
                        pricePerUnit = 450.00
                )

        every {
            mockUpdateInventoryUseCase.execute(
                    UpdateInventoryUseCase.Params(
                            identifier = identifier,
                            name = inventoryRequest.name,
                            quantity = inventoryRequest.quantity,
                            pricePerUnit = inventoryRequest.pricePerUnit
                    )
            )
        } returns true

        val actual = inventoryService.update(identifier, inventoryRequest)
        Assertions.assertTrue(actual)
    }

    @Test
    fun `Should return boolean value if there is successful or failure to delete an inventory item`() {
        val identifier = "identifier"

        every {
            mockDeleteAnInventoryUseCase.execute(identifier)
        } returns true

        val actual = inventoryService.delete(identifier)
        Assertions.assertTrue(actual)
    }
}