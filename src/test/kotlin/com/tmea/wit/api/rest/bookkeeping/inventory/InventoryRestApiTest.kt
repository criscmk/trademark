package com.tmea.wit.api.rest.bookkeeping.inventory

import com.tmea.wit.api.rest.bookkeeping.inventory.dto.InventoryRequestDto
import com.tmea.wit.api.rest.bookkeeping.inventory.dto.InventoryResponseDto
import io.mockk.every
import io.mockk.mockk
import org.junit.Before
import org.junit.Test
import org.junit.jupiter.api.Assertions
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import java.time.LocalDateTime
import java.util.Collections

class InventoryRestApiTest {
    private val mockInventoryService = mockk<InventoryService>()
    private lateinit var inventoryRestApi: InventoryRestApi

    @Before
    fun setUp() {
        inventoryRestApi = InventoryRestApi(mockInventoryService)
    }

    @Test
    fun `Should return a list of inventories when inventories service returns inventories`() {
        val inventoriesDto = arrayListOf(
                InventoryResponseDto(
                        identifier = "14124142",
                        name = "Rice",
                        quantity = 1,
                        pricePerUnit = 450.00,
                        dateAdded = LocalDateTime.of(2020, 1, 1, 1, 1)
                )
        )

        every {
            mockInventoryService.getAll()
        } returns inventoriesDto

        val expectedResponse = ResponseEntity(inventoriesDto, HttpStatus.OK)

        val actual = inventoryRestApi.getAllInventories()
        Assertions.assertEquals(expectedResponse, actual)
    }

    @Test
    fun `Should return an inventory item when inventory service returns an inventory item`() {
        val identifier = "14124142"
        val inventoryDto = InventoryResponseDto(
                identifier = "14124142",
                name = "Rice",
                quantity = 1,
                pricePerUnit = 450.00,
                dateAdded = LocalDateTime.of(2020, 1, 1, 1, 1)
        )

        every {
            mockInventoryService.getAnInventoryItem(identifier)
        } returns inventoryDto

        val expectedResponse = ResponseEntity(inventoryDto, HttpStatus.OK)

        val actual = inventoryRestApi.getAnInventoryItem(identifier)
        Assertions.assertEquals(expectedResponse, actual)
    }

    @Test
    fun `Should return identifier if there is a successful creation of an inventory item from inventories service`() {
        val identifier = "identifier"
        val inventoriesDto = InventoryRequestDto(
                name = "Rice",
                quantity = 1,
                pricePerUnit = 450.00
        )

        every {
            mockInventoryService.create(inventoriesDto)
        } returns identifier

        val expectedResponse = ResponseEntity(Collections.singletonMap("identifier", identifier), HttpStatus.CREATED)

        val actual = inventoryRestApi.createInventory(inventoriesDto)
        Assertions.assertEquals(expectedResponse, actual)
    }

    @Test
    fun `Should return error message if there is an unsuccessful creation of an inventory item from inventories svc`() {
        val inventoriesDto = InventoryRequestDto(
                name = "Rice",
                quantity = 1,
                pricePerUnit = 450.00
        )

        every {
            mockInventoryService.create(inventoriesDto)
        } returns null

        val expectedResponse =
                ResponseEntity(Collections.singletonMap("Error", "failed to create inventory"), HttpStatus.NOT_ACCEPTABLE)

        val actual = inventoryRestApi.createInventory(inventoriesDto)
        Assertions.assertEquals(expectedResponse, actual)
    }

    @Test
    fun `Should return boolean value depending on update inventory use case`() {
        val identifier = "identifier"
        val inventoryDto = InventoryRequestDto(
                name = "Rice",
                quantity = 1,
                pricePerUnit = 450.00
        )

        every {
            mockInventoryService.update(identifier, inventoryDto)
        } returns true

        val expectedResponse =
                ResponseEntity(Collections.singletonMap("success", true), HttpStatus.ACCEPTED)

        val actual = inventoryRestApi.updateInventory(identifier, inventoryDto)
        Assertions.assertEquals(expectedResponse, actual)
    }

    @Test
    fun `Should return boolean value depending on deletion of an inventory item use case`() {
        val identifier = "identifier"
        every {
            mockInventoryService.delete(identifier)
        } returns true

        val expectedResponse =
                ResponseEntity(Collections.singletonMap("success", true), HttpStatus.ACCEPTED)

        val actual = inventoryRestApi.deleteInventory(identifier)
        Assertions.assertEquals(expectedResponse, actual)
    }
}
