package com.tmea.wit.database.bookkeeping.inventory

import com.tmea.wit.core.bookkeeping.inventory.exceptions.InventoryException
import com.tmea.wit.core.bookkeeping.inventory.models.Inventory
import com.tmea.wit.core.generateIdentifier
import io.mockk.every
import io.mockk.mockk
import io.mockk.mockkStatic
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import java.time.LocalDateTime

class InventoryDataStoreImplTest {

    private val mockInventoryRepository = mockk<InventoryRepository>()
    private val inventoryDataStore: InventoryDataStoreImpl by lazy {
        InventoryDataStoreImpl(mockInventoryRepository)
    }

    private val inventoryEntity = InventoryEntity(
            identifier = "identifier",
            name = "Rice",
            quantity = 8,
            pricePerUnit = 400.00,
            dateAdded = LocalDateTime.of(2020, 1, 1, 1, 1)
    )

    private val inventory = Inventory(
            identifier = "identifier",
            name = "Rice",
            quantity = 8,
            pricePerUnit = 400.00,
            dateAdded = LocalDateTime.of(2020, 1, 1, 1, 1)
    )

    @Test
    fun `Should get all inventory items from DB`() {
        val expected = setOf(inventory)

        every {
            mockInventoryRepository.findAll()
        } returns listOf(inventoryEntity)

        val actual = inventoryDataStore.getAll()
        Assertions.assertEquals(expected, actual)
    }

    @Test
    fun `Should get an inventory item from DB`() {
        val expected = inventory

        every {
            mockInventoryRepository.findByIdentifier(inventory.identifier)
        } returns inventoryEntity

        val actual = inventoryDataStore.getAnInventoryItem(inventory.identifier)
        Assertions.assertEquals(expected, actual)
    }

    @Test
    fun `Should return null when an inventory item can not be found from DB`() {
        every {
            mockInventoryRepository.findByIdentifier(inventory.identifier)
        } returns null

        val actual = inventoryDataStore.getAnInventoryItem(inventory.identifier)
        Assertions.assertNull(actual)
    }

    @Test
    fun `Should throw Exception when findAll throws an error`() {
        val exceptionMessage = "DB exception"

        every {
            mockInventoryRepository.findAll()
        } throws Exception(exceptionMessage)

        val actual = Assertions.assertThrows(InventoryException::class.java) {
            inventoryDataStore.getAll()
        }
        Assertions.assertEquals("Failed to fetch all inventories, Err: $exceptionMessage", actual.message)
    }

    @Test
    fun `Should throw exception when there is a failure to create a new inventory Item`() {
        val exceptionMessage = "DB exception"
        val identifier = "identifier"
        val name = "Rice"
        val quantity = 10
        val pricePerUnit = 400.00

        mockkStatic("java.time.LocalDateTime")
        every {
            LocalDateTime.now()
        } returns LocalDateTime.of(2020, 1, 1, 1, 1)

        mockkStatic("com.tmea.wit.core.UtilsKt")
        every {
            generateIdentifier(name)
        } returns identifier

        val inventoryItem = InventoryEntity(
                identifier = identifier,
                name = name,
                quantity = quantity,
                pricePerUnit = pricePerUnit,
                dateAdded = LocalDateTime.now()
        )

        every {
            mockInventoryRepository.save(inventoryItem)
        } throws Exception(exceptionMessage)

        val actual = Assertions.assertThrows(InventoryException::class.java) {
            inventoryDataStore.create(name, quantity, pricePerUnit)
        }
        Assertions.assertEquals("Failed to create inventory item", actual.message)
    }

    @Test
    fun `Should return identifier of newly created inventory item`() {
        val identifier = "identifier"
        val name = "Rice"
        val quantity = 10
        val pricePerUnit = 400.00

        mockkStatic("java.time.LocalDateTime")
        every {
            LocalDateTime.now()
        } returns LocalDateTime.of(2020, 1, 1, 1, 1)

        mockkStatic("com.tmea.wit.core.UtilsKt")
        every {
            generateIdentifier(name)
        } returns identifier

        val inventoryItem = InventoryEntity(
                identifier = identifier,
                name = name,
                quantity = quantity,
                pricePerUnit = pricePerUnit,
                dateAdded = LocalDateTime.now()
        )

        every {
            mockInventoryRepository.save(inventoryItem)
        } returns inventoryItem

        val actual = inventoryDataStore.create(name, quantity, pricePerUnit)

        Assertions.assertEquals(identifier, actual)
    }

    @Test
    fun `Should return true when successfully deleting an inventory item`() {
        val identifier = "identifier"
        val name = "Rice"
        val quantity = 10
        val pricePerUnit = 400.00

        val inventoryItem = InventoryEntity(
                identifier = identifier,
                name = name,
                quantity = quantity,
                pricePerUnit = pricePerUnit,
                dateAdded = LocalDateTime.now()
        )

        every {
            mockInventoryRepository.findByIdentifier(identifier)
        } returns inventoryItem

        every {
            mockInventoryRepository.delete(inventoryItem)
        } returns Unit

        val actual = inventoryDataStore.delete(identifier)

        Assertions.assertTrue(actual)
    }

    @Test
    fun `Should return false when an inventory item does not exist`() {
        val identifier = "identifier"
        val name = "Rice"
        val quantity = 10
        val pricePerUnit = 400.00

        every {
            mockInventoryRepository.findByIdentifier(identifier)
        } returns null

        val actual = inventoryDataStore.update(identifier, name, quantity, pricePerUnit)

        Assertions.assertFalse(actual)
    }

    @Test
    fun `Should return false when can not delete an inventory item that does not exist`() {
        val identifier = "identifier"

        every {
            mockInventoryRepository.findByIdentifier(identifier)
        } returns null

        val actual = inventoryDataStore.delete(identifier)

        Assertions.assertFalse(actual)
    }

    @Test
    fun `Should throw InventoryException when there is a failure to update an Inventory item that exists`() {
        val identifier = "identifier"
        val name = "Rice"
        val quantity = 10
        val pricePerUnit = 400.00

        val inventoryItem = InventoryEntity(
                identifier = identifier,
                name = name,
                quantity = quantity,
                pricePerUnit = pricePerUnit,
                dateAdded = LocalDateTime.now()
        )

        every {
            mockInventoryRepository.findByIdentifier(identifier)
        } returns inventoryItem

        every {
            mockInventoryRepository.save(inventoryItem)
        } throws Exception("Some DB Exception")

        val actual = Assertions.assertThrows(InventoryException::class.java) {
            inventoryDataStore.update(identifier, name, quantity, pricePerUnit)
        }
        Assertions.assertEquals("Failed to update inventory item identifier: $identifier", actual.message)
    }

    @Test
    fun `Should throw InventoryException when there is a failure to delete an Inventory item that exists`() {
        val identifier = "identifier"
        val name = "Rice"
        val quantity = 10
        val pricePerUnit = 400.00

        val inventoryItem = InventoryEntity(
                identifier = identifier,
                name = name,
                quantity = quantity,
                pricePerUnit = pricePerUnit,
                dateAdded = LocalDateTime.now()
        )

        every {
            mockInventoryRepository.findByIdentifier(identifier)
        } returns inventoryItem

        every {
            mockInventoryRepository.delete(inventoryItem)
        } throws Exception("Some DB Exception")

        val actual = Assertions.assertThrows(InventoryException::class.java) {
            inventoryDataStore.delete(identifier)
        }
        Assertions.assertEquals("Failed to delete inventory item identifier: $identifier", actual.message)
    }
}