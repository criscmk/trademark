-- auto-generated definition
create table import_export_locations
(
    id          bigserial not null
        constraint import_export_locations_pk
            primary key,
    code        varchar(20),
    name        varchar(100),
    description text,
    added_by    bigint,
    is_active   boolean   default true,
    date_added  timestamp default CURRENT_TIMESTAMP
);

alter table import_export_locations
    owner to postgres;

create unique index import_export_locations_id_uindex
    on import_export_locations (id);


create table standard
(
    id                 bigserial not null
        constraint standard_pk
            primary key,
    title              varchar(50),
    description        text,
    trade_operation_id bigint
        constraint standard_trade_operation_id_fk
            references trade_operation,
    commodity_id        bigint
        constraint standard_commodity_category_id_fk
            references commodity_category,
    import_location    bigint
        constraint standard_import_export_locations_id_fk
            references import_export_locations,
    export_location    bigint
        constraint standard_import_export_locations_id_fk_2
            references import_export_locations,
    is_active          boolean   default true,
    added_by           bigint,
    date_added         timestamp default CURRENT_TIMESTAMP
);

alter table standard
    owner to postgres;

create unique index standard_id_uindex
    on standard (id);


-- auto-generated definition
create table standard_specification_type
(
    id          bigserial   not null
        constraint standard_specification_type_pkey
            primary key,
    name        varchar(100) not null,
    description text,
    is_active   boolean default true
);

alter table standard_specification_type
    owner to postgres;

-- auto-generated definition
create table standard_specification
(
    id          bigserial not null
        constraint standard_specification_pk
            primary key,
    standard_id bigint
        constraint standard_specification_standard_id_fk
            references standard,
    title       varchar(100),
    description text,
    added_by    bigint,
    is_active   boolean   default true,
    date_added  timestamp default CURRENT_TIMESTAMP
);

alter table standard_specification
    owner to postgres;

create unique index standard_specification_id_uindex
    on standard_specification (id);

-- auto-generated definition
create table tax_information
(
    id                     bigserial not null
        constraint tax_information_pk
            primary key,
    tax_id                 bigint,
    description            text,
    basis_of_charge        text,
    taxable_good           text,
    rate                   double precision,
    exemption              text,
    additional_information text,
    regulatory_agency_id   bigint
        constraint tax_information_regulatory_agency_id_fk
            references regulatory_agency
);

alter table tax_information
    owner to postgres;

create unique index tax_information_id_uindex
    on tax_information (id);

