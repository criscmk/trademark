alter table standard_specification
    add standard_specification_type_id BIGINT;
alter table standard_specification
    add constraint standard_specification_standard_specification_type__fk
        foreign key (standard_specification_type_id) references standard_specification_type(id)
            on update cascade on delete cascade;