alter table logistics_service_tariff
    add is_active boolean;

create table logistics_service_order_price
(
    id serial not null,
    logistics_service_order_id int,
    added_by int,
    date_added timestamp,
    is_active boolean,
    constraint fk_logistics_service_order
        foreign key (logistics_service_order_id) references logistics_service_order(id)
            on update cascade on delete cascade
);

create unique index logistics_service_order_price_id_uindex
    on logistics_service_order_price (id);

alter table logistics_service_order_price
    add constraint logistics_service_order_price_pk
        primary key (id);

alter table logistics_vehicle_type
    add added_by bigint;

alter table logistics_vehicle_type
    add date_added timestamp default current_timestamp;

alter table logistics_service_order
    add price double precision;

create table logistics_service_order_request_price_update
(
    id bigserial,
    logistics_service_order_request_id bigint,
    updated_price double precision,
    is_active boolean,
    date_updated timestamp,
    updated_by bigint,
    constraint logistics_service_order_request_price_update__fk
        foreign key (logistics_service_order_request_id) references logistics_service_order_request(id)
            on update cascade on delete cascade
);

create unique index logistics_service_order_request_price_update_id_uindex
    on logistics_service_order_request_price_update (id);

alter table logistics_service_order_request_price_update
    add constraint logistics_service_order_request_price_update_pk
        primary key (id);


alter table logistics_service_tariff
    add maximum_order integer;


create table logistics_trip_status
(
    id bigserial not null,
    name varchar,
    description varchar
);

create unique index logistics_trip_status_id_uindex
    on logistics_trip_status (id);

alter table logistics_trip_status
    add constraint logistics_trip_status_pk
        primary key (id);

create table logistics_trip
(
    id bigserial not null,
    vehicle_id bigint,
    logistics_trip_status_id bigint,
    is_active boolean default true,
    created_by bigint,
    date_created timestamp default current_timestamp,
    constraint logistics_trip_logistics_trader_vehicle__fk
        foreign key (vehicle_id) references logistics_trader_vehicle(id)
            on update cascade on delete cascade,
    constraint logistics_trip_logistics_trip_status__fk
        foreign key (logistics_trip_status_id) references logistics_trip_status(id)
            on update cascade on delete cascade,
    constraint logistics_trip_user__fk
        foreign key (created_by) references "user"(id)
            on update cascade on delete cascade
);

create unique index logistics_trip_id_uindex
    on logistics_trip (id);

alter table logistics_trip
    add constraint logistics_trip_pk
        primary key (id);

create table logistics_trip_order
(
    id bigserial not null,
    logistics_trip_id bigint,
    logistics_order_id bigint,
    is_active boolean default true,
    date_created timestamp default current_timestamp,
    assigned_by bigint,
    constraint logistics_order_trip_logistics_service_order__fk
        foreign key (logistics_order_id) references logistics_service_order(id)
            on update cascade on delete cascade,
    constraint logistics_order_trip_logistics_trip__fk
        foreign key (logistics_trip_id) references logistics_trip(id)
            on update cascade on delete cascade,
    constraint logistics_order_trip_user__fk
        foreign key (assigned_by) references "user"(id)
            on update cascade on delete cascade
);

create unique index logistics_trip_order_id_uindex
    on logistics_trip_order (id);

alter table logistics_trip_order
    add constraint logistics_order_trip_pk
        primary key (id);

create table logistics_trip_cancellation
(
    id bigserial not null,
    logistics_trip_id bigint,
    comment varchar,
    date_cancelled timestamp default current_timestamp,
    cancelled_by bigint,
    constraint logistics_trip_cancellation_logistics_trip__fk
        foreign key (logistics_trip_id) references logistics_trip(id)
            on update cascade on delete cascade,
    constraint logistics_trip_cancellation_user__fk
        foreign key (cancelled_by) references "user"(id)
            on update cascade on delete cascade
);

create unique index logistics_trip_cancellation_id_uindex
    on logistics_trip_cancellation (id);

alter table logistics_trip_cancellation
    add constraint logistics_trip_cancellation_pk
        primary key (id);

create table logistics_trip_dispatch
(
    id serial not null,
    logistics_trip_id bigint,
    dispatched_by int,
    date_dispatched timestamp default current_timestamp,
    constraint logistics_trip_dispatch_logistics_trip__fk
        foreign key (logistics_trip_id) references logistics_trip(id)
            on update cascade on delete cascade,
    constraint logistics_trip_dispatch_user__fk
        foreign key (dispatched_by) references "user"(id)
            on update cascade on delete cascade
);

create unique index logistics_trip_dispatch_id_uindex
    on logistics_trip_dispatch (id);

alter table logistics_trip_dispatch
    add constraint logistics_trip_dispatch_pk
        primary key (id);

create table logistics_trip_completion
(
    id serial not null,
    logistics_trip_id bigint,
    completed_by int,
    date_completed timestamp default current_timestamp,
    constraint logistics_trip_completion_logistics_trip__fk
        foreign key (logistics_trip_id) references logistics_trip(id)
            on update cascade on delete cascade,
    constraint logistics_trip_completion_user__fk
        foreign key (completed_by) references "user"(id)
            on update cascade on delete cascade
);

create unique index logistics_trip_completion_id_uindex
    on logistics_trip_completion (id);

alter table logistics_trip_completion
    add constraint logistics_trip_completion_pk
        primary key (id);






