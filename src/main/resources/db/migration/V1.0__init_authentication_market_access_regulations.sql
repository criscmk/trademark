-- ----------------------------
-- Sequence structure for approval_status_id_seq
-- ----------------------------

CREATE SEQUENCE "public"."approval_status_id_seq"
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

-- ----------------------------
-- Sequence structure for association_id_seq
-- ----------------------------

CREATE SEQUENCE "public"."association_id_seq"
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

-- ----------------------------
-- Sequence structure for association_member_id_seq
-- ----------------------------
CREATE SEQUENCE "public"."association_member_id_seq"
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

-- ----------------------------
-- Sequence structure for association_staff_id_seq
-- ----------------------------
CREATE SEQUENCE "public"."association_staff_id_seq"
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

-- ----------------------------
-- Sequence structure for charge_mode_id_seq
-- ----------------------------
CREATE SEQUENCE "public"."charge_mode_id_seq"
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

-- ----------------------------
-- Sequence structure for commodity_category_id_seq
-- ----------------------------
CREATE SEQUENCE "public"."commodity_category_id_seq"
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

-- ----------------------------
-- Sequence structure for commodity_id_seq
-- ----------------------------
CREATE SEQUENCE "public"."commodity_id_seq"
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

-- ----------------------------
-- Sequence structure for commodity_sub_category_id_seq
-- ----------------------------
CREATE SEQUENCE "public"."commodity_sub_category_id_seq"
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

-- ----------------------------
-- Sequence structure for country_id_seq
-- ----------------------------
CREATE SEQUENCE "public"."country_id_seq"
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

-- ----------------------------
-- Sequence structure for currency_id_seq
-- ----------------------------
CREATE SEQUENCE "public"."currency_id_seq"
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

-- ----------------------------
-- Sequence structure for delivery_option_id_seq
-- ----------------------------
CREATE SEQUENCE "public"."delivery_option_id_seq"
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

-- ----------------------------
-- Sequence structure for duration_id_seq
-- ----------------------------
CREATE SEQUENCE "public"."duration_id_seq"
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

-- ----------------------------
-- Sequence structure for email_template_config_id_seq
-- ----------------------------
CREATE SEQUENCE "public"."email_template_config_id_seq"
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

-- ----------------------------
-- Sequence structure for global_config_id_seq
-- ----------------------------

CREATE SEQUENCE "public"."global_config_id_seq"
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

-- ----------------------------
-- Sequence structure for international_location_description_id_seq
-- ----------------------------
CREATE SEQUENCE "public"."international_location_description_id_seq"
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

-- ----------------------------
-- Sequence structure for international_region_id_seq
-- ----------------------------

CREATE SEQUENCE "public"."international_region_id_seq"
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

-- ----------------------------
-- Sequence structure for language_id_seq
-- ----------------------------

CREATE SEQUENCE "public"."language_id_seq"
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

-- ----------------------------
-- Sequence structure for location_id_seq
-- ----------------------------

CREATE SEQUENCE "public"."location_id_seq"
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

-- ----------------------------
-- Sequence structure for location_level_id_seq
-- ----------------------------

CREATE SEQUENCE "public"."location_level_id_seq"
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

-- ----------------------------
-- Sequence structure for logistics_rating_user_type_id_seq
-- ----------------------------

CREATE SEQUENCE "public"."logistics_rating_user_type_id_seq"
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

-- ----------------------------
-- Sequence structure for logistics_service_additional_charge_id_seq
-- ----------------------------

CREATE SEQUENCE "public"."logistics_service_additional_charge_id_seq"
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

-- ----------------------------
-- Sequence structure for logistics_service_id_seq
-- ----------------------------

CREATE SEQUENCE "public"."logistics_service_id_seq"
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

-- ----------------------------
-- Sequence structure for logistics_service_order_cancellation_id_seq
-- ----------------------------

CREATE SEQUENCE "public"."logistics_service_order_cancellation_id_seq"
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

-- ----------------------------
-- Sequence structure for logistics_service_order_id_seq
-- ----------------------------

CREATE SEQUENCE "public"."logistics_service_order_id_seq"
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

-- ----------------------------
-- Sequence structure for logistics_service_order_payment_id_seq
-- ----------------------------

CREATE SEQUENCE "public"."logistics_service_order_payment_id_seq"
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

-- ----------------------------
-- Sequence structure for logistics_service_order_rating_id_seq
-- ----------------------------

CREATE SEQUENCE "public"."logistics_service_order_rating_id_seq"
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

-- ----------------------------
-- Sequence structure for logistics_service_order_request_approval_id_seq
-- ----------------------------

CREATE SEQUENCE "public"."logistics_service_order_request_approval_id_seq"
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

-- ----------------------------
-- Sequence structure for logistics_service_order_request_id_seq
-- ----------------------------

CREATE SEQUENCE "public"."logistics_service_order_request_id_seq"
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

-- ----------------------------
-- Sequence structure for logistics_service_order_status_id_seq
-- ----------------------------

CREATE SEQUENCE "public"."logistics_service_order_status_id_seq"
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

-- ----------------------------
-- Sequence structure for logistics_service_tariff_charge_id_seq
-- ----------------------------

CREATE SEQUENCE "public"."logistics_service_tariff_charge_id_seq"
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

-- ----------------------------
-- Sequence structure for logistics_service_tariff_id_seq
-- ----------------------------

CREATE SEQUENCE "public"."logistics_service_tariff_id_seq"
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

-- ----------------------------
-- Sequence structure for logistics_service_type_id_seq
-- ----------------------------

CREATE SEQUENCE "public"."logistics_service_type_id_seq"
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

-- ----------------------------
-- Sequence structure for logistics_trader_vehicle_id_seq
-- ----------------------------

CREATE SEQUENCE "public"."logistics_trader_vehicle_id_seq"
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

-- ----------------------------
-- Sequence structure for logistics_vehicle_type_id_seq
-- ----------------------------

CREATE SEQUENCE "public"."logistics_vehicle_type_id_seq"
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

-- ----------------------------
-- Sequence structure for market_order_approval_id_seq
-- ----------------------------

CREATE SEQUENCE "public"."market_order_approval_id_seq"
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

-- ----------------------------
-- Sequence structure for market_order_cancellation_id_seq
-- ----------------------------

CREATE SEQUENCE "public"."market_order_cancellation_id_seq"
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

-- ----------------------------
-- Sequence structure for market_order_id_seq
-- ----------------------------

CREATE SEQUENCE "public"."market_order_id_seq"
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

-- ----------------------------
-- Sequence structure for market_order_payment_id_seq
-- ----------------------------

CREATE SEQUENCE "public"."market_order_payment_id_seq"
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

-- ----------------------------
-- Sequence structure for market_order_rating_id_seq
-- ----------------------------

CREATE SEQUENCE "public"."market_order_rating_id_seq"
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

-- ----------------------------
-- Sequence structure for market_order_request_id_seq
-- ----------------------------

CREATE SEQUENCE "public"."market_order_request_id_seq"
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

-- ----------------------------
-- Sequence structure for market_order_status_id_seq
-- ----------------------------

CREATE SEQUENCE "public"."market_order_status_id_seq"
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

-- ----------------------------
-- Sequence structure for market_price_id_seq
-- ----------------------------

CREATE SEQUENCE "public"."market_price_id_seq"
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

-- ----------------------------
-- Sequence structure for measurement_metric_id_seq
-- ----------------------------

CREATE SEQUENCE "public"."measurement_metric_id_seq"
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

-- ----------------------------
-- Sequence structure for measurement_unit_id_seq
-- ----------------------------

CREATE SEQUENCE "public"."measurement_unit_id_seq"
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

-- ----------------------------
-- Sequence structure for menu_id_seq
-- ----------------------------

CREATE SEQUENCE "public"."menu_id_seq"
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

-- ----------------------------
    -- Sequence structure for organization_size_id_seq
-- ----------------------------

CREATE SEQUENCE "public"."organization_size_id_seq"
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

-- ----------------------------
-- Sequence structure for organization_type_id_seq
-- ----------------------------

CREATE SEQUENCE "public"."organization_type_id_seq"
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

-- ----------------------------
-- Sequence structure for payment_id_seq
-- ----------------------------

CREATE SEQUENCE "public"."payment_id_seq"
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

-- ----------------------------
-- Sequence structure for payment_option_id_seq
-- ----------------------------

CREATE SEQUENCE "public"."payment_option_id_seq"
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

-- ----------------------------
-- Sequence structure for permission_id_seq
-- ----------------------------

CREATE SEQUENCE "public"."permission_id_seq"
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

-- ----------------------------
-- Sequence structure for permission_menu_id_seq
-- ----------------------------

CREATE SEQUENCE "public"."permission_menu_id_seq"
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

-- ----------------------------
-- Sequence structure for price_inquiry_id_seq
-- ----------------------------

CREATE SEQUENCE "public"."price_inquiry_id_seq"
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

-- ----------------------------
-- Sequence structure for product_id_seq
-- ----------------------------

CREATE SEQUENCE "public"."product_id_seq"
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

-- ----------------------------
-- Sequence structure for product_image_id_seq
-- ----------------------------

CREATE SEQUENCE "public"."product_image_id_seq"
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

-- ----------------------------
-- Sequence structure for product_post_approval_id_seq
-- ----------------------------

CREATE SEQUENCE "public"."product_post_approval_id_seq"
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

-- ----------------------------
-- Sequence structure for region_country_id_seq
-- ----------------------------

CREATE SEQUENCE "public"."region_country_id_seq"
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

-- ----------------------------
-- Sequence structure for role_permission_id_seq
-- ----------------------------

CREATE SEQUENCE "public"."role_permission_id_seq"
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

-- ----------------------------
-- Sequence structure for sms_template_config_id_seq
-- ----------------------------

CREATE SEQUENCE "public"."sms_template_config_id_seq"
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

-- ----------------------------
-- Sequence structure for tax_id_seq
-- ----------------------------

CREATE SEQUENCE "public"."tax_id_seq"
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

-- ----------------------------
-- Sequence structure for template_config_variable_id_seq
-- ----------------------------

CREATE SEQUENCE "public"."template_config_variable_id_seq"
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

-- ----------------------------
-- Sequence structure for template_name_id_seq
-- ----------------------------

CREATE SEQUENCE "public"."template_name_id_seq"
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

-- ----------------------------
-- Sequence structure for trade_operation_id_seq
-- ----------------------------

CREATE SEQUENCE "public"."trade_operation_id_seq"
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

-- ----------------------------
-- Sequence structure for trade_volume_statistic_id_seq
-- ----------------------------

CREATE SEQUENCE "public"."trade_volume_statistic_id_seq"
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

-- ----------------------------
-- Sequence structure for translations_id_seq
-- ----------------------------

CREATE SEQUENCE "public"."translations_id_seq"
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

-- ----------------------------
-- Sequence structure for user_account_verification_id_seq
-- ----------------------------

CREATE SEQUENCE "public"."user_account_verification_id_seq"
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

-- ----------------------------
-- Sequence structure for user_approval_id_seq
-- ----------------------------

CREATE SEQUENCE "public"."user_approval_id_seq"
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

-- ----------------------------
-- Sequence structure for user_association_join_request_id_seq
-- ----------------------------

CREATE SEQUENCE "public"."user_association_join_request_id_seq"
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

-- ----------------------------
-- Sequence structure for user_association_request_approval_id_seq
-- ----------------------------

CREATE SEQUENCE "public"."user_association_request_approval_id_seq"
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

-- ----------------------------
-- Sequence structure for user_gender_id_seq
-- ----------------------------

CREATE SEQUENCE "public"."user_gender_id_seq"
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

-- ----------------------------
-- Sequence structure for user_id_seq
-- ----------------------------

CREATE SEQUENCE "public"."user_id_seq"
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

-- ----------------------------
-- Sequence structure for user_individual_detail_id_seq
-- ----------------------------

CREATE SEQUENCE "public"."user_individual_detail_id_seq"
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

-- ----------------------------
-- Sequence structure for user_organization_detail_id_seq
-- ----------------------------

CREATE SEQUENCE "public"."user_organization_detail_id_seq"
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

-- ----------------------------
-- Sequence structure for user_role_allocation_id_seq
-- ----------------------------

CREATE SEQUENCE "public"."user_role_allocation_id_seq"
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

-- ----------------------------
-- Sequence structure for user_role_id_seq
-- ----------------------------

CREATE SEQUENCE "public"."user_role_id_seq"
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

-- ----------------------------
-- Sequence structure for user_type_id_seq
-- ----------------------------

CREATE SEQUENCE "public"."user_type_id_seq"
    INCREMENT 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    START 1
    CACHE 1;

-- ----------------------------
-- Table structure for approval_status
-- ----------------------------

CREATE TABLE "public"."approval_status" (
                                            "id" int4 DEFAULT nextval('approval_status_id_seq'::regclass) NOT NULL,
                                            "code" varchar(50) COLLATE "default",
                                            "name" varchar(50) COLLATE "default",
                                            "is_active" bool DEFAULT true
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for association
-- ----------------------------

CREATE TABLE "public"."association" (
                                        "id" int8 DEFAULT nextval('association_id_seq'::regclass) NOT NULL,
                                        "name" varchar(100) COLLATE "default",
                                        "code" varchar(20) COLLATE "default",
                                        "country_id" int8,
                                        "email_address" varchar(100) COLLATE "default",
                                        "phone_number" varchar(30) COLLATE "default",
                                        "logo_url" varchar(250) COLLATE "default",
                                        "website_url" varchar(250) COLLATE "default",
                                        "physical_address" varchar(100) COLLATE "default",
                                        "is_active" bool,
                                        "added_by" int8,
                                        "date_added" timestamp(6)
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for association_member
-- ----------------------------

CREATE TABLE "public"."association_member" (
                                               "id" int8 DEFAULT nextval('association_member_id_seq'::regclass) NOT NULL,
                                               "association_id" int8,
                                               "user_id" int8,
                                               "is_active" bool,
                                               "date_joined" timestamp(6)
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for association_staff
-- ----------------------------

CREATE TABLE "public"."association_staff" (
                                              "id" int8 DEFAULT nextval('association_staff_id_seq'::regclass) NOT NULL,
                                              "association_id" int8 NOT NULL,
                                              "user_id" int8,
                                              "is_active" bool,
                                              "date_joined" timestamp(6)
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for charge_mode
-- ----------------------------

CREATE TABLE "public"."charge_mode" (
                                        "id" int8 DEFAULT nextval('charge_mode_id_seq'::regclass) NOT NULL,
                                        "name" varchar(50) COLLATE "default"
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for commodity
-- ----------------------------

CREATE TABLE "public"."commodity" (
                                      "id" int8 DEFAULT nextval('commodity_id_seq'::regclass) NOT NULL,
                                      "name" varchar(50) COLLATE "default",
                                      "description" varchar(250) COLLATE "default",
                                      "commodity_sub_category_id" int8,
                                      "is_active" bool,
                                      "added_by" int8,
                                      "date_added" timestamp(6)
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for commodity_category
-- ----------------------------

CREATE TABLE "public"."commodity_category" (
                                               "id" int8 DEFAULT nextval('commodity_category_id_seq'::regclass) NOT NULL,
                                               "name" varchar(100) COLLATE "default",
                                               "description" varchar(200) COLLATE "default",
                                               "is_active" bool,
                                               "added_by" int8,
                                               "date_added" timestamp(6)
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for commodity_sub_category
-- ----------------------------

CREATE TABLE "public"."commodity_sub_category" (
                                                   "id" int4 DEFAULT nextval('commodity_sub_category_id_seq'::regclass) NOT NULL,
                                                   "name" varchar(100) COLLATE "default",
                                                   "description" varchar(200) COLLATE "default",
                                                   "commodity_category_id" int8,
                                                   "is_active" bool,
                                                   "added_by" int8,
                                                   "date_added" timestamp(6)
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for country
-- ----------------------------

CREATE TABLE "public"."country" (
                                    "id" int8 DEFAULT nextval('country_id_seq'::regclass) NOT NULL,
                                    "name" varchar(100) COLLATE "default",
                                    "code_name" varchar(20) COLLATE "default",
                                    "default_language" int8,
                                    "is_active" bool
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for currency
-- ----------------------------

CREATE TABLE "public"."currency" (
                                     "id" int8 DEFAULT nextval('currency_id_seq'::regclass) NOT NULL,
                                     "name" varchar(50) COLLATE "default",
                                     "code" varchar(20) COLLATE "default",
                                     "currency_symbol" varchar(20) COLLATE "default",
                                     "display" int4,
                                     "country_id" int8,
                                     "is_active" bool
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for delivery_option
-- ----------------------------

CREATE TABLE "public"."delivery_option" (
                                            "id" int8 DEFAULT nextval('delivery_option_id_seq'::regclass) NOT NULL,
                                            "name" varchar(50) COLLATE "default",
                                            "description" varchar(200) COLLATE "default",
                                            "commodity_sub_category_id" int8,
                                            "is_active" bool,
                                            "added_by" int8,
                                            "date_added" timestamp(6)
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for duration
-- ----------------------------

CREATE TABLE "public"."duration" (
                                     "id" int8 DEFAULT nextval('duration_id_seq'::regclass) NOT NULL,
                                     "name" varchar(30) COLLATE "default",
                                     "is_active" bool
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for email_template_config
-- ----------------------------

CREATE TABLE "public"."email_template_config" (
                                                  "id" int8 DEFAULT nextval('email_template_config_id_seq'::regclass) NOT NULL,
                                                  "template_name_id" int8,
                                                  "subject" varchar(50) COLLATE "default",
                                                  "message" text COLLATE "default",
                                                  "is_active" bool
)
    WITH (OIDS=FALSE)

;


-- ----------------------------
-- Table structure for global_config
-- ----------------------------

CREATE TABLE "public"."global_config" (
                                          "id" int8 DEFAULT nextval('global_config_id_seq'::regclass) NOT NULL,
                                          "config" varchar(50) COLLATE "default",
                                          "value" varchar(50) COLLATE "default"
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for international_location_description
-- ----------------------------

CREATE TABLE "public"."international_location_description" (
                                                               "id" int8 DEFAULT nextval('international_location_description_id_seq'::regclass) NOT NULL,
                                                               "name" varchar(25) COLLATE "default",
                                                               "is_active" bool
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for international_region
-- ----------------------------

CREATE TABLE "public"."international_region" (
                                                 "id" int8 DEFAULT nextval('international_region_id_seq'::regclass) NOT NULL,
                                                 "name" varchar(50) COLLATE "default",
                                                 "code_name" varchar(25) COLLATE "default",
                                                 "is_active" bool
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for language
-- ----------------------------

CREATE TABLE "public"."language" (
                                     "id" int4 DEFAULT nextval('language_id_seq'::regclass) NOT NULL,
                                     "name" varchar(30) COLLATE "default",
                                     "code_name" varchar(10) COLLATE "default",
                                     "is_active" bool
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for location
-- ----------------------------

CREATE TABLE "public"."location" (
                                     "id" int8 DEFAULT nextval('location_id_seq'::regclass) NOT NULL,
                                     "name" varchar(100) COLLATE "default",
                                     "code_name" varchar(30) COLLATE "default",
                                     "level_id" int8
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for location_level
-- ----------------------------

CREATE TABLE "public"."location_level" (
                                           "id" int8 DEFAULT nextval('location_level_id_seq'::regclass) NOT NULL,
                                           "name" varchar(50) COLLATE "default",
                                           "parent_id" int8
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for logistics_service
-- ----------------------------

CREATE TABLE "public"."logistics_service" (
                                              "id" int8 DEFAULT nextval('logistics_service_id_seq'::regclass) NOT NULL,
                                              "name" varchar(100) COLLATE "default",
                                              "description" varchar(200) COLLATE "default",
                                              "location_from" int8,
                                              "location_to" int8,
                                              "is_active" bool,
                                              "provider_user_id" int8
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for logistics_service_additional_charge
-- ----------------------------

CREATE TABLE "public"."logistics_service_additional_charge" (
                                                                "id" int8 DEFAULT nextval('logistics_service_additional_charge_id_seq'::regclass) NOT NULL,
                                                                "logistics_service_id" int8,
                                                                "logistics_service_type_id" int8,
                                                                "amount" float8 DEFAULT 0,
                                                                "is_active" bool
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for logistics_service_order
-- ----------------------------

CREATE TABLE "public"."logistics_service_order" (
                                                    "id" int8 DEFAULT nextval('logistics_service_order_id_seq'::regclass) NOT NULL,
                                                    "order_number" varchar(100) COLLATE "default",
                                                    "logistics_service_order_request_id" int8,
                                                    "is_payment_complete" bool,
                                                    "logistics_service_order_status_id" int8,
                                                    "is_active" bool,
                                                    "date_added" timestamp(6)
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for logistics_service_order_cancellation
-- ----------------------------

CREATE TABLE "public"."logistics_service_order_cancellation" (
                                                                 "id" int8 DEFAULT nextval('logistics_service_order_cancellation_id_seq'::regclass) NOT NULL,
                                                                 "logistics_service_order_id" int8,
                                                                 "cancelled_by" int8,
                                                                 "reason_for_cancellation" varchar(200) COLLATE "default",
                                                                 "date_cancelled" timestamp(6)
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for logistics_service_order_payment
-- ----------------------------

CREATE TABLE "public"."logistics_service_order_payment" (
                                                            "id" int8 DEFAULT nextval('logistics_service_order_payment_id_seq'::regclass) NOT NULL,
                                                            "logistics_service_order_id" int8,
                                                            "payment_id" int8
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for logistics_service_order_rating
-- ----------------------------

CREATE TABLE "public"."logistics_service_order_rating" (
                                                           "id" int8 DEFAULT nextval('logistics_service_order_rating_id_seq'::regclass) NOT NULL,
                                                           "logistics_service_order_id" int8,
                                                           "rated_by" int8,
                                                           "logistics_service_order_rating_user_type_id" int8,
                                                           "rating" int4,
                                                           "date_rated" timestamp(6)
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for logistics_service_order_request
-- ----------------------------

CREATE TABLE "public"."logistics_service_order_request" (
                                                            "id" int8 DEFAULT nextval('logistics_service_order_request_id_seq'::regclass) NOT NULL,
                                                            "logistics_service_id" int8,
                                                            "market_order_id" int8,
                                                            "description" varchar(100) COLLATE "default",
                                                            "ordered_by" int8,
                                                            "quantity" int4,
                                                            "logistics_service_tariff_id" int8,
                                                            "price" float8 DEFAULT 0,
                                                            "approval_status_id" int8,
                                                            "date_requested" timestamp(6)
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for logistics_service_order_request_approval
-- ----------------------------

CREATE TABLE "public"."logistics_service_order_request_approval" (
                                                                     "id" int8 DEFAULT nextval('logistics_service_order_request_approval_id_seq'::regclass) NOT NULL,
                                                                     "logistics_service_order_request_id" int8,
                                                                     "approved_by" int8,
                                                                     "comment" varchar(300) COLLATE "default",
                                                                     "date_approved" timestamp(6)
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for logistics_service_order_status
-- ----------------------------

CREATE TABLE "public"."logistics_service_order_status" (
                                                           "id" int8 DEFAULT nextval('logistics_service_order_status_id_seq'::regclass) NOT NULL,
                                                           "name" varchar(50) COLLATE "default",
                                                           "description" varchar(200) COLLATE "default",
                                                           "is_active" bool
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for logistics_service_tariff
-- ----------------------------

CREATE TABLE "public"."logistics_service_tariff" (
                                                     "id" int8 DEFAULT nextval('logistics_service_tariff_id_seq'::regclass) NOT NULL,
                                                     "logistics_service_id" int8,
                                                     "charging_mode_id" int8,
                                                     "currency_id" int8,
                                                     "measurement_unit_id" int8,
                                                     "minimum_order" int4
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for logistics_service_tariff_charge
-- ----------------------------

CREATE TABLE "public"."logistics_service_tariff_charge" (
                                                            "id" int8 DEFAULT nextval('logistics_service_tariff_charge_id_seq'::regclass) NOT NULL,
                                                            "logistics_service_tariff_id" int8,
                                                            "from_unit" int4,
                                                            "to_unit" int4,
                                                            "charge" float8 DEFAULT 0
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for logistics_service_type
-- ----------------------------

CREATE TABLE "public"."logistics_service_type" (
                                                   "id" int8 DEFAULT nextval('logistics_service_type_id_seq'::regclass) NOT NULL,
                                                   "name" varchar(50) COLLATE "default",
                                                   "description" varchar(200) COLLATE "default",
                                                   "added_by" int8,
                                                   "date_added" timestamp(6),
                                                   "is_active" bool
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for logistics_trader_vehicle
-- ----------------------------

CREATE TABLE "public"."logistics_trader_vehicle" (
                                                     "id" int8 DEFAULT nextval('logistics_trader_vehicle_id_seq'::regclass) NOT NULL,
                                                     "plate_number" varchar(50) COLLATE "default",
                                                     "description" varchar(200) COLLATE "default",
                                                     "logistics_vehicle_type_id" int4,
                                                     "is_active" bool,
                                                     "provider_user_id" int8
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for logistics_vehicle_type
-- ----------------------------

CREATE TABLE "public"."logistics_vehicle_type" (
                                                   "id" int4 DEFAULT nextval('logistics_vehicle_type_id_seq'::regclass) NOT NULL,
                                                   "name" varchar(30) COLLATE "default",
                                                   "is_active" bool
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for market_order
-- ----------------------------

CREATE TABLE "public"."market_order" (
                                         "id" int8 DEFAULT nextval('market_order_id_seq'::regclass) NOT NULL,
                                         "product_id" int8,
                                         "ordered_by" int8,
                                         "is_processed" bool,
                                         "date_ordered" timestamp(6)
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for market_order_approval
-- ----------------------------

CREATE TABLE "public"."market_order_approval" (
                                                  "id" int8 DEFAULT nextval('market_order_approval_id_seq'::regclass) NOT NULL,
                                                  "market_order_request_id" int8,
                                                  "approved_by" int8,
                                                  "date_approved" timestamp(6)
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for market_order_cancellation
-- ----------------------------

CREATE TABLE "public"."market_order_cancellation" (
                                                      "id" int8 DEFAULT nextval('market_order_cancellation_id_seq'::regclass) NOT NULL,
                                                      "market_order_id" int8,
                                                      "cancelled_by" int8,
                                                      "reason_for_cancellation" varchar(200) COLLATE "default",
                                                      "date_cancelled" timestamp(6)
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for market_order_payment
-- ----------------------------

CREATE TABLE "public"."market_order_payment" (
                                                 "id" int8 DEFAULT nextval('market_order_payment_id_seq'::regclass) NOT NULL,
                                                 "market_order_id" int8,
                                                 "payment_id" int8
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for market_order_rating
-- ----------------------------

CREATE TABLE "public"."market_order_rating" (
                                                "id" int8 DEFAULT nextval('market_order_rating_id_seq'::regclass) NOT NULL,
                                                "market_order_id" int8,
                                                "rating" int4,
                                                "rated_by" int8,
                                                "rating_user_type_id" int8,
                                                "date_rated" int4
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for market_order_request
-- ----------------------------

CREATE TABLE "public"."market_order_request" (
                                                 "id" int8 DEFAULT nextval('market_order_request_id_seq'::regclass) NOT NULL,
                                                 "product_id" int8,
                                                 "ordered_by" int8,
                                                 "quantity_ordered" int4,
                                                 "unit_price" float8,
                                                 "total_price" float8,
                                                 "customer_comments" varchar(200) COLLATE "default",
                                                 "approval_status_id" int8,
                                                 "date_requested" timestamp(6)
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for market_order_status
-- ----------------------------

CREATE TABLE "public"."market_order_status" (
                                                "id" int8 DEFAULT nextval('market_order_status_id_seq'::regclass) NOT NULL,
                                                "name" varchar(50) COLLATE "default",
                                                "description" varchar(100) COLLATE "default",
                                                "is_active" bool
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for market_price
-- ----------------------------

CREATE TABLE "public"."market_price" (
                                         "id" int8 DEFAULT nextval('market_price_id_seq'::regclass) NOT NULL,
                                         "commodity_id" int8,
                                         "location_id" int8,
                                         "price" float8,
                                         "currency_id" int8,
                                         "date" timestamp(6),
                                         "is_active" bool,
                                         "added_by" int8,
                                         "date_added" timestamp(6)
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for measurement_metric
-- ----------------------------

CREATE TABLE "public"."measurement_metric" (
                                               "id" int8 DEFAULT nextval('measurement_metric_id_seq'::regclass) NOT NULL,
                                               "name" varchar(50) COLLATE "default",
                                               "description" varchar(200) COLLATE "default",
                                               "code" varchar(20) COLLATE "default",
                                               "added_by" int8,
                                               "date_added" timestamp(6),
                                               "is_active" bool
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for measurement_unit
-- ----------------------------

CREATE TABLE "public"."measurement_unit" (
                                             "id" int8 DEFAULT nextval('measurement_unit_id_seq'::regclass) NOT NULL,
                                             "name" varchar(50) COLLATE "default",
                                             "description" varchar(200) COLLATE "default",
                                             "code" varchar(20) COLLATE "default",
                                             "measurement_metric_id" int8,
                                             "added_by" int8,
                                             "date_added" timestamp(6),
                                             "is_active" bool
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for menu
-- ----------------------------

CREATE TABLE "public"."menu" (
                                 "id" int8 DEFAULT nextval('menu_id_seq'::regclass) NOT NULL,
                                 "icon" varchar(50) COLLATE "default",
                                 "label" varchar(50) COLLATE "default",
                                 "to" varchar(100) COLLATE "default",
                                 "parent_id" int8,
                                 "priority" int4,
                                 "is_active" bool
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for oauth_access_token
-- ----------------------------

CREATE TABLE "public"."oauth_access_token" (
                                               "token_id" varchar(256) COLLATE "default",
                                               "token" bytea,
                                               "authentication_id" varchar(256) COLLATE "default",
                                               "user_name" varchar(256) COLLATE "default",
                                               "client_id" varchar(256) COLLATE "default",
                                               "authentication" bytea,
                                               "refresh_token" varchar(256) COLLATE "default"
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for oauth_approvals
-- ----------------------------

CREATE TABLE "public"."oauth_approvals" (
                                            "userid" varchar(256) COLLATE "default",
                                            "clientid" varchar(256) COLLATE "default",
                                            "scope" varchar(256) COLLATE "default",
                                            "status" varchar(10) COLLATE "default",
                                            "expiresat" timestamp(6),
                                            "lastmodifiedat" timestamp(6)
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for oauth_client_details
-- ----------------------------

CREATE TABLE "public"."oauth_client_details" (
                                                 "client_id" varchar(256) COLLATE "default" NOT NULL,
                                                 "resource_ids" varchar(256) COLLATE "default",
                                                 "client_secret" varchar(256) COLLATE "default",
                                                 "scope" varchar(256) COLLATE "default",
                                                 "authorized_grant_types" varchar(256) COLLATE "default",
                                                 "web_server_redirect_uri" varchar(256) COLLATE "default",
                                                 "authorities" varchar(256) COLLATE "default",
                                                 "access_token_validity" int4,
                                                 "refresh_token_validity" int4,
                                                 "additional_information" varchar(4096) COLLATE "default",
                                                 "autoapprove" varchar(256) COLLATE "default"
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for oauth_client_token
-- ----------------------------

CREATE TABLE "public"."oauth_client_token" (
                                               "token_id" varchar(256) COLLATE "default",
                                               "token" bytea,
                                               "authentication_id" varchar(256) COLLATE "default",
                                               "user_name" varchar(256) COLLATE "default",
                                               "client_id" varchar(256) COLLATE "default"
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for oauth_code
-- ----------------------------

CREATE TABLE "public"."oauth_code" (
                                       "code" varchar(256) COLLATE "default",
                                       "authentication" bytea
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for oauth_refresh_token
-- ----------------------------

CREATE TABLE "public"."oauth_refresh_token" (
                                                "token_id" varchar(256) COLLATE "default",
                                                "token" bytea,
                                                "authentication" bytea
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for organization_size
-- ----------------------------

CREATE TABLE "public"."organization_size" (
                                              "id" int8 DEFAULT nextval('organization_size_id_seq'::regclass) NOT NULL,
                                              "name" varchar(50) COLLATE "default",
                                              "is_active" bool DEFAULT true
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for organization_type
-- ----------------------------

CREATE TABLE "public"."organization_type" (
                                              "id" int8 DEFAULT nextval('organization_type_id_seq'::regclass) NOT NULL,
                                              "name" varchar(50) COLLATE "default",
                                              "is_active" bool DEFAULT true
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for payment
-- ----------------------------

CREATE TABLE "public"."payment" (
                                    "id" int8 DEFAULT nextval('payment_id_seq'::regclass) NOT NULL,
                                    "payment_option_id" int8,
                                    "payment_reference" varchar(100) COLLATE "default",
                                    "narration" varchar(500) COLLATE "default",
                                    "amount" float8,
                                    "third_party_reference" varchar(200) COLLATE "default",
                                    "account_reference" varchar(200) COLLATE "default",
                                    "third_party_payment_time" timestamp(6),
                                    "payment_time_stamp" timestamp(6) DEFAULT now()
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for payment_option
-- ----------------------------

CREATE TABLE "public"."payment_option" (
                                           "id" int8 DEFAULT nextval('payment_option_id_seq'::regclass) NOT NULL,
                                           "name" varchar(50) COLLATE "default",
                                           "description" varchar(200) COLLATE "default",
                                           "added_by" int8,
                                           "date_added" timestamp(6),
                                           "is_active" bool
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for permission
-- ----------------------------

CREATE TABLE "public"."permission" (
                                       "id" int8 DEFAULT nextval('permission_id_seq'::regclass) NOT NULL,
                                       "name" varchar(50) COLLATE "default",
                                       "code" varchar(50) COLLATE "default",
                                       "is_active" bool DEFAULT true
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for permission_menu
-- ----------------------------

CREATE TABLE "public"."permission_menu" (
                                            "id" int8 DEFAULT nextval('permission_menu_id_seq'::regclass) NOT NULL,
                                            "permission_id" int8,
                                            "menu_id" int8
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for price_inquiry
-- ----------------------------

CREATE TABLE "public"."price_inquiry" (
                                          "id" int8 DEFAULT nextval('price_inquiry_id_seq'::regclass) NOT NULL,
                                          "commodity_id" int8,
                                          "location_id" int8,
                                          "date" timestamp(6),
                                          "is_answered" bool,
                                          "date_inquired" timestamp(6),
                                          "inquired_by" int8,
                                          "comment" varchar(200) COLLATE "default",
                                          "date_answered" timestamp(6)
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for product
-- ----------------------------

CREATE TABLE "public"."product" (
                                    "id" int8 DEFAULT nextval('product_id_seq'::regclass) NOT NULL,
                                    "commodity_id" int8,
                                    "name" varchar(50) COLLATE "default",
                                    "description" varchar(200) COLLATE "default",
                                    "price" float8,
                                    "trader_id" int8,
                                    "minimum_order_quantity" int4,
                                    "measurement_unit_id" int8,
                                    "is_post_to_inventory" bool,
                                    "is_update_sales" int4,
                                    "is_approved" bool,
                                    "date_added" timestamp(6)
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for product_image
-- ----------------------------

CREATE TABLE "public"."product_image" (
                                          "id" int8 DEFAULT nextval('product_image_id_seq'::regclass) NOT NULL,
                                          "product_id" int8,
                                          "url" varchar(200) COLLATE "default",
                                          "is_approved" bool,
                                          "is_active" bool,
                                          "date_added" timestamp(6)
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for product_image_approval
-- ----------------------------

CREATE TABLE "public"."product_image_approval" (
                                                   "id" int8 NOT NULL,
                                                   "product_image_id" int8,
                                                   "approval_status_id" int8,
                                                   "approved_by" int8,
                                                   "comment" varchar(200) COLLATE "default",
                                                   "date_approved" timestamp(6)
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for product_post_approval
-- ----------------------------

CREATE TABLE "public"."product_post_approval" (
                                                  "id" int8 DEFAULT nextval('product_post_approval_id_seq'::regclass) NOT NULL,
                                                  "product_id" int8,
                                                  "approval_status_id" int8,
                                                  "approved_by" int8,
                                                  "comment" varchar(200) COLLATE "default",
                                                  "date_approved" timestamp(6)
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for rating_user_type
-- ----------------------------

CREATE TABLE "public"."rating_user_type" (
                                             "id" int8 DEFAULT nextval('logistics_rating_user_type_id_seq'::regclass) NOT NULL,
                                             "name" varchar(30) COLLATE "default",
                                             "is_active" bool,
                                             "description" varchar(100) COLLATE "default"
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for region_country
-- ----------------------------

CREATE TABLE "public"."region_country" (
                                           "id" int8 DEFAULT nextval('region_country_id_seq'::regclass) NOT NULL,
                                           "region_id" int8,
                                           "country_id" int8,
                                           "is_active" bool
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for role_permission
-- ----------------------------

CREATE TABLE "public"."role_permission" (
                                            "id" int8 DEFAULT nextval('role_permission_id_seq'::regclass) NOT NULL,
                                            "role_id" int8,
                                            "permission_id" int8,
                                            "is_active" bool
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for sms_template_config
-- ----------------------------

CREATE TABLE "public"."sms_template_config" (
                                                "id" int8 DEFAULT nextval('sms_template_config_id_seq'::regclass) NOT NULL,
                                                "template_name_id" int8,
                                                "message" varchar(200) COLLATE "default",
                                                "is_active" bool DEFAULT true
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for tax
-- ----------------------------

CREATE TABLE "public"."tax" (
                                "id" int8 DEFAULT nextval('tax_id_seq'::regclass) NOT NULL,
                                "name" varchar(50) COLLATE "default",
                                "value" int4,
                                "charge_mode_id" int8,
                                "added_by" int8,
                                "date_added" timestamp(6),
                                "is_active" bool
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for template_config_variable
-- ----------------------------

CREATE TABLE "public"."template_config_variable" (
                                                     "id" int8 DEFAULT nextval('template_config_variable_id_seq'::regclass) NOT NULL,
                                                     "context" varchar(25) COLLATE "default",
                                                     "template_id" int8,
                                                     "code_name" varchar(50) COLLATE "default",
                                                     "description" varchar(200) COLLATE "default",
                                                     "friendly_name" varchar(200) COLLATE "default",
                                                     "is_active" bool
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for template_name
-- ----------------------------

CREATE TABLE "public"."template_name" (
                                          "id" int8 DEFAULT nextval('template_name_id_seq'::regclass) NOT NULL,
                                          "name" varchar(25) COLLATE "default",
                                          "description" varchar(200) COLLATE "default",
                                          "is_active" bool
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for trade_operation
-- ----------------------------

CREATE TABLE "public"."trade_operation" (
                                            "id" int8 DEFAULT nextval('trade_operation_id_seq'::regclass) NOT NULL,
                                            "name" varchar(50) COLLATE "default",
                                            "description" varchar(200) COLLATE "default",
                                            "is_active" bool
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for trade_volume_statistic
-- ----------------------------

CREATE TABLE "public"."trade_volume_statistic" (
                                                   "id" int8 DEFAULT nextval('trade_volume_statistic_id_seq'::regclass) NOT NULL,
                                                   "trade_operation_id" int8,
                                                   "commodity_id" int8,
                                                   "from_country" int8,
                                                   "to_country" int8,
                                                   "duration_id" int8,
                                                   "from_date" timestamp(6),
                                                   "to_date" timestamp(6),
                                                   "quantity" int4,
                                                   "measurement_unit_id" int8,
                                                   "is_active" bool,
                                                   "added_by" int8,
                                                   "date_added" timestamp(6)
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for translations
-- ----------------------------

CREATE TABLE "public"."translations" (
                                         "id" int8 DEFAULT nextval('translations_id_seq'::regclass) NOT NULL,
                                         "context" varchar(200) COLLATE "default",
                                         "field" varchar(200) COLLATE "default",
                                         "language" varchar(25) COLLATE "default",
                                         "is_default" bool DEFAULT false,
                                         "value" text COLLATE "default"
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for user
-- ----------------------------

CREATE TABLE "public"."user" (
                                 "id" int8 DEFAULT nextval('user_id_seq'::regclass) NOT NULL,
                                 "email" varchar(50) COLLATE "default",
                                 "phone_number" varchar(50) COLLATE "default",
                                 "profile_image" varchar(50) COLLATE "default",
                                 "password" varchar(100) COLLATE "default",
                                 "date_joined" timestamp(6) DEFAULT now(),
                                 "user_type_id" int4,
                                 "country_id" int8,
                                 "language_preference" int4,
                                 "location_id" int4,
                                 "approval_status_id" int4,
                                 "is_active" bool DEFAULT false,
                                 "account_expired" bool DEFAULT false,
                                 "account_locked" bool DEFAULT false,
                                 "credentials_expired" bool DEFAULT false,
                                 account_verified bool DEFAULT false,
                                 "username" varchar(20) COLLATE "default"
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for user_account_verification
-- ----------------------------

CREATE TABLE "public"."user_account_verification" (
                                                      "id" int8 DEFAULT nextval('user_account_verification_id_seq'::regclass) NOT NULL,
                                                      "code" varchar(50) COLLATE "default",
                                                      "type" varchar(10) COLLATE "default",
                                                      "contact" varchar(50) COLLATE "default",
                                                      "user_id" int8,
                                                      "used" bool DEFAULT false,
                                                      "time_created" timestamp(6) DEFAULT now(),
                                                      "expiry_time" timestamp(6),
                                                      "sent" bool DEFAULT false,
                                                      "is_active" bool DEFAULT true
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for user_approval
-- ----------------------------

CREATE TABLE "public"."user_approval" (
                                          "id" int8 DEFAULT nextval('user_approval_id_seq'::regclass) NOT NULL,
                                          "user_id" int8,
                                          "approval_status_id" int4,
                                          "comment" varchar(200) COLLATE "default",
                                          "approved_by" int8,
                                          "date_approved" timestamp(6) DEFAULT now()
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for user_association_join_request
-- ----------------------------

CREATE TABLE "public"."user_association_join_request" (
                                                          "id" int8 DEFAULT nextval('user_association_join_request_id_seq'::regclass) NOT NULL,
                                                          "user_id" int8,
                                                          "member_no" varchar(50) COLLATE "default",
                                                          "association_id" int8,
                                                          "approval_status_id" int8,
                                                          "date_requested" timestamp(6)
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for user_association_request_approval
-- ----------------------------

CREATE TABLE "public"."user_association_request_approval" (
                                                              "id" int8 DEFAULT nextval('user_association_request_approval_id_seq'::regclass) NOT NULL,
                                                              "user_association_request_id" int8,
                                                              "comment" varchar(300) COLLATE "default",
                                                              "approved_by" int8,
                                                              "date_approved" timestamp(6)
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for user_gender
-- ----------------------------

CREATE TABLE "public"."user_gender" (
                                        "id" int8 DEFAULT nextval('user_gender_id_seq'::regclass) NOT NULL,
                                        "name" varchar(50) COLLATE "default",
                                        "is_active" bool DEFAULT true
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for user_individual_detail
-- ----------------------------

CREATE TABLE "public"."user_individual_detail" (
                                                   "id" int8 DEFAULT nextval('user_individual_detail_id_seq'::regclass) NOT NULL,
                                                   "user_id" int8,
                                                   "first_name" varchar(100) COLLATE "default",
                                                   "middle_name" varchar(100) COLLATE "default",
                                                   "last_name" varchar(100) COLLATE "default",
                                                   "gender_id" int4
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for user_organization_detail
-- ----------------------------

CREATE TABLE "public"."user_organization_detail" (
                                                     "id" int8 DEFAULT nextval('user_organization_detail_id_seq'::regclass) NOT NULL,
                                                     "organization_name" varchar(200) COLLATE "default",
                                                     "organization_type_id" int4,
                                                     "organization_size_id" int4,
                                                     "postal_address" varchar(50) COLLATE "default",
                                                     "physical_address" varchar(50) COLLATE "default",
                                                     "website" varchar(50) COLLATE "default"
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for user_role
-- ----------------------------

CREATE TABLE "public"."user_role" (
                                      "id" int8 DEFAULT nextval('user_role_id_seq'::regclass) NOT NULL,
                                      "name" varchar(50) COLLATE "default",
                                      "date_added" timestamp(6) DEFAULT now(),
                                      "is_active" bool DEFAULT true,
                                      is_admin bool DEFAULT true
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for user_role_allocation
-- ----------------------------

CREATE TABLE "public"."user_role_allocation" (
                                                 "id" int8 DEFAULT nextval('user_role_allocation_id_seq'::regclass) NOT NULL,
                                                 "user_id" int8,
                                                 "role_id" int8,
                                                 "is_active" bool,
                                                 "date_allocated" timestamp(6) DEFAULT now()
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for user_type
-- ----------------------------

CREATE TABLE "public"."user_type" (
                                      "id" int8 DEFAULT nextval('user_type_id_seq'::regclass) NOT NULL,
                                      "name" varchar(200) COLLATE "default",
                                      "is_active" bool DEFAULT true
)
    WITH (OIDS=FALSE)

;

-- ----------------------------
-- Alter Sequences Owned By
-- ----------------------------
ALTER SEQUENCE "public"."approval_status_id_seq" OWNED BY "approval_status"."id";
ALTER SEQUENCE "public"."association_id_seq" OWNED BY "association"."id";
ALTER SEQUENCE "public"."association_member_id_seq" OWNED BY "association_member"."id";
ALTER SEQUENCE "public"."association_staff_id_seq" OWNED BY "association_staff"."id";
ALTER SEQUENCE "public"."charge_mode_id_seq" OWNED BY "charge_mode"."id";
ALTER SEQUENCE "public"."commodity_category_id_seq" OWNED BY "commodity_category"."id";
ALTER SEQUENCE "public"."commodity_id_seq" OWNED BY "commodity"."id";
ALTER SEQUENCE "public"."commodity_sub_category_id_seq" OWNED BY "commodity_sub_category"."id";
ALTER SEQUENCE "public"."country_id_seq" OWNED BY "country"."id";
ALTER SEQUENCE "public"."currency_id_seq" OWNED BY "currency"."id";
ALTER SEQUENCE "public"."delivery_option_id_seq" OWNED BY "delivery_option"."id";
ALTER SEQUENCE "public"."duration_id_seq" OWNED BY "duration"."id";
ALTER SEQUENCE "public"."email_template_config_id_seq" OWNED BY "email_template_config"."id";
ALTER SEQUENCE "public"."global_config_id_seq" OWNED BY "global_config"."id";
ALTER SEQUENCE "public"."international_location_description_id_seq" OWNED BY "international_location_description"."id";
ALTER SEQUENCE "public"."international_region_id_seq" OWNED BY "international_region"."id";
ALTER SEQUENCE "public"."language_id_seq" OWNED BY "language"."id";
ALTER SEQUENCE "public"."location_id_seq" OWNED BY "location"."id";
ALTER SEQUENCE "public"."location_level_id_seq" OWNED BY "location_level"."id";
ALTER SEQUENCE "public"."logistics_rating_user_type_id_seq" OWNED BY "rating_user_type"."id";
ALTER SEQUENCE "public"."logistics_service_additional_charge_id_seq" OWNED BY "logistics_service_additional_charge"."id";
ALTER SEQUENCE "public"."logistics_service_id_seq" OWNED BY "logistics_service"."id";
ALTER SEQUENCE "public"."logistics_service_order_cancellation_id_seq" OWNED BY "logistics_service_order_cancellation"."id";
ALTER SEQUENCE "public"."logistics_service_order_id_seq" OWNED BY "logistics_service_order"."id";
ALTER SEQUENCE "public"."logistics_service_order_payment_id_seq" OWNED BY "logistics_service_order_payment"."id";
ALTER SEQUENCE "public"."logistics_service_order_rating_id_seq" OWNED BY "logistics_service_order_rating"."id";
ALTER SEQUENCE "public"."logistics_service_order_request_approval_id_seq" OWNED BY "logistics_service_order_request_approval"."id";
ALTER SEQUENCE "public"."logistics_service_order_request_id_seq" OWNED BY "logistics_service_order_request"."id";
ALTER SEQUENCE "public"."logistics_service_order_status_id_seq" OWNED BY "logistics_service_order_status"."id";
ALTER SEQUENCE "public"."logistics_service_tariff_charge_id_seq" OWNED BY "logistics_service_tariff_charge"."id";
ALTER SEQUENCE "public"."logistics_service_tariff_id_seq" OWNED BY "logistics_service_tariff"."id";
ALTER SEQUENCE "public"."logistics_service_type_id_seq" OWNED BY "logistics_service_type"."id";
ALTER SEQUENCE "public"."logistics_trader_vehicle_id_seq" OWNED BY "logistics_trader_vehicle"."id";
ALTER SEQUENCE "public"."logistics_vehicle_type_id_seq" OWNED BY "logistics_vehicle_type"."id";
ALTER SEQUENCE "public"."market_order_approval_id_seq" OWNED BY "market_order_approval"."id";
ALTER SEQUENCE "public"."market_order_cancellation_id_seq" OWNED BY "market_order_cancellation"."id";
ALTER SEQUENCE "public"."market_order_id_seq" OWNED BY "market_order"."id";
ALTER SEQUENCE "public"."market_order_payment_id_seq" OWNED BY "market_order_payment"."id";
ALTER SEQUENCE "public"."market_order_rating_id_seq" OWNED BY "market_order_rating"."id";
ALTER SEQUENCE "public"."market_order_request_id_seq" OWNED BY "market_order_request"."id";
ALTER SEQUENCE "public"."market_order_status_id_seq" OWNED BY "market_order_status"."id";
ALTER SEQUENCE "public"."market_price_id_seq" OWNED BY "market_price"."id";
ALTER SEQUENCE "public"."measurement_metric_id_seq" OWNED BY "measurement_metric"."id";
ALTER SEQUENCE "public"."measurement_unit_id_seq" OWNED BY "measurement_unit"."id";
ALTER SEQUENCE "public"."menu_id_seq" OWNED BY "menu"."id";
ALTER SEQUENCE "public"."organization_size_id_seq" OWNED BY "organization_size"."id";
ALTER SEQUENCE "public"."organization_type_id_seq" OWNED BY "organization_type"."id";
ALTER SEQUENCE "public"."payment_id_seq" OWNED BY "payment"."id";
ALTER SEQUENCE "public"."payment_option_id_seq" OWNED BY "payment_option"."id";
ALTER SEQUENCE "public"."permission_id_seq" OWNED BY "permission"."id";
ALTER SEQUENCE "public"."permission_menu_id_seq" OWNED BY "permission_menu"."id";
ALTER SEQUENCE "public"."price_inquiry_id_seq" OWNED BY "price_inquiry"."id";
ALTER SEQUENCE "public"."product_id_seq" OWNED BY "product"."id";
ALTER SEQUENCE "public"."product_image_id_seq" OWNED BY "product_image"."id";
ALTER SEQUENCE "public"."product_post_approval_id_seq" OWNED BY "product_post_approval"."id";
ALTER SEQUENCE "public"."region_country_id_seq" OWNED BY "region_country"."id";
ALTER SEQUENCE "public"."role_permission_id_seq" OWNED BY "role_permission"."id";
ALTER SEQUENCE "public"."sms_template_config_id_seq" OWNED BY "sms_template_config"."id";
ALTER SEQUENCE "public"."tax_id_seq" OWNED BY "tax"."id";
ALTER SEQUENCE "public"."template_config_variable_id_seq" OWNED BY "template_config_variable"."id";
ALTER SEQUENCE "public"."template_name_id_seq" OWNED BY "template_name"."id";
ALTER SEQUENCE "public"."trade_operation_id_seq" OWNED BY "trade_operation"."id";
ALTER SEQUENCE "public"."trade_volume_statistic_id_seq" OWNED BY "trade_volume_statistic"."id";
ALTER SEQUENCE "public"."translations_id_seq" OWNED BY "translations"."id";
ALTER SEQUENCE "public"."user_account_verification_id_seq" OWNED BY "user_account_verification"."id";
ALTER SEQUENCE "public"."user_approval_id_seq" OWNED BY "user_approval"."id";
ALTER SEQUENCE "public"."user_association_join_request_id_seq" OWNED BY "user_association_join_request"."id";
ALTER SEQUENCE "public"."user_association_request_approval_id_seq" OWNED BY "user_association_request_approval"."id";
ALTER SEQUENCE "public"."user_gender_id_seq" OWNED BY "user_gender"."id";
ALTER SEQUENCE "public"."user_id_seq" OWNED BY "user"."id";
ALTER SEQUENCE "public"."user_individual_detail_id_seq" OWNED BY "user_individual_detail"."id";
ALTER SEQUENCE "public"."user_organization_detail_id_seq" OWNED BY "user_organization_detail"."id";
ALTER SEQUENCE "public"."user_role_allocation_id_seq" OWNED BY "user_role_allocation"."id";
ALTER SEQUENCE "public"."user_role_id_seq" OWNED BY "user_role"."id";
ALTER SEQUENCE "public"."user_type_id_seq" OWNED BY "user_type"."id";

-- ----------------------------
-- Primary Key structure for table approval_status
-- ----------------------------
ALTER TABLE "public"."approval_status" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table association
-- ----------------------------
CREATE UNIQUE INDEX "association_id_uindex" ON "public"."association" USING btree ("id");

-- ----------------------------
-- Primary Key structure for table association
-- ----------------------------
ALTER TABLE "public"."association" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table association_member
-- ----------------------------
CREATE UNIQUE INDEX "association_member_id_uindex" ON "public"."association_member" USING btree ("id");

-- ----------------------------
-- Primary Key structure for table association_member
-- ----------------------------
ALTER TABLE "public"."association_member" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table association_staff
-- ----------------------------
ALTER TABLE "public"."association_staff" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table charge_mode
-- ----------------------------
ALTER TABLE "public"."charge_mode" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table commodity
-- ----------------------------
ALTER TABLE "public"."commodity" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table commodity_category
-- ----------------------------
ALTER TABLE "public"."commodity_category" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table commodity_sub_category
-- ----------------------------
ALTER TABLE "public"."commodity_sub_category" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table country
-- ----------------------------
ALTER TABLE "public"."country" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table currency
-- ----------------------------
ALTER TABLE "public"."currency" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table delivery_option
-- ----------------------------
ALTER TABLE "public"."delivery_option" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table duration
-- ----------------------------
ALTER TABLE "public"."duration" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table email_template_config
-- ----------------------------
ALTER TABLE "public"."email_template_config" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table global_config
-- ----------------------------
ALTER TABLE "public"."global_config" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table international_location_description
-- ----------------------------
ALTER TABLE "public"."international_location_description" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table international_region
-- ----------------------------
ALTER TABLE "public"."international_region" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table language
-- ----------------------------
ALTER TABLE "public"."language" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table location
-- ----------------------------
ALTER TABLE "public"."location" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table location_level
-- ----------------------------
ALTER TABLE "public"."location_level" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table logistics_service
-- ----------------------------
ALTER TABLE "public"."logistics_service" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table logistics_service_additional_charge
-- ----------------------------
ALTER TABLE "public"."logistics_service_additional_charge" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table logistics_service_order
-- ----------------------------
ALTER TABLE "public"."logistics_service_order" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table logistics_service_order_cancellation
-- ----------------------------
ALTER TABLE "public"."logistics_service_order_cancellation" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table logistics_service_order_payment
-- ----------------------------
ALTER TABLE "public"."logistics_service_order_payment" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table logistics_service_order_rating
-- ----------------------------
ALTER TABLE "public"."logistics_service_order_rating" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table logistics_service_order_request
-- ----------------------------
ALTER TABLE "public"."logistics_service_order_request" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table logistics_service_order_request_approval
-- ----------------------------
ALTER TABLE "public"."logistics_service_order_request_approval" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table logistics_service_order_status
-- ----------------------------
ALTER TABLE "public"."logistics_service_order_status" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table logistics_service_tariff
-- ----------------------------
ALTER TABLE "public"."logistics_service_tariff" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table logistics_service_tariff_charge
-- ----------------------------
ALTER TABLE "public"."logistics_service_tariff_charge" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table logistics_service_type
-- ----------------------------
ALTER TABLE "public"."logistics_service_type" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table logistics_trader_vehicle
-- ----------------------------
ALTER TABLE "public"."logistics_trader_vehicle" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table logistics_vehicle_type
-- ----------------------------
ALTER TABLE "public"."logistics_vehicle_type" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table market_order
-- ----------------------------
ALTER TABLE "public"."market_order" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table market_order_approval
-- ----------------------------
ALTER TABLE "public"."market_order_approval" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table market_order_cancellation
-- ----------------------------
ALTER TABLE "public"."market_order_cancellation" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table market_order_payment
-- ----------------------------
ALTER TABLE "public"."market_order_payment" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table market_order_rating
-- ----------------------------
ALTER TABLE "public"."market_order_rating" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table market_order_request
-- ----------------------------
ALTER TABLE "public"."market_order_request" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table market_order_status
-- ----------------------------
ALTER TABLE "public"."market_order_status" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table market_price
-- ----------------------------
ALTER TABLE "public"."market_price" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table measurement_metric
-- ----------------------------
ALTER TABLE "public"."measurement_metric" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table measurement_unit
-- ----------------------------
ALTER TABLE "public"."measurement_unit" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table menu
-- ----------------------------
ALTER TABLE "public"."menu" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table oauth_client_details
-- ----------------------------
ALTER TABLE "public"."oauth_client_details" ADD PRIMARY KEY ("client_id");

-- ----------------------------
-- Primary Key structure for table organization_size
-- ----------------------------
ALTER TABLE "public"."organization_size" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table organization_type
-- ----------------------------
ALTER TABLE "public"."organization_type" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table payment
-- ----------------------------
ALTER TABLE "public"."payment" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table payment_option
-- ----------------------------
ALTER TABLE "public"."payment_option" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table permission
-- ----------------------------
ALTER TABLE "public"."permission" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table permission_menu
-- ----------------------------
ALTER TABLE "public"."permission_menu" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table price_inquiry
-- ----------------------------
ALTER TABLE "public"."price_inquiry" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table product
-- ----------------------------
ALTER TABLE "public"."product" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table product_image
-- ----------------------------
ALTER TABLE "public"."product_image" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table product_image_approval
-- ----------------------------
ALTER TABLE "public"."product_image_approval" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table product_post_approval
-- ----------------------------
ALTER TABLE "public"."product_post_approval" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table rating_user_type
-- ----------------------------
ALTER TABLE "public"."rating_user_type" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table region_country
-- ----------------------------
ALTER TABLE "public"."region_country" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table role_permission
-- ----------------------------
ALTER TABLE "public"."role_permission" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table sms_template_config
-- ----------------------------
ALTER TABLE "public"."sms_template_config" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table tax
-- ----------------------------
ALTER TABLE "public"."tax" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table template_config_variable
-- ----------------------------
ALTER TABLE "public"."template_config_variable" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table template_name
-- ----------------------------
ALTER TABLE "public"."template_name" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table trade_operation
-- ----------------------------
ALTER TABLE "public"."trade_operation" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table trade_volume_statistic
-- ----------------------------
ALTER TABLE "public"."trade_volume_statistic" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table translations
-- ----------------------------
ALTER TABLE "public"."translations" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table user
-- ----------------------------
ALTER TABLE "public"."user" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table user_account_verification
-- ----------------------------
ALTER TABLE "public"."user_account_verification" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table user_approval
-- ----------------------------
ALTER TABLE "public"."user_approval" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table user_association_join_request
-- ----------------------------
ALTER TABLE "public"."user_association_join_request" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table user_association_request_approval
-- ----------------------------
ALTER TABLE "public"."user_association_request_approval" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table user_gender
-- ----------------------------
ALTER TABLE "public"."user_gender" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table user_individual_detail
-- ----------------------------
ALTER TABLE "public"."user_individual_detail" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table user_organization_detail
-- ----------------------------
ALTER TABLE "public"."user_organization_detail" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table user_role
-- ----------------------------
ALTER TABLE "public"."user_role" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table user_type
-- ----------------------------
ALTER TABLE "public"."user_type" ADD PRIMARY KEY ("id");

-- ----------------------------
-- Foreign Key structure for table "public"."association_member"
-- ----------------------------
ALTER TABLE "public"."association_member" ADD FOREIGN KEY ("association_id") REFERENCES "public"."association" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE "public"."association_member" ADD FOREIGN KEY ("user_id") REFERENCES "public"."user" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;

-- ----------------------------
-- Foreign Key structure for table "public"."association_staff"
-- ----------------------------
ALTER TABLE "public"."association_staff" ADD FOREIGN KEY ("user_id") REFERENCES "public"."user" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE "public"."association_staff" ADD FOREIGN KEY ("association_id") REFERENCES "public"."association" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;

-- ----------------------------
-- Foreign Key structure for table "public"."commodity"
-- ----------------------------
ALTER TABLE "public"."commodity" ADD FOREIGN KEY ("added_by") REFERENCES "public"."user" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE "public"."commodity" ADD FOREIGN KEY ("commodity_sub_category_id") REFERENCES "public"."commodity_sub_category" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;

-- ----------------------------
-- Foreign Key structure for table "public"."commodity_category"
-- ----------------------------
ALTER TABLE "public"."commodity_category" ADD FOREIGN KEY ("added_by") REFERENCES "public"."user" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE "public"."commodity_category" ADD FOREIGN KEY ("added_by") REFERENCES "public"."user" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;

-- ----------------------------
-- Foreign Key structure for table "public"."commodity_sub_category"
-- ----------------------------
ALTER TABLE "public"."commodity_sub_category" ADD FOREIGN KEY ("commodity_category_id") REFERENCES "public"."commodity_category" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE "public"."commodity_sub_category" ADD FOREIGN KEY ("added_by") REFERENCES "public"."user" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;

-- ----------------------------
-- Foreign Key structure for table "public"."country"
-- ----------------------------
ALTER TABLE "public"."country" ADD FOREIGN KEY ("default_language") REFERENCES "public"."language" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;

-- ----------------------------
-- Foreign Key structure for table "public"."currency"
-- ----------------------------
ALTER TABLE "public"."currency" ADD FOREIGN KEY ("country_id") REFERENCES "public"."country" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."delivery_option"
-- ----------------------------
ALTER TABLE "public"."delivery_option" ADD FOREIGN KEY ("commodity_sub_category_id") REFERENCES "public"."commodity_sub_category" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE "public"."delivery_option" ADD FOREIGN KEY ("added_by") REFERENCES "public"."user" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;

-- ----------------------------
-- Foreign Key structure for table "public"."location"
-- ----------------------------
ALTER TABLE "public"."location" ADD FOREIGN KEY ("level_id") REFERENCES "public"."location_level" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;

-- ----------------------------
-- Foreign Key structure for table "public"."logistics_service"
-- ----------------------------
ALTER TABLE "public"."logistics_service" ADD FOREIGN KEY ("location_from") REFERENCES "public"."location" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE "public"."logistics_service" ADD FOREIGN KEY ("location_to") REFERENCES "public"."location" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."logistics_service" ADD FOREIGN KEY ("provider_user_id") REFERENCES "public"."user" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;

-- ----------------------------
-- Foreign Key structure for table "public"."logistics_service_additional_charge"
-- ----------------------------
ALTER TABLE "public"."logistics_service_additional_charge" ADD FOREIGN KEY ("logistics_service_id") REFERENCES "public"."logistics_service" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE "public"."logistics_service_additional_charge" ADD FOREIGN KEY ("logistics_service_type_id") REFERENCES "public"."logistics_service_type" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;

-- ----------------------------
-- Foreign Key structure for table "public"."logistics_service_order"
-- ----------------------------
ALTER TABLE "public"."logistics_service_order" ADD FOREIGN KEY ("logistics_service_order_status_id") REFERENCES "public"."logistics_service_order_status" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE "public"."logistics_service_order" ADD FOREIGN KEY ("logistics_service_order_request_id") REFERENCES "public"."logistics_service_order_request" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;

-- ----------------------------
-- Foreign Key structure for table "public"."logistics_service_order_cancellation"
-- ----------------------------
ALTER TABLE "public"."logistics_service_order_cancellation" ADD FOREIGN KEY ("cancelled_by") REFERENCES "public"."user" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE "public"."logistics_service_order_cancellation" ADD FOREIGN KEY ("logistics_service_order_id") REFERENCES "public"."logistics_service_order" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;

-- ----------------------------
-- Foreign Key structure for table "public"."logistics_service_order_payment"
-- ----------------------------
ALTER TABLE "public"."logistics_service_order_payment" ADD FOREIGN KEY ("payment_id") REFERENCES "public"."payment" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."logistics_service_order_payment" ADD FOREIGN KEY ("logistics_service_order_id") REFERENCES "public"."logistics_service_order" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."logistics_service_order_rating"
-- ----------------------------
ALTER TABLE "public"."logistics_service_order_rating" ADD FOREIGN KEY ("logistics_service_order_id") REFERENCES "public"."logistics_service_order" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE "public"."logistics_service_order_rating" ADD FOREIGN KEY ("rated_by") REFERENCES "public"."user" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE "public"."logistics_service_order_rating" ADD FOREIGN KEY ("logistics_service_order_rating_user_type_id") REFERENCES "public"."rating_user_type" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;

-- ----------------------------
-- Foreign Key structure for table "public"."logistics_service_order_request"
-- ----------------------------
ALTER TABLE "public"."logistics_service_order_request" ADD FOREIGN KEY ("approval_status_id") REFERENCES "public"."approval_status" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE "public"."logistics_service_order_request" ADD FOREIGN KEY ("logistics_service_id") REFERENCES "public"."logistics_service" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE "public"."logistics_service_order_request" ADD FOREIGN KEY ("logistics_service_tariff_id") REFERENCES "public"."logistics_service_tariff" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE "public"."logistics_service_order_request" ADD FOREIGN KEY ("market_order_id") REFERENCES "public"."market_order" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE "public"."logistics_service_order_request" ADD FOREIGN KEY ("ordered_by") REFERENCES "public"."user" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;

-- ----------------------------
-- Foreign Key structure for table "public"."logistics_service_order_request_approval"
-- ----------------------------
ALTER TABLE "public"."logistics_service_order_request_approval" ADD FOREIGN KEY ("logistics_service_order_request_id") REFERENCES "public"."logistics_service_order_request" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE "public"."logistics_service_order_request_approval" ADD FOREIGN KEY ("approved_by") REFERENCES "public"."user" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;

-- ----------------------------
-- Foreign Key structure for table "public"."logistics_service_tariff"
-- ----------------------------
ALTER TABLE "public"."logistics_service_tariff" ADD FOREIGN KEY ("charging_mode_id") REFERENCES "public"."charge_mode" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE "public"."logistics_service_tariff" ADD FOREIGN KEY ("logistics_service_id") REFERENCES "public"."logistics_service" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE "public"."logistics_service_tariff" ADD FOREIGN KEY ("measurement_unit_id") REFERENCES "public"."measurement_unit" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;

-- ----------------------------
-- Foreign Key structure for table "public"."logistics_service_tariff_charge"
-- ----------------------------
ALTER TABLE "public"."logistics_service_tariff_charge" ADD FOREIGN KEY ("logistics_service_tariff_id") REFERENCES "public"."logistics_service_tariff" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;

-- ----------------------------
-- Foreign Key structure for table "public"."logistics_service_type"
-- ----------------------------
ALTER TABLE "public"."logistics_service_type" ADD FOREIGN KEY ("added_by") REFERENCES "public"."user" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;

-- ----------------------------
-- Foreign Key structure for table "public"."logistics_trader_vehicle"
-- ----------------------------
ALTER TABLE "public"."logistics_trader_vehicle" ADD FOREIGN KEY ("provider_user_id") REFERENCES "public"."user" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE "public"."logistics_trader_vehicle" ADD FOREIGN KEY ("logistics_vehicle_type_id") REFERENCES "public"."logistics_vehicle_type" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;

-- ----------------------------
-- Foreign Key structure for table "public"."market_order"
-- ----------------------------
ALTER TABLE "public"."market_order" ADD FOREIGN KEY ("product_id") REFERENCES "public"."product" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE "public"."market_order" ADD FOREIGN KEY ("ordered_by") REFERENCES "public"."user" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;

-- ----------------------------
-- Foreign Key structure for table "public"."market_order_approval"
-- ----------------------------
ALTER TABLE "public"."market_order_approval" ADD FOREIGN KEY ("market_order_request_id") REFERENCES "public"."market_order_request" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE "public"."market_order_approval" ADD FOREIGN KEY ("approved_by") REFERENCES "public"."user" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."market_order_cancellation"
-- ----------------------------
ALTER TABLE "public"."market_order_cancellation" ADD FOREIGN KEY ("cancelled_by") REFERENCES "public"."user" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE "public"."market_order_cancellation" ADD FOREIGN KEY ("market_order_id") REFERENCES "public"."market_order" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;

-- ----------------------------
-- Foreign Key structure for table "public"."market_order_payment"
-- ----------------------------
ALTER TABLE "public"."market_order_payment" ADD FOREIGN KEY ("payment_id") REFERENCES "public"."payment" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."market_order_payment" ADD FOREIGN KEY ("market_order_id") REFERENCES "public"."market_order" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;

-- ----------------------------
-- Foreign Key structure for table "public"."market_order_rating"
-- ----------------------------
ALTER TABLE "public"."market_order_rating" ADD FOREIGN KEY ("rated_by") REFERENCES "public"."user" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE "public"."market_order_rating" ADD FOREIGN KEY ("market_order_id") REFERENCES "public"."market_order" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;

-- ----------------------------
-- Foreign Key structure for table "public"."market_order_request"
-- ----------------------------
ALTER TABLE "public"."market_order_request" ADD FOREIGN KEY ("ordered_by") REFERENCES "public"."user" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE "public"."market_order_request" ADD FOREIGN KEY ("approval_status_id") REFERENCES "public"."approval_status" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE "public"."market_order_request" ADD FOREIGN KEY ("product_id") REFERENCES "public"."product" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;

-- ----------------------------
-- Foreign Key structure for table "public"."market_price"
-- ----------------------------
ALTER TABLE "public"."market_price" ADD FOREIGN KEY ("commodity_id") REFERENCES "public"."commodity" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE "public"."market_price" ADD FOREIGN KEY ("currency_id") REFERENCES "public"."currency" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE "public"."market_price" ADD FOREIGN KEY ("location_id") REFERENCES "public"."location" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE "public"."market_price" ADD FOREIGN KEY ("added_by") REFERENCES "public"."user" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;

-- ----------------------------
-- Foreign Key structure for table "public"."measurement_unit"
-- ----------------------------
ALTER TABLE "public"."measurement_unit" ADD FOREIGN KEY ("measurement_metric_id") REFERENCES "public"."measurement_metric" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE "public"."measurement_unit" ADD FOREIGN KEY ("added_by") REFERENCES "public"."user" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;

-- ----------------------------
-- Foreign Key structure for table "public"."payment"
-- ----------------------------
ALTER TABLE "public"."payment" ADD FOREIGN KEY ("payment_option_id") REFERENCES "public"."payment_option" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."payment_option"
-- ----------------------------
ALTER TABLE "public"."payment_option" ADD FOREIGN KEY ("added_by") REFERENCES "public"."user" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;

-- ----------------------------
-- Foreign Key structure for table "public"."price_inquiry"
-- ----------------------------
ALTER TABLE "public"."price_inquiry" ADD FOREIGN KEY ("commodity_id") REFERENCES "public"."commodity" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE "public"."price_inquiry" ADD FOREIGN KEY ("location_id") REFERENCES "public"."location" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE "public"."price_inquiry" ADD FOREIGN KEY ("inquired_by") REFERENCES "public"."user" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;

-- ----------------------------
-- Foreign Key structure for table "public"."product"
-- ----------------------------
ALTER TABLE "public"."product" ADD FOREIGN KEY ("commodity_id") REFERENCES "public"."commodity" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE "public"."product" ADD FOREIGN KEY ("trader_id") REFERENCES "public"."user" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE "public"."product" ADD FOREIGN KEY ("measurement_unit_id") REFERENCES "public"."measurement_unit" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;

-- ----------------------------
-- Foreign Key structure for table "public"."product_image"
-- ----------------------------
ALTER TABLE "public"."product_image" ADD FOREIGN KEY ("product_id") REFERENCES "public"."product" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;

-- ----------------------------
-- Foreign Key structure for table "public"."product_image_approval"
-- ----------------------------
ALTER TABLE "public"."product_image_approval" ADD FOREIGN KEY ("approval_status_id") REFERENCES "public"."approval_status" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE "public"."product_image_approval" ADD FOREIGN KEY ("approved_by") REFERENCES "public"."user" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "public"."product_image_approval" ADD FOREIGN KEY ("product_image_id") REFERENCES "public"."product_image" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;

-- ----------------------------
-- Foreign Key structure for table "public"."product_post_approval"
-- ----------------------------
ALTER TABLE "public"."product_post_approval" ADD FOREIGN KEY ("product_id") REFERENCES "public"."product" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE "public"."product_post_approval" ADD FOREIGN KEY ("approval_status_id") REFERENCES "public"."approval_status" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE "public"."product_post_approval" ADD FOREIGN KEY ("approved_by") REFERENCES "public"."user" ("id") ON DELETE NO ACTION ON UPDATE NO ACTION;

-- ----------------------------
-- Foreign Key structure for table "public"."tax"
-- ----------------------------
ALTER TABLE "public"."tax" ADD FOREIGN KEY ("charge_mode_id") REFERENCES "public"."charge_mode" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;

-- ----------------------------
-- Foreign Key structure for table "public"."trade_volume_statistic"
-- ----------------------------
ALTER TABLE "public"."trade_volume_statistic" ADD FOREIGN KEY ("commodity_id") REFERENCES "public"."commodity" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE "public"."trade_volume_statistic" ADD FOREIGN KEY ("from_country") REFERENCES "public"."country" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE "public"."trade_volume_statistic" ADD FOREIGN KEY ("to_country") REFERENCES "public"."country" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE "public"."trade_volume_statistic" ADD FOREIGN KEY ("duration_id") REFERENCES "public"."duration" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE "public"."trade_volume_statistic" ADD FOREIGN KEY ("measurement_unit_id") REFERENCES "public"."measurement_unit" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE "public"."trade_volume_statistic" ADD FOREIGN KEY ("trade_operation_id") REFERENCES "public"."trade_operation" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE "public"."trade_volume_statistic" ADD FOREIGN KEY ("added_by") REFERENCES "public"."user" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;

-- ----------------------------
-- Foreign Key structure for table "public"."user"
-- ----------------------------
ALTER TABLE "public"."user" ADD FOREIGN KEY ("user_type_id") REFERENCES "public"."user_type" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE "public"."user" ADD FOREIGN KEY ("language_preference") REFERENCES "public"."language" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE "public"."user" ADD FOREIGN KEY ("location_id") REFERENCES "public"."location" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE "public"."user" ADD FOREIGN KEY ("country_id") REFERENCES "public"."country" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE "public"."user" ADD FOREIGN KEY ("approval_status_id") REFERENCES "public"."approval_status" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;

-- ----------------------------
-- Foreign Key structure for table "public"."user_association_join_request"
-- ----------------------------
ALTER TABLE "public"."user_association_join_request" ADD FOREIGN KEY ("user_id") REFERENCES "public"."user" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE "public"."user_association_join_request" ADD FOREIGN KEY ("association_id") REFERENCES "public"."association" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;

-- ----------------------------
-- Foreign Key structure for table "public"."user_association_request_approval"
-- ----------------------------
ALTER TABLE "public"."user_association_request_approval" ADD FOREIGN KEY ("approved_by") REFERENCES "public"."user" ("id") ON DELETE RESTRICT ON UPDATE RESTRICT;


-- ---------------------------------
-- Insert Initial Data
-- ---------------------------------

-- ----------------------------
-- Init Data for approval_status
-- ----------------------------
INSERT INTO "approval_status" ("id", "code", "name", "is_active") VALUES (1, 'approved', 'Approved', 'true');
INSERT INTO "approval_status" ("id", "code", "name", "is_active") VALUES (2, 'pending', 'Pending', 'true');
INSERT INTO "approval_status" ("id", "code", "name", "is_active") VALUES (3, 'rejected', 'Rejected', 'true');


-- ----------------------------
-- Init Data for global_config
-- ----------------------------
INSERT INTO "global_config" ("id", "config", "value") VALUES (1, 'user_registration_approval', 'false');


-- ----------------------------
-- Init Data for user_type
-- ----------------------------
INSERT INTO user_type (id, name, is_active) VALUES (1, 'Individual', true);
INSERT INTO user_type (id, name, is_active) VALUES (2, 'Organization', true);

-- ----------------------------
-- Init Data for user_gender
-- ----------------------------
INSERT INTO user_gender (id, name, is_active) VALUES (1, 'Male', true);
INSERT INTO user_gender (id, name, is_active) VALUES (2, 'Female', true);

-- ----------------------------
-- Init Data for user_role
-- ----------------------------
INSERT INTO user_role (id, name, is_active, is_admin) VALUES (1, 'Trader', true, false);

-- ----------------------------
-- Init Data for language
-- ----------------------------
INSERT INTO language (id, name, code_name, is_active) VALUES (1, 'English', 'en', true);

-- ----------------------------
-- Init Data for country
-- ----------------------------
INSERT INTO country (id, name, code_name, default_language, is_active) VALUES (1, 'Kenya', 'ken', 1, true);

-- ----------------------------
-- Init Data for location_level
-- ----------------------------
INSERT INTO location_level (id, name, parent_id) VALUES (1, 'County', null);


-- ----------------------------
-- Init Data for location
-- ----------------------------
INSERT INTO location (id, name, code_name, level_id) VALUES (1, 'Nairobi', 'nbo', 1);

-- ----------------------------
-- Init Data for organization_type
-- ----------------------------
INSERT INTO organization_type (id, name, is_active) VALUES (1, 'Private Company', true);


-- ----------------------------
-- Init Data for organization_size
-- ----------------------------

INSERT INTO organization_size (id, name, is_active) VALUES (1, '10-49 (Small Enterprise)', true);


-- ----------------------------
-- Init Data for oauth_client_details
-- ----------------------------
INSERT INTO public.oauth_client_details (client_id, resource_ids, client_secret, scope, authorized_grant_types, web_server_redirect_uri, authorities, access_token_validity, refresh_token_validity, additional_information, autoapprove) VALUES ('wit_web_app', 'oauth2-resource', '$2y$12$xzgzUqOdUhBMba3JtqT9COvi99flh5lh3DWnrO2LhAj/vSgfIo/Y.', 'REGISTER_USER', 'client_credentials,password,refresh_token', '', 'ROLE_WIT_AUTHORIZED_CLIENT', 3600, -1, '{}', 'REGISTER_USER');
INSERT INTO public.oauth_client_details (client_id, resource_ids, client_secret, scope, authorized_grant_types, web_server_redirect_uri, authorities, access_token_validity, refresh_token_validity, additional_information, autoapprove) VALUES ('wit_android_app', 'oauth2-resource', '$2y$12$4SLZ8/vAreBB9RB7VnbKFOMMdm6Dxu9g7THjHu1cpTa9p2vHl.s.a', 'REGISTER_USER', 'client_credentials,password,refresh_token', '', 'ROLE_WIT_AUTHORIZED_CLIENT', 3600, -1, '{}', 'REGISTER_USER');