alter table market_price
    add measurement_unit_id bigint;

alter table market_price
    add constraint market_price_measurement_unit_id_fkey
        foreign key (measurement_unit_id) references measurement_unit
            on update restrict on delete restrict;

alter table market_price alter column date type date using date::date;

alter table price_inquiry alter column date type date using date::date;

alter table price_inquiry rename to market_price_inquiry;

alter table commodity
    add market_price_measurement_unit_id bigint;

alter table commodity
    add constraint commodity_market_price_measurement_unit_id_fk
        foreign key (market_price_measurement_unit_id) references measurement_unit
            on update restrict on delete restrict;

