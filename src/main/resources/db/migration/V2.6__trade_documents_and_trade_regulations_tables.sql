--
-- PostgreSQL database dump
--

-- Dumped from database version 12.4 (Ubuntu 12.4-1.pgdg20.04+1)
-- Dumped by pg_dump version 12.4 (Ubuntu 12.4-1.pgdg20.04+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: regulation; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.regulation (
                                   id bigint NOT NULL,
                                   trade_operation_id bigint,
                                   country_from bigint,
                                   country_to bigint,
                                   is_active boolean,
                                   added_by bigint,
                                   date_added timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
                                   sub_category_id bigint
);


ALTER TABLE public.regulation OWNER TO postgres;

--
-- Name: regulation_category; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.regulation_category (
                                            id bigint NOT NULL,
                                            "title " character varying(100),
                                            description character varying(200),
                                            is_active boolean,
                                            added_by bigint,
                                            date_added timestamp without time zone DEFAULT CURRENT_TIMESTAMP
);


ALTER TABLE public.regulation_category OWNER TO postgres;

--
-- Name: regulation_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.regulation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.regulation_id_seq OWNER TO postgres;

--
-- Name: regulation_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.regulation_id_seq OWNED BY public.regulation.id;


--
-- Name: regulation_procedure_requirement; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.regulation_procedure_requirement (
                                                         id bigint NOT NULL,
                                                         regulation_procedure_requirement_type_id bigint,
                                                         is_active boolean,
                                                         added_by bigint,
                                                         date_added timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
                                                         text text
);


ALTER TABLE public.regulation_procedure_requirement OWNER TO postgres;

--
-- Name: regulation_procedure_requirement_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.regulation_procedure_requirement_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.regulation_procedure_requirement_id_seq OWNER TO postgres;

--
-- Name: regulation_procedure_requirement_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.regulation_procedure_requirement_id_seq OWNED BY public.regulation_procedure_requirement.id;


--
-- Name: regulation_procedure_requirement_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.regulation_procedure_requirement_type (
                                                              id bigint NOT NULL,
                                                              name character varying(50),
                                                              description character varying(250),
                                                              is_active boolean
);


ALTER TABLE public.regulation_procedure_requirement_type OWNER TO postgres;

--
-- Name: regulation_procedure_requirement_type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.regulation_procedure_requirement_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.regulation_procedure_requirement_type_id_seq OWNER TO postgres;

--
-- Name: regulation_procedure_requirement_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.regulation_procedure_requirement_type_id_seq OWNED BY public.regulation_procedure_requirement_type.id;


--
-- Name: regulation_regulatory_agency; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.regulation_regulatory_agency (
                                                     id bigint NOT NULL,
                                                     regulatory_agency_id bigint,
                                                     regulation_id bigint,
                                                     is_active boolean,
                                                     date_added timestamp without time zone DEFAULT CURRENT_TIMESTAMP
);


ALTER TABLE public.regulation_regulatory_agency OWNER TO postgres;

--
-- Name: regulation_regulatory_agency_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.regulation_regulatory_agency_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.regulation_regulatory_agency_id_seq OWNER TO postgres;

--
-- Name: regulation_regulatory_agency_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.regulation_regulatory_agency_id_seq OWNED BY public.regulation_regulatory_agency.id;


--
-- Name: regulations_categories_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.regulations_categories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.regulations_categories_id_seq OWNER TO postgres;

--
-- Name: regulations_categories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.regulations_categories_id_seq OWNED BY public.regulation_category.id;


--
-- Name: regulatory_agency; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.regulatory_agency (
                                          id bigint NOT NULL,
                                          code character varying(20),
                                          name character varying(50),
                                          description character varying(250),
                                          logo_url character varying(250),
                                          country_id bigint,
                                          email_address character varying(50),
                                          phone_number character varying(50),
                                          website_url character varying(250),
                                          postal_address character varying(50),
                                          physical_address character varying(50)
);


ALTER TABLE public.regulatory_agency OWNER TO postgres;

--
-- Name: regulatory_agency_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.regulatory_agency_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.regulatory_agency_id_seq OWNER TO postgres;

--
-- Name: regulatory_agency_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.regulatory_agency_id_seq OWNED BY public.regulatory_agency.id;


--
-- Name: regulatory_procedure; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.regulatory_procedure (
                                             id bigint NOT NULL,
                                             regulation_id bigint,
                                             title character varying(100),
                                             description character varying(250),
                                             is_apply_online boolean DEFAULT false,
                                             application_url character varying(50),
                                             time_frame character varying(50),
                                             cost double precision,
                                             payment_process character varying(250),
                                             priority_number bigint,
                                             added_by bigint,
                                             date_added timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
                                             is_active boolean,
                                             "regulation_category_id " bigint
);


ALTER TABLE public.regulatory_procedure OWNER TO postgres;

--
-- Name: regulatory_procedure_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.regulatory_procedure_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.regulatory_procedure_id_seq OWNER TO postgres;

--
-- Name: regulatory_procedure_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.regulatory_procedure_id_seq OWNED BY public.regulatory_procedure.id;


--
-- Name: trade_document_category; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.trade_document_category (
                                                id integer NOT NULL,
                                                name character varying(50),
                                                added_by bigint,
                                                date_added timestamp without time zone DEFAULT CURRENT_TIMESTAMP
);


ALTER TABLE public.trade_document_category OWNER TO postgres;

--
-- Name: trade_document_category_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.trade_document_category_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.trade_document_category_id_seq OWNER TO postgres;

--
-- Name: trade_document_category_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.trade_document_category_id_seq OWNED BY public.trade_document_category.id;


--
-- Name: trade_document_information; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.trade_document_information (
                                                   id bigint NOT NULL,
                                                   name character varying(50),
                                                   description text,
                                                   trade_document_type_id bigint,
                                                   trade_operation_id bigint,
                                                   country_from bigint,
                                                   country_to bigint,
                                                   commodity_id bigint,
                                                   is_active boolean,
                                                   added_by bigint,
                                                   date_added timestamp without time zone DEFAULT CURRENT_TIMESTAMP
);


ALTER TABLE public.trade_document_information OWNER TO postgres;

--
-- Name: trade_document_information_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.trade_document_information_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.trade_document_information_id_seq OWNER TO postgres;

--
-- Name: trade_document_information_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.trade_document_information_id_seq OWNED BY public.trade_document_information.id;


--
-- Name: trade_document_sample; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.trade_document_sample (
                                              id integer NOT NULL,
                                              name character varying(50),
                                              document_information_id bigint,
                                              path character varying(250),
                                              is_active boolean
);


ALTER TABLE public.trade_document_sample OWNER TO postgres;

--
-- Name: trade_document_sample_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.trade_document_sample_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.trade_document_sample_id_seq OWNER TO postgres;

--
-- Name: trade_document_sample_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.trade_document_sample_id_seq OWNED BY public.trade_document_sample.id;


--
-- Name: trade_document_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.trade_document_type (
                                            id bigint NOT NULL,
                                            name character varying(50),
                                            trade_document_category_id bigint,
                                            added_by bigint,
                                            date_added timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
                                            is_active boolean
);


ALTER TABLE public.trade_document_type OWNER TO postgres;

--
-- Name: trade_document_type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.trade_document_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.trade_document_type_id_seq OWNER TO postgres;

--
-- Name: trade_document_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.trade_document_type_id_seq OWNED BY public.trade_document_type.id;


--
-- Name: regulation id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.regulation ALTER COLUMN id SET DEFAULT nextval('public.regulation_id_seq'::regclass);


--
-- Name: regulation_category id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.regulation_category ALTER COLUMN id SET DEFAULT nextval('public.regulations_categories_id_seq'::regclass);


--
-- Name: regulation_procedure_requirement id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.regulation_procedure_requirement ALTER COLUMN id SET DEFAULT nextval('public.regulation_procedure_requirement_id_seq'::regclass);


--
-- Name: regulation_procedure_requirement_type id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.regulation_procedure_requirement_type ALTER COLUMN id SET DEFAULT nextval('public.regulation_procedure_requirement_type_id_seq'::regclass);


--
-- Name: regulation_regulatory_agency id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.regulation_regulatory_agency ALTER COLUMN id SET DEFAULT nextval('public.regulation_regulatory_agency_id_seq'::regclass);


--
-- Name: regulatory_agency id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.regulatory_agency ALTER COLUMN id SET DEFAULT nextval('public.regulatory_agency_id_seq'::regclass);


--
-- Name: regulatory_procedure id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.regulatory_procedure ALTER COLUMN id SET DEFAULT nextval('public.regulatory_procedure_id_seq'::regclass);


--
-- Name: trade_document_category id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.trade_document_category ALTER COLUMN id SET DEFAULT nextval('public.trade_document_category_id_seq'::regclass);


--
-- Name: trade_document_information id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.trade_document_information ALTER COLUMN id SET DEFAULT nextval('public.trade_document_information_id_seq'::regclass);


--
-- Name: trade_document_sample id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.trade_document_sample ALTER COLUMN id SET DEFAULT nextval('public.trade_document_sample_id_seq'::regclass);


--
-- Name: trade_document_type id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.trade_document_type ALTER COLUMN id SET DEFAULT nextval('public.trade_document_type_id_seq'::regclass);


--
-- Data for Name: regulation; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.regulation (id, trade_operation_id, country_from, country_to, is_active, added_by, date_added, sub_category_id) FROM stdin;
1	1	2	3	t	\N	2020-10-23 14:47:33.118018	2
2	1	3	3	t	\N	2020-10-23 14:55:31.644476	2
3	1	3	1	t	\N	2020-10-23 14:58:20.41603	2
4	2	3	1	t	2	2020-10-23 15:03:52.402022	2
\.


--
-- Data for Name: regulation_category; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.regulation_category (id, "title ", description, is_active, added_by, date_added) FROM stdin;
1	Preliminaries registrations 	imports and exports preliminary registration	t	2	2020-10-23 11:56:19.508185
2	Preliminaries registrations01	imports and exports preliminary	t	2	2020-10-23 12:13:40.486258
3	Preliminaries registrations	imports and exports preliminary	t	2	2020-10-23 21:21:34.056235
\.


--
-- Data for Name: regulation_procedure_requirement; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.regulation_procedure_requirement (id, regulation_procedure_requirement_type_id, is_active, added_by, date_added, text) FROM stdin;
\.


--
-- Data for Name: regulation_procedure_requirement_type; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.regulation_procedure_requirement_type (id, name, description, is_active) FROM stdin;
1	hello	world	t
2	legal	legal requirements	t
\.


--
-- Data for Name: regulation_regulatory_agency; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.regulation_regulatory_agency (id, regulatory_agency_id, regulation_id, is_active, date_added) FROM stdin;
\.


--
-- Data for Name: regulatory_agency; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.regulatory_agency (id, code, name, description, logo_url, country_id, email_address, phone_number, website_url, postal_address, physical_address) FROM stdin;
1	KRA	Kenya Revenue Authority	Collection of Revenue	\N	1	kra@gmail.com	0706559254	\N	\N	\N
\.


--
-- Data for Name: regulatory_procedure; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.regulatory_procedure (id, regulation_id, title, description, is_apply_online, application_url, time_frame, cost, payment_process, priority_number, added_by, date_added, is_active, "regulation_category_id ") FROM stdin;
1	1	firm inseption	firm inseption	t		one year	2000	payment process sample	1	\N	2020-10-23 17:53:35.28393	t	2
2	1	firm inseption	firm inseption	t		one year	2000	payment process sample	1	\N	2020-10-23 17:55:19.227368	t	2
3	1	firm inseption	firm inseption	t		one year	2000	payment process sample	1	\N	2020-10-23 17:58:47.452116	t	2
\.


--
-- Data for Name: trade_document_category; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.trade_document_category (id, name, added_by, date_added) FROM stdin;
\.


--
-- Data for Name: trade_document_information; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.trade_document_information (id, name, description, trade_document_type_id, trade_operation_id, country_from, country_to, commodity_id, is_active, added_by, date_added) FROM stdin;
\.


--
-- Data for Name: trade_document_sample; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.trade_document_sample (id, name, document_information_id, path, is_active) FROM stdin;
\.


--
-- Data for Name: trade_document_type; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.trade_document_type (id, name, trade_document_category_id, added_by, date_added, is_active) FROM stdin;
\.


--
-- Name: regulation_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.regulation_id_seq', 4, true);


--
-- Name: regulation_procedure_requirement_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.regulation_procedure_requirement_id_seq', 1, false);


--
-- Name: regulation_procedure_requirement_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.regulation_procedure_requirement_type_id_seq', 2, true);


--
-- Name: regulation_regulatory_agency_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.regulation_regulatory_agency_id_seq', 1, false);


--
-- Name: regulations_categories_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.regulations_categories_id_seq', 3, true);


--
-- Name: regulatory_agency_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.regulatory_agency_id_seq', 1, true);


--
-- Name: regulatory_procedure_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.regulatory_procedure_id_seq', 3, true);


--
-- Name: trade_document_category_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.trade_document_category_id_seq', 1, false);


--
-- Name: trade_document_information_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.trade_document_information_id_seq', 1, false);


--
-- Name: trade_document_sample_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.trade_document_sample_id_seq', 1, false);


--
-- Name: trade_document_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.trade_document_type_id_seq', 1, false);


--
-- Name: regulation regulation_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.regulation
    ADD CONSTRAINT regulation_pk PRIMARY KEY (id);


--
-- Name: regulation_procedure_requirement regulation_procedure_requirement_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.regulation_procedure_requirement
    ADD CONSTRAINT regulation_procedure_requirement_pk PRIMARY KEY (id);


--
-- Name: regulation_procedure_requirement_type regulation_procedure_requirement_type_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.regulation_procedure_requirement_type
    ADD CONSTRAINT regulation_procedure_requirement_type_pk PRIMARY KEY (id);


--
-- Name: regulation_regulatory_agency regulation_regulatory_agency_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.regulation_regulatory_agency
    ADD CONSTRAINT regulation_regulatory_agency_pk PRIMARY KEY (id);


--
-- Name: regulation_category regulations_categories_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.regulation_category
    ADD CONSTRAINT regulations_categories_pk PRIMARY KEY (id);


--
-- Name: regulatory_agency regulatory_agency_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.regulatory_agency
    ADD CONSTRAINT regulatory_agency_pk PRIMARY KEY (id);


--
-- Name: regulatory_procedure regulatory_procedure_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.regulatory_procedure
    ADD CONSTRAINT regulatory_procedure_pk PRIMARY KEY (id);


--
-- Name: trade_document_category trade_document_category_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.trade_document_category
    ADD CONSTRAINT trade_document_category_pk PRIMARY KEY (id);


--
-- Name: trade_document_information trade_document_information_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.trade_document_information
    ADD CONSTRAINT trade_document_information_pk PRIMARY KEY (id);


--
-- Name: trade_document_sample trade_document_sample_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.trade_document_sample
    ADD CONSTRAINT trade_document_sample_pk PRIMARY KEY (id);


--
-- Name: trade_document_type trade_document_type_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.trade_document_type
    ADD CONSTRAINT trade_document_type_pk PRIMARY KEY (id);


--
-- Name: regulation_id_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX regulation_id_uindex ON public.regulation USING btree (id);


--
-- Name: regulation_procedure_requirement_id_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX regulation_procedure_requirement_id_uindex ON public.regulation_procedure_requirement USING btree (id);


--
-- Name: regulation_procedure_requirement_type_id_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX regulation_procedure_requirement_type_id_uindex ON public.regulation_procedure_requirement_type USING btree (id);


--
-- Name: regulation_regulatory_agency_id_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX regulation_regulatory_agency_id_uindex ON public.regulation_regulatory_agency USING btree (id);


--
-- Name: regulations_categories_id_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX regulations_categories_id_uindex ON public.regulation_category USING btree (id);


--
-- Name: regulatory_agency_id_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX regulatory_agency_id_uindex ON public.regulatory_agency USING btree (id);


--
-- Name: regulatory_procedure_id_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX regulatory_procedure_id_uindex ON public.regulatory_procedure USING btree (id);


--
-- Name: trade_document_sample_id_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX trade_document_sample_id_uindex ON public.trade_document_sample USING btree (id);


--
-- Name: trade_document_type_id_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX trade_document_type_id_uindex ON public.trade_document_type USING btree (id);


--
-- Name: regulation regulation_commodity_sub_category__fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.regulation
    ADD CONSTRAINT regulation_commodity_sub_category__fk FOREIGN KEY (sub_category_id) REFERENCES public.commodity_sub_category(id);


--
-- Name: regulation regulation_country__fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.regulation
    ADD CONSTRAINT regulation_country__fk FOREIGN KEY (country_to) REFERENCES public.country(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: regulation regulation_country__fk_2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.regulation
    ADD CONSTRAINT regulation_country__fk_2 FOREIGN KEY (country_from) REFERENCES public.country(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: regulation_procedure_requirement regulation_procedure_requirement_regulation_procedure_requireme; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.regulation_procedure_requirement
    ADD CONSTRAINT regulation_procedure_requirement_regulation_procedure_requireme FOREIGN KEY (regulation_procedure_requirement_type_id) REFERENCES public.regulation_procedure_requirement_type(id);


--
-- Name: regulation_regulatory_agency regulation_regulatory_agency_regulation__fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.regulation_regulatory_agency
    ADD CONSTRAINT regulation_regulatory_agency_regulation__fk FOREIGN KEY (regulation_id) REFERENCES public.regulation(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: regulation_regulatory_agency regulation_regulatory_agency_regulatory_agency__fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.regulation_regulatory_agency
    ADD CONSTRAINT regulation_regulatory_agency_regulatory_agency__fk FOREIGN KEY (regulatory_agency_id) REFERENCES public.regulatory_agency(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: regulation regulation_trade_operation__fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.regulation
    ADD CONSTRAINT regulation_trade_operation__fk FOREIGN KEY (trade_operation_id) REFERENCES public.trade_operation(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: regulation regulation_user__fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.regulation
    ADD CONSTRAINT regulation_user__fk FOREIGN KEY (added_by) REFERENCES public."user"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: regulation_category regulations_categories_user__fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.regulation_category
    ADD CONSTRAINT regulations_categories_user__fk FOREIGN KEY (added_by) REFERENCES public."user"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: regulatory_agency regulatory_agency_country__fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.regulatory_agency
    ADD CONSTRAINT regulatory_agency_country__fk FOREIGN KEY (country_id) REFERENCES public.country(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: regulatory_procedure regulatory_procedure_regulation__fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.regulatory_procedure
    ADD CONSTRAINT regulatory_procedure_regulation__fk FOREIGN KEY (regulation_id) REFERENCES public.regulation(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: regulatory_procedure regulatory_procedure_user__fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.regulatory_procedure
    ADD CONSTRAINT regulatory_procedure_user__fk FOREIGN KEY (added_by) REFERENCES public."user"(id);


--
-- Name: trade_document_category trade_document_category_user__fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.trade_document_category
    ADD CONSTRAINT trade_document_category_user__fk FOREIGN KEY (added_by) REFERENCES public."user"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: trade_document_information trade_document_information_commodity__fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.trade_document_information
    ADD CONSTRAINT trade_document_information_commodity__fk FOREIGN KEY (commodity_id) REFERENCES public.commodity(id);


--
-- Name: trade_document_information trade_document_information_country__fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.trade_document_information
    ADD CONSTRAINT trade_document_information_country__fk FOREIGN KEY (country_from) REFERENCES public.country(id);


--
-- Name: trade_document_information trade_document_information_country__fk_2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.trade_document_information
    ADD CONSTRAINT trade_document_information_country__fk_2 FOREIGN KEY (country_to) REFERENCES public.country(id);


--
-- Name: trade_document_information trade_document_information_trade_document_type__fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.trade_document_information
    ADD CONSTRAINT trade_document_information_trade_document_type__fk FOREIGN KEY (trade_document_type_id) REFERENCES public.trade_document_type(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: trade_document_information trade_document_information_trade_operation__fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.trade_document_information
    ADD CONSTRAINT trade_document_information_trade_operation__fk FOREIGN KEY (trade_operation_id) REFERENCES public.trade_operation(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: trade_document_information trade_document_information_user__fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.trade_document_information
    ADD CONSTRAINT trade_document_information_user__fk FOREIGN KEY (added_by) REFERENCES public."user"(id);


--
-- Name: trade_document_sample trade_document_sample_trade_document_information__fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.trade_document_sample
    ADD CONSTRAINT trade_document_sample_trade_document_information__fk FOREIGN KEY (document_information_id) REFERENCES public.trade_document_information(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: trade_document_type trade_document_type_trade_document_category__fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.trade_document_type
    ADD CONSTRAINT trade_document_type_trade_document_category__fk FOREIGN KEY (trade_document_category_id) REFERENCES public.trade_document_category(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: trade_document_type trade_document_type_user__fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.trade_document_type
    ADD CONSTRAINT trade_document_type_user__fk FOREIGN KEY (added_by) REFERENCES public."user"(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--
