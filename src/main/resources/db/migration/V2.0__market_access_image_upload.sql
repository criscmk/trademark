alter table commodity
    add image_url varchar(250);

alter table commodity_sub_category
    add image_url varchar(250);

alter table commodity_category
    add image_url varchar(250);

alter table events drop column full_names;
