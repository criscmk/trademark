CREATE TABLE IF NOT EXISTS inventories
(
    id             int              NOT NULL,
    created_on     timestamp default current_timestamp,
    updated_on     timestamp default current_timestamp,
    identifier     varchar(255)     NOT NULL,
    name           varchar(50)      NOT NULL,
    quantity       int              NOT NULL,
    price_per_unit double precision NOT NULL,
    date_added     timestamp        NOT NULL,
    constraint inventories_pk
        primary key (id)
);

create unique index inventories_identifier_uindex
    on inventories (identifier);