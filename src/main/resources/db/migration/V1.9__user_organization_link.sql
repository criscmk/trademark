alter table user_organization_detail
    add user_id bigint;

create unique index user_organization_detail_user_id_uindex
    on user_organization_detail (user_id);

alter table user_organization_detail
    add constraint user_organization_detail_user__fk
        foreign key (user_id) references "user"(id);

