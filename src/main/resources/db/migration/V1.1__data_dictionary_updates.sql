-- ----------------------------
-- Table commodity_category: Set default value for is_active and date_added columns
-- ----------------------------
alter table commodity_category alter column is_active set default true;

alter table commodity_category alter column date_added set default now();


-----------------------------------------------------------------
-- Table commodity_sub_category: Set default value for is_active and date_added columns
-----------------------------------------------------------------
alter table commodity_sub_category alter column is_active set default true;

alter table commodity_sub_category alter column date_added set default now();


-----------------------------------------------------------------
-- Table commodity : Set default value for is_active and date_added columns
-----------------------------------------------------------------

alter table commodity alter column is_active set default true;

alter table commodity alter column date_added set default now();


-----------------------------------------------------------------
-- Table measurement_metric : Set default value for is_active and date_added columns
-----------------------------------------------------------------
alter table measurement_metric alter column date_added set default now();

alter table measurement_metric alter column is_active set default true;


-----------------------------------------------------------------
-- Table measurement_unit : Set default value for is_active and date_added columns
-----------------------------------------------------------------

alter table measurement_unit alter column date_added set default now();

alter table measurement_unit alter column is_active set default true;

-----------------------------------------------------------------
-- Table location_level : Add is_active, added_by and date_added columns
-----------------------------------------------------------------

alter table location_level add is_active boolean default true;

alter table location_level add added_by bigint;

alter table location_level add date_added timestamp(6) default now();


-----------------------------------------------------------------
-- Table currency : Set default value of is_active column
-----------------------------------------------------------------
alter table currency alter column is_active set default true;


-----------------------------------------------------------------
-- Table delivery_option : Set default values and drop column
-- commodity_sub_category_id
-----------------------------------------------------------------
alter table delivery_option alter column is_active set default true;

alter table delivery_option alter column date_added set default now();

alter table delivery_option drop constraint delivery_option_commodity_sub_category_id_fkey;

alter table delivery_option drop column commodity_sub_category_id;


-----------------------------------------------------------------
-- Table payment_option : Set default value of is_active and date_added columns
-----------------------------------------------------------------

alter table payment_option alter column date_added set default now();

alter table payment_option alter column is_active set default false;


-----------------------------------------------------------------
-- Table tax : Set default value of is_active and date_added columns
-----------------------------------------------------------------

alter table tax alter column date_added set default now();

alter table tax alter column is_active set default true;


-----------------------------------------------------------------
-- Table location : Add added_by and date_added columns
---                 Add foreign key constraint
-----------------------------------------------------------------

alter table location add added_by bigint;

alter table location add date_added timestamp(6) default now();

alter table location
    add constraint location_user_id_fk
        foreign key (added_by) references "user"
            on update restrict on delete restrict;

-----------------------------------------------------------------
-- Table location : Add is_active column and set  default value
-----------------------------------------------------------------
alter table location add is_active boolean default true;