package com.tmea.wit.services.storage

/**
 * These policies are set according to the Policy language as specified in AWS S3
 * https://docs.aws.amazon.com/AmazonS3/latest/dev/access-policy-language-overview.html
 */
object Policies {
    /**
     * This grants READ-ONLY permission to an anonymous user
     * This permission allows anyone to read the object data under bucket
     * @param bucket [String]
     * @return [String]
     */
    fun readPolicyJson(bucket: String) : String {
        return "{\n" +
                "  \"Version\": \"2012-10-17\",\n" +
                "  \"Statement\": [\n" +
                "    {\n" +
                "      \"Action\": [\n" +
                "        \"s3:GetObject\"\n" +
                "      ],\n" +
                "      \"Effect\": \"Allow\",\n" +
                "      \"Principal\": {\n" +
                "        \"AWS\": [\n" +
                "          \"*\"\n" +
                "        ]\n" +
                "      },\n" +
                "      \"Resource\": [\n" +
                "        \"arn:aws:s3:::$bucket/*\"\n" +
                "      ],\n" +
                "      \"Sid\": \"\"\n" +
                "    }\n" +
                "  ]\n" +
                "}"
    }
    const val writePolicyJson = ""
    const val readAndWritePolicyJson = ""
}