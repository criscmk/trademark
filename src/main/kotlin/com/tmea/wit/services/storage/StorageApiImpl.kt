package com.tmea.wit.services.storage

import com.tmea.wit.core.storage.PolicyTypes
import com.tmea.wit.core.storage.StorageApi
import com.tmea.wit.core.storage.getDocumentData
import io.minio.BucketExistsArgs
import io.minio.MakeBucketArgs
import io.minio.MinioClient
import io.minio.PutObjectArgs
import io.minio.SetBucketPolicyArgs
import io.minio.errors.MinioException
import org.slf4j.LoggerFactory
import java.io.ByteArrayInputStream
import java.time.LocalDate

class StorageApiImpl(
        private val objectStorageUrl: String,
        private val accessKey: String,
        private val secretKey: String
) : StorageApi {

    private val logger = LoggerFactory.getLogger(this::class.java)

    private val minioClient by lazy {
        MinioClient.builder().endpoint(objectStorageUrl).credentials(accessKey, secretKey).build()
    }

    /**
     * Checks if a bucket exists
     */
    private fun bucketExists(bucket: String): Boolean {
        return minioClient.bucketExists(BucketExistsArgs.builder().bucket(bucket).build())
    }

    /**
     * Create given bucket if available
     */
    private fun createBucket(bucket: String) {
        minioClient.makeBucket(MakeBucketArgs.builder().bucket(bucket).build());
    }

    override fun saveObjectData(bucket: String, documentName: String, objectData: String, policyType: PolicyTypes?): String? {
        return try {
            val doesBucketExist = bucketExists(bucket)
            if (doesBucketExist) {
                // bucket exists
                logger.warn("Bucket $bucket already exists")
            } else {
                createBucket(bucket)
            }
            val document = getDocumentData(objectData)

            // get the file extension
            val mimeTypeParts = document.mimeType.split("/")
            val fileExtension = mimeTypeParts[1]

            val bais = ByteArrayInputStream(document.documentData)
            val objectName = "$documentName.$fileExtension"

            minioClient.putObject(
                    PutObjectArgs.builder()
                            .stream(bais, bais.available().toLong(), -1)
                            .contentType(document.mimeType)
                            .bucket(bucket)
                            .`object`(objectName)
                            .build()
            )

            // only set a policy to the bucket if there is a policy set on call site
            if (policyType != null) {
                minioClient.setBucketPolicy(
                        SetBucketPolicyArgs
                                .builder()
                                .bucket(bucket)
                                .config(when(policyType) {
                                    PolicyTypes.READ_ONLY -> Policies.readPolicyJson(bucket)
                                    PolicyTypes.WRITE_ONLY -> Policies.writePolicyJson
                                    PolicyTypes.READ_AND_WRITE -> Policies.readAndWritePolicyJson
                                })
                                .build()
                )
            }

            minioClient.getObjectUrl(bucket, objectName)
        } catch (me: MinioException) {
            null
        }
    }
}