package com.tmea.wit.services

class ServiceException(message: String) : Exception(message)
