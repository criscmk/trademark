package com.tmea.wit.core.events.usecases

import com.tmea.wit.core.events.EventsDataStore
import com.tmea.wit.core.interactor.UseCase
import com.tmea.wit.core.EventIdentifier
import com.tmea.wit.core.events.usecases.exceptions.EventsException

class DeleteAnEventUseCase(private val eventsDataStore: EventsDataStore) :
    UseCase<EventIdentifier, Boolean>() {

    override fun execute(params: EventIdentifier?): Boolean {
        requireNotNull(params) { "Must pass in valid identifier" }
        return try {
            eventsDataStore.delete(params)
        } catch (e: EventsException) {
            false
        }
    }
}
