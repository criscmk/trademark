package com.tmea.wit.core.events.models

enum class TicketType {
    FULL_PASS,
    GROUP_PASS
}
