package com.tmea.wit.core.events.usecases.exceptions

class EventsException(message: String) : Exception(message)
