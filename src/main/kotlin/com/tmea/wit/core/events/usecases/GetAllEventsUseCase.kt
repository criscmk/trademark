package com.tmea.wit.core.events.usecases

import com.tmea.wit.core.events.EventsDataStore
import com.tmea.wit.core.interactor.UseCase
import com.tmea.wit.core.events.models.Event
import com.tmea.wit.core.events.usecases.exceptions.EventsException

class GetAllEventsUseCase(private val eventsDataStore: EventsDataStore) :
    UseCase<Unit, Set<Event>>() {

    override fun execute(params: Unit?): Set<Event> {
        return try {
            eventsDataStore.getAll()
        } catch (e: EventsException) {
            emptySet()
        }
    }
}
