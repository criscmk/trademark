package com.tmea.wit.core.events.usecases

import com.tmea.wit.core.EventIdentifier
import com.tmea.wit.core.events.EventsDataStore
import com.tmea.wit.core.events.models.Ticket
import com.tmea.wit.core.events.usecases.exceptions.EventsException
import com.tmea.wit.core.interactor.UseCase
import com.tmea.wit.core.storage.PolicyTypes
import com.tmea.wit.core.storage.StorageApi
import java.time.LocalDateTime
import java.time.LocalTime

class CreateEventUseCase(private val eventsDataStore: EventsDataStore, private val storageApi: StorageApi) :
        UseCase<CreateEventUseCase.Params, EventIdentifier?>() {

    override fun execute(params: Params?): EventIdentifier? {
        requireNotNull(params) { "Must pass in an event item" }

        val bannerImageUrl = storageApi.saveObjectData("events", "${params.title}-${params.organization}-banner-image", params.bannerImage, PolicyTypes.READ_ONLY)
                ?: throw EventsException("failed to save banner image")
        val imageUrl = storageApi.saveObjectData("events", "${params.title}-${params.organization}-image", params.image, PolicyTypes.READ_ONLY)
                ?: throw EventsException("Failed to save image")

        return try {
            eventsDataStore.create(
                    description = params.description,
                    organization = params.organization,
                    type = params.type,
                    date = params.date,
                    startTime = params.startTime,
                    endTime = params.endTime,
                    title = params.title,
                    location = params.location,
                    bannerImageUrl = bannerImageUrl,
                    imageUrl = imageUrl,
                    tickets = params.tickets
            )
        } catch (e: EventsException) {
            null
        }
    }

    data class Params(
            val organization: String,
            val type: String,
            val date: LocalDateTime,
            val startTime: LocalTime,
            val endTime: LocalTime,
            val title: String,
            val description: String,
            val location: String,
            val bannerImage: String,
            val image: String,
            val tickets: Collection<Ticket>
    )
}
