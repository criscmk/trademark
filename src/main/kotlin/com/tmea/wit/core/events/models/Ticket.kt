package com.tmea.wit.core.events.models

data class Ticket(
    val identifier: String? = null,
    val type: TicketType,
    val description: String,
    val price: String
)
