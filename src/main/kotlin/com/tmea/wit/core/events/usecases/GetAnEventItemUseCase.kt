package com.tmea.wit.core.events.usecases

import com.tmea.wit.core.events.EventsDataStore
import com.tmea.wit.core.events.usecases.exceptions.EventsException
import com.tmea.wit.core.interactor.UseCase
import com.tmea.wit.core.events.models.Event
import com.tmea.wit.core.EventIdentifier

class GetAnEventItemUseCase(private val eventsDataStore: EventsDataStore) :
    UseCase<EventIdentifier, Event?>() {
    override fun execute(params: EventIdentifier?): Event? {
        requireNotNull(params) { "Must have an identifier to get an event item" }
        return try {
            eventsDataStore.getItem(params)
        } catch (e: EventsException) {
            null
        }
    }
}
