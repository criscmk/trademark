package com.tmea.wit.core

typealias EventIdentifier = String
typealias ExpenseId = String
typealias ExpenseIdentifier = String
typealias InventoryIdentifier = String
typealias SaleIdentifier = String
typealias SaleId = String
