package com.tmea.wit.core.storage

import java.util.Base64



fun getDocumentData(documentObject: String): Document {

    val base64Parts = documentObject.split(":", ";", ",")
    val mimeType = base64Parts[1]
    val imageString = base64Parts[base64Parts.size - 1]
    val imageByte = Base64.getDecoder().decode(imageString)
    return Document(mimeType, imageByte)
}
