package com.tmea.wit.core.storage

enum class PolicyTypes {
    READ_ONLY,
    WRITE_ONLY,
    READ_AND_WRITE
}
