package com.tmea.wit.core.storage

/**
 * Representation of a document
 */
data class Document(
    val mimeType: String,
    val documentData: ByteArray = byteArrayOf()
//    val versionId: String,
)