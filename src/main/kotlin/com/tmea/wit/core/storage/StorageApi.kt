package com.tmea.wit.core.storage

/**
 * Storage API that handles storage of objects. Objects in this case could be files, images documents, etc
 */
interface StorageApi {

    /**
     * Uploads given objects to a given bucket
     * @param bucket [String] A bucket is a container of objects
     * @param objectData [String] object data to save in the bucket
     */
    fun saveObjectData(bucket: String, documentName: String, objectData: String, policyType: PolicyTypes? = null): String?
}