package com.tmea.wit.core.bookkeeping.inventory.usecases

import com.tmea.wit.core.InventoryIdentifier
import com.tmea.wit.core.bookkeeping.inventory.InventoryDataStore
import com.tmea.wit.core.bookkeeping.inventory.exceptions.InventoryException
import com.tmea.wit.core.interactor.UseCase


class DeleteAnInventoryUseCase(private val inventoryDataStore: InventoryDataStore) :
    UseCase<InventoryIdentifier, Boolean>() {

    override fun execute(params: InventoryIdentifier?): Boolean {
        requireNotNull(params) { "Must pass in valid inventory identifier" }
        return try {
            inventoryDataStore.delete(params)
        } catch (e: InventoryException) {
            false
        }
    }
}
