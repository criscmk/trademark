package com.tmea.wit.core.bookkeeping.sales.usecases

import com.tmea.wit.core.SaleIdentifier
import com.tmea.wit.core.bookkeeping.sales.SalesDataStore
import com.tmea.wit.core.bookkeeping.sales.exceptions.SalesException
import com.tmea.wit.core.interactor.UseCase

class DeleteASaleUseCase(private val salesDataStore: SalesDataStore) :
    UseCase<SaleIdentifier, Boolean>() {

    override fun execute(params: SaleIdentifier?): Boolean {
        requireNotNull(params) { "Must pass in valid sale identifier" }
        return try {
            salesDataStore.delete(params)
        } catch (e: SalesException) {
            false
        }
    }
}
