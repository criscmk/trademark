package com.tmea.wit.core.bookkeeping.inventory.models

import java.time.LocalDateTime

data class Inventory(
    val identifier: String,
    val name: String,
    val quantity: Int,
    val pricePerUnit: Double,
    val dateAdded: LocalDateTime
)
