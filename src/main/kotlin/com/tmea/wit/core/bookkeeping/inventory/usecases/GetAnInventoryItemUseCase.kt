package com.tmea.wit.core.bookkeeping.inventory.usecases

import com.tmea.wit.core.InventoryIdentifier
import com.tmea.wit.core.bookkeeping.inventory.InventoryDataStore
import com.tmea.wit.core.bookkeeping.inventory.exceptions.InventoryException
import com.tmea.wit.core.bookkeeping.inventory.models.Inventory
import com.tmea.wit.core.interactor.UseCase

class GetAnInventoryItemUseCase(private val inventoryDataStore: InventoryDataStore) :
    UseCase<InventoryIdentifier, Inventory?>() {
    override fun execute(params: InventoryIdentifier?): Inventory? {
        requireNotNull(params) { "Must have an identifier to get an Inventory item" }
        return try {
            inventoryDataStore.getAnInventoryItem(params)
        } catch (e: InventoryException) {
            null
        }
    }
}
