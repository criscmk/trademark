package com.tmea.wit.core.bookkeeping.sales

import com.tmea.wit.core.SaleIdentifier
import com.tmea.wit.core.bookkeeping.sales.exceptions.SalesException
import com.tmea.wit.core.bookkeeping.sales.models.Sale
import java.time.LocalDateTime

interface SalesDataStore {

    @Throws(SalesException::class)
    fun getAll(): Set<Sale>

    @Throws(SalesException::class)
    fun getItem(identifier: SaleIdentifier): Sale?

    @Suppress("LongParameterList")
    @Throws(SalesException::class)
    fun create(
        saleId: String,
        item: String,
        quantity: Int,
        saleUnitPrice: Double,
        saleAmount: Double,
        saleDate: LocalDateTime
    ): String

    @Throws(SalesException::class)
    fun update(
        identifier: SaleIdentifier,
        item: String,
        quantity: Int,
        saleUnitPrice: Double,
        saleDate: LocalDateTime
    ): Boolean

    @Throws(SalesException::class)
    fun delete(identifier: SaleIdentifier): Boolean
}
