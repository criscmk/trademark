package com.tmea.wit.core.bookkeeping.inventory

import com.tmea.wit.core.InventoryIdentifier
import com.tmea.wit.core.bookkeeping.inventory.exceptions.InventoryException
import com.tmea.wit.core.bookkeeping.inventory.models.Inventory

interface InventoryDataStore {

    @Throws(InventoryException::class)
    fun getAll(): Set<Inventory>

    @Throws(InventoryException::class)
    fun getAnInventoryItem(identifier: InventoryIdentifier): Inventory?

    @Throws(InventoryException::class)
    fun create(name: String, quantity: Int, pricePerUnit: Double): String

    @Throws(InventoryException::class)
    fun update(identifier: InventoryIdentifier, name: String, quantity: Int, pricePerUnit: Double): Boolean

    @Throws(InventoryException::class)
    fun delete(identifier: InventoryIdentifier): Boolean
}
