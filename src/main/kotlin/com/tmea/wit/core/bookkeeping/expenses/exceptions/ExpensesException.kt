package com.tmea.wit.core.bookkeeping.expenses.exceptions

class ExpensesException(message: String) : Exception(message)
