package com.tmea.wit.core.bookkeeping.sales.usecases

import com.tmea.wit.core.SaleId
import com.tmea.wit.core.SaleIdentifier
import com.tmea.wit.core.bookkeeping.sales.SalesDataStore
import com.tmea.wit.core.bookkeeping.sales.exceptions.SalesException
import com.tmea.wit.core.interactor.UseCase
import java.time.LocalDateTime

class UpdateSaleUseCase(private val salesDataStore: SalesDataStore) :
    UseCase<UpdateSaleUseCase.Params, Boolean>() {

    override fun execute(params: Params?): Boolean {
        requireNotNull(params) { "Must pass in valid params to update a sale item" }
        return try {
            salesDataStore.update(
                identifier = params.identifier,
                item = params.item,
                quantity = params.quantity,
                saleUnitPrice = params.saleUnitPrice,
//                saleId = params.saleId,
//                saleAmount = params.saleAmount,
                saleDate = params.saleDate
            )
        } catch (e: SalesException) {
            false
        }
    }

    data class Params(
        val identifier: SaleIdentifier,
        val item: String,
        val quantity: Int,
        val saleUnitPrice: Double,
        val saleId: SaleId,
        val saleAmount: Double,
        val saleDate: LocalDateTime
    )
}
