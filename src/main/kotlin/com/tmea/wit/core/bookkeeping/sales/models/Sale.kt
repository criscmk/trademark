package com.tmea.wit.core.bookkeeping.sales.models

import com.tmea.wit.core.SaleId
import com.tmea.wit.core.SaleIdentifier
import java.time.LocalDateTime

data class Sale(
    val saleId: SaleId,
    val identifier: SaleIdentifier,
    val item: String,
    val quantity: Int,
    val saleUnitPrice: Double,
    val saleAmount: Double,
    val saleDate: LocalDateTime
)
