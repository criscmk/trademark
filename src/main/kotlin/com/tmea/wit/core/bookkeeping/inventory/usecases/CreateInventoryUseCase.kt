package com.tmea.wit.core.bookkeeping.inventory.usecases

import com.tmea.wit.core.InventoryIdentifier
import com.tmea.wit.core.bookkeeping.inventory.InventoryDataStore
import com.tmea.wit.core.bookkeeping.inventory.exceptions.InventoryException
import com.tmea.wit.core.interactor.UseCase

class CreateInventoryUseCase(private val inventoryDataStore: InventoryDataStore) :
    UseCase<CreateInventoryUseCase.Params, InventoryIdentifier?>() {

    override fun execute(params: Params?): InventoryIdentifier? {
        requireNotNull(params) { "Must pass in an inventory item" }
        return try {
            inventoryDataStore.create(
                name = params.name,
                quantity = params.quantity,
                pricePerUnit = params.pricePerUnit
            )
        } catch (e: InventoryException) {
            null
        }
    }

    data class Params(
        val name: String,
        val quantity: Int,
        val pricePerUnit: Double
    )
}
