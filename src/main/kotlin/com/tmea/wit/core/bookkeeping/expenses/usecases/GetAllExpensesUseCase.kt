package com.tmea.wit.core.bookkeeping.expenses.usecases

import com.tmea.wit.core.bookkeeping.expenses.ExpensesDataStore
import com.tmea.wit.core.bookkeeping.expenses.exceptions.ExpensesException
import com.tmea.wit.core.bookkeeping.expenses.models.Expense
import com.tmea.wit.core.interactor.UseCase

class GetAllExpensesUseCase(private val expensesDataStore: ExpensesDataStore) : UseCase<Unit, Set<Expense>>() {
    override fun execute(params: Unit?): Set<Expense> {
        return try {
            expensesDataStore.getAll()
        } catch (e: ExpensesException) {
            emptySet()
        }
    }
}
