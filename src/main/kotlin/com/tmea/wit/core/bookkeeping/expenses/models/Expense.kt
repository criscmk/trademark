package com.tmea.wit.core.bookkeeping.expenses.models

import com.tmea.wit.core.ExpenseId
import com.tmea.wit.core.ExpenseIdentifier
import java.time.LocalDateTime

data class Expense(
    val identifier: ExpenseIdentifier,
    val expenseId: ExpenseId,
    val category: String,
    val description: String,
    val amount: Double,
    val expenseDate: LocalDateTime
)
