package com.tmea.wit.core.bookkeeping.expenses.usecases

import com.tmea.wit.core.ExpenseId
import com.tmea.wit.core.ExpenseIdentifier
import com.tmea.wit.core.bookkeeping.expenses.ExpensesDataStore
import com.tmea.wit.core.bookkeeping.expenses.exceptions.ExpensesException
import com.tmea.wit.core.interactor.UseCase
import java.time.LocalDateTime

class UpdateExpenseUseCase(private val expensesDataStore: ExpensesDataStore) :
    UseCase<UpdateExpenseUseCase.Params, Boolean>() {

    override fun execute(params: Params?): Boolean {
        requireNotNull(params) { "Must pass in valid params to update an expense item" }
        return try {
            expensesDataStore.update(
                identifier = params.identifier,
                expenseId = params.expenseId,
                category = params.category,
                description = params.description,
                amount = params.amount,
                expenseDate = params.expenseDate
            )
        } catch (e: ExpensesException) {
            false
        }
    }

    data class Params(
        val identifier: ExpenseIdentifier,
        val expenseId: ExpenseId,
        val category: String,
        val description: String,
        val amount: Double,
        val expenseDate: LocalDateTime
    )
}
