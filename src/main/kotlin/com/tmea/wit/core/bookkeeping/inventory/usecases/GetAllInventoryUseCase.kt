package com.tmea.wit.core.bookkeeping.inventory.usecases

import com.tmea.wit.core.bookkeeping.inventory.InventoryDataStore
import com.tmea.wit.core.bookkeeping.inventory.exceptions.InventoryException
import com.tmea.wit.core.bookkeeping.inventory.models.Inventory
import com.tmea.wit.core.interactor.UseCase


class GetAllInventoryUseCase(private val inventoryDataStore: InventoryDataStore) : UseCase<Unit, Set<Inventory>>() {
    override fun execute(params: Unit?): Set<Inventory> {
        return try {
            inventoryDataStore.getAll()
        } catch (e: InventoryException) {
            emptySet()
        }
    }
}
