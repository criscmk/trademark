package com.tmea.wit.core.bookkeeping.inventory.usecases

import com.tmea.wit.core.InventoryIdentifier
import com.tmea.wit.core.bookkeeping.inventory.InventoryDataStore
import com.tmea.wit.core.bookkeeping.inventory.exceptions.InventoryException
import com.tmea.wit.core.interactor.UseCase


class UpdateInventoryUseCase(private val inventoryDataStore: InventoryDataStore) :
    UseCase<UpdateInventoryUseCase.Params, Boolean>() {

    override fun execute(params: Params?): Boolean {
        requireNotNull(params) { "Must pass in valid params to update an inventory item" }
        return try {
            inventoryDataStore.update(
                identifier = params.identifier,
                name = params.name,
                quantity = params.quantity,
                pricePerUnit = params.pricePerUnit
            )
        } catch (e: InventoryException) {
            false
        }
    }

    data class Params(
        val identifier: InventoryIdentifier,
        val name: String,
        val quantity: Int,
        val pricePerUnit: Double
    )
}
