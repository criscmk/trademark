package com.tmea.wit.core.bookkeeping.expenses.usecases

import com.tmea.wit.core.ExpenseId
import com.tmea.wit.core.ExpenseIdentifier
import com.tmea.wit.core.bookkeeping.expenses.ExpensesDataStore
import com.tmea.wit.core.bookkeeping.expenses.exceptions.ExpensesException
import com.tmea.wit.core.interactor.UseCase
import java.time.LocalDateTime

class CreateExpenseUseCase(private val expensesDataStore: ExpensesDataStore) :
    UseCase<CreateExpenseUseCase.Params, ExpenseIdentifier?>() {

    override fun execute(params: Params?): ExpenseIdentifier? {
        requireNotNull(params) { "Must pass in an expense item" }
        return try {
            expensesDataStore.create(
                expenseId = params.expenseId,
                category = params.category,
                description = params.description,
                amount = params.amount,
                expenseDate = params.expenseDate
            )
        } catch (e: ExpensesException) {
            null
        }
    }

    data class Params(
        val expenseId: ExpenseId,
        val category: String,
        val description: String,
        val amount: Double,
        val expenseDate: LocalDateTime
    )
}
