package com.tmea.wit.core.bookkeeping.sales.usecases

import com.tmea.wit.core.InventoryIdentifier
import com.tmea.wit.core.SaleIdentifier
import com.tmea.wit.core.bookkeeping.sales.SalesDataStore
import com.tmea.wit.core.bookkeeping.sales.exceptions.SalesException
import com.tmea.wit.core.bookkeeping.sales.models.Sale
import com.tmea.wit.core.interactor.UseCase

class GetASaleItemUseCase(private val salesDataStore: SalesDataStore) :
    UseCase<SaleIdentifier, Sale?>() {
    override fun execute(params: InventoryIdentifier?): Sale? {
        requireNotNull(params) { "Must have an identifier to get a sale item" }
        return try {
            salesDataStore.getItem(params)
        } catch (e: SalesException) {
            null
        }
    }
}
