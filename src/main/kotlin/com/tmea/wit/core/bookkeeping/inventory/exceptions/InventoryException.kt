package com.tmea.wit.core.bookkeeping.inventory.exceptions

class InventoryException(message: String) : Exception(message)
