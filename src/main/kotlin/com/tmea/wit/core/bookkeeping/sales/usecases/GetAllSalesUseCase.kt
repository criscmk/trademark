package com.tmea.wit.core.bookkeeping.sales.usecases

import com.tmea.wit.core.bookkeeping.sales.SalesDataStore
import com.tmea.wit.core.bookkeeping.sales.exceptions.SalesException
import com.tmea.wit.core.bookkeeping.sales.models.Sale
import com.tmea.wit.core.interactor.UseCase

class GetAllSalesUseCase(private val salesDataStore: SalesDataStore) : UseCase<Unit, Set<Sale>>() {
    override fun execute(params: Unit?): Set<Sale> {
        return try {
            salesDataStore.getAll()
        } catch (e: SalesException) {
            emptySet()
        }
    }
}
