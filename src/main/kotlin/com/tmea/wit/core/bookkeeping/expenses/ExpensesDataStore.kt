package com.tmea.wit.core.bookkeeping.expenses


import com.tmea.wit.core.ExpenseId
import com.tmea.wit.core.ExpenseIdentifier
import com.tmea.wit.core.bookkeeping.expenses.exceptions.ExpensesException
import com.tmea.wit.core.bookkeeping.expenses.models.Expense
import java.time.LocalDateTime

interface ExpensesDataStore {

    @Throws(ExpensesException::class)
    fun getAll(): Set<Expense>

    @Throws(ExpensesException::class)
    fun getItem(identifier: ExpenseIdentifier): Expense?

    @Suppress("LongParameterList")
    @Throws(ExpensesException::class)
    fun create(
        expenseId: ExpenseId,
        category: String,
        description: String,
        amount: Double,
        expenseDate: LocalDateTime
    ): String

    @Suppress("LongParameterList")
    @Throws(ExpensesException::class)
    fun update(
        identifier: ExpenseIdentifier,
        expenseId: ExpenseId,
        category: String,
        description: String,
        amount: Double,
        expenseDate: LocalDateTime
    ): Boolean

    @Throws(ExpensesException::class)
    fun delete(identifier: ExpenseIdentifier): Boolean
}
