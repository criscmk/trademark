package com.tmea.wit.core.bookkeeping.sales.usecases

import com.tmea.wit.core.SaleId
import com.tmea.wit.core.SaleIdentifier
import com.tmea.wit.core.bookkeeping.sales.SalesDataStore
import com.tmea.wit.core.bookkeeping.sales.exceptions.SalesException
import com.tmea.wit.core.interactor.UseCase
import java.time.LocalDateTime


class CreateSaleUseCase(private val salesDataStore: SalesDataStore) :
    UseCase<CreateSaleUseCase.Params, SaleIdentifier?>() {

    override fun execute(params: Params?): SaleIdentifier? {
        requireNotNull(params) { "Must pass in a sale item" }
        return try {
            salesDataStore.create(
                item = params.item,
                quantity = params.quantity,
                saleUnitPrice = params.saleUnitPrice,
                saleId = params.saleId,
                saleAmount = params.saleAmount,
                saleDate = params.saleDate
            )
        } catch (e: SalesException) {
            null
        }
    }

    data class Params(
        val item: String,
        val quantity: Int,
        val saleUnitPrice: Double,
        val saleId: SaleId,
        val saleAmount: Double,
        val saleDate: LocalDateTime
    )
}
