package com.tmea.wit.core.bookkeeping.sales.exceptions

class SalesException(message: String) : Exception(message)
