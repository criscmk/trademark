package com.tmea.wit.core.bookkeeping.expenses.usecases

import com.tmea.wit.core.ExpenseIdentifier
import com.tmea.wit.core.bookkeeping.expenses.ExpensesDataStore
import com.tmea.wit.core.bookkeeping.expenses.exceptions.ExpensesException
import com.tmea.wit.core.interactor.UseCase

class DeleteAnExpenseUseCase(private val expensesDataStore: ExpensesDataStore) :
    UseCase<ExpenseIdentifier, Boolean>() {

    override fun execute(params: ExpenseIdentifier?): Boolean {
        requireNotNull(params) { "Must pass in valid identifier" }
        return try {
            expensesDataStore.delete(params)
        } catch (e: ExpensesException) {
            false
        }
    }
}
