package com.tmea.wit.core.bookkeeping.expenses.usecases

import com.tmea.wit.core.ExpenseIdentifier
import com.tmea.wit.core.bookkeeping.expenses.ExpensesDataStore
import com.tmea.wit.core.bookkeeping.expenses.exceptions.ExpensesException
import com.tmea.wit.core.bookkeeping.expenses.models.Expense
import com.tmea.wit.core.interactor.UseCase

class GetAnExpenseItemUseCase(private val expensesDataStore: ExpensesDataStore) :
    UseCase<ExpenseIdentifier, Expense?>() {
    override fun execute(params: ExpenseIdentifier?): Expense? {
        requireNotNull(params) { "Must have an identifier to get an expense item" }
        return try {
            expensesDataStore.getItem(params)
        } catch (e: ExpensesException) {
            null
        }
    }
}
