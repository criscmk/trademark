package com.tmea.wit.config.core.bookkeeping

import com.tmea.wit.core.bookkeeping.sales.SalesDataStore
import com.tmea.wit.core.bookkeeping.sales.usecases.CreateSaleUseCase
import com.tmea.wit.core.bookkeeping.sales.usecases.DeleteASaleUseCase
import com.tmea.wit.core.bookkeeping.sales.usecases.GetASaleItemUseCase
import com.tmea.wit.core.bookkeeping.sales.usecases.GetAllSalesUseCase
import com.tmea.wit.core.bookkeeping.sales.usecases.UpdateSaleUseCase
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class SalesUseCasesConfig {

    @Bean
    fun createCreateSalesUseCase(
            salesDataStore: SalesDataStore
    ): CreateSaleUseCase {
        return CreateSaleUseCase(salesDataStore)
    }

    @Bean
    fun createGetAllSalesUseCase(
            salesDataStore: SalesDataStore
    ): GetAllSalesUseCase {
        return GetAllSalesUseCase(salesDataStore)
    }

    @Bean
    fun createGetAnSalesItemUseCase(
            salesDataStore: SalesDataStore
    ): GetASaleItemUseCase {
        return GetASaleItemUseCase(salesDataStore)
    }

    @Bean
    fun createUpdateSalesUseCase(
            salesDataStore: SalesDataStore
    ): UpdateSaleUseCase {
        return UpdateSaleUseCase(salesDataStore)
    }

    @Bean
    fun createDeleteAnSalesItemUseCase(
            salesDataStore: SalesDataStore
    ): DeleteASaleUseCase {
        return DeleteASaleUseCase(salesDataStore)
    }
}
