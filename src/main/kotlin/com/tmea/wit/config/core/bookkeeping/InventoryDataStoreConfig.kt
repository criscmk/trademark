package com.tmea.wit.config.core.bookkeeping

import com.tmea.wit.core.bookkeeping.inventory.InventoryDataStore
import com.tmea.wit.database.bookkeeping.inventory.InventoryDataStoreImpl
import com.tmea.wit.database.bookkeeping.inventory.InventoryRepository
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class InventoryDataStoreConfig {

    @Bean
    fun createInventoryDataStore(
        inventoryRepository: InventoryRepository
    ): InventoryDataStore {
        return InventoryDataStoreImpl(
            inventoryRepository
        )
    }
}
