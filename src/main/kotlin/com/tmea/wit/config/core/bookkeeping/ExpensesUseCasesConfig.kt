package com.tmea.wit.config.core.bookkeeping

import com.tmea.wit.core.bookkeeping.expenses.ExpensesDataStore
import com.tmea.wit.core.bookkeeping.expenses.usecases.CreateExpenseUseCase
import com.tmea.wit.core.bookkeeping.expenses.usecases.DeleteAnExpenseUseCase
import com.tmea.wit.core.bookkeeping.expenses.usecases.GetAllExpensesUseCase
import com.tmea.wit.core.bookkeeping.expenses.usecases.GetAnExpenseItemUseCase
import com.tmea.wit.core.bookkeeping.expenses.usecases.UpdateExpenseUseCase
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class ExpensesUseCasesConfig {

    @Bean
    fun createCreateExpensesUseCase(
        expensesDataStore: ExpensesDataStore
    ): CreateExpenseUseCase {
        return CreateExpenseUseCase(expensesDataStore)
    }

    @Bean
    fun createGetAllExpensesUseCase(
        expensesDataStore: ExpensesDataStore
    ): GetAllExpensesUseCase {
        return GetAllExpensesUseCase(expensesDataStore)
    }

    @Bean
    fun createGetAnExpenseItemUseCase(
        expensesDataStore: ExpensesDataStore
    ): GetAnExpenseItemUseCase {
        return GetAnExpenseItemUseCase(expensesDataStore)
    }

    @Bean
    fun createUpdateExpensesUseCase(
        expensesDataStore: ExpensesDataStore
    ): UpdateExpenseUseCase {
        return UpdateExpenseUseCase(expensesDataStore)
    }

    @Bean
    fun createDeleteAnExpenseItemUseCase(
        expensesDataStore: ExpensesDataStore
    ): DeleteAnExpenseUseCase {
        return DeleteAnExpenseUseCase(expensesDataStore)
    }
}
