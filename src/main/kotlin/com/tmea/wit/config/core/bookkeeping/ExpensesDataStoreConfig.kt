package com.tmea.wit.config.core.bookkeeping

import com.tmea.wit.core.bookkeeping.expenses.ExpensesDataStore
import com.tmea.wit.database.bookkeeping.expenses.ExpensesDataStoreImpl
import com.tmea.wit.database.bookkeeping.expenses.ExpensesRepository
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class ExpensesDataStoreConfig {

    @Bean
    fun createExpensesDataStore(
        expensesRepository: ExpensesRepository
    ): ExpensesDataStore {
        return ExpensesDataStoreImpl(
            expensesRepository
        )
    }
}
