package com.tmea.wit.config.core.events

import com.tmea.wit.core.events.EventsDataStore
import com.tmea.wit.database.events.EventsDataStoreImpl
import com.tmea.wit.database.events.EventsRepository
import com.tmea.wit.database.events.TicketsRepository
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class EventDataStoreConfig {

    @Bean
    fun createEventDataStore(
        eventsRepository: EventsRepository,
        ticketsRepository: TicketsRepository
    ): EventsDataStore {
        return EventsDataStoreImpl(
            eventsRepository,
            ticketsRepository
        )
    }
}
