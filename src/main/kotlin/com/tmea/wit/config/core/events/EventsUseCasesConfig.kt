package com.tmea.wit.config.core.events

import com.tmea.wit.core.events.EventsDataStore
import com.tmea.wit.core.events.usecases.CreateEventUseCase
import com.tmea.wit.core.events.usecases.DeleteAnEventUseCase
import com.tmea.wit.core.events.usecases.GetAllEventsUseCase
import com.tmea.wit.core.events.usecases.GetAnEventItemUseCase
import com.tmea.wit.core.events.usecases.UpdateEventUseCase
import com.tmea.wit.core.storage.StorageApi
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class EventsUseCasesConfig {

    @Bean
    fun createGetAllEventsUseCase(
        eventsDataStore: EventsDataStore
    ): GetAllEventsUseCase {
        return GetAllEventsUseCase(
            eventsDataStore
        )
    }

    @Bean
    fun createGetAnEventItemUseCase(
        eventsDataStore: EventsDataStore
    ): GetAnEventItemUseCase {
        return GetAnEventItemUseCase(
            eventsDataStore
        )
    }

    @Bean
    fun createCreateAnEventUseCase(
        eventsDataStore: EventsDataStore,
        storageApi: StorageApi
    ): CreateEventUseCase {
        return CreateEventUseCase(
            eventsDataStore,
            storageApi
        )
    }

    @Bean
    fun createUpdateAnEventUseCase(
        eventsDataStore: EventsDataStore
    ): UpdateEventUseCase {
        return UpdateEventUseCase(
            eventsDataStore
        )
    }

    @Bean
    fun createDeleteAnEventUseCase(
        eventsDataStore: EventsDataStore
    ): DeleteAnEventUseCase {
        return DeleteAnEventUseCase(
            eventsDataStore
        )
    }
}
