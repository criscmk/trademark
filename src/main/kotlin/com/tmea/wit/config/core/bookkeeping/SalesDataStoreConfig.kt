package com.tmea.wit.config.core.bookkeeping

import com.tmea.wit.core.bookkeeping.sales.SalesDataStore
import com.tmea.wit.database.bookkeeping.sales.SalesDataStoreImpl
import com.tmea.wit.database.bookkeeping.sales.SalesRepository
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class SalesDataStoreConfig {

    @Bean
    fun createSalesDataStore(
        salesRepository: SalesRepository
    ): SalesDataStore {
        return SalesDataStoreImpl(
            salesRepository
        )
    }
}
