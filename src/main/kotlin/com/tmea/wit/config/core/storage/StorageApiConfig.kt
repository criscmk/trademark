package com.tmea.wit.config.core.storage

import com.tmea.wit.core.storage.StorageApi
import com.tmea.wit.services.storage.StorageApiImpl
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class StorageApiConfig(
        @Value("\${services.storage.endpoint}")
        private val objectStorageUrl: String,

        @Value("\${services.storage.accessKey}")
        private val accessKey: String,

        @Value("\${services.storage.secretKey}")
        private val secretKey: String
) {

    @Bean
    fun createStorageApi(): StorageApi {
        return StorageApiImpl(objectStorageUrl, accessKey, secretKey)
    }
}
