package com.tmea.wit.config.core.bookkeeping

import com.tmea.wit.core.bookkeeping.inventory.InventoryDataStore
import com.tmea.wit.core.bookkeeping.inventory.usecases.CreateInventoryUseCase
import com.tmea.wit.core.bookkeeping.inventory.usecases.DeleteAnInventoryUseCase
import com.tmea.wit.core.bookkeeping.inventory.usecases.GetAllInventoryUseCase
import com.tmea.wit.core.bookkeeping.inventory.usecases.GetAnInventoryItemUseCase
import com.tmea.wit.core.bookkeeping.inventory.usecases.UpdateInventoryUseCase
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class InventoryUseCasesConfig {

    @Bean
    fun createCreateInventoryUseCase(
        inventoryDataStore: InventoryDataStore
    ): CreateInventoryUseCase {
        return CreateInventoryUseCase(inventoryDataStore)
    }

    @Bean
    fun createGetAllInventoryUseCase(
        inventoryDataStore: InventoryDataStore
    ): GetAllInventoryUseCase {
        return GetAllInventoryUseCase(inventoryDataStore)
    }

    @Bean
    fun createGetAnInventoryItemUseCase(
        inventoryDataStore: InventoryDataStore
    ): GetAnInventoryItemUseCase {
        return GetAnInventoryItemUseCase(inventoryDataStore)
    }

    @Bean
    fun createUpdateInventoryUseCase(
        inventoryDataStore: InventoryDataStore
    ): UpdateInventoryUseCase {
        return UpdateInventoryUseCase(inventoryDataStore)
    }

    @Bean
    fun createDeleteAnInventoryItemUseCase(
        inventoryDataStore: InventoryDataStore
    ): DeleteAnInventoryUseCase {
        return DeleteAnInventoryUseCase(inventoryDataStore)
    }
}
