package com.tmea.wit.api.rest.bookkeeping.expenses.dto

import com.fasterxml.jackson.annotation.JsonProperty
import java.time.LocalDateTime

sealed class ExpenseDto(
    @JsonProperty("expense_id", required = true)
    open val expenseId: String,

    @JsonProperty("category", required = true)
    open val category: String,

    @JsonProperty("description", required = true)
    open val description: String,

    @JsonProperty("amount", required = true)
    open val amount: Double,

    @JsonProperty("expense_date", required = true)
    open val expenseDate: LocalDateTime
)

data class ExpenseResponseDto(
    @JsonProperty("identifier", required = true)
    val identifier: String,
    @JsonProperty("expense_id", required = true)
    override val expenseId: String,
    override val category: String,
    override val description: String,
    override val amount: Double,
    @JsonProperty("expense_date", required = true)
    override val expenseDate: LocalDateTime
) : ExpenseDto(expenseId, category, description, amount, expenseDate)

data class ExpenseRequestDto(
    @JsonProperty("expense_id", required = true)
    override val expenseId: String,
    override val category: String,
    override val description: String,
    override val amount: Double,
    @JsonProperty("expense_date", required = true)
    override val expenseDate: LocalDateTime
) : ExpenseDto(expenseId, category, description, amount, expenseDate)
