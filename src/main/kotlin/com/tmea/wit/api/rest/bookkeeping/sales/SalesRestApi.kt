package com.tmea.wit.api.rest.bookkeeping.sales

import com.tmea.wit.api.rest.bookkeeping.sales.dto.SaleRequestDto
import com.tmea.wit.api.rest.bookkeeping.sales.dto.SaleResponseDto
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PatchMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.util.Collections

@RestController
@RequestMapping(path = ["/api/bookkeeping/sales"])
class SalesRestApi(
    private val salesService: SalesService
) {
    @GetMapping(value = ["/"], produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getAll(): ResponseEntity<Collection<SaleResponseDto>> {
        val allInventories = salesService.getAll()
        return ResponseEntity(allInventories, HttpStatus.OK)
    }

    @GetMapping(value = ["/{identifier}"], produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getAnItem(@PathVariable identifier: String): ResponseEntity<SaleResponseDto> {
        val inventory = salesService.getItem(identifier)
        if (inventory != null)
            return ResponseEntity(inventory, HttpStatus.OK)
        return ResponseEntity(HttpStatus.NOT_FOUND)
    }

    @PostMapping(value = ["/"], produces = [MediaType.APPLICATION_JSON_VALUE])
    fun create(
        @Validated
        @RequestBody
        payload: SaleRequestDto
    ): ResponseEntity<Map<String, String>> {
        val result = salesService.create(payload)
        return if (result != null) {
            ResponseEntity(
                Collections.singletonMap("identifier", result),
                HttpStatus.CREATED
            )
        } else {
            ResponseEntity(
                Collections.singletonMap("Error", "failed to create sale item"),
                HttpStatus.NOT_ACCEPTABLE
            )
        }
    }

    @PatchMapping(value = ["/{identifier}"], produces = [MediaType.APPLICATION_JSON_VALUE])
    fun update(
        @PathVariable identifier: String,
        @Validated
        @RequestBody
        payload: SaleRequestDto
    ): ResponseEntity<Map<String, Boolean>> {
        val result = salesService.update(identifier, payload)
        return ResponseEntity(
            Collections.singletonMap("success", result),
            if (result) HttpStatus.ACCEPTED else HttpStatus.NOT_ACCEPTABLE
        )
    }

    @DeleteMapping(value = ["/{identifier}"], produces = [MediaType.APPLICATION_JSON_VALUE])
    fun delete(
        @PathVariable identifier: String
    ): ResponseEntity<Map<String, Boolean>> {
        val result = salesService.delete(identifier)
        return ResponseEntity(
            Collections.singletonMap("success", result),
            if (result) HttpStatus.ACCEPTED else HttpStatus.NOT_FOUND
        )
    }
}
