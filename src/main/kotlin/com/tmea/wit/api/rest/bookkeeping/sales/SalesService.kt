package com.tmea.wit.api.rest.bookkeeping.sales

import com.tmea.wit.api.rest.bookkeeping.sales.dto.SaleRequestDto
import com.tmea.wit.api.rest.bookkeeping.sales.dto.SaleResponseDto
import com.tmea.wit.core.SaleIdentifier
import com.tmea.wit.core.bookkeeping.sales.usecases.CreateSaleUseCase
import com.tmea.wit.core.bookkeeping.sales.usecases.DeleteASaleUseCase
import com.tmea.wit.core.bookkeeping.sales.usecases.GetASaleItemUseCase
import com.tmea.wit.core.bookkeeping.sales.usecases.GetAllSalesUseCase
import com.tmea.wit.core.bookkeeping.sales.usecases.UpdateSaleUseCase
import org.springframework.stereotype.Service

@Service
class SalesService(
        private val getAllSalesUseCase: GetAllSalesUseCase,
        private val createSaleUseCase: CreateSaleUseCase,
        private val updateSaleUseCase: UpdateSaleUseCase,
        private val getAnSaleItemUseCase: GetASaleItemUseCase,
        private val deleteASaleUseCase: DeleteASaleUseCase
) {
    fun getAll(): Collection<SaleResponseDto> {
        val inventories = getAllSalesUseCase.execute()
        return inventories.map {
            SaleResponseDto(
                identifier = it.identifier,
                saleId = it.saleId,
                item = it.item,
                saleUnitPrice = it.saleUnitPrice,
                quantity = it.quantity,
                saleDate = it.saleDate,
                saleAmount = it.saleAmount
            )
        }
    }

    fun getItem(identifier: SaleIdentifier): SaleResponseDto? {
        val result = getAnSaleItemUseCase.execute(identifier) ?: return null
        return SaleResponseDto(
            identifier = result.identifier,
            item = result.item,
            saleId = result.saleId,
            saleUnitPrice = result.saleUnitPrice,
            quantity = result.quantity,
            saleDate = result.saleDate,
            saleAmount = result.saleAmount
        )
    }

    fun create(payload: SaleRequestDto): SaleIdentifier? {
        return createSaleUseCase.execute(
            CreateSaleUseCase.Params(
                item = payload.item,
                quantity = payload.quantity,
                saleUnitPrice = payload.saleUnitPrice,
                saleAmount = payload.saleAmount,
                saleId = payload.saleId,
                saleDate = payload.saleDate
            )
        )
    }

    fun update(identifier: SaleIdentifier, payload: SaleRequestDto): Boolean {
        return updateSaleUseCase.execute(
            UpdateSaleUseCase.Params(
                identifier = identifier,
                item = payload.item,
                quantity = payload.quantity,
                saleUnitPrice = payload.saleUnitPrice,
                saleAmount = payload.saleAmount,
                saleDate = payload.saleDate,
                saleId = payload.saleId
            )
        )
    }

    fun delete(identifier: SaleIdentifier): Boolean {
        return deleteASaleUseCase.execute(identifier)
    }
}
