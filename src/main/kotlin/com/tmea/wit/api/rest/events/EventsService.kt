package com.tmea.wit.api.rest.events

import com.tmea.wit.core.EventIdentifier
import com.tmea.wit.core.events.models.Ticket
import com.tmea.wit.core.events.usecases.CreateEventUseCase
import com.tmea.wit.core.events.usecases.DeleteAnEventUseCase
import com.tmea.wit.core.events.usecases.GetAllEventsUseCase
import com.tmea.wit.core.events.usecases.GetAnEventItemUseCase
import com.tmea.wit.core.events.usecases.UpdateEventUseCase
import org.springframework.stereotype.Service

@Service
class EventsService(
    private val getAllEventsUseCase: GetAllEventsUseCase,
    private val getAnEventItemUseCase: GetAnEventItemUseCase,
    private val createEventUseCase: CreateEventUseCase,
    private val updateEventUseCase: UpdateEventUseCase,
    private val deleteAnEventUseCase: DeleteAnEventUseCase
) {

    /**
     * Gets all events
     * @return [Collection] Collection of events
     */
    fun getAll(): Collection<EventResponseDto> {
        val events = getAllEventsUseCase.execute()
        return events.map {
            EventResponseDto(
                identifier = it.identifier,
                organization = it.organization,
                type = it.type,
                date = it.date,
                startTime = it.startTime,
                endTime = it.endTime,
                title = it.title,
                description = it.description,
                location = it.location,
                bannerImageUrl = it.bannerImage,
                imageUrl = it.image,
                tickets = it.tickets.map { ticket ->
                    TicketResponseDto(
                        identifier = ticket.identifier!!,
                        type = ticket.type,
                        price = ticket.price,
                        description = ticket.description
                    )
                }.toSet()
            )
        }
    }

    fun getItem(identifier: EventIdentifier): EventResponseDto? {
        val item = getAnEventItemUseCase.execute(identifier) ?: return null
        return EventResponseDto(
            identifier,
            organization = item.organization,
            type = item.type,
            date = item.date,
            startTime = item.startTime,
            endTime = item.endTime,
            title = item.title,
            description = item.description,
            location = item.location,
            bannerImageUrl = item.bannerImage,
            imageUrl = item.image,
            tickets = item.tickets.map {
                TicketResponseDto(
                    identifier = it.identifier!!,
                    type = it.type,
                    description = it.description,
                    price = it.price
                )
            }.toSet()
        )
    }

    fun create(
        eventRequestDto: EventRequestDto
    ): EventIdentifier? {
        return createEventUseCase.execute(
            CreateEventUseCase.Params(
                eventRequestDto.organization,
                eventRequestDto.type,
                eventRequestDto.date,
                eventRequestDto.startTime,
                eventRequestDto.endTime,
                eventRequestDto.title,
                eventRequestDto.description,
                eventRequestDto.location,
                eventRequestDto.bannerImage,
                eventRequestDto.image,
                eventRequestDto.tickets.map {
                    Ticket(
                        type = it.type, description = it.description, price = it.price
                    )
                }
            )
        ) ?: return null
    }

    fun update(
        eventRequestDto: EventRequestDto
    ): Boolean {
        return updateEventUseCase.execute(
            UpdateEventUseCase.Params(
                eventRequestDto.identifier!!,
                eventRequestDto.organization,
                eventRequestDto.type,
                eventRequestDto.date,
                eventRequestDto.startTime,
                eventRequestDto.endTime,
                eventRequestDto.title,
                eventRequestDto.description,
                eventRequestDto.location,
                eventRequestDto.bannerImage,
                eventRequestDto.image,
                eventRequestDto.tickets.map {
                    Ticket(
                        type = it.type, description = it.description, price = it.price
                    )
                }
            )
        )
    }

    fun delete(identifier: EventIdentifier): Boolean {
        return deleteAnEventUseCase.execute(identifier)
    }
}
