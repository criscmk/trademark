package com.tmea.wit.api.rest.bookkeeping.inventory

import com.tmea.wit.api.rest.bookkeeping.inventory.dto.InventoryRequestDto
import com.tmea.wit.api.rest.bookkeeping.inventory.dto.InventoryResponseDto
import com.tmea.wit.core.InventoryIdentifier
import com.tmea.wit.core.bookkeeping.inventory.usecases.CreateInventoryUseCase
import com.tmea.wit.core.bookkeeping.inventory.usecases.DeleteAnInventoryUseCase
import com.tmea.wit.core.bookkeeping.inventory.usecases.GetAllInventoryUseCase
import com.tmea.wit.core.bookkeeping.inventory.usecases.GetAnInventoryItemUseCase
import com.tmea.wit.core.bookkeeping.inventory.usecases.UpdateInventoryUseCase
import org.springframework.stereotype.Service

@Service
class InventoryService(
        private val getAllInventoryUseCase: GetAllInventoryUseCase,
        private val createInventoryUseCase: CreateInventoryUseCase,
        private val updateInventoryUseCase: UpdateInventoryUseCase,
        private val getAnInventoryItemUseCase: GetAnInventoryItemUseCase,
        private val deleteAnInventoryUseCase: DeleteAnInventoryUseCase
) {
    fun getAll(): Collection<InventoryResponseDto> {
        val inventories = getAllInventoryUseCase.execute()
        return inventories.map {
            InventoryResponseDto(
                identifier = it.identifier,
                name = it.name,
                pricePerUnit = it.pricePerUnit,
                quantity = it.quantity,
                dateAdded = it.dateAdded
            )
        }
    }

    fun getAnInventoryItem(identifier: InventoryIdentifier): InventoryResponseDto? {
        val result = getAnInventoryItemUseCase.execute(identifier) ?: return null
        return InventoryResponseDto(
            identifier = result.identifier,
            name = result.name,
            pricePerUnit = result.pricePerUnit,
            quantity = result.quantity,
            dateAdded = result.dateAdded
        )
    }

    fun create(inventoryRequestDto: InventoryRequestDto): InventoryIdentifier? {
        return createInventoryUseCase.execute(
            CreateInventoryUseCase.Params(
                name = inventoryRequestDto.name,
                quantity = inventoryRequestDto.quantity,
                pricePerUnit = inventoryRequestDto.pricePerUnit
            )
        )
    }

    fun update(identifier: InventoryIdentifier, inventoryDto: InventoryRequestDto): Boolean {
        return updateInventoryUseCase.execute(
            UpdateInventoryUseCase.Params(
                identifier = identifier,
                name = inventoryDto.name,
                quantity = inventoryDto.quantity,
                pricePerUnit = inventoryDto.pricePerUnit
            )
        )
    }

    fun delete(identifier: InventoryIdentifier): Boolean {
        return deleteAnInventoryUseCase.execute(identifier)
    }
}
