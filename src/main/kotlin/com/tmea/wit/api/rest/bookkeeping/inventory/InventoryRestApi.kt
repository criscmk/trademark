package com.tmea.wit.api.rest.bookkeeping.inventory

import com.tmea.wit.api.rest.bookkeeping.inventory.dto.InventoryRequestDto
import com.tmea.wit.api.rest.bookkeeping.inventory.dto.InventoryResponseDto
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PatchMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.util.Collections

@RestController
@RequestMapping(path = ["/api/bookkeeping/inventories"])
class InventoryRestApi(
    private val inventoryService: InventoryService
) {
    @GetMapping(value = ["/"], produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getAllInventories(): ResponseEntity<Collection<InventoryResponseDto>> {
        val allInventories = inventoryService.getAll()
        return ResponseEntity(allInventories, HttpStatus.OK)
    }

    @GetMapping(value = ["/{identifier}"], produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getAnInventoryItem(@PathVariable identifier: String): ResponseEntity<InventoryResponseDto> {
        val inventory = inventoryService.getAnInventoryItem(identifier)
        if (inventory != null)
            return ResponseEntity(inventory, HttpStatus.OK)
        return ResponseEntity(HttpStatus.NOT_FOUND)
    }

    @PostMapping(value = ["/"], produces = [MediaType.APPLICATION_JSON_VALUE])
    fun createInventory(
        @Validated
        @RequestBody
        inventoryRequestDto: InventoryRequestDto
    ): ResponseEntity<Map<String, String>> {
        val result = inventoryService.create(inventoryRequestDto)
        return if (result != null) {
            ResponseEntity(
                Collections.singletonMap("identifier", result),
                HttpStatus.CREATED
            )
        } else {
            ResponseEntity(
                Collections.singletonMap("Error", "failed to create inventory"),
                HttpStatus.NOT_ACCEPTABLE
            )
        }
    }

    @PatchMapping(value = ["/{identifier}"], produces = [MediaType.APPLICATION_JSON_VALUE])
    fun updateInventory(
        @PathVariable identifier: String,
        @Validated
        @RequestBody
        inventoryRequestDto: InventoryRequestDto
    ): ResponseEntity<Map<String, Boolean>> {
        val result = inventoryService.update(identifier, inventoryRequestDto)
        return ResponseEntity(
            Collections.singletonMap("success", result),
            if (result) HttpStatus.ACCEPTED else HttpStatus.NOT_ACCEPTABLE
        )
    }

    @DeleteMapping(value = ["/{identifier}"], produces = [MediaType.APPLICATION_JSON_VALUE])
    fun deleteInventory(
        @PathVariable identifier: String
    ): ResponseEntity<Map<String, Boolean>> {
        val result = inventoryService.delete(identifier)
        return ResponseEntity(
            Collections.singletonMap("success", result),
            if (result) HttpStatus.ACCEPTED else HttpStatus.NOT_FOUND
        )
    }
}
