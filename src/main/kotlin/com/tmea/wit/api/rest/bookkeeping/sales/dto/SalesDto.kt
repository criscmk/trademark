package com.tmea.wit.api.rest.bookkeeping.sales.dto

import com.fasterxml.jackson.annotation.JsonProperty
import java.time.LocalDateTime

sealed class SalesDto(
    @JsonProperty("sale_id", required = true)
    open val saleId: String,

    @JsonProperty("item", required = true)
    open val item: String,

    @JsonProperty("quantity", required = true)
    open val quantity: Int,

    @JsonProperty("sale_unit_price", required = true)
    open val saleUnitPrice: Double,

    @JsonProperty("sale_amount", required = true)
    open val saleAmount: Double
)

data class SaleResponseDto(
    @JsonProperty("identifier", required = true)
    val identifier: String,

    @JsonProperty("sale_id", required = true)
    override var saleId: String,
    override var item: String,
    override var quantity: Int,


    @JsonProperty("sale_unit_price", required = true)
    override var saleUnitPrice: Double,

    @JsonProperty("sale_amount", required = true)
    override var saleAmount: Double,

    @JsonProperty("sale_date", required = true)
    val saleDate: LocalDateTime
) : SalesDto(saleId, item, quantity, saleUnitPrice, saleAmount)

data class SaleRequestDto(
    @JsonProperty("sale_id", required = true)
    override var saleId: String,
    override var item: String,
    override var quantity: Int,

    @JsonProperty("sale_unit_price", required = true)
    override var saleUnitPrice: Double,

    @JsonProperty("sale_amount", required = true)
    override var saleAmount: Double,

    @JsonProperty("sale_date", required = true)
    val saleDate: LocalDateTime

) : SalesDto(saleId, item, quantity, saleUnitPrice, saleAmount)
