package com.tmea.wit.api.rest.bookkeeping.inventory.dto

import com.fasterxml.jackson.annotation.JsonProperty
import java.time.LocalDateTime

sealed class InventoryDto(
    @JsonProperty("name", required = true)
    open val name: String,

    @JsonProperty("quantity", required = true)
    open val quantity: Int,

    @JsonProperty("price_per_unit", required = true)
    open val pricePerUnit: Double
)

data class InventoryResponseDto(
    @JsonProperty("identifier", required = true)
    val identifier: String,

    override var name: String,

    override var quantity: Int,

    @JsonProperty("price_per_unit", required = true)
    override var pricePerUnit: Double,

    @JsonProperty("date_added", required = true)
    val dateAdded: LocalDateTime
) : InventoryDto(name, quantity, pricePerUnit)

data class InventoryRequestDto(
    override var name: String,
    override var quantity: Int,

    @JsonProperty("price_per_unit", required = true)
    override var pricePerUnit: Double
) : InventoryDto(name, quantity, pricePerUnit)
