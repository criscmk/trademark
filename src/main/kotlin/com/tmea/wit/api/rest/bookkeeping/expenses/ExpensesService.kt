package com.tmea.wit.api.rest.bookkeeping.expenses

import com.tmea.wit.api.rest.bookkeeping.expenses.dto.ExpenseRequestDto
import com.tmea.wit.api.rest.bookkeeping.expenses.dto.ExpenseResponseDto
import com.tmea.wit.core.ExpenseIdentifier
import com.tmea.wit.core.bookkeeping.expenses.usecases.CreateExpenseUseCase
import com.tmea.wit.core.bookkeeping.expenses.usecases.DeleteAnExpenseUseCase
import com.tmea.wit.core.bookkeeping.expenses.usecases.GetAllExpensesUseCase
import com.tmea.wit.core.bookkeeping.expenses.usecases.GetAnExpenseItemUseCase
import com.tmea.wit.core.bookkeeping.expenses.usecases.UpdateExpenseUseCase
import org.springframework.stereotype.Service

@Service
class ExpensesService(
        private val getAllExpensesUseCase: GetAllExpensesUseCase,
        private val createAnExpenseUseCase: CreateExpenseUseCase,
        private val updateAnExpenseUseCase: UpdateExpenseUseCase,
        private val getAnExpenseItemUseCase: GetAnExpenseItemUseCase,
        private val deleteAnExpenseUseCase: DeleteAnExpenseUseCase
) {
    fun getAll(): Collection<ExpenseResponseDto> {
        val items = getAllExpensesUseCase.execute()
        return items.map {
            ExpenseResponseDto(
                identifier = it.identifier,
                expenseId = it.expenseId,
                category = it.category,
                description = it.description,
                amount = it.amount,
                expenseDate = it.expenseDate
            )
        }
    }

    fun getItem(identifier: ExpenseIdentifier): ExpenseResponseDto? {
        val result = getAnExpenseItemUseCase.execute(identifier) ?: return null
        return ExpenseResponseDto(
            identifier = result.identifier,
            expenseId = result.expenseId,
            category = result.category,
            description = result.description,
            amount = result.amount,
            expenseDate = result.expenseDate
        )
    }

    fun create(payload: ExpenseRequestDto): ExpenseIdentifier? {
        return createAnExpenseUseCase.execute(
            CreateExpenseUseCase.Params(
                expenseId = payload.expenseId,
                category = payload.category,
                description = payload.description,
                amount = payload.amount,
                expenseDate = payload.expenseDate
            )
        )
    }

    fun update(identifier: ExpenseIdentifier, payload: ExpenseRequestDto): Boolean {
        return updateAnExpenseUseCase.execute(
            UpdateExpenseUseCase.Params(
                identifier = identifier,
                expenseId = payload.expenseId,
                category = payload.category,
                description = payload.description,
                amount = payload.amount,
                expenseDate = payload.expenseDate
            )
        )
    }

    fun delete(identifier: ExpenseIdentifier): Boolean {
        return deleteAnExpenseUseCase.execute(identifier)
    }
}
