package com.tmea.wit.database.events

import com.tmea.wit.core.events.usecases.exceptions.EventsException
import com.tmea.wit.core.generateIdentifier
import com.tmea.wit.core.EventIdentifier
import com.tmea.wit.core.events.EventsDataStore
import com.tmea.wit.core.events.models.Event
import com.tmea.wit.core.events.models.Ticket
import org.slf4j.LoggerFactory
import java.time.LocalDateTime
import java.time.LocalTime

@Suppress("TooGenericExceptionCaught")
class EventsDataStoreImpl(
    private val eventsRepository: EventsRepository,
    private val ticketsRepository: TicketsRepository
) : EventsDataStore {

    private val logger = LoggerFactory.getLogger(this::class.java)

    override fun getAll(): Set<Event> {
        logger.info("Fetching events from data store")

        @Suppress("TooGenericExceptionCaught")
        try {
            val events = eventsRepository.findAll()
            return events.map {
                Event(
                    identifier = it.identifier,
                    organization = it.organization,
                    type = it.type,
                    date = it.date,
                    startTime = it.startTime,
                    endTime = it.endTime,
                    title = it.title,
                    description = it.description,
                    location = it.location,
                    bannerImage = it.bannerImage,
                    image = it.image,
                    tickets = it.tickets.map { ticket ->
                        Ticket(
                            identifier = ticket.identifier,
                            type = ticket.type,
                            price = ticket.price,
                            description = ticket.description
                        )
                    }
                )
            }.toMutableSet()
        } catch (e: Exception) {
            logger.error("Failed to fetch events, Err: ${e.message}")
            throw EventsException(
                "Failed to fetch Events, Err: ${e.message}"
            )
        }
    }

    override fun getItem(identifier: EventIdentifier): Event? {
        val foundEvent = eventsRepository.findByIdentifier(identifier) ?: return null
        return Event(
            identifier = foundEvent.identifier,
            organization = foundEvent.organization,
            type = foundEvent.type,
            date = foundEvent.date,
            startTime = foundEvent.startTime,
            endTime = foundEvent.endTime,
            title = foundEvent.title,
            description = foundEvent.description,
            location = foundEvent.location,
            bannerImage = foundEvent.bannerImage,
            image = foundEvent.image,
            tickets = foundEvent.tickets.map {
                Ticket(
                    identifier = it.identifier,
                    type = it.type,
                    description = it.description,
                    price = it.price
                )
            }
        )
    }

    override fun create(
        organization: String,
        type: String,
        date: LocalDateTime,
        startTime: LocalTime,
        endTime: LocalTime,
        title: String,
        description: String,
        location: String,
        bannerImageUrl: String,
        imageUrl: String,
        tickets: Collection<Ticket>
    ): EventIdentifier {

        val identifier = generateIdentifier("$organization:$type:$title:$description:$location")

        val event = EventEntity(
            identifier = identifier,
            organization = organization,
            type = type,
            date = date,
            startTime = startTime,
            endTime = endTime,
            title = title,
            description = description,
            location = location,
            bannerImage = bannerImageUrl,
            image = imageUrl
        )

        return try {
            val createdEvent = eventsRepository.save(event)
            val ticketEntities = tickets.map {
                TicketEntity(
                        identifier = generateIdentifier("${it.type}:${it.description}"),
                        type = it.type,
                        description = it.description,
                        price = it.price
                )
            }.toMutableSet()

            ticketEntities.map {
                it.event = createdEvent
                ticketsRepository.save(it)
            }
            identifier
        } catch (e: Exception) {
            logger.error("Failed to create event ${e.message}")
            throw EventsException(
                "Failed to create event"
            )
        }
    }

    override fun update(
        identifier: EventIdentifier,
        organization: String,
        type: String,
        date: LocalDateTime,
        startTime: LocalTime,
        endTime: LocalTime,
        title: String,
        description: String,
        location: String,
        bannerImageUrl: String,
        imageUrl: String,
        tickets: Collection<Ticket>
    ): Boolean {
        val foundEvent = eventsRepository.findByIdentifier(identifier) ?: return false
        val updatedEvent = foundEvent.apply {
            this.organization = organization
            this.type = type
            this.date = date
            this.startTime = startTime
            this.endTime = endTime
            this.title = title
            this.description = description
            this.location = location
            this.bannerImage = bannerImageUrl
            this.image = imageUrl
        }

        /** TODO get all found tickets */
        //  val foundTickets = ticketsRepository.findAllByEvent(foundEvent)

        return try {
            eventsRepository.save(updatedEvent)
            true
        } catch (e: Exception) {
            logger.error("Failed to update event $identifier")
            throw EventsException(
                "Failed to update event"
            )
        }
    }

    override fun delete(identifier: EventIdentifier): Boolean {
        val event = eventsRepository.findByIdentifier(identifier) ?: return false

        return try {
            eventsRepository.delete(event)
            true
        } catch (e: Exception) {
            logger.error("Failed to delete event item ${e.message}")
            throw EventsException(
                "Failed to delete event $identifier"
            )
        }
    }
}
