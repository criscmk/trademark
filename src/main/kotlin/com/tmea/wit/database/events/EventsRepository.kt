package com.tmea.wit.database.events

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import com.tmea.wit.database.events.EventEntity

@Repository
interface EventsRepository : JpaRepository<EventEntity, Long> {
    fun findByIdentifier(identifier: String): EventEntity?
}
