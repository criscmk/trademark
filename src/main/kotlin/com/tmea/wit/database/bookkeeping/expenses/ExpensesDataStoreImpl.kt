package com.tmea.wit.database.bookkeeping.expenses

import com.tmea.wit.core.ExpenseId
import com.tmea.wit.core.ExpenseIdentifier
import com.tmea.wit.core.bookkeeping.expenses.ExpensesDataStore
import com.tmea.wit.core.bookkeeping.expenses.exceptions.ExpensesException
import com.tmea.wit.core.bookkeeping.expenses.models.Expense
import com.tmea.wit.core.generateIdentifier
import org.jboss.logging.Logger
import java.time.LocalDateTime

class ExpensesDataStoreImpl(private val expensesRepository: ExpensesRepository) : ExpensesDataStore {

    private val logger = Logger.getLogger(this::class.java)

    override fun getItem(identifier: ExpenseIdentifier): Expense? {
        val item = expensesRepository.findByIdentifier(identifier) ?: return null

        return Expense(
            expenseId = item.expenseId,
            identifier = item.identifier,
            category = item.category,
            description = item.description,
            amount = item.amount,
            expenseDate = item.expenseDate
        )
    }

    override fun getAll(): Set<Expense> {
        @Suppress("TooGenericExceptionCaught")
        return try {
            val items = expensesRepository.findAll()
            items.map {
                Expense(
                    expenseId = it.expenseId,
                    identifier = it.identifier,
                    category = it.category,
                    description = it.description,
                    amount = it.amount,
                    expenseDate = it.expenseDate
                )
            }.toMutableSet()
        } catch (e: Exception) {
            logger.error("Failed to retrieve all inventories, err: ${e.message}")
            throw ExpensesException("Failed to fetch all inventories, Err: ${e.message}")
        }
    }

    override fun create(
        expenseId: ExpenseId,
        category: String,
        description: String,
        amount: Double,
        expenseDate: LocalDateTime
    ): String {
        val identifier = generateIdentifier("${category}:${description}")
        val item = ExpenseEntity(
            expenseId = expenseId,
            identifier = identifier,
            category = category,
            description = description,
            amount = amount,
            expenseDate = expenseDate
        )
        @Suppress("TooGenericExceptionCaught")
        try {
            expensesRepository.save(item)
            return identifier
        } catch (e: Exception) {
            logger.error("Failed to create Sale item ${e.message}")
            throw ExpensesException("Failed to create Sale item")
        }
    }

    @Suppress("TooGenericExceptionCaught")
    override fun update(
        identifier: ExpenseIdentifier,
        expenseId: ExpenseId,
        category: String,
        description: String,
        amount: Double,
        expenseDate: LocalDateTime
    ): Boolean {
        val item = expensesRepository.findByIdentifier(identifier) ?: return false

        val updatedItem = item.apply {
            this.expenseId = item.expenseId
            this.category = item.category
            this.description = item.description
            this.amount = item.amount
            this.expenseDate = item.expenseDate
        }

        try {
            expensesRepository.save(updatedItem)
            return true
        } catch (e: Exception) {
            logger.error("Failed to update Sale item ${e.message}")
            throw ExpensesException("Failed to update Sale item identifier: $identifier")
        }
    }

    @Suppress("TooGenericExceptionCaught")
    override fun delete(identifier: ExpenseIdentifier): Boolean {
        val item = expensesRepository.findByIdentifier(identifier) ?: return false

        try {
            expensesRepository.delete(item)
            return true
        } catch (e: Exception) {
            logger.error("Failed to delete Sale item ${e.message}")
            throw ExpensesException("Failed to delete Sale item identifier: $identifier")
        }
    }
}
