package com.tmea.wit.database.bookkeeping.expenses

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface ExpensesRepository : JpaRepository<ExpenseEntity, Long> {
    fun findByIdentifier(identifier: String): ExpenseEntity?
}
