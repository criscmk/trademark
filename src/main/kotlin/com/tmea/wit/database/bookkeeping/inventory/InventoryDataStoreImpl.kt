package com.tmea.wit.database.bookkeeping.inventory

import com.tmea.wit.core.InventoryIdentifier
import com.tmea.wit.core.bookkeeping.inventory.InventoryDataStore
import com.tmea.wit.core.bookkeeping.inventory.exceptions.InventoryException
import com.tmea.wit.core.bookkeeping.inventory.models.Inventory
import com.tmea.wit.core.generateIdentifier
import org.jboss.logging.Logger
import java.time.LocalDateTime

class InventoryDataStoreImpl(private val inventoryRepository: InventoryRepository) : InventoryDataStore {

    private val logger = Logger.getLogger(this::class.java)

    override fun getAnInventoryItem(identifier: InventoryIdentifier): Inventory? {
        val inventoryItem = inventoryRepository.findByIdentifier(identifier) ?: return null

        return Inventory(
            identifier = inventoryItem.identifier,
            name = inventoryItem.name,
            quantity = inventoryItem.quantity,
            pricePerUnit = inventoryItem.pricePerUnit,
            dateAdded = inventoryItem.dateAdded
        )
    }

    override fun getAll(): Set<Inventory> {
        @Suppress("TooGenericExceptionCaught")
        return try {
            val inventoryItems = inventoryRepository.findAll()
            inventoryItems.map {
                Inventory(
                    identifier = it.identifier,
                    name = it.name,
                    quantity = it.quantity,
                    pricePerUnit = it.pricePerUnit,
                    dateAdded = it.dateAdded
                )
            }.toMutableSet()
        } catch (e: Exception) {
            logger.error("Failed to retrieve all inventories, err: ${e.message}")
            throw InventoryException("Failed to fetch all inventories, Err: ${e.message}")
        }
    }

    override fun create(name: String, quantity: Int, pricePerUnit: Double): String {
        val identifier = generateIdentifier(name)
        val inventoryItem = InventoryEntity(
            identifier = identifier,
            name = name,
            quantity = quantity,
            pricePerUnit = pricePerUnit,
            dateAdded = LocalDateTime.now()
        )
        @Suppress("TooGenericExceptionCaught")
        try {
            inventoryRepository.save(inventoryItem)
            return identifier
        } catch (e: Exception) {
            logger.error("Failed to create inventory item ${e.message}")
            throw InventoryException("Failed to create inventory item")
        }
    }

    @Suppress("TooGenericExceptionCaught")
    override fun update(identifier: InventoryIdentifier, name: String, quantity: Int, pricePerUnit: Double): Boolean {
        val inventoryItem = inventoryRepository.findByIdentifier(identifier) ?: return false

        val updatedItem = inventoryItem.apply {
            this.name = name
            this.pricePerUnit = pricePerUnit
            this.quantity = quantity
        }

        try {
            inventoryRepository.save(updatedItem)
            return true
        } catch (e: Exception) {
            logger.error("Failed to update inventory item ${e.message}")
            throw InventoryException("Failed to update inventory item identifier: $identifier")
        }
    }

    @Suppress("TooGenericExceptionCaught")
    override fun delete(identifier: InventoryIdentifier): Boolean {
        val inventoryItem = inventoryRepository.findByIdentifier(identifier) ?: return false

        try {
            inventoryRepository.delete(inventoryItem)
            return true
        } catch (e: Exception) {
            logger.error("Failed to delete inventory item ${e.message}")
            throw InventoryException("Failed to delete inventory item identifier: $identifier")
        }
    }
}
