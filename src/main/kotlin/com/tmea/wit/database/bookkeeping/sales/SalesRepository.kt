package com.tmea.wit.database.bookkeeping.sales

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface SalesRepository : JpaRepository<SaleEntity, Long> {
    fun findByIdentifier(identifier: String): SaleEntity?
}
