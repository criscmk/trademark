package com.tmea.wit.database.bookkeeping.inventory

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface InventoryRepository : JpaRepository<InventoryEntity, Long> {
    fun findByIdentifier(identifier: String): InventoryEntity?
}
