package com.tmea.wit.database.bookkeeping.expenses

import com.tmea.wit.database.BaseEntity
import java.time.LocalDateTime
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Table

@Entity(name = "expense")
@Table(name = "expenses")
data class ExpenseEntity(
    @Column(name = "identifier", nullable = false)
    val identifier: String,

    @Column(name = "expense_id", nullable = false)
    var expenseId: String,

    @Column(name = "category", nullable = false)
    var category: String,

    @Column(name = "description", nullable = false)
    var description: String,

    @Column(name = "amount", nullable = false)
    var amount: Double,

    @Column(name = "expense_date", nullable = false)
    var expenseDate: LocalDateTime
) : BaseEntity()
