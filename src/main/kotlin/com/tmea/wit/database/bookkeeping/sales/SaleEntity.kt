package com.tmea.wit.database.bookkeeping.sales

import com.tmea.wit.database.BaseEntity
import java.time.LocalDateTime
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Table

@Entity(name = "sale")
@Table(name = "sales")
data class SaleEntity(
    @Column(name = "identifier", nullable = false)
    val identifier: String,

    @Column(name = "sale_id", nullable = false)
    val saleId: String,

    @Column(name = "item", nullable = false)
    var item: String,

    @Column(name = "quantity", nullable = false)
    var quantity: Int,

    @Column(name = "sale_unit_price", nullable = false)
    var saleUnitPrice: Double,

    @Column(name = "sale_amount", nullable = false)
    var saleAmount: Double,

    @Column(name = "sale_date", nullable = false)
    var saleDate: LocalDateTime
) : BaseEntity()
