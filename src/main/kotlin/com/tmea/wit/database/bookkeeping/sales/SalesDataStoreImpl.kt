package com.tmea.wit.database.bookkeeping.sales

import com.tmea.wit.core.SaleId
import com.tmea.wit.core.SaleIdentifier
import com.tmea.wit.core.bookkeeping.sales.SalesDataStore
import com.tmea.wit.core.bookkeeping.sales.exceptions.SalesException
import com.tmea.wit.core.bookkeeping.sales.models.Sale
import com.tmea.wit.core.generateIdentifier
import org.jboss.logging.Logger
import java.time.LocalDateTime

class SalesDataStoreImpl(private val salesRepository: SalesRepository) : SalesDataStore {

    private val logger = Logger.getLogger(this::class.java)

    override fun getItem(identifier: SaleIdentifier): Sale? {
        val saleItem = salesRepository.findByIdentifier(identifier) ?: return null

        return Sale(
            saleId = saleItem.saleId,
            identifier = saleItem.identifier,
            item = saleItem.item,
            quantity = saleItem.quantity,
            saleUnitPrice = saleItem.saleUnitPrice,
            saleDate = saleItem.saleDate,
            saleAmount = saleItem.saleAmount
        )
    }

    override fun getAll(): Set<Sale> {
        @Suppress("TooGenericExceptionCaught")
        return try {
            val saleItems = salesRepository.findAll()
            saleItems.map { saleItem ->
                Sale(
                    saleId = saleItem.saleId,
                    identifier = saleItem.identifier,
                    item = saleItem.item,
                    quantity = saleItem.quantity,
                    saleUnitPrice = saleItem.saleUnitPrice,
                    saleDate = saleItem.saleDate,
                    saleAmount = saleItem.saleAmount
                )
            }.toMutableSet()
        } catch (e: Exception) {
            logger.error("Failed to retrieve all inventories, err: ${e.message}")
            throw SalesException("Failed to fetch all inventories, Err: ${e.message}")
        }
    }

    override fun create(
        saleId: SaleId,
        item: String,
        quantity: Int,
        saleUnitPrice: Double,
        saleAmount: Double,
        saleDate: LocalDateTime
    ): String {
        val identifier = generateIdentifier(item)
        val saleItem = SaleEntity(
            saleId = saleId,
            identifier = identifier,
            quantity = quantity,
            item = item,
            saleUnitPrice = saleUnitPrice,
            saleAmount = saleAmount,
            saleDate = saleDate
        )
        @Suppress("TooGenericExceptionCaught")
        try {
            salesRepository.save(saleItem)
            return identifier
        } catch (e: Exception) {
            logger.error("Failed to create Sale item ${e.message}")
            throw SalesException("Failed to create Sale item")
        }
    }

    @Suppress("TooGenericExceptionCaught")
    override fun update(
        identifier: SaleIdentifier,
        item: String,
        quantity: Int,
        saleUnitPrice: Double,
        saleDate: LocalDateTime
    ): Boolean {
        val saleItem = salesRepository.findByIdentifier(identifier) ?: return false

        val updatedItem = saleItem.apply {
            this.item = item
            this.saleUnitPrice = saleUnitPrice
            this.quantity = quantity
            this.saleDate = saleDate
        }

        try {
            salesRepository.save(updatedItem)
            return true
        } catch (e: Exception) {
            logger.error("Failed to update Sale item ${e.message}")
            throw SalesException("Failed to update Sale item identifier: $identifier")
        }
    }

    @Suppress("TooGenericExceptionCaught")
    override fun delete(identifier: SaleIdentifier): Boolean {
        val saleItem = salesRepository.findByIdentifier(identifier) ?: return false

        try {
            salesRepository.delete(saleItem)
            return true
        } catch (e: Exception) {
            logger.error("Failed to delete Sale item ${e.message}")
            throw SalesException("Failed to delete Sale item identifier: $identifier")
        }
    }
}
