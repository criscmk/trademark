package com.tmea.wit.database.bookkeeping.inventory

import com.tmea.wit.database.BaseEntity
import java.time.LocalDateTime
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Table

@Entity(name = "inventory")
@Table(name = "inventories")
data class InventoryEntity(
    @Column(name = "identifier", nullable = false)
    val identifier: String,

    @Column(name = "name", nullable = false)
    var name: String,

    @Column(name = "quantity", nullable = false)
    var quantity: Int,

    @Column(name = "price_per_unit", nullable = false)
    var pricePerUnit: Double,

    @Column(name = "date_added", nullable = false)
    val dateAdded: LocalDateTime
): BaseEntity()
