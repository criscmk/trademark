package com.tmea.wit.service

import com.tmea.wit.model.BaseApiResponse
import com.tmea.wit.model.GeneralApiListResponse
import com.tmea.wit.model.dto.FieldErrorDto
import com.tmea.wit.model.dto.request.ApiEditTradeDocumentTypePostDto
import com.tmea.wit.model.dto.request.ApiStatusRequestDto
import com.tmea.wit.model.dto.request.ApiTradeDocumentTypePostDto
import com.tmea.wit.model.dto.response.ApiDocumentTypeResponseDto
import com.tmea.wit.util.CommonDBFunctions
import com.tmea.wit.util.ObjectUtils
import groovy.sql.GroovyRowResult
import groovy.sql.Sql
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service

import javax.sql.DataSource

@Service
class TradeDocumentsTypeService {
    DataSource dataSource
    CommonDBFunctions commonDBFunctions

    @Autowired
    UserService userService

    TradeDocumentsTypeService(DataSource dataSource, CommonDBFunctions commonDBFunctions) {
        this.dataSource = dataSource
        this.commonDBFunctions = commonDBFunctions
    }

    BaseApiResponse addTradeDocumentType(ApiTradeDocumentTypePostDto body, String name) {
        Sql sql = Sql.newInstance(dataSource)
        BaseApiResponse response = new BaseApiResponse()
        Map params = ObjectUtils.asMap(body)
        Long userId = userService.getUserIdFromUsername(name)
        List<FieldErrorDto> errors = new ArrayList<>()
        params.put("addedBy", userId)
        params.put("isActive", true)

        if (userId) {
            def nameExist = getDocumentTypeByName(params.get("name") as String)

            if (!nameExist) {
                def insertQuery = sql.executeInsert(
                        """INSERT INTO trade_document_type(name, trade_document_category_id, added_by,date_added, is_active) 
                                VALUES (?.name, ?.documentCategoryId, ?.addedBy, current_timestamp, ?.isActive)""", params
                )

                if (insertQuery) {
                    Long id = insertQuery.get(0).get(0)
                    def documentType = getDocumentTypeById(id)
                    def data = ObjectUtils.mapToSnakeCaseMap(documentType)

                    response.status = HttpStatus.OK.value()
                    response.data = data
                    response.message = "Document type added successfully"
                } else {
                    response.status = HttpStatus.BAD_REQUEST.value()
                    response.message = "Operation failed"
                }
            } else {
                response.status = HttpStatus.NOT_FOUND.value()
                response.message = "Record with the same name already exists. Do you wish to update?"
            }

        } else {
            errors.add(new FieldErrorDto("userId", "Failed to fetch user details"))
            response.data = null
            response.message = "Failed to add new document type"
            response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
            response.errors = errors
        }

        sql.close()
        return response
    }

    BaseApiResponse editDocumentType(ApiEditTradeDocumentTypePostDto body, Long id, String name) {
        Sql sql = Sql.newInstance(dataSource)
        BaseApiResponse response = new BaseApiResponse()
        Map params = ObjectUtils.asMap(body)
        List<FieldErrorDto> errors = new ArrayList<>()

        Long userId = userService.getUserIdFromUsername(name)

        if (userId) {
            def recordExist = sql.firstRow(
                    """SELECT * FROM trade_document_type WHERE id = ?""", id
            )

            if (recordExist) {
                Map updateParams = ObjectUtils.rowAsMap(recordExist)
                updateParams = ObjectUtils.mapToSnakeCaseMap(updateParams) as Map
                ObjectUtils.copyMapProperties(params, updateParams)
                updateParams.put("id", id)

                def updateQuery = sql.executeUpdate(
                        """UPDATE trade_document_type SET name = ?.name, trade_document_category_id = ?.tradeDocumentCategoryId WHERE id = ?.id""", updateParams
                )

                if (updateQuery == 1) {
                    def documentType = getDocumentTypeById(id)
                    def dataResponse = ObjectUtils.mapToSnakeCaseMap(documentType)

                    response.status = HttpStatus.OK.value()
                    response.data = dataResponse
                    response.message = "Success"
                } else {
                    response.status = HttpStatus.BAD_REQUEST.value()
                    response.message = "Operation failed"
                }

            } else {
                response.status = HttpStatus.NOT_FOUND.value()
                response.message = "Record not found"
            }

        } else {
            errors.add(new FieldErrorDto("userId", "Failed to fetch user details"))
            response.data = null
            response.message = "Failed to edit trade document type"
            response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
            response.errors = errors
        }

        sql.close()
        return response
    }

    GeneralApiListResponse fetchAllDocumentType(Map parameterMap, boolean isAdmin) {
        Sql sql = Sql.newInstance(dataSource)
        GeneralApiListResponse response = new GeneralApiListResponse()
        List<FieldErrorDto> errors = new ArrayList<>()

        def params = ObjectUtils.flattenListParam(parameterMap)
        Map requestParams = [start: 0, limit: 25]

        def status = params.get("status")
        Long documentCategory = params.get("documentCategory") as Long
        def query = params.get("query")

        def statusFilter = ""
        if (status) {
            def booleanStatus = CommonDBFunctions.getStatusAsBoolean(status)

            if (booleanStatus != null) {
                requestParams.status = booleanStatus
                statusFilter = " AND trade_document_type.is_active = ?.status "
            } else {
                errors.add(new FieldErrorDto("status", "Invalid value passed. Check Documentation"))
                response.data = []
                response.total = 0
                response.status = HttpStatus.BAD_REQUEST.value()
                response.message = "Invalid status value"
                return response

            }
        }

        def documentCategoryFilter = ""
        if (documentCategory) {
            documentCategoryFilter = " AND trade_document_type.trade_document_category_id = ?.categoryId "
            requestParams.put("categoryId", documentCategory)
        }

        def queryFilter = ""
        if (query) {
            String formattedQuery = "%$query%"
            queryFilter = """ AND trade_document_type.name ILIKE ?.query """
            requestParams.put("query", formattedQuery)
        }

        if (!isAdmin) {
            requestParams.put("status", true)
            statusFilter = " AND trade_document_type.is_active = ?.status "
        }

        def selectAdminQuery = """SELECT trade_document_type.id, 
                              trade_document_type.name, 
                              trade_document_type.is_active,
                              trade_document_category.name    AS document_category,
                             CASE
                              WHEN "user".user_type_id = 1 THEN (SELECT user_individual_detail.first_name||' '||user_individual_detail.middle_name||' '||user_individual_detail.last_name  FROM user_individual_detail WHERE "user".id = user_individual_detail.user_id)
                              WHEN "user".user_type_id = 2 THEN (SELECT user_organization_detail.organization_name FROM user_organization_detail WHERE "user".id = user_organization_detail.user_id)
                              END AS added_by,
                              trade_document_category.date_added
                        FROM trade_document_type,
                            trade_document_category,
                            "user"
                        WHERE trade_document_type.trade_document_category_id = trade_document_category.id 
                        AND trade_document_category.added_by = "user".id"""

        def selectQuery = """SELECT trade_document_type.id, 
                              trade_document_type.name, 
                              trade_document_type.is_active,
                              trade_document_category.name    AS document_category
                             
                        FROM trade_document_type,
                            trade_document_category
                            
                        WHERE trade_document_type.trade_document_category_id = trade_document_category.id 
                        """

        def countQuery = """SELECT COUNT(1) FROM trade_document_type,
                            trade_document_category
                            WHERE trade_document_type.trade_document_category_id = trade_document_category.id """

        requestParams = commonDBFunctions.paginationSqlParams(requestParams, params)

        String limitClause = " limit ?.limit offset ?.start"
        String queryString = documentCategoryFilter + queryFilter + statusFilter + limitClause

        def filterQuery
        if (isAdmin){
            filterQuery = selectAdminQuery + queryString
        } else {
            filterQuery = selectQuery + queryString
        }

        def count = countQuery + queryString

        def data = sql.rows(filterQuery, requestParams);
        def total

        if (filterQuery) {
            total = sql.firstRow(count, requestParams).get("count")
        } else {
            total = sql.firstRow(count).get("count")
        }

        if (data) {

            def responseDtos = ObjectUtils.mapToSnakeCaseArrayMap(data)

            response.status = HttpStatus.OK.value()
            response.data = responseDtos
            response.total = total
            response.message = "Success"
        } else {
            response.status = HttpStatus.NOT_FOUND.value()
            response.message = "No records found!"
        }

        sql.close()
        return response
    }

    BaseApiResponse updateDocumentTypeStatus(ApiStatusRequestDto body, Long id, String name) {
        Sql sql = new Sql(dataSource)
        BaseApiResponse response = new BaseApiResponse()
        Map bodyParams = ObjectUtils.asMap(body)
        Long user = userService.getUserIdFromUsername(name)

        bodyParams.put("id", id)
        def statusMsg
        if (user) {
            def recordExist = getDocumentTypeById(id)

            if (recordExist) {
                def status = CommonDBFunctions.getStatusAsBoolean(bodyParams.get("status"))
                bodyParams.replace("status", status)

                def updateStatusQuery = sql.executeUpdate(
                        """UPDATE trade_document_type SET is_active = ?.status WHERE id = ?.id""", bodyParams
                )

                statusMsg = status ? "activated" : "deactivated"
                if (updateStatusQuery == 1) {
                    def record = getDocumentTypeById(id)
                    def data = ObjectUtils.mapToSnakeCaseMap(record)

                    response.status = HttpStatus.OK.value()
                    response.data = data
                    response.message = updateStatusQuery + " record " + statusMsg + " successfully"
                } else {
                    response.message = "Failed to " + statusMsg + " document type"
                    response.status = HttpStatus.BAD_REQUEST.value()
                }

            } else {
                response.message = "No record found"
                response.status = HttpStatus.NOT_FOUND.value()
            }

        } else {
            List<FieldErrorDto> errors = new ArrayList<>()
            errors.add(new FieldErrorDto("userId", "Failed to fetch user details"))
            response.data = null
            response.message = "Failed to " + statusMsg + " document type"
            response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
            response.errors = errors
        }

        sql.close()
        return response
    }


    Map getDocumentTypeByName(String name) {
        Sql sql = Sql.newInstance(dataSource)

        def category = sql.firstRow(
                """SELECT * FROM trade_document_type WHERE name ILIKE ?""", name
        )

        sql.close()
        return category
    }

    Map getDocumentTypeById(Long id) {
        Sql sql = Sql.newInstance(dataSource)

        def category = sql.firstRow(
                """SELECT trade_document_type.id, 
                              trade_document_type.name, 
                              trade_document_type.is_active,
                              trade_document_category.name    AS document_category
                        FROM trade_document_type,
                            trade_document_category
                        WHERE trade_document_type.trade_document_category_id = trade_document_category.id
                        AND trade_document_type.id = ?""", id
        )

        sql.close()
        return category
    }

}
