package com.tmea.wit.service

import com.tmea.wit.model.BaseApiResponse
import com.tmea.wit.model.dto.request.ApiResendVerificationCodeDto
import com.tmea.wit.model.dto.request.ApiValidateCodeDto
import com.tmea.wit.util.ObjectUtils
import groovy.sql.Sql
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import javax.sql.DataSource

@Service
class SmsVerificationService {


    private final DataSource dataSource;
    private final SmsSendService smsSendService;
    @Value('${wit.duration.verificationcodeexpiry}')
    private Integer verificationCodeExpiryDuration;
    private final String SMS_VERIFICATION_TYPE = "SMS";
    @Autowired
    GlobalConfigService globalConfigService


    @Autowired
    SmsVerificationService(DataSource dataSource, SmsSendService smsSendService) {
        this.dataSource = dataSource;
        this.smsSendService = smsSendService;
    }

    public boolean sendAccountVerificationSms(long userId) {
        boolean status = false;
        Sql sql = Sql.newInstance(dataSource);

        def userDetails = sql.firstRow("""SELECT id,phone_number AS contact FROM "user" WHERE id = ?""", userId);

        if (userDetails) {

            //Disable all previous Verification codes
            sql.executeUpdate("UPDATE user_account_verification SET is_active = FALSE WHERE user_id = ?", userId);


            String phoneVerificationCode = randomOTP(5);
            userDetails.put("code", phoneVerificationCode);
            userDetails.put("type", SMS_VERIFICATION_TYPE);

            String expiryClause = "CURRENT_TIMESTAMP+\'$verificationCodeExpiryDuration hour\'::interval";

            def verificationCodeInsertRes = sql.executeInsert("INSERT INTO user_account_verification(user_id,code,type,contact,expiry_time) VALUES (?.id,?.code,?.type,?.contact," + expiryClause + ")", userDetails);

            Long phoneVerificationId = verificationCodeInsertRes.get(0).get(0);

            def message = "Use $phoneVerificationCode as your WIT verification code";
            status = smsSendService.sendSingleSms(userDetails.get("contact"), message);

            if (status) {
                sql.executeUpdate("UPDATE user_account_verification SET sent = TRUE WHERE id=?", phoneVerificationId);
            }

        } else {
            return false;
        }

        sql.close();
        return status;
    }

    public String randomOTP(int len) {
        // Using numeric values
        String numbers = "0123456789";

        // Using random method
        Random rndm_method = new Random();

        char[] otp = new char[len];

        for (int i = 0; i < len; i++) {
            // Use of charAt() method : to get character value
            // Use of nextInt() as it is scanning the value as int
            otp[i] = numbers.charAt(rndm_method.nextInt(numbers.length()));
        }
        return String.valueOf(otp);
    }

    public BaseApiResponse resendVerificationCode(ApiResendVerificationCodeDto apiResendVerificationCodeDto) {

        BaseApiResponse response;
        def status = sendAccountVerificationSms(apiResendVerificationCodeDto.userId);
        if (status) {
            response = new BaseApiResponse(null, HttpStatus.OK.value(), "SMS Sent Successfully");
        } else {
            response = new BaseApiResponse(null, HttpStatus.INTERNAL_SERVER_ERROR.value(), "Could not send verification SMS please try again later");
        }
        return response;
    }

    public BaseApiResponse validateCode(ApiValidateCodeDto apiValidateCodeDto) {

        Map requestParams = ObjectUtils.asMap(apiValidateCodeDto);

        BaseApiResponse response = new BaseApiResponse();

        Sql sql = Sql.newInstance(dataSource);
        def codeDetails = sql.firstRow("SELECT *,current_timestamp > user_account_verification.expiry_time AS expired FROM user_account_verification WHERE user_id = ?.userId AND code=?.code", requestParams);

        if (codeDetails) {
            Long userId = codeDetails.get("user_id");
            if (codeDetails.get("is_active")) {
                if (codeDetails.get("expired")) {
                    response.setStatus(HttpStatus.BAD_REQUEST.value());
                    response.setData(null);
                    response.setMessage("Invalid verification code");

                } else {
                    if (codeDetails.get("used")) {
                        response.setStatus(HttpStatus.BAD_REQUEST.value());
                        response.setData(null);
                        response.setMessage("Invalid verification code");
                    } else {
                        sql.withTransaction {
                            sql.executeUpdate("UPDATE user_account_verification SET used = TRUE,is_active = FALSE WHERE id=?", codeDetails.get("id"));
                            sql.executeUpdate('UPDATE "user" SET account_verified = TRUE WHERE id = ?', userId);

                            //If Approval not required Activate user
                            Boolean approvalConfigActive = Boolean.valueOf(globalConfigService.getConfigValue("user_registration_approval"));
                            String successmessage = "";

                            if (!approvalConfigActive) {
                                sql.executeUpdate('UPDATE "user" SET is_active = TRUE WHERE id = ?', userId);
                                successmessage = "Phone number verified. Account Activated.";
                            } else {
                                successmessage = "Phone number verified. Account will be Activated after approval.";
                            }

                            response.setStatus(HttpStatus.OK.value());
                            response.setData(null);
                            response.setMessage(successmessage);
                        }

                    }
                }
            } else {
                response.setStatus(HttpStatus.BAD_REQUEST.value());
                response.setData(null);
                response.setMessage("Invalid verification code");
            }

        } else {
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            response.setData(null);
            response.setMessage("Invalid verification code");
        }

        sql.close();

        return response;

    }
}
