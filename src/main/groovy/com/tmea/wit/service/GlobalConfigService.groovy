package com.tmea.wit.service

import groovy.sql.Sql
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

import javax.sql.DataSource

@Service
class GlobalConfigService {
    @Autowired
    DataSource dataSource;

    public String getConfigValue(String config){
        Sql sql = Sql.newInstance(dataSource);
        String configValue = sql.firstRow("""SELECT value FROM "global_config" WHERE "global_config".config = ?""",config)?.get("value");
        sql.close();
        return configValue;
    }
}
