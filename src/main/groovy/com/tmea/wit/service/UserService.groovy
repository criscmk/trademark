package com.tmea.wit.service

import com.tmea.wit.model.BaseApiResponse
import com.tmea.wit.model.dto.FieldErrorDto
import com.tmea.wit.model.dto.response.ApiUserIndividualResponseDto
import com.tmea.wit.model.dto.response.ApiUserOrganizationResponseDto
import com.tmea.wit.util.ObjectUtils
import groovy.sql.Sql
import org.hashids.Hashids
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpStatus
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service
import javax.sql.DataSource

@Service
public class UserService {

    @Autowired
    DataSource dataSource;
    @Autowired
    GlobalConfigService globalConfigService
    @Autowired
    SmsVerificationService smsVerificationService;
    @Value('${hashids.username.salt}')
    private String usernameHashIdSalt;
    @Value('${approval-status.key.pending}')
    private Integer pendingStatus;
    @Value('${approval-status.key.approved}')
    private Integer approvedStatus;

    public BaseApiResponse registerUser(def userRegDto, Integer userType) {

        Map requestParams = ObjectUtils.asMap(userRegDto);
        BaseApiResponse response = new BaseApiResponse();

        Sql sql = Sql.newInstance(dataSource);
        List<FieldErrorDto> errors = [];


        //Check if user exists
        def exitingUser = sql.firstRow("""SELECT
                email,
                phone_number 
            FROM "user"
            WHERE 
            email ILIKE ?.email OR 
            phone_number ILIKE ?.phoneNumber
        """, requestParams);

        if (exitingUser) {
            if (exitingUser.get("email").toString().equalsIgnoreCase(requestParams.get("email"))) {
                errors.add(new FieldErrorDto("email", "Email already registered"));
            }

            if (exitingUser.get("phone_number").toString().equalsIgnoreCase(requestParams.get("phoneNumber"))) {
                errors.add(new FieldErrorDto("phoneNumber", "Phone number already registered"));
            }
            response.status = HttpStatus.BAD_REQUEST.value();
            response.setErrors(errors);
            response.setData(null);
            response.setMessage("User with similar details already exists");

        } else {
            if (requestParams.get("password") == requestParams.get("confirmPassword")) {

                //Hash User Password
                BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder(12);
                requestParams.put("encodedPassword", bCryptPasswordEncoder.encode(requestParams.get("password")));
                requestParams.put("userTypeId", userType);


                def insertUserRes = sql.executeInsert("""
                        INSERT INTO "user"(email, phone_number, password, user_type_id, country_id,
                                           language_preference, location_id)
                        VALUES (?.email,?.phoneNumber,?.encodedPassword,?.userTypeId,?.country,?.languagePreference,?.location)
                        """, requestParams);



                if (insertUserRes) {

                    Long userId = insertUserRes.get(0).get(0);
                    requestParams.put("userId", userId);

                    //Save Details Based on User Type
                    switch (userType) {
                        case 1:
                            sql.executeInsert("""INSERT INTO user_individual_detail(user_id, first_name, middle_name, last_name, gender_id) 
                                                         VALUES (?.userId,?.firstName,?.middleName,?.lastName,?.gender)""", requestParams);
                            break;
                        case 2:
                            sql.executeInsert("""INSERT INTO user_organization_detail(organization_name, organization_type_id, organization_size_id, postal_address, physical_address, website) 
                                                        VALUES(?.organizationName,?.organizationType,?.organizationSize,?.postalAddress,?.physicalAddress,?.website)
                                """, requestParams);
                            break;
                    }

                    //Generate Username
                    Hashids hashids = new Hashids(usernameHashIdSalt, 6);
                    String username = hashids.encode(userId).toUpperCase();
                    Map updateMemberParams = [id: userId, username: username];
                    sql.executeUpdate("""UPDATE "user" SET username = ?.username WHERE id = ?.id""", updateMemberParams);

                    //Assign Role
                    Map roleAssignmentParams = [userId: userId, roleId: requestParams.get("role"), isActive: true];
                    sql.executeInsert("""INSERT INTO user_role_allocation(user_id, role_id, is_active) 
                        VALUES (?.userId,?.roleId,?.isActive)""", roleAssignmentParams);

                    //Set approval Status Based on Global Config
                    Boolean approvalConfigActive = Boolean.valueOf(globalConfigService.getConfigValue("user_registration_approval"));
                    Integer approvalStatusId = approvalConfigActive ? pendingStatus : approvedStatus;
                    sql.executeUpdate("""UPDATE "user" SET approval_status_id = ?""", approvalStatusId);


                    //Send verification Code
                    boolean smsSent = smsVerificationService.sendAccountVerificationSms(userId);
                    Map registrationStatus = [smsSent: smsSent, username: username, userId: userId];

                    response.status = HttpStatus.OK.value();
                    response.message = "Success. User registered";
                    response.data = registrationStatus;

                } else {
                    response.data = null;
                    response.status = HttpStatus.INTERNAL_SERVER_ERROR.value();
                    response.message = "Could not register. Please try again";
                }

            } else {
                response.data = null;
                response.status = HttpStatus.BAD_REQUEST.value();
                response.message = "Password Mismatch";
                errors.add(new FieldErrorDto("confirmPassword", "Does not match password"))
            }

        }


        sql.close();

        return response;
    }

    public Long getUserIdFromUsername(String username) {
        Sql sql = Sql.newInstance(dataSource);
        def userId = sql.firstRow("""SELECT  id FROM "user" WHERE "user".username = ?""", username)?.get("id");
        sql.close();
        return userId;
    }

    public BaseApiResponse getUserDetails(Long userId) {
        Sql sql = Sql.newInstance(dataSource);
        BaseApiResponse response;

        def userType = sql.firstRow('SELECT user_type_id FROM "user" WHERE id = ?', userId).get("user_type_id");
        if (userType) {
            def userDetailsResponse
            BigDecimal rating
            def userDetails = fetchUserDetails(userId, userType as Long)
            if (userDetails) {
                rating = getUserRating(userId)
                if (userType == 1) {
                    userDetailsResponse = ObjectUtils.snakeCaseMapToPojo(userDetails, new ApiUserIndividualResponseDto()) as ApiUserIndividualResponseDto;
                } else if (userType == 2) {
                    userDetailsResponse = ObjectUtils.snakeCaseMapToPojo(userDetails, new ApiUserOrganizationResponseDto()) as ApiUserOrganizationResponseDto;
                }
                userDetailsResponse.setRating(rating)

                response = new BaseApiResponse(userDetailsResponse, HttpStatus.OK.value(), "Success");
            }

        } else {
            response = new BaseApiResponse(null, HttpStatus.BAD_REQUEST.value(), "Unrecognized user");
        }

        sql.close();

        return response;
    }

    Map fetchUserDetails(Long id, Long userType) {
        Sql sql = Sql.newInstance(dataSource)
        def userDetails

        if (userType == 1) {
            userDetails = sql.firstRow("""SELECT
                "user"."id",
                "user".email,
                "user".phone_number,
                "user".profile_image,
                country."name" AS country_name,
                "language"."name" AS language_name,
                "user".is_active,
                "user".username,
                "user".user_type_id,
                user_type."name" AS user_type_name,
                "location"."name" AS location_name,
                user_individual_detail.first_name,
                user_individual_detail.middle_name,
                user_individual_detail.last_name,
                user_gender."name" AS gender_name
                FROM
                "user",
                country,
                "language",
                "location",
                user_type,
                user_individual_detail,
                user_gender
                WHERE 
                "user".country_id = country."id" AND
                "user".language_preference = "language"."id" AND 
                "user".location_id = "location"."id" AND
                "user".user_type_id = user_type."id" AND
                user_individual_detail.user_id = "user"."id" AND
                user_individual_detail.gender_id = user_gender."id" AND
                "user".id = ?
                """, id);


        } else if (userType == 2) {
            userDetails = sql.firstRow("""SELECT
                    "user"."id",
                    "user".email,
                    "user".phone_number,
                    "user".profile_image,
                    country."name" AS country_name,
                    "language"."name" AS language_name,
                    "user".is_active,
                    "user".username,
                    "user".user_type_id,
                    user_type."name" AS user_type_name,
                    "location"."name" AS location_name,
                    user_organization_detail.organization_name,
                    user_organization_detail.postal_address,
                    user_organization_detail.physical_address,
                    user_organization_detail.website,
                    organization_type."name" AS organization_type,
                    organization_size."name" AS organization_size
                    FROM
                    "user" ,
                    country ,
                    "language" ,
                    "location" ,
                    user_organization_detail,
                    user_type,
                    organization_type,
                    organization_size
                    WHERE
                    "user".country_id = country."id" AND
                    "user".language_preference = "language"."id" AND
                    "user".location_id = "location"."id" AND
                    "user".user_type_id = user_type."id" AND
                    user_organization_detail.organization_type_id = organization_type."id" AND 
                    user_organization_detail.organization_size_id = "public".organization_type."id" AND
                    "user"."id" = ?
                """, id);

        }

        sql.close()
        return userDetails

    }

    Double getUserRating(Long userId) {
        Sql sql = Sql.newInstance(dataSource)
        def ratingResult = sql.firstRow(
                """SELECT AVG(rating)
                        FROM market_order_rating,
                             market_order,
                             rating_user_type,
                             product
                        WHERE market_order_rating.market_order_id = market_order.id
                          AND market_order.product_id = product.id
                          AND market_order_rating.rating_user_type_id = 1
                          AND product.trader_id = ?""", userId
        )
        def rating = ratingResult.get("avg")
        sql.close()
        return rating
    }


}
