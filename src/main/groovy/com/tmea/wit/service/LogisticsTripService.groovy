package com.tmea.wit.service

import com.tmea.wit.model.BaseApiResponse
import com.tmea.wit.model.GeneralApiListResponse
import com.tmea.wit.model.dto.FieldErrorDto
import com.tmea.wit.model.dto.response.ApiFetchLogisticsTripResponseDto
import com.tmea.wit.repository.UserRepository
import com.tmea.wit.util.CommonDBFunctions
import com.tmea.wit.util.ObjectUtils
import groovy.sql.Sql
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service

import javax.sql.DataSource

@Service
class LogisticsTripService {

    DataSource dataSource
    UserRepository userRepository
    CommonDBFunctions commonDBFunctions
    final String INITIAL_TRIP_STATUS = "scheduled";

    @Autowired
    LogisticsTripService(DataSource dataSource, UserRepository userRepository, CommonDBFunctions commonDBFunctions) {
        this.dataSource = dataSource
        this.userRepository = userRepository
        this.commonDBFunctions = commonDBFunctions
    }

    public BaseApiResponse createTrip(def body) {
        BaseApiResponse response = new BaseApiResponse();
        List<FieldErrorDto> errors = []
        Sql sql = new Sql(dataSource)
        def queryParams = ObjectUtils.asMap(body)
        Authentication auth = SecurityContextHolder.getContext().getAuthentication()
        def vehicleId = queryParams.get("vehicleId")
        def userId = userRepository.findUserIdByDetails(auth.getName())
        queryParams.put("userId", userId)
        //check if the vehicle id exists

        def vehicleExist = commonDBFunctions.getDBRecordByIdAndStatus(vehicleId, "logistics_trader_vehicle", "id")
        if (vehicleExist) {
            //check if vehicle is assigned
            def assigned = sql.firstRow("""select *  from logistics_trip where is_Active AND vehicle_id = ?.vehicleId ORDER BY  date_created desc""", queryParams)
            def statusName = ""
            if (assigned) {
                statusName = commonDBFunctions.getDBRecordByTableColumn(assigned.get("logistics_trip_status_id"), "logistics_trip_status", "id").get("name")
            }

            if (!assigned || (assigned && (statusName == "completed"||statusName == "cancelled"))) {
                def getStatus = commonDBFunctions.getDBRecordByTableColumn(INITIAL_TRIP_STATUS, "logistics_trip_status", "name")
                if (getStatus) {
                    def initialStatusId = getStatus.get("id")
                    queryParams.put("statusId", initialStatusId)
                    def insertTrip = sql.executeInsert("""INSERT INTO logistics_trip
                                                  ( vehicle_id, logistics_trip_status_id, created_by, date_created)
                                                  VALUES (?.vehicleId,?.statusId,?.userId,current_timestamp )""", queryParams)
                    if (insertTrip) {
                        def getInsertedRecord = commonDBFunctions.getDBRecordByTableColumn(insertTrip.get(0).get(0), "logistics_trip", "id")
                        response.setData(getInsertedRecord)
                        response.setMessage("Trip created successfully")
                        response.setStatus(HttpStatus.OK.value())
                    } else {
                        response.setMessage("record failed to insert")
                        response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value())
                    }
                } else {
                    response.setMessage("status " + INITIAL_TRIP_STATUS + " does not exists")
                    response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value())
                }

            } else {
                response.setMessage("vehicle " + vehicleExist.get("plate_number") + " is already assigned to an active trip")
                response.setStatus(HttpStatus.BAD_REQUEST.value())
            }
            //get status id
        } else {
            response.setMessage("vehicle does not exist")
            response.setStatus(HttpStatus.BAD_REQUEST.value())
        }
        sql.close()
        response

    }

    public GeneralApiListResponse getLogisticsTrip(Map parameterMap, Boolean provider) {
        Sql sql = new Sql(dataSource)
        GeneralApiListResponse response = new GeneralApiListResponse();
        List<FieldErrorDto> errors = []
        def params = ObjectUtils.flattenListParam(parameterMap)
        Authentication auth = SecurityContextHolder.getContext().getAuthentication()
        def userId = userRepository.findUserIdByDetails(auth.getName())
        def queryParams = [:]
        queryParams.put("userId", userId)
        def filterStatusQuery = ""
        def counterStatusQuery = ""
        def page = params.page
        def size = params.size
        def filterStatus = ""
        def isActive = params.get("isActive")
        def isActiveFilter=""
        if(isActive)
        {
            queryParams.put("isActive",Boolean.parseBoolean(isActive))
            isActiveFilter = " AND logistics_trip.is_active = ?.isActive"

        }
        def filterByProvider=""
        if(provider)
        {
            filterByProvider = " AND logistics_trip.created_by = ?.userId"
        }

        if (params.status) {
            String status = params.status.toLowerCase()

            if (status == 'scheduled' || status == 'dispatched' || status == 'completed') {
                filterStatus = commonDBFunctions.getDBRecordByTableColumn(status, "logistics_trip_status", "name").get("id")
                if (filterStatus) {
                    queryParams.put("statusId", filterStatus)
                    filterStatusQuery = " AND logistics_trip.logistics_trip_status_id = ?.statusId "
                }
            } else {
                response.setMessage("Invalid status value")
                response.setData(null)
                response.setStatus(HttpStatus.BAD_REQUEST.value())
                sql.close()
                return response
            }
        }
        Map paginateParams = [start: 0, limit: 25]
        def paginate = commonDBFunctions.paginationSqlParams(paginateParams, params)
               def paginationFilter = " LIMIT ?.limit OFFSET ?.start"
        if (page && size) {
            queryParams = queryParams + paginate

        }
        def countQuery = """ SELECT count(*)  FROM 
                                        logistics_trip,
                                        logistics_trip_status,
                                        logistics_trader_vehicle
                                       WHERE
                                       logistics_trip.logistics_trip_status_id = logistics_trip_status.id
                                        AND
                                       logistics_trip.vehicle_id = logistics_trader_vehicle.id  
                                       """
        def queryFilter=filterStatusQuery + isActiveFilter +filterByProvider
        def total =0
        if(queryFilter)
        {
           total = sql.firstRow(countQuery+queryFilter, queryParams)?.get('count')
        }
        else
        {
           total = sql.firstRow(countQuery)?.get('count')
        }
        def fetchLogisticsTripQuery = sql.rows(fetchLogisticStrip() + queryFilter + paginationFilter, queryParams)
        if (fetchLogisticsTripQuery) {
            List<ApiFetchLogisticsTripResponseDto> fetchLogisticsTripResponseDtoList;
            fetchLogisticsTripResponseDtoList = ObjectUtils.snakeCaseArrayMapToListPojos(fetchLogisticsTripQuery, ApiFetchLogisticsTripResponseDto);
            response.setData(fetchLogisticsTripResponseDtoList)
            response.setMessage("records found")
            response.setTotal(total)
            response.setStatus(HttpStatus.OK.value())
        } else {
            response.setData(null)
            response.setMessage("No " + params.status + " orders found")
            response.setStatus(HttpStatus.BAD_REQUEST.value())
        }

        sql.close()
        response

    }

    public BaseApiResponse getLogisticsTripById(Long Id) {
        Sql sql = new Sql(dataSource)
        BaseApiResponse response = new BaseApiResponse();
        List<FieldErrorDto> errors = []
        def tripId = Id
        Authentication auth = SecurityContextHolder.getContext().getAuthentication()
        def userId = userRepository.findUserIdByDetails(auth.getName())
        def queryParams = [:]
        queryParams.put("userId", userId)
        queryParams.put("tripId", tripId)
        def filterQuery = " AND logistics_trip.id = ?.tripId "
        def getTripQueryById = sql.firstRow(fetchLogisticStrip() + filterQuery, queryParams)
        if (getTripQueryById) {
            response.setData(getTripQueryById)
            response.setMessage("records found")
            response.setStatus(HttpStatus.OK.value())
        } else {
            response.setData(null)
            response.setMessage("No record  found")
            response.setStatus(HttpStatus.BAD_REQUEST.value())
        }


        sql.close()
        response
    }
    public BaseApiResponse cancelLogisticsTrip(def body) {
        Sql sql = new Sql(dataSource)
        BaseApiResponse response = new BaseApiResponse();
        List<FieldErrorDto> errors = []
        def queryParams = ObjectUtils.asMap(body)
        Authentication auth = SecurityContextHolder.getContext().getAuthentication()
        def userId = userRepository.findUserIdByDetails(auth.getName())
        def tripId = queryParams.get("tripId")
        queryParams.put("userId", userId)
        queryParams.put("tripId",tripId)

        //check if trip exist
        def tripExists = commonDBFunctions.getDBRecordByIdAndStatus(tripId,"logistics_trip","id")

        if(tripExists)
        {
            //check if trip is cancelled
            def currentTripStatus = sql.firstRow("""SELECT 
                                                 logistics_trip_status.name
                                                  FROM 
                                                  logistics_trip_status,
                                                  logistics_trip
                                                  WHERE logistics_trip.id= ?""",tripId).get("name")

            if(currentTripStatus =="scheduled")
            {
                def cancelledStatusId = commonDBFunctions.getDBRecordByTableColumn("cancelled","logistics_trip_status","name").get("id")
                queryParams.put("cancelledStatusId",cancelledStatusId)
                //update the status and de activate
                sql.withTransaction {
                    def cancelTrip = sql.executeUpdate("""UPDATE  logistics_trip SET is_active = false AND logistics_trip_status_id =?.cancelledStatusId  WHERE id = ?.tripId""",queryParams)

                    def logCancellation = sql.executeInsert("""INSERT INTO logistics_trip_cancellation 
                                                                 (logistics_trip_id, comment, date_cancelled, cancelled_by) 
                                                                 VALUES (?.tripId,?.comment,CURRENT_TIMESTAMP,?.userId)""",queryParams)
                    //cancel trip orders
                    def ordersToCancel = sql.rows("""SELECT * FROM logistics_trip_order WHERE is_active AND logistics_trip_id =?.tripId""",queryParams)

                    def batchUpdate=""
                    sql.withBatch { stmt ->
                        ordersToCancel.each {
                            Long logisticsTripId = it.get("logistics_trip_id");
                            stmt.addBatch("UPDATE logistics_trip_order SET is_active = FALSE WHERE  logistics_trip_id= $logisticsTripId")
                        }
                        stmt.executeBatch()
                    }
                    if (logCancellation) {
                        Map data = [:]
                        data.put("tripId",tripId)
                        response.setData(data)
                        response.setMessage("Trip cancelled successfully")
                        response.setStatus(HttpStatus.OK.value())
                    } else {
                        response.setMessage("Trip cancellation failed")
                        response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value())
                    }

                }

            }
            else
            {
                response.setStatus(HttpStatus.BAD_REQUEST.value())
                response.setMessage("The logistics trip is not scheduled")
            }

        }
        else
        {
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value())
            response.setMessage("Trip id "+tripId+" does not exist or has already been cancelled")
        }

        sql.close()
        response
    }

    public BaseApiResponse dispatchLogisticsTrip(Long Id) {
        Sql sql = new Sql(dataSource)
        BaseApiResponse response = new BaseApiResponse();
        List<FieldErrorDto> errors = []
        def tripId = Id
        Authentication auth = SecurityContextHolder.getContext().getAuthentication()
        def userId = userRepository.findUserIdByDetails(auth.getName())
        def queryParams = [:]
        queryParams.put("userId", userId)
        queryParams.put("tripId", Id)

        //check if trip exist
        def isScheduledTrip = scheduledTrip(queryParams)

        if(isScheduledTrip) {
            //check if trip is dispatched
            if (isScheduledTrip.get("status") != "scheduled") {
                response.setMessage("Trip is already " + isScheduledTrip.get("status") )
                response.setStatus(HttpStatus.BAD_REQUEST.value())
            }
            else
            {
            def batchUpdate=""
            sql.withTransaction {
                //update trip status
                def newTripStatusId = commonDBFunctions.getDBRecordByTableColumn("dispatched", "logistics_trip_status", "name").get("id")
                queryParams.put("newTripStatusId",newTripStatusId)

                def updateTripStatus = sql.executeUpdate("""UPDATE logistics_trip SET logistics_trip_status_id = ?.newTripStatusId WHERE id=?.tripId""", queryParams)

                //update logistics order status
                def newOrderNewLogisticsStatusId = commonDBFunctions.getDBRecordByTableColumn("dispatched", "logistics_service_order_status", "name").get("id")

                def logDispatch = sql.executeInsert("""INSERT INTO logistics_trip_dispatch 
                                                                 (logistics_trip_id, date_dispatched, dispatched_by) 
                                                                 VALUES (?.tripId,CURRENT_TIMESTAMP,?.userId)""",queryParams)
                def dispatchedOrders = sql.rows("""SELECT logistics_order_id  FROM logistics_trip_order WHERE logistics_trip_id = ?""", tripId)

                sql.withBatch { stmt ->
                    dispatchedOrders.each {
                        Long logisticsOrderId = it.get("logistics_order_id");
                        stmt.addBatch("UPDATE logistics_service_order SET logistics_service_order_status_id = $newOrderNewLogisticsStatusId WHERE id = $logisticsOrderId")
                    }
                    batchUpdate = stmt.executeBatch()
                }

                if (updateTripStatus && logDispatch && batchUpdate) {
                    Map data = [:]
                    data.put("tripId",Id)
                    response.setMessage("Trip dispatched successfully")
                    response.setStatus(HttpStatus.OK.value())
                } else {
                    response.setMessage("Trip dispatch failed")
                    response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value())
                }


            }
        }


            //update order status

        }else
        {
            response.setMessage("record does not exists or not scheduled")
            response.setStatus(HttpStatus.BAD_REQUEST.value())
        }


        sql.close()
        response
    }
    public BaseApiResponse completeLogisticsTrip(Long Id) {
        Sql sql = new Sql(dataSource)
        BaseApiResponse response = new BaseApiResponse();
        List<FieldErrorDto> errors = []
        def tripId = Id
        Authentication auth = SecurityContextHolder.getContext().getAuthentication()
        def userId = userRepository.findUserIdByDetails(auth.getName())
        def queryParams = [:]
        queryParams.put("userId", userId)
        queryParams.put("tripId", Id)

        def isScheduledTrip = scheduledTrip(queryParams)
        //check if trip id exist
        def tripExists

        if(isScheduledTrip) {
            //check if trip is dispatched
            if (isScheduledTrip.get("status") != "dispatched") {
                response.setMessage("Trip with status "+ isScheduledTrip.get("status") +  " cannot be completed" )
                response.setStatus(HttpStatus.BAD_REQUEST.value())
            }
            else
            {
                sql.withTransaction {
                    //get completed status id
                    def newTripStatusId = commonDBFunctions.getDBRecordByTableColumn("completed", "logistics_trip_status", "name").get("id")
                    queryParams.put("newTripStatusId",newTripStatusId)

                    //update status and deactivate the trip
                    def updateTripStatus = sql.executeUpdate("""UPDATE logistics_trip SET is_active=FALSE AND  logistics_trip_status_id =?.newTripStatusId WHERE id =?.tripId""",queryParams)


                    //update logistics order status
                    def NewLogisticsOrderStatusId = commonDBFunctions.getDBRecordByTableColumn("delivered", "logistics_service_order_status", "name").get("id")

                    def logCompletion = sql.executeInsert("""INSERT INTO logistics_trip_completion 
                                                                 (logistics_trip_id, date_completed,completed_by) 
                                                                 VALUES (?.tripId,CURRENT_TIMESTAMP,?.userId)""", queryParams)
                    def completedOrders = sql.rows("""SELECT logistics_order_id  FROM logistics_trip_order WHERE logistics_trip_id = ?""", tripId)
                    sql.withBatch { stmt ->
                        completedOrders.each {
                            Long logisticsOrderId = it.get("logistics_order_id");
                            stmt.addBatch("UPDATE logistics_service_order SET is_active=FALSE AND logistics_service_order_status_id = $NewLogisticsOrderStatusId WHERE id = $logisticsOrderId")
                        }
                        stmt.executeBatch()
                    }
                    if (updateTripStatus) {
                        Map data = [:]
                        data.put("tripId",Id)
                        response.setData(data)
                        response.setMessage("Trip completed successfully")
                        response.setStatus(HttpStatus.OK.value())
                    } else {
                        response.setMessage("Trip dispatch failed")
                        response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value())
                    }

                }
            }
        }
        else
        {
            response.setMessage("record does not exists or not dispatched")
            response.setStatus(HttpStatus.BAD_REQUEST.value())
        }
        sql.close()
        response
    }



    public String fetchLogisticStrip() {
        def getTrip = """             SELECT 
                                       logistics_trip.id,
                                       logistics_trip_status.name  AS status,
                                       logistics_trip.is_active,
                                        logistics_trader_vehicle.plate_number AS vehicle_plate_number
                                       FROM
                                       logistics_trip,
                                        logistics_trip_status,
                                        logistics_trader_vehicle
                                       WHERE
                                       logistics_trip.logistics_trip_status_id = logistics_trip_status.id
                                        AND
                                       logistics_trip.vehicle_id = logistics_trader_vehicle.id  
                                                                             
                                       """
    }
    public def scheduledTrip(Map queryParams) {
        Sql sql = new Sql(dataSource)
        def trip = sql.firstRow("""SELECT logistics_trip_status.name AS status
                FROM
                logistics_trip,
                logistics_trip_status
                WHERE
                logistics_trip.logistics_trip_status_id = logistics_trip_status.id
                AND
                logistics_trip.id = ?.tripId
                ORDER BY logistics_trip.date_created Desc """,queryParams)
        sql.close()
        trip
    }

}
