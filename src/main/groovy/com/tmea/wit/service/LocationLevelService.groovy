package com.tmea.wit.service

import com.tmea.wit.model.BaseApiResponse
import com.tmea.wit.model.GeneralApiListResponse
import com.tmea.wit.model.dto.FieldErrorDto
import com.tmea.wit.model.dto.response.ApiLocationLevelResponseDto
import com.tmea.wit.repository.UserRepository
import com.tmea.wit.util.CommonDBFunctions
import com.tmea.wit.util.ObjectUtils
import groovy.sql.Sql
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service

import javax.sql.DataSource

@Service
class LocationLevelService {
    DataSource dataSource
    UserRepository userRepository
    CommonDBFunctions commonDBFunctions

    @Autowired
    LocationLevelService(DataSource dataSource, UserRepository userRepository, CommonDBFunctions commonDBFunctions) {
        this.dataSource = dataSource
        this.userRepository = userRepository
        this.commonDBFunctions = commonDBFunctions
    }

    public BaseApiResponse addLocationLevel(def body){
        Sql sql = new Sql(dataSource)
        BaseApiResponse response = new BaseApiResponse()
        List<FieldErrorDto> errors = []
        Map requestParams = ObjectUtils.asMap(body)
        Authentication auth = SecurityContextHolder.getContext().getAuthentication()
        String userName = auth.getName()
        Long addedBy = userRepository.findUserIdByDetails(userName)
        if(addedBy){
            requestParams.put("addedBy", addedBy)
            def isLevelExist = commonDBFunctions.getDBRecordByTableColumn(requestParams.name, 'location_level', 'name')
            if(isLevelExist){
                response.status = HttpStatus.BAD_REQUEST.value()
                response.message = 'Location Level name ('+ requestParams.name +') already exist!'
            }else{
                def query = """
                            INSERT INTO location_level (name, parent_id, added_by)
                            VALUES (?.name, ?.parentId, ?.addedBy)
                        """
                def insertLocationLevel = sql.executeInsert(query, requestParams)
                if(insertLocationLevel){
                    Long levelId = insertLocationLevel.get(0).get(0)
                    def locationLevel = commonDBFunctions.getDBRecordById(levelId, 'location_level', 'id')
                    def data = ObjectUtils.snakeCaseMapToPojo(locationLevel, new ApiLocationLevelResponseDto())
                    response.data = data
                    response.message = "Success"
                    response.status = HttpStatus.OK.value()
                }else{
                    response.data = []
                    response.message = "Failed to add location level"
                    response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
                }
            }

        }else{
            errors.add(new FieldErrorDto("userId", "Failed to fetch user details of the user adding the location level"))
            response.data = []
            response.message = "Failed to add location level"
            response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
            response.errors = errors
        }

        response
    }

    public GeneralApiListResponse getLocationLevels(Map parameterMap){
        Sql sql = new Sql(dataSource)
        GeneralApiListResponse response
        List<ApiLocationLevelResponseDto> levelList = new ArrayList<>()
        List<FieldErrorDto> errors = []

        def params = ObjectUtils.flattenListParam(parameterMap)
        def status = params.status
        def sqlParams = [start: 0, limit: 25]
        if(status){
            def booleanStatus = CommonDBFunctions.getStatusAsBoolean(status)
            if(booleanStatus != null){
                sqlParams.status = booleanStatus
            }else{
                errors.add(new FieldErrorDto("status", "Invalid value passed. Check Documentation"))
                response = new GeneralApiListResponse([], 0, HttpStatus.BAD_REQUEST.value(), "Invalid status value", errors)
                return response
            }
        }

        //Pagination parameters & logic
        sqlParams = commonDBFunctions.paginationSqlParams(sqlParams, params)

        def query = "SELECT * FROM location_level"
        def countQuery = "SELECT count(*) FROM location_level"
        Map queryMap = CommonDBFunctions.addQueryFilters(status, query, countQuery )

        String filteredSelectQuery = queryMap.selectQuery
        String filteredCountQuery = queryMap.countQuery
        String queryFilter = queryMap.queryFilter

        def data = sql.rows(filteredSelectQuery, sqlParams)
        def total = commonDBFunctions.getTotalCountBasedOnFilters(queryFilter, filteredCountQuery, sqlParams)
        sql.close()
        data.each {
            def apiLevelResponse = ObjectUtils.snakeCaseMapToPojo(it, new ApiLocationLevelResponseDto()) as ApiLocationLevelResponseDto
            levelList.add(apiLevelResponse)
        }
        if(data){
            response = new GeneralApiListResponse(levelList, total as Long, HttpStatus.OK.value(), "Success")
        }else{
            response = new GeneralApiListResponse([], total as Long, HttpStatus.OK.value(), "No records found")
        }
        return response
    }

    public BaseApiResponse updateLocationLevel(Long levelId, def body){
        Sql sql = new Sql(dataSource)
        BaseApiResponse response = new BaseApiResponse([], 200, "")
        def params = ObjectUtils.asMap(body)
        params.id = levelId

        def isLevelExist = commonDBFunctions.getDBRecordById(levelId, 'location_level', 'id')
        if(isLevelExist){
            def update = sql.executeUpdate "UPDATE location_level set name = ?.name, parent_id = ?.parentId WHERE id = ?.id", params
            sql.close()

            if(update == 1){
                def locationLevel = commonDBFunctions.getDBRecordById(levelId, 'location_level', 'id')
                def message = "Success"
                def locationLevelResponse = ObjectUtils.snakeCaseMapToPojo(locationLevel, new ApiLocationLevelResponseDto())
                response.data = locationLevelResponse
                response.status = HttpStatus.OK.value()
                response.message = message
            }else{
                response.status = HttpStatus.BAD_REQUEST.value()
                response.message = "Update failed!!"
            }
        }else{
            response.status = HttpStatus.BAD_REQUEST.value()
            response.message = 'Location Level with id ('+ levelId +') does not exist!'
        }

        response
    }

    def BaseApiResponse updateLocationLevelStatus(long levelId, def body) {
        Sql sql = new Sql(dataSource)
        BaseApiResponse response = new BaseApiResponse([], 200, "")
        def params = ObjectUtils.asMap(body)
        params.id = levelId
        def newStatus = (params.status == 'ACTIVE' || params.status == 'active')
        params.status = newStatus

        def isLevelExist = commonDBFunctions.getDBRecordById(levelId, 'location_level', 'id')
        if(isLevelExist){
            def update = sql.executeUpdate "UPDATE location_level set is_active = ?.status WHERE id = ?.id", params
            sql.close()
            def statusMessage= params.status ? 'activated' : 'deactivated'

            if(update == 1){
                def locationLevel = commonDBFunctions.getDBRecordById(levelId, 'location_level', 'id')
                def message = "Location Level ("+ locationLevel.name+ ") " + statusMessage
                def locationLevelResponse = ObjectUtils.snakeCaseMapToPojo(locationLevel, new ApiLocationLevelResponseDto())
                response.data = locationLevelResponse
                response.status = HttpStatus.OK.value()
                response.message = message
            }else{
                response.status = HttpStatus.BAD_REQUEST.value()
                response.message = "Update failed!!"
            }
        }else{
            response.status = HttpStatus.BAD_REQUEST.value()
            response.message = 'Location Level with id ('+ levelId +') does not exist!'
        }

        response
    }
}
