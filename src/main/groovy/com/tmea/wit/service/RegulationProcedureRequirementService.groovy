package com.tmea.wit.service

import com.tmea.wit.model.BaseApiResponse
import com.tmea.wit.model.dto.FieldErrorDto
import com.tmea.wit.repository.UserRepository
import com.tmea.wit.util.CommonDBFunctions
import com.tmea.wit.util.ObjectUtils
import groovy.sql.Sql
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service

import javax.sql.DataSource

@Service
class RegulationProcedureRequirementService {
    DataSource dataSource
    UserRepository userRepository
    CommonDBFunctions commonDBFunctions

    @Autowired
    RegulationCategoryService(DataSource dataSource, UserRepository userRepository, CommonDBFunctions commonDBFunctions) {
        this.dataSource = dataSource
        this.userRepository = userRepository
        this.commonDBFunctions = commonDBFunctions
    }

    public BaseApiResponse addRegulationProcedureRequirement(def body, String userName) {
        Sql sql = new Sql(dataSource)
        BaseApiResponse response = new BaseApiResponse([], 400, "")

        List<FieldErrorDto> errors = []
        Map requestParams = ObjectUtils.asMap(body)
        Long addedBy = userRepository.findUserIdByDetails(userName);
        //check if added by exist
        if (addedBy) {
            requestParams.addedBy = addedBy
            def isRegulationProcedureRequirementTypeExist = commonDBFunctions.getDBRecordByIdAndStatus(requestParams.regulationProcedureRequirementType, "regulation_procedure_requirement_type", "id")
            if (isRegulationProcedureRequirementTypeExist) {

                def isRegulationProcedureExist = commonDBFunctions.getDBRecordByIdAndStatus(requestParams.regulationProcedure, "regulation_procedure", "id")
                if (isRegulationProcedureExist) {
                    //check if record exist
                    def recordExist = sql.firstRow("""SELECT * FROM  regulation_procedure_requirement 
                                                          WHERE regulation_procedure_id =?.regulationProcedure
                                                          AND regulation_procedure_requirement_type_id =?.regulationProcedureRequirementType""", requestParams)
                    if (!recordExist) {
                        def insertRecord = sql.executeInsert("""INSERT INTO regulation_procedure_requirement (regulation_procedure_requirement_type_id, is_active, added_by, text,"regulation_procedure_id ") 
                                                            VALUES (?.regulationProcedureRequirementType,TRUE,?.addedBy,?.text,?.regulationProcedure)""", requestParams)

                        if (insertRecord) {
                            def insertedRecord = sql.firstRow("""SELECT * FROM  regulation_procedure_requirement WHERE id =?""", insertRecord.get(0).get(0))
                            response.message = "Regulation Category added successfully"
                            response.status = HttpStatus.OK.value()
                            response.data = ObjectUtils.mapToSnakeCaseMap(insertedRecord)
                        } else {
                            response.message = "an error occurred while inserting record"
                            response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
                        }
                    } else {
                        def responseStatus = recordExist.is_active
                        def message = responseStatus ? 'regulation procedure requirement exist' : 'regulation procedure requirement exist but has been de activated'
                        response.data = ObjectUtils.mapToSnakeCaseMap(recordExist)
                        response.message = message
                    }


                } else {
                    response.message = "regulation procedure " + "'$requestParams.regulationProcedure'" + " does not exist"
                }

            } else {
                response.message = "Regulation Procedure Requirement type" + "'$requestParams.regulationProcedureRequirementType'" + " doest not exist"
            }

        } else {
            response.message = 'user does not exist'
        }
        sql.close()
        response
    }

    public BaseApiResponse editRegulationProcedureRequirement(def body, requirementId, String username) {
        Sql sql = Sql.newInstance(dataSource)
        BaseApiResponse response = new BaseApiResponse([], 400, "")
        Map requestParams = ObjectUtils.asMap(body)
        Long userId = userRepository.findUserIdByDetails(username);
        requestParams.id = requirementId
        List<FieldErrorDto> errorDto = new ArrayList<>()

        if (userId) {
            def isRequirementExist = commonDBFunctions.getDBRecordByIdAndStatus(requestParams.id, "regulation_procedure_requirement", "id")
            if (isRequirementExist) {
                if (requestParams) {
                    Map updateParams = ObjectUtils.rowAsMap(isRequirementExist)
                    updateParams = ObjectUtils.mapToSnakeCaseMap(updateParams) as Map
                    ObjectUtils.copyMapProperties(requestParams, updateParams)
                    def updateRecord = sql.executeUpdate("""UPDATE regulation_procedure_requirement SET 
                                                                "regulation_procedure_id "=?.regulationProcedure,
                                                                regulation_procedure_requirement_type_id =?.regulationProcedureRequirementType,
                                                                text=?.requirement""", requestParams)
                    if (updateRecord) {
                        def updatedRecord = sql.firstRow("""SELECT * FROM regulation_procedure_requirement WHERE id=?""", requestParams.id)
                        response.message = 'record updated successfully'
                        response.status = HttpStatus.OK.value()
                        response.data = ObjectUtils.mapToSnakeCaseMap(updatedRecord)
                    } else {
                        response.message = 'an error occurred while updating record'
                        response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
                    }

                } else {
                    response.message = 'cannot update an empty pay load'
                }
            } else {
                response.message = "requirement id (" + requirementId + ") does not exist or has been de activated"
            }

        } else {
            response.message = "could not update record.user does not exist"
        }
        sql.close()
        return response
    }
    public BaseApiResponse updateRegulationProcedureRequirementStatus(long requirementId, def body) {
        Sql sql = new Sql(dataSource)
        BaseApiResponse response = new BaseApiResponse([], 400, "")
        def params = ObjectUtils.asMap(body)
        params.requirementId = requirementId
        def newStatus = ""
        if (params.status.toLowerCase() == 'active') {
            newStatus = true;
        }
        if (params.status.toLowerCase() == 'inactive') {
            newStatus = false;
        }
        params.status = newStatus
        def statusMessage = params.status ? 'activated' : 'deactivated'

        def isRequirementExist = commonDBFunctions.getDBRecordById(requirementId, 'regulation_procedure_requirement', 'id')
        if(isRequirementExist)
        {
            def updateStatus = sql.executeUpdate("""UPDATE regulation_procedure_requirement SET is_active=?.status""",params)
            if (updateStatus) {
                    def updatedRecord = sql.firstRow("""SELECT * FROM regulation_procedure_requirement WHERE id=?""", requirementId)
                    response.message = "regulation procedure requirement ("+statusMessage+")  successfully"
                    response.status = HttpStatus.OK.value()
                    response.data = ObjectUtils.mapToSnakeCaseMap(updatedRecord)
                } else {
                    response.message = "an error occurred while updating record"
                    response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
                }

        }else{
            response.message ='Regulation procedure requirement id('+ requirementId + ') does not exist'

        }
        sql.close()
        return  response

    }
}
