package com.tmea.wit.service

import com.tmea.wit.model.BaseApiResponse
import com.tmea.wit.model.GeneralApiListResponse
import com.tmea.wit.model.dto.FieldErrorDto
import com.tmea.wit.model.dto.request.ApiTradeDocumentCategoryPostDto
import com.tmea.wit.model.dto.response.ApiTradeDocumentCategoryResponseDto
import com.tmea.wit.util.CommonDBFunctions
import com.tmea.wit.util.ObjectUtils
import groovy.sql.Sql
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service

import javax.sql.DataSource

@Service
class TradeDocumentCategoryService {
    DataSource dataSource
    CommonDBFunctions commonDBFunctions

    @Autowired
    UserService userService

    TradeDocumentCategoryService(DataSource dataSource, CommonDBFunctions commonDBFunctions) {
        this.dataSource = dataSource
        this.commonDBFunctions = commonDBFunctions
    }

    BaseApiResponse addCategory(ApiTradeDocumentCategoryPostDto body, String name) {
        Sql sql = Sql.newInstance(dataSource)
        BaseApiResponse response = new BaseApiResponse()
        Map params = ObjectUtils.asMap(body)
        List<FieldErrorDto> errors = new ArrayList<>()

        Long userId = userService.getUserIdFromUsername(name)
        params.put("userId", userId)

        if (userId) {
            def isNameExisting = getCategoryByName(params.get("name") as String)

            if (!isNameExisting) {
                def insert = sql.executeInsert(
                        """INSERT INTO trade_document_category(name, added_by, date_added) 
                                    VALUES (?.name, ?.userId, current_timestamp)""", params
                )

                if (insert) {
                    Long categoryId = insert.get(0).get(0) as long

                    def category = getCategoryById(categoryId)
                    def data = ObjectUtils.snakeCaseMapToPojo(category, new ApiTradeDocumentCategoryResponseDto())

                    response.status = HttpStatus.OK.value()
                    response.message = "Trade document category added successfully"
                    response.data = data
                } else {
                    response.status = HttpStatus.BAD_REQUEST.value()
                    response.message = "Failed to add new trade document category"
                }
            } else {
                response.status = HttpStatus.BAD_REQUEST.value()
                response.message = "A record with the same name exist. Do you wish to update?"
            }
        } else {
            errors.add(new FieldErrorDto("userId", "Failed to fetch user details"))
            response.data = null
            response.message = "Failed to add trade document category"
            response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
            response.errors = errors
        }

        sql.close()
        return response
    }

    BaseApiResponse editCategory(ApiTradeDocumentCategoryPostDto body, Long id, String name) {
        Sql sql = Sql.newInstance(dataSource)
        BaseApiResponse response = new BaseApiResponse()
        Map sqlParams = ObjectUtils.asMap(body)
        sqlParams.put("id", id)

        List<FieldErrorDto> errors = new ArrayList<>()
        Long userId = userService.getUserIdFromUsername(name)

        if (userId) {
            def recordExist = getCategoryById(id)

            if (recordExist) {
                def affectedRow = sql.executeUpdate(
                        """UPDATE trade_document_category SET name = ?.name WHERE id = ?.id""", sqlParams
                )

                if (affectedRow == 1) {
                    def category = getCategoryById(id)
                    def data = ObjectUtils.snakeCaseMapToPojo(category, new ApiTradeDocumentCategoryResponseDto())

                    response.status = HttpStatus.OK.value()
                    response.data = data
                    response.message = "Record edited successfully"
                } else {
                    response.status = HttpStatus.BAD_REQUEST.value()
                    response.message = "Operation failed"
                }
            } else {
                response.status = HttpStatus.NOT_FOUND.value()
                response.message = "Record not found"
            }
        } else {
            errors.add(new FieldErrorDto("userId", "Failed to fetch user details"))
            response.data = null
            response.message = "Failed to edit trade document category"
            response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
            response.errors = errors
        }

        sql.close()
        return response
    }

    GeneralApiListResponse fetchAllCategories(Map parameterMap) {
        Sql sql = Sql.newInstance(dataSource)
        GeneralApiListResponse response = new GeneralApiListResponse()

        def params = ObjectUtils.flattenListParam(parameterMap)
        Map requestParams = [start: 0, limit: 25]

        def query = params.get("query")
        def queryFilter = ""
        if (query) {
            String formattedQuery = "%$query%"
            queryFilter = """ WHERE trade_document_category.name ILIKE ?.query """
            requestParams.put("query", formattedQuery)
        }

        def selectQuery = """SELECT id, name, date_added FROM trade_document_category """
        def countQuery = """SELECT COUNT(1) FROM trade_document_category """

        requestParams = commonDBFunctions.paginationSqlParams(requestParams, params)

        String limitClause = " limit ?.limit offset ?.start"
        String queryString = queryFilter + limitClause
        def filterQuery = selectQuery + queryString
        def count = countQuery + queryString

        def data = sql.rows(filterQuery, requestParams);
        def total

        if (filterQuery) {
            total = sql.firstRow(count, requestParams).get("count")
        } else {
            total = sql.firstRow(count).get("count")
        }

        if (data) {
            List<ApiTradeDocumentCategoryResponseDto> responseDto = ObjectUtils.snakeCaseArrayMapToListPojos(data, ApiTradeDocumentCategoryResponseDto)

            response.status = HttpStatus.OK.value()
            response.data = data
            response.total = total
            response.message = "Success"
        } else {
            response.message = "No records found"
        }

        sql.close()
        return response
    }

    Map getCategoryByName(String name) {
        Sql sql = Sql.newInstance(dataSource)

        def category = sql.firstRow(
                """SELECT * FROM trade_document_category WHERE name ILIKE ?""", name
        )

        sql.close()
        return category
    }

    Map getCategoryById(Long id) {
        Sql sql = Sql.newInstance(dataSource)

        def category = sql.firstRow(
                """SELECT id, name, date_added FROM trade_document_category WHERE id = ?""", id
        )

        sql.close()
        return category
    }
}
