package com.tmea.wit.service

import com.tmea.wit.model.BaseApiResponse
import com.tmea.wit.model.dto.FieldErrorDto
import com.tmea.wit.model.dto.request.ApiUnapprovedProductPostDto
import com.tmea.wit.model.dto.response.ApiProductDetailsResponseDto
import com.tmea.wit.model.dto.response.ApiUserIndividualResponseDto
import com.tmea.wit.model.dto.response.ApiUserOrganizationResponseDto
import com.tmea.wit.util.CommonDBFunctions
import com.tmea.wit.util.ObjectUtils
import groovy.sql.Sql
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service

import javax.sql.DataSource
import java.math.RoundingMode

@Service
class ProductApprovalService {
    private DataSource dataSource
    CommonDBFunctions commonDBFunctions

    @Autowired
    ProductService productService
    @Autowired
    GlobalConfigService globalConfigService
    @Autowired
    UserService userService

    @Autowired
    ProductApprovalService(DataSource dataSource, CommonDBFunctions commonDBFunctions) {
        this.dataSource = dataSource
        this.commonDBFunctions = commonDBFunctions
    }


    BaseApiResponse approveProduct(List<ApiUnapprovedProductPostDto> apiProducts, String name) {
        BaseApiResponse response = new BaseApiResponse()
        Sql sql = Sql.newInstance(dataSource)
        Long userId = userService.getUserIdFromUsername(name)

        List<FieldErrorDto> errorList = new ArrayList<>()

        if (userId) {
            def updateQuery = """UPDATE product SET is_approved = true, is_active = true WHERE id =  ?"""
            def query = sql.withBatch(updateQuery) { stmt ->
                apiProducts.each {
                    Long productId = it.getProperty("id");

                    stmt.addBatch(productId)
                }
            }

            if (query.size() > 0) {
                def msg = query.size() > 1 ? " product " : " products "
                response.status = HttpStatus.OK.value()
                response.message = query.size() + msg + "approved successfully"

            } else {
                response.message = "Failed to approve products"
                response.status = HttpStatus.BAD_REQUEST.value()
            }
        } else {
            errorList.add(new FieldErrorDto("userId", "Failed to fetch user details"))
            response.data = null
            response.message = "Failed to approve product!"
            response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
            response.errors = errorList
        }

        sql.close()
        return response
    }

    BaseApiResponse getProductDetails(Long id) {
        Sql sql = Sql.newInstance(dataSource)
        BaseApiResponse response = new BaseApiResponse()

        def product = productService.getProductDetailsById(id)

        if (product) {
            BigDecimal productPrice = product.get("price")
            product.replace("price", (productPrice.setScale(2, RoundingMode.CEILING)))
            ApiProductDetailsResponseDto productDetailsResponseDto = ObjectUtils.snakeCaseMapToPojo(product, new ApiProductDetailsResponseDto()) as ApiProductDetailsResponseDto

            response.status = HttpStatus.OK.value()
            response.data = productDetailsResponseDto
            response.message = "Success"
        } else {
            response.message = "Record not found"
            response.status = HttpStatus.BAD_REQUEST.value()
        }

        sql.close()
        return response
    }


}
