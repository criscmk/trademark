package com.tmea.wit.service

import com.tmea.wit.model.BaseApiResponse
import com.tmea.wit.model.dto.FieldErrorDto
import com.tmea.wit.model.dto.request.ApiEditImportExportPostDto
import com.tmea.wit.model.dto.request.ApiEditTaxInfoPostDto
import com.tmea.wit.model.dto.request.ApiStatusRequestDto
import com.tmea.wit.model.dto.request.ApiTaxInfoPostDto
import com.tmea.wit.util.CommonDBFunctions
import com.tmea.wit.util.ObjectUtils
import groovy.sql.Sql
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service

import javax.sql.DataSource

@Service
class TaxInformationService {
    DataSource dataSource
    CommonDBFunctions commonDBFunctions

    @Autowired
    UserService userService

    TaxInformationService(DataSource dataSource, CommonDBFunctions commonDBFunctions) {
        this.dataSource = dataSource
        this.commonDBFunctions = commonDBFunctions
    }

    BaseApiResponse addTaxInfo(ApiTaxInfoPostDto body, String name) {
        Sql sql = Sql.newInstance(dataSource)
        BaseApiResponse response = new BaseApiResponse()
        Map sqlParams = ObjectUtils.asMap(body)

        Long userId = userService.getUserIdFromUsername(name)
        sqlParams.put("addedBy", userId)

        if (userId) {
            def recordExists = sql.firstRow(
                    """SELECT * FROM tax_information WHERE tax_id = ?.taxId""", sqlParams
            )

            if (!recordExists) {
                def insertQuery = sql.executeInsert(
                        """INSERT INTO tax_information(tax_id, description, basis_of_charge, taxable_good, rate, exemption, additional_information, regulatory_agency_id, added_by) 
                                VALUES(?.taxId, ?.description, ?.basisOfCharge, ?.taxableGood, ?.rate, ?.exemption, ?.additionalInformation, ?.regulatoryAgency, ?.addedBy) """, sqlParams
                )

                if (insertQuery) {
                    long taxInfoId = insertQuery.get(0).get(0)

                    def taxInfo = sql.firstRow("""SELECT * FROM tax_information WHERE id = ?""", taxInfoId)
                    def data = ObjectUtils.mapToSnakeCaseMap(taxInfo)

                    response.status = HttpStatus.OK.value()
                    response.message = "Success"
                    response.data = data
                } else {
                    response.status = HttpStatus.BAD_REQUEST.value()
                    response.message = "Failed to add new tax information"
                }

            } else {
                response.status = HttpStatus.BAD_REQUEST.value()
                response.message = "Similar record already exists"
            }

        } else {
            List<FieldErrorDto> errors = new ArrayList<>()
            errors.add(new FieldErrorDto("userId", "Failed to fetch user details"))
            response.message = "Failed to add tax information"
            response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
            response.errors = errors
        }


        sql.close()
        return response
    }

    BaseApiResponse editTaxInfo(Long id, ApiEditTaxInfoPostDto body, String name) {
        Sql sql = Sql.newInstance(dataSource)
        BaseApiResponse response = new BaseApiResponse()
        Map editParams = ObjectUtils.asMap(body)

        Long userId = userService.getUserIdFromUsername(name)
        editParams.put("id", id)

        if (userId) {
            def isRecordExist = commonDBFunctions.getDBRecordById(id, "tax_information", "id")

            if (isRecordExist) {
                Map updateParams = ObjectUtils.rowAsMap(isRecordExist)
                updateParams = ObjectUtils.mapToSnakeCaseMap(updateParams) as Map
                ObjectUtils.copyMapProperties(editParams, updateParams)

                def updateQuery = sql.executeUpdate(
                        """UPDATE tax_information
                                SET description = ?.description,
                                basis_of_charge = ?.basisOfCharge,
                                taxable_good = ?.taxableGood,
                                    rate = ?.rate,
                                    exemption = ?.exemption,
                                    additional_information = ?.additionalInformation,
                                    regulatory_agency_id = ?.regulatoryAgencyId
                                WHERE id = ?.id""", updateParams
                )

                if (updateQuery == 1) {
                    def editedLocation = commonDBFunctions.getDBRecordById(id, "tax_information", "id")

                    def data = ObjectUtils.mapToSnakeCaseMap(editedLocation)

                    response.status = HttpStatus.OK.value()
                    response.data = data
                    response.message = updateQuery + " record edited successfully"
                } else {
                    response.status = HttpStatus.BAD_REQUEST.value()
                    response.message = "Failed to edit record"
                }

            } else {
                response.status = HttpStatus.NOT_FOUND.value()
                response.message = "Record not found"
            }

        } else {
            List<FieldErrorDto> errors = new ArrayList<>()
            errors.add(new FieldErrorDto("userId", "Failed to fetch user details of the user editing the location"))
            response.status = HttpStatus.BAD_REQUEST.value()
            response.message = "Failed to edit tax information"
        }

        sql.close()
        return response

    }

    BaseApiResponse activateDeactivateTaxInfo(ApiStatusRequestDto body, Long taxInfoId, String name) {
        Sql sql = Sql.newInstance(dataSource)
        BaseApiResponse response = new BaseApiResponse()
        Map params = ObjectUtils.asMap(body);

        Long user = userService.getUserIdFromUsername(name)
        def statusMsg
        if (user) {
            def taxInfoRecord = commonDBFunctions.getDBRecordById(taxInfoId, "tax_information", "id")

            if (taxInfoRecord) {
                def status = CommonDBFunctions.getStatusAsBoolean(params.get("status"))

                params.replace("status", status)
                params.put("id", taxInfoId)

                statusMsg = status ? "activated" : "deactivated"

                def affectedRow = sql.executeUpdate(
                        """Update tax_information 
                                SET is_active = ?.status WHERE id = ?.id""", params
                )


                if (affectedRow == 1) {
                    def record = sql.firstRow("""SELECT tax.name, tax_information.description, tax_information.is_active
                                                    FROM tax_information, tax WHERE tax_information.id = ?""", taxInfoId)

                    def data = ObjectUtils.mapToSnakeCaseMap(record)

                    response.status = HttpStatus.OK.value()
                    response.message = "Record " + statusMsg + " successfully"
                    response.data = data
                } else {
                    response.status = HttpStatus.BAD_REQUEST.value()
                    response.message = "Failed to " + statusMsg + " record"
                }
            } else {
                response.status = HttpStatus.BAD_REQUEST.value()
                response.message = "Record not found"
            }

        } else {
            List<FieldErrorDto> errors = new ArrayList<>()
            errors.add(new FieldErrorDto("userId", "Failed to fetch user details"))
            response.message = "Failed to " + statusMsg + " tax information"
            response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
            response.errors = errors
        }

        sql.close()
        return response
    }

    BaseApiResponse getTaxInfo(Long taxId, boolean isAdmin){
        Sql sql = Sql.newInstance(dataSource)
        BaseApiResponse response = new BaseApiResponse()

        def selectQuery = sql.firstRow("""
                            SELECT tax.name,
                                   tax_information.id,
                                   tax_information.description,
                                   tax_information.basis_of_charge,
                                   tax_information.rate,
                                   tax_information.exemption,
                                   tax_information.taxable_good,
                                   tax_information.additional_information,
                                   regulatory_agency.name,
                                   tax_information.is_active,
                                   tax_information.added_by,
                                   tax_information.date_added
                            FROM tax,
                                 tax_information,
                                 regulatory_agency
                            WHERE tax_information.tax_id = tax.id
                                AND tax_information.regulatory_agency_id = regulatory_agency.id
                                AND tax_information.tax_id = ?
                            """, taxId)
        if (!isAdmin){
            selectQuery = sql.firstRow("""
                            SELECT tax.name,
                                   tax_information.id,
                                   tax_information.description,
                                   tax_information.basis_of_charge,
                                   tax_information.rate,
                                   tax_information.exemption,
                                   tax_information.taxable_good,
                                   tax_information.additional_information,
                                   regulatory_agency.name
                            FROM tax,
                                 tax_information,
                                 regulatory_agency
                            WHERE tax_information.tax_id = tax.id
                                AND tax_information.regulatory_agency_id = regulatory_agency.id
                                AND tax_information.is_active
                                AND tax_information.tax_id = ?
                            """, taxId)
        }

        if (selectQuery){
            def data = ObjectUtils.asMap(selectQuery)

            response.status = HttpStatus.OK.value()
            response.message = "Success"
            response.data = data
        } else {
            response.status = HttpStatus.NOT_FOUND.value()
            response.message = "Record not found"
        }


        sql.close()
        return response
    }
}
