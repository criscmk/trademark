package com.tmea.wit.service

import com.tmea.wit.model.BaseApiResponse
import com.tmea.wit.model.GeneralApiListResponse
import com.tmea.wit.model.dto.FieldErrorDto
import com.tmea.wit.model.dto.response.ApiCommodityResponseDto
import com.tmea.wit.model.dto.response.ApiLocationResponseDto
import com.tmea.wit.model.dto.response.ApiLogisticsVehiclesResponseDto
import com.tmea.wit.repository.UserRepository
import com.tmea.wit.util.CommonDBFunctions
import com.tmea.wit.util.ObjectUtils
import groovy.sql.Sql
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service

import javax.sql.DataSource

@Service
class LogisticsVehicleService {
    DataSource dataSource
    UserRepository userRepository
    CommonDBFunctions commonDBFunctions

    @Autowired
    LogisticsVehicleService(DataSource dataSource, UserRepository userRepository, CommonDBFunctions commonDBFunctions) {
        this.dataSource = dataSource
        this.userRepository = userRepository
        this.commonDBFunctions = commonDBFunctions
    }

    public BaseApiResponse addVehicle(def body) {
        BaseApiResponse response = new BaseApiResponse();
        List<FieldErrorDto> errors = []
        Sql sql = new Sql(dataSource)
        def params = ObjectUtils.asMap(body)
        Authentication auth = SecurityContextHolder.getContext().getAuthentication()
        def userId = userRepository.findUserIdByDetails(auth.getName())
        if (userId) {
            def vehicleTypeExists = sql.firstRow("""SELECT  id FROM  logistics_vehicle_type WHERE  id=?""", params.get("logisticsVehicleType"))
            if (vehicleTypeExists){
                def isPlateNumberExist = getVehicleByPlateNumber(params.plateNumber)
                if (isPlateNumberExist) {
                    response.setStatus(HttpStatus.BAD_REQUEST.value())
                    response.setMessage('Location name (' + params.plateNumber + ') already exist!')
                    return  response
                } else {

                    params.put("userId", userId);
                    params.put("isActive", true);

                    def query = """
                               INSERT INTO logistics_trader_vehicle (plate_number,description,logistics_vehicle_type_id ,is_active,provider_user_id)
                                VALUES (?.plateNumber, ?.description,?.logisticsVehicleType, ?.isActive, ?.userId) 
                            """
                    def insertLogisticsVehicle = sql.executeInsert query, params
                    if (insertLogisticsVehicle) {
                        Long vehicleId = insertLogisticsVehicle?.get(0)?.get(0)
                        def vehicle = getVehicleById(vehicleId)
                        def locationResponse = ObjectUtils.snakeCaseMapToPojo(vehicle, new ApiLogisticsVehiclesResponseDto())
                        response.status = HttpStatus.OK.value()
                        response.setData(locationResponse);
                        response.setMessage("Record inserted successfully")

                    } else {
                        response.setData(null)
                        response.setMessage("Failed to add vehicle")
                        response.setMessage(HttpStatus.INTERNAL_SERVER_ERROR.value())
                    }

                }
            }else
            {
                response.setStatus(HttpStatus.BAD_REQUEST.value())
                response.message = "Logistics vehicle type does not exists"
            }

        }
        return response
        sql.close()
    }

    public def getVehicleById(Long vehicleId) {
        Sql sql = new Sql(dataSource)
        def vehicle = sql.firstRow "SELECT * FROM logistics_trader_vehicle WHERE id = ?", vehicleId
        sql.close()
        vehicle
    }

    public def getVehicleByPlateNumber(String vehiclePlateNumber) {
        Sql sql = new Sql(dataSource)
        def vehicle = sql.firstRow "SELECT * FROM logistics_trader_vehicle WHERE plate_number = ?", vehiclePlateNumber
        sql.close()
        vehicle
    }
    //get all vehicles
    public GeneralApiListResponse getVehicles(Map parameterMap, boolean provider) {
        Sql sql = new Sql(dataSource)
        GeneralApiListResponse response = new GeneralApiListResponse();
        List<ApiLocationResponseDto> locationList = new ArrayList<>()
        List<FieldErrorDto> errors = []
        def params = ObjectUtils.flattenListParam(parameterMap)
        Authentication auth = SecurityContextHolder.getContext().getAuthentication()
        def userId = userRepository.findUserIdByDetails(auth.getName())
        def page = (params.page)
        def  size = (params.size)
        def filterStatus = params.status
        def queryParams = params
        def vehicleTypeFilter = ""
        queryParams.put("userId",userId)
        def filterStatusQuery=""
        def query = ""
        def status =""
        def searchFilter = ""
        if (filterStatus) {
            status = filterStatus.toLowerCase()
            if (status == 'active') {

                queryParams.put("status", true)
            } else if (params.status == 'inactive' ) {
                queryParams.put("status", false)
            } else {
                response.setMessage("Invalid status value")
                response.setData(null)
                response.setStatus(HttpStatus.BAD_REQUEST.value())
                return response
            }

            filterStatusQuery = " AND  logistics_trader_vehicle.is_active = ?.status "
        }
        def providerFilter=""
        if(provider)
        {
            providerFilter= " AND logistics_trader_vehicle.provider_user_id = ?.userId"
        }
        if(params.vehicleTypeId)
        {
            vehicleTypeFilter= " AND logistics_trader_vehicle.logistics_vehicle_type_id = ?.vehicleTypeId::BigInt"
        }
        if(params.query)
        {
            String querySearch = "%$params.query%"
            queryParams.put("query",querySearch)
            searchFilter = """ AND (
                                    logistics_trader_vehicle.plate_number ILIKE ?.query OR
                                    logistics_trader_vehicle.description ILIKE ?.query 
                                    
                                  )
                          """
        }
        if(!provider && params.providerId)
        {
            providerFilter= " AND logistics_trader_vehicle.provider_user_id = ?.providerId::BigInt"
        }

        //pagination
        def paginateQuery=""
        def paginate=""

        Map paginateParams = [start: 0, limit: 25]
        paginate = commonDBFunctions.paginationSqlParams(paginateParams, params)
        paginateQuery = " LIMIT ?.limit OFFSET ?.start "
        queryParams = queryParams + paginate


        def filterQuery = filterStatusQuery +providerFilter + vehicleTypeFilter + searchFilter


        query = """SELECT 
                   logistics_trader_vehicle.id,
                   logistics_trader_vehicle.plate_number,
                   logistics_trader_vehicle.description,
                   logistics_trader_vehicle.is_active,
                   logistics_trader_vehicle.provider_user_id,
                   logistics_vehicle_type.id AS logistics_vehicle_type_id,
                   logistics_vehicle_type.name AS logistics_vehicle_type,
                   "user".phone_number AS provider_phone,
                   "user".email AS provider_email,
                    CASE
                                 WHEN "user".user_type_id = 1 THEN (SELECT user_individual_detail.first_name||' '||user_individual_detail.middle_name||' '||user_individual_detail.last_name  FROM user_individual_detail WHERE "user".id = user_individual_detail.user_id)
                                 WHEN "user".user_type_id = 2 THEN (SELECT user_organization_detail.organization_name FROM user_organization_detail WHERE "user".id = user_organization_detail.user_id)
                                 END AS provider_name  
                   FROM 
                   logistics_trader_vehicle,
                   logistics_vehicle_type,
                   "user"
                   WHERE 
                   logistics_trader_vehicle.logistics_vehicle_type_id = logistics_vehicle_type.id 
                   AND
                   logistics_trader_vehicle.provider_user_id = "user".id
                 
                 """ + filterQuery+ " ORDER BY logistics_trader_vehicle.id DESC"+ paginateQuery


        String countQuery = """SELECT count(*)   FROM 
                   logistics_trader_vehicle,
                   logistics_vehicle_type,
                    user_individual_detail,
                    "user"
                   WHERE 
                   logistics_trader_vehicle.logistics_vehicle_type_id = logistics_vehicle_type.id
                   AND "user".id = logistics_trader_vehicle.provider_user_id 
                   AND "user".id = user_individual_detail.user_id """ + filterStatusQuery + providerFilter
        def total = 0
        if(filterQuery){
            total = sql.firstRow(countQuery +filterQuery, queryParams)?.get('count')

        }else{
            total = sql.firstRow(countQuery)?.get('count')
        }

        def data = sql.rows(query, queryParams)
        if (data) {
            response.setMessage("records found")
            response.setData(ObjectUtils.snakeCaseArrayMapToListPojos(data, ApiLogisticsVehiclesResponseDto))
            response.setTotal(total)
            response.setStatus(HttpStatus.OK.value())
        } else {
            response.setMessage("No records found")
            response.setStatus(HttpStatus.NO_CONTENT.value())
        }


        return response


    }

    //update vehicle details

    public BaseApiResponse updateVehicle(long vehicleId, def body) {
        Sql sql = new Sql(dataSource)
        BaseApiResponse response = new BaseApiResponse(null, 200, "")
        def params = ObjectUtils.asMap(body)
        params.id = vehicleId

        def isVehicleExist = getVehicleById(vehicleId)
        if (isVehicleExist) {
            def query = """
                            UPDATE logistics_trader_vehicle set plate_number = ?.plateNumber, 
                            description = ?.description, logistics_vehicle_type_id = ?.logisticsVehicleType
                             WHERE id = ?.id
                        """
            def update = sql.executeUpdate query, params

            if (update) {
                def vehicle = getVehicleById(vehicleId)
                def message = "Success"
                def vehicleResponse = ObjectUtils.snakeCaseMapToPojo(vehicle, new ApiLogisticsVehiclesResponseDto())
                response.data = vehicleResponse
                response.status = HttpStatus.OK.value()
                response.message = message
            } else {
                response.status = HttpStatus.BAD_REQUEST.value()
                response.message = "Update failed!!"
            }

        } else {
            response.status = HttpStatus.BAD_REQUEST.value()
            response.message = 'Vehicle with id (' + vehicleId + ') does not exist!'
        }
        sql.close()
        response
    }

    public BaseApiResponse updateVehicleStatus(def body, Long vehicleId) {
        BaseApiResponse response = new BaseApiResponse(null, 200, "")
        Sql sql = new Sql(dataSource)
        def params = ObjectUtils.asMap(body)
        params.id = vehicleId
        def newStatus;
        if (params.status == 'ACTIVE' || params.status == 'active') {
            newStatus = true;
        }
        if (params.status == 'INACTIVE' || params.status == 'inactive') {
            newStatus = false;
        }
        params.status = newStatus

        def isVehicleExist = getVehicleById(vehicleId)
        if (isVehicleExist) {
            def update = sql.executeUpdate "UPDATE logistics_trader_vehicle set is_active = ?.status WHERE id = ?.id", params
            sql.close()
            def vehicleName = getVehicleById(vehicleId)
            def statusMessage = newStatus ? 'activated' : 'deactivated'
            response.message = "Vehicle  (" + vehicleName + ") " + statusMessage

            if (update) {
                def vehicle = getVehicleById(vehicleId)
                def message = "Vehicle (" + vehicle.plate_number + ") " + statusMessage
                def vehicleResponse = ObjectUtils.snakeCaseMapToPojo(vehicle, new ApiLogisticsVehiclesResponseDto())
                response.data = vehicleResponse
                response.status = HttpStatus.OK.value()
                response.message = message
            } else {
                response.status = HttpStatus.BAD_REQUEST.value()
                response.message = "Update failed!!"
            }
        } else {
            response.status = HttpStatus.BAD_REQUEST.value()
            response.message = 'vehicle with id (' + vehicleId + ') does not exist!'
        }

        response

    }


}
