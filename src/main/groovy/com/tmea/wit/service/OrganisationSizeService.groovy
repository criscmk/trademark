package com.tmea.wit.service

import com.tmea.wit.model.BaseApiResponse
import com.tmea.wit.model.GeneralApiListResponse
import com.tmea.wit.model.dto.FieldErrorDto
import com.tmea.wit.model.dto.response.ApiOrganisationSizeResponseDto
import com.tmea.wit.util.CommonDBFunctions
import com.tmea.wit.util.ObjectUtils
import groovy.sql.Sql
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service

import javax.sql.DataSource

@Service
class OrganisationSizeService {
    DataSource dataSource
    CommonDBFunctions commonDBFunctions

    @Autowired
    OrganisationSizeService(DataSource dataSource, CommonDBFunctions commonDBFunctions) {
        this.dataSource = dataSource
        this.commonDBFunctions = commonDBFunctions
    }

    public GeneralApiListResponse getOrganisationSize(Map parameterMap){
        Sql sql = new Sql(dataSource)
        GeneralApiListResponse response
        List<ApiOrganisationSizeResponseDto> organizationSizeList = new ArrayList<>()
        List<FieldErrorDto> errors = []

        def params = ObjectUtils.flattenListParam(parameterMap)
        def status = params.status
        def sqlParams = [start: 0, limit: 25]
        if(status){
            def booleanStatus = CommonDBFunctions.getStatusAsBoolean(status)
            if(booleanStatus != null){
                sqlParams.status = booleanStatus
            }else{
                errors.add(new FieldErrorDto("status", "Invalid value passed. Check Documentation"))
                response = new GeneralApiListResponse([], 0, HttpStatus.BAD_REQUEST.value(), "Invalid status value", errors)
                return response
            }
        }

        //Pagination parameters & logic
        sqlParams = commonDBFunctions.paginationSqlParams(sqlParams, params)

        def query = "SELECT * FROM organization_size"
        def countQuery = "SELECT count(*) FROM organization_size"
        Map queryMap = CommonDBFunctions.addQueryFilters(status, query, countQuery )

        String filteredSelectQuery = queryMap.selectQuery
        String filteredCountQuery = queryMap.countQuery
        String queryFilter = queryMap.queryFilter

        def data = sql.rows(filteredSelectQuery, sqlParams)
        def total = commonDBFunctions.getTotalCountBasedOnFilters(queryFilter, filteredCountQuery, sqlParams)
        sql.close()
        data.each {
            def apiOrganisationSizeResponse = ObjectUtils.snakeCaseMapToPojo(it, new ApiOrganisationSizeResponseDto()) as ApiOrganisationSizeResponseDto
            organizationSizeList.add(apiOrganisationSizeResponse)
        }
        if(data){
            response = new GeneralApiListResponse(organizationSizeList, total as Long, HttpStatus.OK.value(), "Success")
        }else{
            response = new GeneralApiListResponse([], total as Long, HttpStatus.OK.value(), "No records found")
        }
        return response
    }

    public BaseApiResponse updateOrganisationSizeStatus(Long organisationSizeId, def body){
        Sql sql = new Sql(dataSource)
        BaseApiResponse response = new BaseApiResponse([], 200, "")
        def params = ObjectUtils.asMap(body)
        params.id = organisationSizeId
        def newStatus = (params.status == 'ACTIVE' || params.status == 'active')
        params.status = newStatus

        def isOrganisationSizeExist = commonDBFunctions.getDBRecordById(organisationSizeId, 'organization_size', 'id')
        if(isOrganisationSizeExist){
            def update = sql.executeUpdate "UPDATE organization_size set is_active = ?.status WHERE id = ?.id", params
            sql.close()
            def statusMessage= newStatus ? 'activated' : 'deactivated'

            if(update == 1){
                def organisationSize = commonDBFunctions.getDBRecordById(organisationSizeId, 'organization_size', 'id')
                def message = "Organisation Size (" + organisationSize.name + ") " + statusMessage
                def organisationSizeResponse = ObjectUtils.snakeCaseMapToPojo(organisationSize, new ApiOrganisationSizeResponseDto())
                response.data = organisationSizeResponse
                response.status = HttpStatus.OK.value()
                response.message = message
            }else{
                response.status = HttpStatus.BAD_REQUEST.value()
                response.message = "Update failed!!"
            }
        }else{
            response.status = HttpStatus.BAD_REQUEST.value()
            response.message = 'Organisation size with id ('+ organisationSizeId +') does not exist!'
        }

        response
    }

}
