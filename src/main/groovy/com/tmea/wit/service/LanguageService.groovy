package com.tmea.wit.service

import com.tmea.wit.model.BaseApiResponse
import com.tmea.wit.model.GeneralApiListResponse
import com.tmea.wit.model.dto.FieldErrorDto
import com.tmea.wit.model.dto.response.ApiLanguageResponseDto
import com.tmea.wit.util.CommonDBFunctions
import com.tmea.wit.util.ObjectUtils
import groovy.sql.Sql
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service

import javax.sql.DataSource

@Service
class LanguageService {
    DataSource dataSource
    CommonDBFunctions commonDBFunctions

    @Autowired
    LanguageService(DataSource dataSource, CommonDBFunctions commonDBFunctions) {
        this.dataSource = dataSource
        this.commonDBFunctions = commonDBFunctions
    }

    public GeneralApiListResponse getLanguages(Map parameterMap){
        Sql sql = new Sql(dataSource)
        GeneralApiListResponse response

        List<FieldErrorDto> errors = []

        def params = ObjectUtils.flattenListParam(parameterMap)
        def status = params.get("status")
        def code = params.get("code")
        def query = params.get("query")
        def sqlParams = [start: 0, limit: 25]
        boolean whereIncluded = false;

        def nameFilter = ""
        def codeFilter = ""
        def statusFilter = ""
        if(status){
            def booleanStatus = CommonDBFunctions.getStatusAsBoolean(status)
            if(booleanStatus != null){
                sqlParams.status = booleanStatus
                statusFilter = " WHERE is_active = ?.status"
                whereIncluded = true
            }else{
                errors.add(new FieldErrorDto("status", "Invalid value passed. Check Documentation"))
                response = new GeneralApiListResponse([], 0, HttpStatus.BAD_REQUEST.value(), "Invalid status value", errors)
                return response
            }
        }

        if(query)
        {

            String querySearch = "%$query%"
            sqlParams.put("query",querySearch)
            def queryFilterClause = """ language.name ILIKE ?.query """
            nameFilter = whereIncluded ? " AND "+queryFilterClause : " WHERE "+queryFilterClause
        }

        if (code){
            String codeSearch = "%$code%"
            sqlParams.put("code",codeSearch)
            def codeFilterClause = """ language.code_name ILIKE ?.code """
            codeFilter = whereIncluded ? " AND "+codeFilterClause : " WHERE "+codeFilterClause
        }

        //Pagination parameters & logic
        sqlParams = commonDBFunctions.paginationSqlParams(sqlParams, params)
        def queryFilters = statusFilter+nameFilter+codeFilter
        def fetchLanguagesQuery = "SELECT * FROM language" + queryFilters + " ORDER BY name ASC "+ CommonDBFunctions.getLimitClause()
        def countQuery = "SELECT count(*) FROM language" + queryFilters

        def data = sql.rows(fetchLanguagesQuery, sqlParams)
        def total = queryFilters ? sql.firstRow(countQuery,sqlParams).get("count") : sql.firstRow(countQuery).get("count")
        sql.close()

        if(data){
            response = new GeneralApiListResponse(ObjectUtils.snakeCaseArrayMapToListPojos(data, ApiLanguageResponseDto), total as Long, HttpStatus.OK.value(), "Success")
        }else{
            response = new GeneralApiListResponse([], total as Long, HttpStatus.OK.value(), "No records found")
        }
        return response
    }

    public BaseApiResponse updateLanguageStatus(Long languageId, def body){
        Sql sql = new Sql(dataSource)
        BaseApiResponse response = new BaseApiResponse([], 200, "")
        def params = ObjectUtils.asMap(body)
        params.id = languageId
        def newStatus = (params.status == 'ACTIVE' || params.status == 'active')
        params.status = newStatus

        def isLangugageExist = commonDBFunctions.getDBRecordById(languageId, 'language', 'id')
        if(isLangugageExist){
            def update = sql.executeUpdate "UPDATE language set is_active = ?.status WHERE id = ?.id", params
            sql.close()
            def statusMessage= newStatus ? 'activated' : 'deactivated'

            if(update == 1){
                def language = commonDBFunctions.getDBRecordById(languageId, 'language', 'id')
                def message = "Language ("+ language.name+ ") " + statusMessage
                def languageResponse = ObjectUtils.snakeCaseMapToPojo(language, new ApiLanguageResponseDto())
                response.data = languageResponse
                response.status = HttpStatus.OK.value()
                response.message = message
            }else{
                response.status = HttpStatus.BAD_REQUEST.value()
                response.message = "Update failed!!"
            }
        }else{
            response.status = HttpStatus.BAD_REQUEST.value()
            response.message = 'Language with id ('+ languageId +') does not exist!'
        }

        response
    }

}
