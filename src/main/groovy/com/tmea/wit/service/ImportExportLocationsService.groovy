package com.tmea.wit.service

import com.tmea.wit.model.BaseApiResponse
import com.tmea.wit.model.GeneralApiListResponse
import com.tmea.wit.model.dto.FieldErrorDto
import com.tmea.wit.model.dto.request.ApiEditImportExportPostDto
import com.tmea.wit.model.dto.request.ApiImportExportLocationPostDto
import com.tmea.wit.model.dto.request.ApiStatusRequestDto
import com.tmea.wit.util.CommonDBFunctions
import com.tmea.wit.util.ObjectUtils
import groovy.sql.Sql
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service

import javax.sql.DataSource

@Service
class ImportExportLocationsService {

    DataSource dataSource
    CommonDBFunctions commonDBFunctions

    @Autowired
    UserService userService

    ImportExportLocationsService(DataSource dataSource, CommonDBFunctions commonDBFunctions) {
        this.dataSource = dataSource
        this.commonDBFunctions = commonDBFunctions
    }

    BaseApiResponse addLocation(ApiImportExportLocationPostDto body, String name) {
        Sql sql = Sql.newInstance(dataSource)
        BaseApiResponse response = new BaseApiResponse()
        Map params = ObjectUtils.asMap(body)
        Long userId = userService.getUserIdFromUsername(name)

        params.put("addedBy", userId)

        if (userId) {
            def nameExist = sql.firstRow(
                    """SELECT * FROM import_export_locations 
                            WHERE name ILIKE ?.name 
                              AND code ILIKE ?.code 
                              AND is_active""", params
            )

            if (!nameExist) {
                def insertNew = sql.executeInsert(
                        """INSERT INTO import_export_locations(code, name, description, added_by) 
                                VALUES(?.code, ?.name, ?.description, ?.addedBy) """, params
                )

                if (insertNew) {
                    Long id = insertNew.get(0).get(0)

                    def location = getLocationById(id)

                    def responseData = ObjectUtils.mapToSnakeCaseMap(location)

                    response.status = HttpStatus.OK.value()
                    response.message = "Successful"
                    response.data = responseData

                } else {
                    response.status = HttpStatus.BAD_REQUEST.value()
                    response.message = "Operation failed"
                }

            } else {
                response.status = HttpStatus.BAD_REQUEST.value()
                response.message = "A record with similar details already exists."
            }

        } else {
            List<FieldErrorDto> errors = new ArrayList<>()
            errors.add(new FieldErrorDto("userId", "Failed to fetch user details of the user adding the location"))
            response.status = HttpStatus.BAD_REQUEST.value()
            response.message = "Failed to add Import/Export location"
        }

        sql.close()
        return response

    }

    BaseApiResponse editImportExportLocation(Long id, ApiEditImportExportPostDto body, String name) {
        Sql sql = Sql.newInstance(dataSource)
        BaseApiResponse response = new BaseApiResponse()
        Map editParams = ObjectUtils.asMap(body)

        Long userId = userService.getUserIdFromUsername(name)
        editParams.put("id", id)

        if (userId) {
            def isRecordExist = getLocationById(id)

            if (isRecordExist) {
                Map updateParams = ObjectUtils.rowAsMap(isRecordExist)
                updateParams = ObjectUtils.mapToSnakeCaseMap(updateParams) as Map
                ObjectUtils.copyMapProperties(editParams, updateParams)

                def update = sql.executeUpdate(
                        """UPDATE import_export_locations
                                SET name = ?.name,
                                code = ?.code,
                                description = ?.description
                                WHERE id = ?.id""", updateParams
                )

                if (update == 1) {
                    def editedLocation = getLocationById(id)

                    def data = ObjectUtils.mapToSnakeCaseMap(editedLocation)

                    response.status = HttpStatus.OK.value()
                    response.data = data
                    response.message = update + " record edited successfully"
                } else {
                    response.status = HttpStatus.BAD_REQUEST.value()
                    response.message = "Failed to edit record"
                }

            } else {
                response.status = HttpStatus.NOT_FOUND.value()
                response.message = "Record not found"
            }

        } else {
            List<FieldErrorDto> errors = new ArrayList<>()
            errors.add(new FieldErrorDto("userId", "Failed to fetch user details of the user editing the location"))
            response.status = HttpStatus.BAD_REQUEST.value()
            response.message = "Failed to edit Import/Export location"
        }

        sql.close()
        return response

    }

    BaseApiResponse activateDeactivateLocation(ApiStatusRequestDto body, Long locationId, String name) {
        Sql sql = Sql.newInstance(dataSource)
        BaseApiResponse response = new BaseApiResponse()
        Map params = ObjectUtils.asMap(body);

        Long user = userService.getUserIdFromUsername(name)
        def statusMsg
        if (user) {
            def locationExists = getLocationById(locationId)

            if (locationExists) {
                def status = CommonDBFunctions.getStatusAsBoolean(params.get("status"))

                params.replace("status", status)
                params.put("id", locationId)

                def affectedRow = sql.executeUpdate(
                        """Update import_export_locations 
                                SET is_active = ?.status WHERE id = ?.id""", params
                )

                statusMsg = status ? "activated" : "deactivated"

                if (affectedRow == 1) {
                    def record = sql.firstRow("""SELECT name, description, is_active 
                                                    FROM import_export_locations WHERE id =?""", locationId)

                    def data = ObjectUtils.mapToSnakeCaseMap(record)

                    response.status = HttpStatus.OK.value()
                    response.message = "Record " + statusMsg + " successfully"
                    response.data = data
                } else {
                    response.status = HttpStatus.BAD_REQUEST.value()
                    response.message = "Failed to " + statusMsg + " record"
                }
            } else {
                response.status = HttpStatus.BAD_REQUEST.value()
                response.message = "Record not found"
            }

        } else {
            List<FieldErrorDto> errors = new ArrayList<>()
            errors.add(new FieldErrorDto("userId", "Failed to fetch user details"))
            response.message = "Failed to " + statusMsg + " import/export location"
            response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
            response.errors = errors
        }

        sql.close()
        return response
    }

    GeneralApiListResponse fetchAllImportExportLocations(Map parameterMap, boolean isAdmin) {
        Sql sql = Sql.newInstance(dataSource)
        GeneralApiListResponse response = new GeneralApiListResponse()

        Map params = ObjectUtils.flattenListParam(parameterMap)
        Map requestParams = [start: 0, limit: 25]

        def status = params.get("status")
        def name = params.get("name")
        def code = params.get("code")

        boolean whereIncluded = false;

        def statusFilter = ""
        def nameFilter = ""
        def codeFilter = ""

        if (status) {
            def booleanStatus = CommonDBFunctions.getStatusAsBoolean(status)

            if (booleanStatus != null) {
                requestParams.status = booleanStatus
                statusFilter = " WHERE is_active = ?.status "
                whereIncluded = true
            } else {
                List<FieldErrorDto> errors = new ArrayList<>()
                errors.add(new FieldErrorDto("status", "Invalid value passed. Check Documentation"))
                response.status = HttpStatus.BAD_REQUEST.value()
                response.message = "Invalid status value"
                response.data = []
                response.total = 0
                return response

            }
        }

        if (!isAdmin){
            requestParams.status = true
            statusFilter = " WHERE is_active = ?.status "
            whereIncluded = true
        }

        if (name) {

            String nameSearch = "%$name%"
            requestParams.put("name", nameSearch)
            def nameFilterClause = """ import_export_locations.name ILIKE ?.name """
            nameFilter = whereIncluded ? " AND " + nameFilterClause : " WHERE " + nameFilterClause
        }

        if (code) {
            String codeSearch = "%$code%"
            requestParams.put("code", codeSearch)
            def codeFilterClause = """ import_export_locations.code ILIKE ?.code """
            codeFilter = whereIncluded ? " AND " + codeFilterClause : " WHERE " + codeFilterClause
        }

        requestParams = commonDBFunctions.paginationSqlParams(requestParams, params)
        def queryFilters = statusFilter + nameFilter + codeFilter
        String adminSelectQuery = "SELECT * FROM import_export_locations" + queryFilters + " ORDER BY name ASC " + CommonDBFunctions.getLimitClause()
        String selectQuery = "SELECT code, name, description, is_active FROM import_export_locations" + queryFilters + " ORDER BY name ASC " + CommonDBFunctions.getLimitClause()
        def fetchImportExportQuery = isAdmin? adminSelectQuery : selectQuery
        def countQuery = "SELECT count(*) FROM import_export_locations" + queryFilters

        def data = sql.rows(fetchImportExportQuery, requestParams)
        def total = queryFilters ? sql.firstRow(countQuery, requestParams).get("count") : sql.firstRow(countQuery).get("count")

        if (data) {
            def responseData = ObjectUtils.mapToSnakeCaseArrayMap(data)

            response.status = HttpStatus.OK.value()
            response.data = responseData
            response.total = total
            response.message = "Success"
        } else {
            response.status = HttpStatus.NO_CONTENT.value()
            response.message = "No records found"
        }


        sql.close()
        return response
    }


    Map getLocationById(Long id) {
        Sql sql = Sql.newInstance(dataSource)
        def location = sql.firstRow("""SELECT * FROM import_export_locations WHERE id = ?""", id)

        sql.close()
        return location
    }
}
