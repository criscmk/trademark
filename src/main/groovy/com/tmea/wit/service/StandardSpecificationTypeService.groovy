package com.tmea.wit.service

import com.tmea.wit.model.BaseApiResponse
import com.tmea.wit.model.GeneralApiListResponse
import com.tmea.wit.model.dto.FieldErrorDto
import com.tmea.wit.repository.UserRepository
import com.tmea.wit.util.CommonDBFunctions
import com.tmea.wit.util.ObjectUtils
import groovy.sql.Sql
import org.springframework.http.HttpStatus
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service

import javax.sql.DataSource

@Service
class StandardSpecificationTypeService {
    DataSource dataSource
    UserRepository userRepository
    CommonDBFunctions commonDBFunctions
    StandardSpecificationTypeService(DataSource dataSource, UserRepository userRepository, CommonDBFunctions commonDBFunctions) {
        this.dataSource = dataSource
        this.userRepository = userRepository
        this.commonDBFunctions = commonDBFunctions
    }
    public BaseApiResponse addStandardSpecificationType(def body) {
        Sql sql = new Sql(dataSource)
        BaseApiResponse response = new BaseApiResponse([], 400, "")
        List<FieldErrorDto> errors = []
        Map requestParams = ObjectUtils.asMap(body)
        Authentication auth = SecurityContextHolder.getContext().getAuthentication()
        String userName = auth.getName()
        Long addedBy = userRepository.findUserIdByDetails(userName)
        if (addedBy) {
            requestParams.put("addedBy", addedBy)
            def isRecordExist = sql.firstRow("""SELECT id FROM standard_specification_type WHERE name =?""", requestParams.name)
            if (!isRecordExist) {
            def insertQuery = sql.executeInsert("""Insert Into standard_specification_type(name, description, is_active) 
                                                                   VALUES (?.name,?.description,TRUE)""", requestParams)
            if (insertQuery) {
                def insertedRecord = sql.firstRow("""SELECT * FROM standard_specification_type where id=? """, insertQuery.get(0).get(0))
                response.message = "record added successfully"
                response.status = HttpStatus.OK.value()
                response.data = ObjectUtils.mapToSnakeCaseMap(insertedRecord)
            } else {
                response.message = "an error occurred while updating record"
                response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
            }
        }else{
                response.message = "record with the name ("+ requestParams.name + ") already exist"
                response.data = isRecordExist
            }
        }
        sql.close()
        return  response
    }
    public BaseApiResponse updateStandardSpecificationTypeStatus(Long id,def body) {
        Sql sql = new Sql(dataSource)
        BaseApiResponse response = new BaseApiResponse([], 400, "")
        List<FieldErrorDto> errors = []
        Map requestParams = ObjectUtils.asMap(body)
        requestParams.id = id
        def newStatus = ""
        Authentication auth = SecurityContextHolder.getContext().getAuthentication()
        String userName = auth.getName()
        Long updatedBy = userRepository.findUserIdByDetails(userName)
        if (requestParams.status.toLowerCase() == 'active') {
            requestParams.newStatus = true;
        }
        else if (requestParams.status.toLowerCase() == 'inactive') {
            requestParams.newStatus = false;
        }
        if(updatedBy) {
            def responseMessage = requestParams.newStatus?"activated":"deactivated"
            def recordExist = commonDBFunctions.getDBRecordById(id,"standard_specification_type","id")
            if (recordExist) {
            def updateQuery = sql.executeUpdate("""UPDATE standard_specification_type SET is_active =?.newStatus WHERE id=?.id""", requestParams)
            if (updateQuery) {
                def updatedRecord = sql.firstRow("""SELECT * FROM standard_specification_type where id=? """, requestParams.id)
                response.message = "record ("+responseMessage +") successfully"
                response.status = HttpStatus.OK.value()
                response.data = ObjectUtils.mapToSnakeCaseMap(updatedRecord)
            }else
            {
                response.message = "an error occurred while updating status"
                response.status =HttpStatus.INTERNAL_SERVER_ERROR.value()
            }
        }else{
                response.message ="record id (" + id +") does not exist"

            }
        }
        sql.close()
        return  response

    }
    public BaseApiResponse updateStandardSpecificationTypeDetails(Long id,def body) {
        Sql sql = new Sql(dataSource)
        BaseApiResponse response = new BaseApiResponse([], 400, "")
        List<FieldErrorDto> errors = []
        Map requestParams = ObjectUtils.asMap(body)
        Authentication auth = SecurityContextHolder.getContext().getAuthentication()
        String userName = auth.getName()
        Long updatedBy = userRepository.findUserIdByDetails(userName)
        if(updatedBy) {
            if (requestParams) {
                //check if product exists
                def recordExist =  commonDBFunctions.getDBRecordByIdAndStatus(id,"standard_specification_type","id")
                if(recordExist)
                {
                    Map updateParams = ObjectUtils.rowAsMap(recordExist)
                    updateParams = ObjectUtils.mapToSnakeCaseMap(updateParams) as Map
                    ObjectUtils.copyMapProperties(requestParams, updateParams)
                    updateParams.put("id", id)
                    def updateQuery = sql.executeUpdate(
                            """UPDATE standard_specification_type SET name = ?.name, description = ?.description 
                                   WHERE id = ?.id""", updateParams)
                    if (updateQuery == 1) {
                        def updatedRecord =  commonDBFunctions.getDBRecordByIdAndStatus(id,"standard_specification_type","id")
                        response.status = HttpStatus.OK.value()
                        response.data = ObjectUtils.mapToSnakeCaseMap(updatedRecord)
                        response.message = "record updated successfully"
                    } else {
                        response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
                        response.message = "Operation failed"
                    }
                }else {
                    response.message = "record id ("+id+") could not be found or is de activated"
                }
            } else {
                response.message = "Payload cannot be empty"
            }
        }else{
            response.message = "could not find logged in user"
        }
        sql.close()
        return  response
    }
    public GeneralApiListResponse fetchStandardSpecificationTypeDetails(Map parameterMap,Boolean isAdmin) {
        Sql sql = new Sql(dataSource)
        GeneralApiListResponse response
        List<FieldErrorDto> errors = []
        def params = ObjectUtils.flattenListParam(parameterMap)
        def status = params.status
        def query = params.query
        def statusFilter = ""
        def sqlParams = [start: 0, limit: 25]
        boolean whereIncluded = false;
        //OverRide Status Filter if not Admin
        status = isAdmin ? status : "active"
        if (status) {
            def booleanStatus = CommonDBFunctions.getStatusAsBoolean(status)
            if (booleanStatus != null) {
                sqlParams.status = booleanStatus
                statusFilter = " WHERE is_active = ?.status"
                whereIncluded = true
            } else {
                errors.add(new FieldErrorDto("status", "Invalid value passed. Check Documentation"))
                response = new GeneralApiListResponse([], 0, HttpStatus.BAD_REQUEST.value(), "Invalid status value", errors)
                return response
            }
        }
        def searchFilter=""
        if(query)
        {
            String querySearch = "%$query%"
            sqlParams.put("query",querySearch)
            def queryFilterClause = """name ILIKE ?.query OR
                                       description ILIKE ?.query
                                       """
            searchFilter = whereIncluded ? " AND "+queryFilterClause : " WHERE "+queryFilterClause
        }

        //Pagination parameters & logic
        sqlParams = commonDBFunctions.paginationSqlParams(sqlParams,params)
        def countQuery = """SELECT count(*) FROM standard_specification_type"""
        def queryFilters = statusFilter  + searchFilter
        def fetchQuery= """SELECT * FROM standard_specification_type """
        def data = sql.rows(fetchQuery + queryFilters + " ORDER BY id DESC" + commonDBFunctions.getLimitClause(), sqlParams)
        def total = queryFilters ? sql.firstRow(countQuery + queryFilters,sqlParams).get("count") : sql.firstRow(countQuery).get("count")
        if(data){

            response = new GeneralApiListResponse(  ObjectUtils.mapToSnakeCaseArrayMap(data), total as Long, HttpStatus.OK.value(), "Success")
        }else{
            response = new GeneralApiListResponse([], total as Long, HttpStatus.NO_CONTENT.value(), "No records found")
        }

        sql.close()
        return response

    }
    public BaseApiResponse fetchStandardSpecificationTypeDetailsById(Long id) {
        Sql sql = new Sql(dataSource)
        BaseApiResponse response = new BaseApiResponse([], 400, "")
        List<FieldErrorDto> errors = []
        Authentication auth = SecurityContextHolder.getContext().getAuthentication()
        def data = sql.rows("""SELECT * FROM standard_specification_type WHERE id =?""",id)
        if(data){
            response.message = "success"
            response.data = ObjectUtils.mapToSnakeCaseArrayMap(data)
            response.status = HttpStatus.OK.value()
        }else {
            response.message = "record id + ("+ id +") does not exist"
            response.status = HttpStatus.BAD_REQUEST.value()
        }
        sql.close()
        return response

    }
}
