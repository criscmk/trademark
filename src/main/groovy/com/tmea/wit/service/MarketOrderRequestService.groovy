package com.tmea.wit.service

import com.tmea.wit.model.BaseApiResponse
import com.tmea.wit.model.GeneralApiListResponse
import com.tmea.wit.model.dto.FieldErrorDto
import com.tmea.wit.model.dto.response.ApiFetchMarketOrderRequestResponseDto
import com.tmea.wit.repository.UserRepository
import com.tmea.wit.util.CommonDBFunctions
import com.tmea.wit.util.ObjectUtils
import groovy.sql.Sql
import org.hashids.Hashids
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpStatus
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service

import javax.sql.DataSource

@Service
class MarketOrderRequestService {
    @Value('${hashids.ordernumber.salt}')
    private String orderNumberHashIdSalt;
    @Value('${wit.ordernumber.seperator}')
    private String OrderNumberSeparator;
    @Autowired
    DataSource dataSource
    @Autowired
    UserService userService
    @Autowired
    GlobalConfigService globalConfigService
    CommonDBFunctions commonDBFunctions
    UserRepository userRepository

    MarketOrderRequestService(DataSource dataSource, UserRepository userRepository, CommonDBFunctions commonDBFunctions) {
        this.dataSource = dataSource
        this.commonDBFunctions = commonDBFunctions
        this.userRepository = userRepository
    }

    public BaseApiResponse addMarketOrderRequest(def body) {
        BaseApiResponse response = new BaseApiResponse(null, 400, "", [])
        List<FieldErrorDto> errors = []
        Sql sql = new Sql(dataSource)
        def params = ObjectUtils.asMap(body)
        Authentication auth = SecurityContextHolder.getContext().getAuthentication()
        def orderedBy = userRepository.findUserIdByDetails(auth.getName())
        def quantityOrdered = params.get("quantity")
        def queryParams = params
        if (orderedBy) {
            //check if product exists
            def productExists = sql.firstRow("""SELECT * FROM  product WHERE is_approved AND is_approved AND is_published AND id = ? """, params.get("product"))
            def unitPrice
            def isInStock
            def minimumOrderQuantity
            if (productExists) {
                unitPrice = productExists.get("price")
                isInStock = productExists.get("is_in_stock")
                minimumOrderQuantity = productExists.get("minimum_order_quantity")

                //compare quantity ordered and minimum quantity to be sold
                if (quantityOrdered >= minimumOrderQuantity) {
                    def pendingApprovalStatusId = commonDBFunctions.getDBRecordByNameAndStatus("pending", "approval_status", "code").get("id")
                    if (pendingApprovalStatusId) {
                        def totalPrice = quantityOrdered * unitPrice
                        queryParams.put("pendingApprovalStatusId", pendingApprovalStatusId)
                        queryParams.put("totalPrice", totalPrice)
                        queryParams.put("orderedBy", orderedBy)
                        queryParams.put("unitPrice", unitPrice)
                        def postMarketOrderRequest = sql.executeInsert("""Insert Into market_order_request 
                                                                    (product_id, ordered_by, quantity_ordered, unit_price, total_price, customer_comments, approval_status_id, date_requested) 
                                                                    VALUES (?.product, ?.orderedBy ,?.quantity, ?.unitPrice,?.totalPrice,?.comment,?.pendingApprovalStatusId,CURRENT_TIMESTAMP)
                                                                """, queryParams)
                        if (postMarketOrderRequest) {
                            def postedRequest = commonDBFunctions.getDBRecordById(postMarketOrderRequest.get(0).get(0), "market_order_request", "id")
                            response.data = ObjectUtils.mapToSnakeCaseMap(postedRequest)
                            response.message = "market order request posted successfully"
                            response.status = HttpStatus.OK.value()
                        } else {
                            response.setMessage("an error occurred while posting market order request")
                        }
                    } else {
                        response.message = "The approval status does not exists"
                    }

                } else {
                    response.setMessage("Quantity ordered is less than minimum order quantity. minimum quantity to be ordered is " + minimumOrderQuantity)
                }

            } else {
                response.message = "product does not exists or is not published or out of stock"
            }
        } else {
            response.message = "user does not exist"

        }


        sql.close()
        response
    }

    public GeneralApiListResponse getAllMarketOrderRequest(def parameterMap, int userType) {
        Sql sql = new Sql(dataSource)
        GeneralApiListResponse response = new GeneralApiListResponse();
        def params = ObjectUtils.flattenListParam(parameterMap)
        Authentication auth = SecurityContextHolder.getContext().getAuthentication()
        def userId = userRepository.findUserIdByDetails(auth.getName())
        def sellerId = params.get("seller")
        def buyerId = params.get("buyer")
        def page = params.get("page")
        def size = params.get("size")
        def dateTo = params.get("dateTo")
        def dateFrom = params.get("dateFrom")
        def date = params.get("date")
        def commodityId = params.get("commodityId")
        def isActive = params.get("isActive")
        def queryParams = params
        def filterStatusQuery = ""
        def providerFilter = ""
        def filterBySeller = ""
        def filterByBuyer = ""
        def dateRangeFilter = ""
        def dateFilter = ""
        def commodityFilter = ""
        def productFilter = ""

        if (userType == 1) {
            providerFilter = " AND product.trader_id = ?.userId"
            queryParams.put("userId", userId)
        }
        if (userType == 2) {
            providerFilter = " AND market_order_request.ordered_by = ?.userId"
            queryParams.put("userId", userId)
        }
        if (params.status) {
            String status = params.status.toLowerCase()

            if (status == 'pending' || status == 'approved' || status == 'rejected') {
                def filterStatus = commonDBFunctions.getDBRecordByNameAndStatus(status, "approval_status", "code").get("id")
                if (filterStatus) {
                    queryParams.put("statusId", filterStatus)
                    filterStatusQuery = " AND market_order_request.approval_status_id = ?.statusId "

                }
            } else {
                response.setMessage("Invalid status value")
                response.setData(null)
                response.setStatus(HttpStatus.BAD_REQUEST.value())
                sql.close()
                return response
            }
        }

        if (date) {

            dateFilter = ' AND market_order_request.date_requested::date  = ?.date::date ';
            queryParams.put("date", date);

        }


        if (dateFrom && dateTo) {

            dateFilter = "";
            dateRangeFilter = " AND market_order_request.date_requested::date BETWEEN date(?.dateFrom::date) AND date(?.dateTo::date)";
            queryParams.put("dateFrom", dateFrom);
            queryParams.put("dateTo", dateTo);

        }
        if (commodityId) {

            commodityFilter = " AND product.commodity_id =?.commodityId::BigInt"
            queryParams.put("commodityId", commodityId)
        }
        if (params.productId) {

            productFilter = " AND product.id =?.productId::BigInt"
            queryParams.put("productId", params.productId)
        }

        if (userType == 0) {
            //filter by seller
            if (sellerId) {

                sellerId = Integer.parseInt(String.valueOf(sellerId));
                filterBySeller = " AND product.trader_id = ?.sellerId"
                queryParams.put("sellerId", sellerId)

            }
            //filter by buyer
            if (buyerId) {
                buyerId = Integer.parseInt(String.valueOf(buyerId));
                filterByBuyer = " AND market_order_request.ordered_by = ?.buyerId"
                queryParams.put("buyerId", buyerId)

            }
        }
        def searchFilter = ""
        if (queryParams.query) {
            String querySearch = "%$queryParams.query%"
            queryParams.put("query", querySearch)
            searchFilter = """ AND (
                                    product.name ILIKE ?.query OR
                                    product.description ILIKE ?.query OR
                                    commodity.name ILIKE ?.query
                                   
                                  )
                          """
        }

        //pagination
        Map paginateParams = [start: 0, limit: 25]
        def paginate = commonDBFunctions.paginationSqlParams(paginateParams, params)
        def paginationFilter = ""
        if (page && size) {
            paginationFilter = " LIMIT ?.limit OFFSET ?.start"
            queryParams = queryParams + paginate

        }

        String countQuery = """                  SELECT count(*) 
                                                  FROM 
                                                  product,
                                                  market_order_request,
                                                  approval_status,
                                                  measurement_unit,
                                                  "user",
                                                  user_individual_detail,
                                                  commodity
                                                  WHERE
                                                  product.id = market_order_request.product_id
                                                  AND
                                                  commodity.id = product.commodity_id
                                                  AND 
                                                  approval_status.id = market_order_request.approval_status_id
                                                  AND
                                                  measurement_unit.id = product.measurement_unit_id
                                                  AND
                                               market_order_request.ordered_by = "user".id 
                                                AND
                                                user_individual_detail.user_id = "user".id
                                    """
        def total = 0
        def queryFilter = filterStatusQuery + providerFilter + searchFilter + filterByBuyer + filterBySeller + dateRangeFilter + dateFilter + commodityFilter + productFilter

        def getAllMarketOrdersRequestQuery = sql.rows(getMarketOrderRequest() + queryFilter + " ORDER BY id DESC" + paginationFilter, queryParams)
        if (queryFilter) {
            total = sql.firstRow(countQuery + queryFilter, queryParams)?.get('count')

        } else {
            total = sql.firstRow(countQuery)?.get('count')
        }

        if (getAllMarketOrdersRequestQuery) {
            List<ApiFetchMarketOrderRequestResponseDto> apiFetchMarketOrderRequestResponseDtoList;
            apiFetchMarketOrderRequestResponseDtoList = ObjectUtils.snakeCaseArrayMapToListPojos(getAllMarketOrdersRequestQuery, ApiFetchMarketOrderRequestResponseDto);
            response.data = apiFetchMarketOrderRequestResponseDtoList
            response.message = "records found"
            response.status = HttpStatus.OK.value()
            response.total = total
        } else {
            response.message = "no records found"
            response.total = total
            response.status = HttpStatus.NO_CONTENT.value()
        }

        response

    }

    public BaseApiResponse getAllMarketOrderRequestById(Long marketRequestId) {
        Sql sql = new Sql(dataSource)
        BaseApiResponse response = new BaseApiResponse(null, 400, "")
        def queryParams = [:]
        queryParams.put("marketRequestId", marketRequestId)
        def filterById = " AND market_order_request.id = ?.marketRequestId "
        def marketOrderRequestById = sql.firstRow(getMarketOrderRequest() + filterById, queryParams)
        if (marketOrderRequestById) {
            def responseData = ObjectUtils.snakeCaseMapToPojo(marketOrderRequestById, new ApiFetchMarketOrderRequestResponseDto())
            response.data = marketOrderRequestById
            response.message = "record found"
            response.status = HttpStatus.OK.value()
        } else {
            response.message = "no record found"
            response.status = HttpStatus.NO_CONTENT.value()
        }
        sql.close()
        response

    }

    public BaseApiResponse updateMarketOrderRequestPrice(def body, Long marketOrderRequestId) {
        BaseApiResponse response = new BaseApiResponse(null, 400, "")
        Sql sql = new Sql(dataSource)
        def queryParams = ObjectUtils.asMap(body)
        Authentication auth = SecurityContextHolder.getContext().getAuthentication()
        def updatedBy = userRepository.findUserIdByDetails(auth.getName())
        queryParams.put("marketOrderRequestId", marketOrderRequestId)
        queryParams.put("updatedBy", updatedBy)


        if (updatedBy) {
            //check if market order request exists
            def marketOrderRequestExists = marketOrderRequestExist(marketOrderRequestId)
            if (marketOrderRequestExists) {
                def currentStatus = marketOrderRequestExists.get(0).get("code")
                if (currentStatus == "pending") {
                    def quantity = marketOrderRequestExists.get(0).get("quantity_ordered")
                    def newTotal = queryParams.get("price") * quantity
                    queryParams.put("newTotal", newTotal)
                    //update price and new total
                    def updatedPrice = ""
                    def logNewPrice = ""
                    sql.withTransaction {
                        updatedPrice = sql.executeUpdate("""UPDATE  market_order_request SET unit_price =?.price,total_price = ?.newTotal WHERE id = ?.marketOrderRequestId""", queryParams)
                        logNewPrice = sql.executeInsert("""INSERT INTO  market_order_request_price_update
                                                                (market_order_request_id, updated_price, updated_by, is_active, date_updated) 
                                                                VALUES (?.marketOrderRequestId, ?.price, ?.updatedBy,TRUE,CURRENT_TIMESTAMP)""", queryParams)
                    }
                    if (updatedPrice && logNewPrice) {
                        def updatedRecord = commonDBFunctions.getDBRecordById(marketOrderRequestId, "market_order_request", "id")
                        response.message = "Price Updated Successfully"
                        response.status = HttpStatus.OK.value()
                        response.data = ObjectUtils.mapToSnakeCaseMap(updatedRecord)
                    } else {
                        response.message = "AN error occurred while Inserting record"
                    }
                } else {
                    response.message = "market order request have already been " + currentStatus
                }


            } else {
                response.data = queryParams
                response.status = HttpStatus.BAD_REQUEST.value()
                response.message = "Market Order Request Does Not Exists"
            }
        } else {
            response.message = "User Does Not Exist"
        }
        response

    }

    public BaseApiResponse updateMarketOrderRequestQuantity(def body, Long marketOrderRequestId) {
        BaseApiResponse response = new BaseApiResponse(null, 400, "")
        Sql sql = new Sql(dataSource)
        def queryParams = ObjectUtils.asMap(body)
        Authentication auth = SecurityContextHolder.getContext().getAuthentication()
        def updatedBy = userRepository.findUserIdByDetails(auth.getName())
        queryParams.put("marketOrderRequestId", marketOrderRequestId)
        queryParams.put("updatedBy", updatedBy)

        if (updatedBy) {
            //check if market order request exists
            def existingMarketOrderRequest = marketOrderRequestExist(marketOrderRequestId)
            if (existingMarketOrderRequest) {
                def currentStatus = existingMarketOrderRequest.get(0).get("code")
                //check if market order status is pending
                if (currentStatus == "pending") {
                    def price = existingMarketOrderRequest.get(0).get("unit_price")
                    def productId = existingMarketOrderRequest.get(0).get("product_id")
                    def newQuantity = queryParams.get("quantity")
                    def newTotal = newQuantity * price
                    queryParams.put("newTotal", newTotal)
                    queryParams.put("newQuantity", newQuantity)
                    //check if quantity is above the minimum quantity to be ordered
                    def minimumQuantity = sql.rows("""SELECT * FROM product WHERE  id =?""", productId).get(0).get("minimum_order_quantity")
                    if (newQuantity >= minimumQuantity) {
                        def updateQuantity = ""
                        def logNewQuantity = ""
                        sql.withTransaction {
                            updateQuantity = sql.executeUpdate("""UPDATE  market_order_request SET quantity_ordered =?.newQuantity,total_price = ?.newTotal WHERE id = ?.marketOrderRequestId""", queryParams)
                            logNewQuantity = sql.executeInsert("""INSERT INTO market_order_request_updated_quantity 
                                                                  (updated_quantity, updated_by, date_updated) 
                                                                  VALUES (?.newQuantity,?.updatedBy,CURRENT_TIMESTAMP)""", queryParams)
                        }
                        if (updateQuantity && logNewQuantity) {
                            def updatedRecord = commonDBFunctions.getDBRecordById(marketOrderRequestId, "market_order_request", "id")
                            response.message = "Quantity Updated Successfully"
                            response.status = HttpStatus.OK.value()
                            response.data = ObjectUtils.mapToSnakeCaseMap(updatedRecord)
                        } else {
                            response.message = "AN error occurred while updating record"
                        }


                    } else {
                        response.setMessage("Quantity ordered is less than minimum order quantity. minimum quantity to be ordered is " + minimumQuantity)
                    }
                } else {
                    response.message = "market order request have already been " + currentStatus
                }


            } else {
                response.message = "market order request does not exist"
            }
        } else {
            response.message = "User Does Not Exist"
        }
        sql.close()
        response
    }

    public BaseApiResponse approveMarketOrderRequest(def body, Long marketOrderRequestId) {
        BaseApiResponse response = new BaseApiResponse(null, 400, "")
        Sql sql = new Sql(dataSource)
        def queryParams = ObjectUtils.asMap(body)
        Authentication auth = SecurityContextHolder.getContext().getAuthentication()
        def approvedBy = userRepository.findUserIdByDetails(auth.getName())
        def status = queryParams.approval
        def prefix =[:]
        status = status.toLowerCase()
        def updatedStatusId = ""
        if (status) {
            updatedStatusId = commonDBFunctions.getDBRecordByNameAndStatus(status, "approval_status", "code").get("id")
        }
        queryParams.put("updatedStatusId", updatedStatusId)
        queryParams.put("approvedBy", approvedBy)
        queryParams.put("marketOrderRequestId", marketOrderRequestId)
        def marketOrderRequestExist = marketOrderRequestExist(marketOrderRequestId)
        //check if request exist
        if (marketOrderRequestExist) {
            //check if order is approved
            def currentStatusName = marketOrderRequestExist.get(0).get("code").toLowerCase()
            if (currentStatusName == "pending" || currentStatusName == "rejected") {
                if (updatedStatusId) {
                    def updateMarketOrderRequest
                    def logApproval
                    def createMarketOrder
                    prefix = globalConfigService.getConfigValue("market_order_number_prefix")

                    //check if market order number prefix is configured
                    if (prefix) {
                        sql.withTransaction {
                        updateMarketOrderRequest = sql.executeUpdate("""UPDATE market_order_request SET  approval_status_id = ?.updatedStatusId WHERE id =?.marketOrderRequestId """, queryParams)
                        logApproval = sql.executeInsert("""INSERT INTO market_order_approval 
                                                             (market_order_request_id, approved_by, date_approved)  
                                                             VALUES (?.marketOrderRequestId,?.approvedBy,CURRENT_TIMESTAMP)""", queryParams)
                        if (status == 'approved') {
                            prefix = prefix +OrderNumberSeparator
                            def initialMarketOrderStatus = commonDBFunctions.getDBRecordByNameAndStatus("pending", "market_order_status", "name").get("id")
                            queryParams.put("productId", marketOrderRequestExist.get(0).get("product_id"))
                            queryParams.put("dateOrdered", marketOrderRequestExist.get(0).get("date_requested"))
                            queryParams.put("orderedBy", marketOrderRequestExist.get(0).get("ordered_by"))
                            queryParams.put("initialMarketOrderStatus", initialMarketOrderStatus)

                            createMarketOrder = sql.executeInsert("""INSERT INTO market_order 
                                                                      (product_id, ordered_by, is_payment_complete,is_active,market_order_status_id,date_ordered,market_order_request_id) 
                                                                      VALUES (?.productId , ?.orderedBy, FALSE,TRUE,?.initialMarketOrderStatus,?.dateOrdered,?.marketOrderRequestId)""", queryParams)
                            if (createMarketOrder) {
                                //update order number
                                def orderId = createMarketOrder.get(0).get(0)
                                Hashids hashids = new Hashids(orderNumberHashIdSalt, 6)
                                String orderNumber = prefix + hashids.encode(orderId).toUpperCase();
                                queryParams.put("orderId", orderId)
                                queryParams.put("orderNumber", orderNumber)
                                def updateOrderNumber = sql.executeUpdate("""UPDATE market_order set order_number = ?.orderNumber WHERE id= ?.orderId""", queryParams)
                                def createdOrderDetails = sql.firstRow("""SELECT * FROM market_order WHERE id =?.orderId""", queryParams)
                            }
                        }
                    }

                    } else {
                        sql.close()
                        response.message = "market order number prefix is not configured"
                        response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
                        return response
                    }

                    if (updateMarketOrderRequest && logApproval) {
                        def updatedRecord = commonDBFunctions.getDBRecordById(marketOrderRequestId, "market_order_request", "id")
                        response.message = "record " + status + " successfully"
                        response.status = HttpStatus.OK.value()
                        response.data = ObjectUtils.mapToSnakeCaseMap(updatedRecord)
                    } else {
                        response.message = "An error occurred while approving records"
                    }

                } else {
                    response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
                    response.message = "status does not exist"
                }
            } else {

                response.data = ObjectUtils.mapToSnakeCaseMap(marketOrderRequestExist.get(0))
                response.message = "Market Order Request Have Already Been " + currentStatusName
            }
        } else {
            response.message = "market order request does not exist or has been approved"
        }
        sql.close()
        response
    }

    public String getMarketOrderRequest() {
        def getAllMarketOrdersRequestQuery = """ SELECT 
                                                   market_order_request.id AS id,
                                                   product.name AS product,
                                                   product.description,
                                                   market_order_request.unit_price,
                                                   market_order_request.quantity_ordered,
                                                   market_order_request.total_price,
                                                   market_order_request.customer_comments,
                                                  to_char( market_order_request.date_requested,'YYYY-MM-DD') AS date_requested,
                                                   approval_status.code AS approval_status,
                                                   commodity.name AS commodity_name,
                                                   commodity.id AS commodity_id,
                                                   measurement_unit.code AS measurement_unit,
                                                    "buyer".phone_number AS requested_by_phone,
                                                    "buyer".email AS requested_by_email,
                                                                                                      
                                                    CASE
                                                     WHEN "buyer".user_type_id = 1 THEN (SELECT user_individual_detail.first_name||' '||user_individual_detail.middle_name||' '||user_individual_detail.last_name  FROM user_individual_detail WHERE "buyer".id = user_individual_detail.user_id)
                                                     WHEN "buyer".user_type_id = 2 THEN (SELECT user_organization_detail.organization_name FROM user_organization_detail WHERE "buyer".id = user_organization_detail.user_id)
                                                     END AS requested_by_name, 
                                                      "seller".phone_number AS sold_by_phone,
                                                      "seller".email AS sold_by_email,
                                                    CASE
                                                     WHEN "seller".user_type_id = 1 THEN (SELECT user_individual_detail.first_name||' '||user_individual_detail.middle_name||' '||user_individual_detail.last_name  FROM user_individual_detail WHERE "seller".id = user_individual_detail.user_id)
                                                     WHEN "seller".user_type_id = 2 THEN (SELECT user_organization_detail.organization_name FROM user_organization_detail WHERE "seller".id = user_organization_detail.user_id)
                                                     END AS sold_by_name
                                                  FROM 
                                                  product,
                                                  market_order_request,
                                                  approval_status,
                                                  measurement_unit,
                                                  "user" AS buyer,
                                                  "user" AS seller,
                                                  commodity
                                                   WHERE
                                                   
                                                  product.id = market_order_request.product_id
                                                  AND
                                                  approval_status.id = market_order_request.approval_status_id
                                                  AND
                                                  commodity.id = product.commodity_id
                                                  AND
                                                  measurement_unit.id = product.measurement_unit_id
                                                  AND
                                                  market_order_request.ordered_by = "buyer".id 
                                                  AND
                                                  product.trader_id = "seller".id 
                                                                                                      
                                                       """
    }

    public def marketOrderRequestExist(Long marketOrderRequestId) {
        Sql sql = new Sql(dataSource)
        def marketOrderRequestExists = sql.rows("""SELECT * FROM 
                                                           market_order_request,approval_status 
                                                           WHERE
                                                           market_order_request.approval_status_id = approval_status.id
                                                           AND
                                                            market_order_request.id =?""", marketOrderRequestId)
        sql.close()
        marketOrderRequestExists
    }

}