package com.tmea.wit.service

import com.tmea.wit.model.BaseApiResponse
import com.tmea.wit.model.GeneralApiListResponse
import com.tmea.wit.model.dto.FieldErrorDto
import com.tmea.wit.model.dto.response.ApiUserTypeResponseDto
import com.tmea.wit.util.CommonDBFunctions
import com.tmea.wit.util.ObjectUtils
import groovy.sql.Sql
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service

import javax.sql.DataSource

@Service
class UserTypeService {

    DataSource dataSource
    CommonDBFunctions commonDBFunctions

    UserTypeService(DataSource dataSource, CommonDBFunctions commonDBFunctions) {
        this.dataSource = dataSource
        this.commonDBFunctions = commonDBFunctions
    }

    public GeneralApiListResponse getUserTypes(Map parameterMap){
        Sql sql = new Sql(dataSource)
        GeneralApiListResponse response
        List<ApiUserTypeResponseDto> userTypesList = new ArrayList<>()
        List<FieldErrorDto> errors = []

        def params = ObjectUtils.flattenListParam(parameterMap)
        def status = params.status
        def sqlParams = [start: 0, limit: 25]
        if(status){
            def booleanStatus = CommonDBFunctions.getStatusAsBoolean(status)
            if(booleanStatus != null){
                sqlParams.status = booleanStatus
            }else{
                errors.add(new FieldErrorDto("status", "Invalid value passed. Check Documentation"))
                response = new GeneralApiListResponse([], 0, HttpStatus.BAD_REQUEST.value(), "Invalid status value", errors)
                return response
            }
        }

        //Pagination parameters & logic
        sqlParams = commonDBFunctions.paginationSqlParams(sqlParams, params)

        def query = "SELECT * FROM user_type"
        def countQuery = "SELECT count(*) FROM user_type"
        Map queryMap = CommonDBFunctions.addQueryFilters(status, query, countQuery )

        String filteredSelectQuery = queryMap.selectQuery
        String filteredCountQuery = queryMap.countQuery
        String queryFilter = queryMap.queryFilter

        def data = sql.rows(filteredSelectQuery, sqlParams)
        def total = commonDBFunctions.getTotalCountBasedOnFilters(queryFilter, filteredCountQuery, sqlParams)
        sql.close()
        data.each {
           def apiUserTypeResponse = ObjectUtils.snakeCaseMapToPojo(it, new ApiUserTypeResponseDto()) as ApiUserTypeResponseDto
            userTypesList.add(apiUserTypeResponse)
        }
        if(data){
            userTypesList.forEach(System.out.&println)
            response = new GeneralApiListResponse(userTypesList, total as Long, HttpStatus.OK.value(), "Success")
        }else{
            response = new GeneralApiListResponse([], total as Long, HttpStatus.OK.value(), "No records found")
        }
        return response
    }

    public BaseApiResponse updateUserTypeStatus(Long userTypeId, def body){
        Sql sql = new Sql(dataSource)
        BaseApiResponse response = new BaseApiResponse(null, 200, "")
        def params = ObjectUtils.asMap(body)
        params.id = userTypeId
        def newStatus = (params.status == 'ACTIVE' || params.status == 'active')
        params.status = newStatus

        def isUserTypeExist = commonDBFunctions.getDBRecordByTableColumn(userTypeId, 'user_type', 'id')
        if(isUserTypeExist){
            def update = sql.executeUpdate "UPDATE user_type set is_active = ?.status WHERE id = ?.id", params
            sql.close()
            def statusMessage= newStatus ? 'activated' : 'deactivated'

            if(update == 1){
                def userType = commonDBFunctions.getDBRecordByTableColumn(userTypeId, 'user_type', 'id')
                def message = "UserType ("+ userType.name+ ") " + statusMessage
                def userTypeResponse = ObjectUtils.snakeCaseMapToPojo(userType, new ApiUserTypeResponseDto())
                response.data = userTypeResponse
                response.status = HttpStatus.OK.value()
                response.message = message
            }else{
                response.status = HttpStatus.BAD_REQUEST.value()
                response.message = "Update failed!!"
            }
        }else{
            response.status = HttpStatus.BAD_REQUEST.value()
            response.message = 'User Type with id ('+ userTypeId +') does not exist!'
        }
        return response
    }

}
