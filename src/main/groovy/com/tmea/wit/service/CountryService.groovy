package com.tmea.wit.service

import com.tmea.wit.model.BaseApiResponse
import com.tmea.wit.model.GeneralApiListResponse
import com.tmea.wit.model.dto.FieldErrorDto
import com.tmea.wit.model.dto.response.ApiCommodityCategoryResponseDto
import com.tmea.wit.model.dto.response.ApiCountryResponseDto
import com.tmea.wit.util.CommonDBFunctions
import com.tmea.wit.util.ObjectUtils
import groovy.sql.Sql
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service

import javax.sql.DataSource

@Service
class CountryService {
    DataSource dataSource
    CommonDBFunctions commonDBFunctions

    @Autowired
    CountryService(DataSource dataSource , CommonDBFunctions commonDBFunctions){
        this.dataSource = dataSource
        this.commonDBFunctions = commonDBFunctions
    }

    public GeneralApiListResponse getCountries(Map parameterMap){
        Sql sql = new Sql(dataSource)
        GeneralApiListResponse response
        List<ApiCountryResponseDto> countryList = new ArrayList<>()
        List<FieldErrorDto> errors = []
        def params = ObjectUtils.flattenListParam(parameterMap)
        def status = params.status
        def query = params.query
        def statusFilter = ""
        def searchFilter =""
        Map sqlParams = [start: 0, limit: 25]
        boolean whereIncluded = false;
        if(status){
            def booleanStatus = CommonDBFunctions.getStatusAsBoolean(status)
            if(booleanStatus != null){
                sqlParams.status = booleanStatus
                statusFilter = " WHERE is_active = ?.status"
                whereIncluded = true
            }else{
                errors.add(new FieldErrorDto("status", "Invalid value passed. Check Documentation"))
                response = new GeneralApiListResponse([], 0, HttpStatus.BAD_REQUEST.value(), "Invalid status value", errors)
                return response
            }
        }

        if(query)
        {

            String querySearch = "%$query%"
            sqlParams.put("query",querySearch)
            def queryFilterClause = """ country.name ILIKE ?.query OR 
                                        country.code_name ILIKE ?.query"""
            searchFilter = whereIncluded ? " AND "+queryFilterClause : " WHERE "+queryFilterClause
        }


        //Pagination parameters & logic
        sqlParams = commonDBFunctions.paginationSqlParams(sqlParams, params)
        def fetchCountryQuery = "SELECT * FROM country"
        def countQuery = "SELECT count(*) FROM country"
        def queryFilters = statusFilter + searchFilter
        println("query"+fetchCountryQuery+queryFilters)
        def data = sql.rows(fetchCountryQuery + queryFilters+ " ORDER BY name ASC" +commonDBFunctions.getLimitClause(), sqlParams)
        def total = queryFilters ? sql.firstRow(countQuery + queryFilters,sqlParams).get("count") : sql.firstRow(countQuery).get("count")
        if(data){
            response = new GeneralApiListResponse(ObjectUtils.snakeCaseArrayMapToListPojos(data, ApiCountryResponseDto), total as Long, HttpStatus.OK.value(), "Success")
        }else{
            response = new GeneralApiListResponse(null, total as Long, HttpStatus.NO_CONTENT.value(), "No records found")
        }
        return response
    }

    public BaseApiResponse updateStatus(Long countryId, def body){
        Sql sql = new Sql(dataSource)
        BaseApiResponse response = new BaseApiResponse([], 200, "")
        def params = ObjectUtils.asMap(body)
        params.id = countryId
        def newStatus = (params.status == 'ACTIVE' || params.status == 'active')
        params.status = newStatus

        def isCountryExist = commonDBFunctions.getDBRecordById(countryId, 'country', 'id')
        if(isCountryExist){
            def update = sql.executeUpdate "UPDATE country set is_active = ?.status WHERE id = ?.id", params
            sql.close()
            def statusMessage= newStatus ? 'activated' : 'deactivated'

            if(update == 1){
                def country = commonDBFunctions.getDBRecordById(countryId, 'country', 'id')
                def message = "Country ("+ country.name+ ") " + statusMessage
                def countryResponse = ObjectUtils.snakeCaseMapToPojo(country, new ApiCountryResponseDto())
                response.data = countryResponse
                response.status = HttpStatus.OK.value()
                response.message = message
            }
        }else{
            response.status = HttpStatus.BAD_REQUEST.value()
            response.message = 'Country with id ('+ countryId +') does not exist!'
        }
        response
    }

}
