package com.tmea.wit.service

import com.tmea.wit.model.BaseApiResponse
import com.tmea.wit.model.GeneralApiListResponse
import com.tmea.wit.model.dto.FieldErrorDto
import com.tmea.wit.model.dto.response.ApiCountryResponseDto
import com.tmea.wit.model.dto.response.ApiCurrencyResponseDto
import com.tmea.wit.util.CommonDBFunctions
import com.tmea.wit.util.ObjectUtils
import groovy.sql.Sql
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service

import javax.sql.DataSource

@Service
class CurrencyService {
    DataSource dataSource
    CommonDBFunctions commonDBFunctions

    @Autowired
    CurrencyService(DataSource dataSource, CommonDBFunctions commonDBFunctions) {
        this.dataSource = dataSource
        this.commonDBFunctions = commonDBFunctions
    }

    public GeneralApiListResponse getCurrencies(Map parameterMap){
        Sql sql = new Sql(dataSource)
        GeneralApiListResponse response
        List<ApiCurrencyResponseDto> currencyList = new ArrayList<>()
        List<FieldErrorDto> errors = []

        def params = ObjectUtils.flattenListParam(parameterMap)
        def status = params.status
        def query = params.query
        def statusFilter = ""
        def searchFilter =""
        Map sqlParams = [start: 0, limit: 25]
        boolean whereIncluded = false;

        if(status){
            def booleanStatus = CommonDBFunctions.getStatusAsBoolean(status)
            if(booleanStatus != null){
                sqlParams.status = booleanStatus
                statusFilter = " WHERE is_active = ?.status"
                whereIncluded = true
            }else{
                errors.add(new FieldErrorDto("status", "Invalid value passed. Check Documentation"))
                response = new GeneralApiListResponse([], 0, HttpStatus.BAD_REQUEST.value(), "Invalid status value", errors)
                return response
            }
        }

        if(query)
        {

            String querySearch = "%$query%"
            sqlParams.put("query",querySearch)
            def queryFilterClause = """ currency.name ILIKE ?.query OR 
                                        currency.code ILIKE ?.query"""
            searchFilter = whereIncluded ? " AND "+queryFilterClause : " WHERE "+queryFilterClause
        }



        //Pagination parameters & logic
        sqlParams = commonDBFunctions.paginationSqlParams(sqlParams, params)

        def fetchCurrencyQuery = "SELECT * FROM currency"
        def countQuery = "SELECT count(*) FROM currency"
        def queryFilters = statusFilter + searchFilter

        def data = sql.rows(fetchCurrencyQuery + queryFilters+ " ORDER BY name ASC" +commonDBFunctions.getLimitClause(), sqlParams)
        def total = queryFilters ? sql.firstRow(countQuery + queryFilters,sqlParams).get("count") : sql.firstRow(countQuery).get("count")
        if(data){
            response = new GeneralApiListResponse(ObjectUtils.snakeCaseArrayMapToListPojos(data, ApiCurrencyResponseDto), total as Long, HttpStatus.OK.value(), "Success")
        }else{
            response = new GeneralApiListResponse(null, total as Long, HttpStatus.NO_CONTENT.value(), "No records found")
        }



        return response
    }


    def BaseApiResponse updateCurrencyStatus(long currencyId, def body) {
        Sql sql = new Sql(dataSource)
        BaseApiResponse response = new BaseApiResponse(null, 200, "")
        def params = ObjectUtils.asMap(body)
        params.id = currencyId
        println("Status: " + params.status)
        def newStatus = (params.status == 'ACTIVE' || params.status == 'active')
        params.status = newStatus

        def isCurrencyExist = commonDBFunctions.getDBRecordById(currencyId, 'currency', 'id')
        if(isCurrencyExist){
            def update = sql.executeUpdate "UPDATE currency set is_active = ?.status WHERE id = ?.id", params
            sql.close()
            def statusMessage= params.status ? 'activated' : 'deactivated'

            if(update == 1){
                def currency = commonDBFunctions.getDBRecordById(currencyId, 'currency', 'id')
                def message = "Currency ("+ currency.name+ ") " + statusMessage
                def currencyResponse = ObjectUtils.snakeCaseMapToPojo(currency, new ApiCurrencyResponseDto())
                response.data = currencyResponse
                response.status = HttpStatus.OK.value()
                response.message = message
            }else{
                response.status = HttpStatus.BAD_REQUEST.value()
                response.message = "Update failed!!"
            }
        }else{
            response.status = HttpStatus.BAD_REQUEST.value()
            response.message = 'Currency with id ('+ currencyId +') does not exist!'
        }
        return response
    }
}
