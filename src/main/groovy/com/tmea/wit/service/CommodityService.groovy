package com.tmea.wit.service

import com.tmea.wit.core.storage.StorageApi
import com.tmea.wit.model.BaseApiResponse
import com.tmea.wit.model.GeneralApiListResponse
import com.tmea.wit.model.dto.FieldErrorDto
import com.tmea.wit.model.dto.response.ApiCommodityResponseDto
import com.tmea.wit.repository.UserRepository
import com.tmea.wit.util.CommonDBFunctions
import com.tmea.wit.util.FileUtils
import com.tmea.wit.util.ObjectUtils
import groovy.sql.Sql
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import javax.sql.DataSource

@Service
class CommodityService {
    DataSource dataSource
    UserRepository userRepository
    CommonDBFunctions commonDBFunctions
    StorageApi storageApi
    FileUtils fileUtils
    @Value('${services.storage.endpoint}')
    String fileStorageEndpoint
    final String COMMODITY_BUCKET_NAME = "commodity"

    @Autowired
    CommodityService(DataSource dataSource, UserRepository userRepository,
                     CommonDBFunctions commonDBFunctions, StorageApi storageApi,FileUtils fileUtils) {
        this.dataSource = dataSource
        this.userRepository = userRepository
        this.commonDBFunctions =commonDBFunctions
        this.storageApi = storageApi
        this.fileUtils = fileUtils
    }

    public BaseApiResponse addCommodity(def body, String userName){
        Sql sql = new Sql(dataSource)
        BaseApiResponse response = new BaseApiResponse()
        List<FieldErrorDto> errors = []
        Map requestParams = ObjectUtils.asMap(body)
        Long addedBy = userRepository.findUserIdByDetails(userName);
        if(addedBy){
            requestParams.put("addedBy", addedBy)
            def isCommodityExist = commonDBFunctions.getDBRecordByTableColumn(requestParams.name, 'commodity', 'name')
            if(isCommodityExist){
                response.data = ObjectUtils.mapToSnakeCaseMap(isCommodityExist)
                response.status = HttpStatus.BAD_REQUEST.value()
                response.message = 'Commodity name ('+ requestParams.name +') already exist!'
            }else{
                def isMeasurementUnitExist = commonDBFunctions.getDBRecordById(requestParams.marketPriceMeasurementUnitId,"measurement_unit","id")
                if(isMeasurementUnitExist) {
                   sql.withTransaction {
                    def query = """
                            INSERT INTO commodity (name, description, commodity_sub_category_id,added_by,
                                        market_price_measurement_unit_id, trade_statistics_measurement_unit_id)
                            VALUES (?.name, ?.description, ?.commoditySubCategoryId, ?.addedBy,
                                    ?.marketPriceMeasurementUnitId, ?.tradeStatisticsMeasurementUnitId)
                        """
                    def insertCommodity = sql.executeInsert(query, requestParams)
                    if (insertCommodity) {
                        Long commodityId = insertCommodity.get(0).get(0)

                        String commodityImage = requestParams.image
                        if (commodityImage) {
                            try{
                                String imageURL = fileUtils.saveFile(userName, commodityId, COMMODITY_BUCKET_NAME, commodityImage, storageApi)
                                Map updateParams = [id: commodityId, url: imageURL]
                                sql.executeUpdate("UPDATE commodity SET image_url = ?.url WHERE id = ?.id", updateParams)
                                response.message = "Success"
                            }catch(ConnectException e){
                                response.message = "Category Saved but Image could not be updated"

                            }

                            }
                        def commodity = sql.firstRow("""SELECT id,name,description,image_url FROM commodity WHERE id =?""",commodityId)

                        response.data = ObjectUtils.mapToSnakeCaseMap(commodity)
                        response.status = HttpStatus.OK.value()
                    } else {
                        response.data = []
                        response.message = "Failed to add commodity"
                        response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
                    }
                }
                }else
                {
                    response.message = "Measurement unit id " + requestParams.marketPriceMeasurementUnitId +" does not exist"
                    response.data = null
                    response.status = HttpStatus.BAD_REQUEST.value()
                }

            }

        }else{
            errors.add(new FieldErrorDto("userId", "Failed to fetch user details of the user adding the commodity"))
            response.data = []
            response.message = "Failed to add commodity"
            response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
            response.errors = errors
        }

        sql.close()
        response
    }

    public GeneralApiListResponse getCommodities(Map parameterMap , boolean isAdmin){
        Sql sql = new Sql(dataSource)
        GeneralApiListResponse response
        List<ApiCommodityResponseDto> commodityList = new ArrayList<>()
        List<FieldErrorDto> errors = []

        def params = ObjectUtils.flattenListParam(parameterMap)
        def status = params.status
        def query = params.query
        def category = params.categoryId
        def subCategory = params.subcategoryId
        def statusFilter  =""
        def searchFilter = ""
        def queryFilters =""
        def categoryFilter=""
        def subCategoryFilter=""
        def adminQuery = ""

        def sqlParams = [start: 0, limit: 25]

        //OverRide Status Filter if not Admin
        status = isAdmin ? status: "active"
        if(status){
            def booleanStatus = CommonDBFunctions.getStatusAsBoolean(status)
            if(booleanStatus != null){
                sqlParams.status = booleanStatus
                statusFilter = " AND commodity.is_active =?.status"
            }else{
                errors.add(new FieldErrorDto("status", "Invalid value passed. Check Documentation"))
                response = new GeneralApiListResponse([], 0, HttpStatus.BAD_REQUEST.value(), "Invalid status value", errors)
                return response
            }
        }
        if(category)
        {
            sqlParams.put("category",category)
            categoryFilter =" AND commodity_category.id =?.category::bigint"
        }
        if(subCategory)
        {
            sqlParams.put("subCategory",subCategory)
            subCategoryFilter =" AND commodity_sub_category.id =?.subCategory::bigint"
        }
        if(query)
        {

            String querySearch = "%$query%"
            sqlParams.put("query",querySearch)
            searchFilter = """ AND (
                                    commodity.name ILIKE ?.query OR
                                    commodity_sub_category.name ILIKE ?.query OR
                                    commodity_category.name ILIKE ?.query
                                  )
                          """

        }


        //Pagination parameters & logic
        sqlParams = commonDBFunctions.paginationSqlParams(sqlParams, params)
      if(isAdmin)
      {
          adminQuery = 'commodity.added_by,commodity.date_added, commodity.is_active,'

      }

        def fetchCommodityQuery = """SELECT 
                              commodity.id, 
                              commodity.name,
                              $adminQuery
                              commodity.image_url,
                              commodity_category.id AS commodity_category_id,
                              commodity_category.name AS commodity_category_name,
                              commodity_sub_category.id AS commodity_sub_category_id,
                              commodity_sub_category.name AS commodity_sub_category_name,
                              commodity.market_price_measurement_unit_id,
                              commodity.trade_statistics_measurement_unit_id,
                              market_measurement_unit.name AS market_price_measurement_unit,
                              trade_statistics_measurement_unit.name AS trade_statistics_measurement_unit
                              FROM commodity,commodity_sub_category,commodity_category,measurement_unit AS market_measurement_unit,measurement_unit AS trade_statistics_measurement_unit
                              WHERE 
                             commodity.commodity_sub_category_id = commodity_sub_category.id
                             AND 
                             market_measurement_unit.id = commodity.market_price_measurement_unit_id
                             AND 
                             trade_statistics_measurement_unit.id = commodity.trade_statistics_measurement_unit_id
                             AND
                             commodity_sub_category.commodity_category_id = commodity_category.id"""

        def countQuery = """SELECT count(*) FROM commodity,commodity_sub_category,commodity_category,measurement_unit AS market_measurement_unit,measurement_unit AS trade_statistics_measurement_unit
                              WHERE 
                             commodity.commodity_sub_category_id = commodity_sub_category.id
                             AND 
                             market_measurement_unit.id = commodity.market_price_measurement_unit_id
                             AND 
                             trade_statistics_measurement_unit.id = commodity.trade_statistics_measurement_unit_id
                             AND
                             commodity_sub_category.commodity_category_id = commodity_category.id """
        queryFilters = statusFilter + subCategoryFilter + categoryFilter + searchFilter

        def data = sql.rows(fetchCommodityQuery + queryFilters + " ORDER BY commodity.name ASC" + commonDBFunctions.getLimitClause(), sqlParams)
        def total = queryFilters ? sql.firstRow(countQuery + queryFilters,sqlParams).get("count") : sql.firstRow(countQuery).get("count")

        if(data){

            response = new GeneralApiListResponse(  ObjectUtils.mapToSnakeCaseArrayMap(data), total as Long, HttpStatus.OK.value(), "Success")
        }else{
            response = new GeneralApiListResponse([], total as Long, HttpStatus.NO_CONTENT.value(), "No records found")
        }
        sql.close()


        return response
    }

    public BaseApiResponse updateCommodity(Long commodityId, def body, String userName){
        Sql sql = new Sql(dataSource)
        BaseApiResponse response = new BaseApiResponse([], 200, "")
        def params = ObjectUtils.asMap(body)
        params.id = commodityId

        def isCommodityExist = commonDBFunctions.getDBRecordById(commodityId, 'commodity', 'id')
        if(isCommodityExist){
            String commodityImage = params.image
            String imageUrl = null
            if(commodityImage){
                imageUrl = fileUtils.saveFile(userName, commodityId, COMMODITY_BUCKET_NAME, commodityImage, storageApi)
            }else{
                imageUrl = isCommodityExist.get("image_url")
            }
            params.put("url", imageUrl)
            def query = """UPDATE commodity set name = ?.name, description = ?.description,
                       commodity_sub_category_id = ?.commoditySubCategoryId, image_url = ?.url,
                       market_price_measurement_unit_id = ?.marketPriceMeasurementUnitId, 
                       trade_statistics_measurement_unit_id = ?.tradeStatisticsMeasurementUnitId
                       WHERE id = ?.id"""
            def update = sql.executeUpdate query, params
            sql.close()

            if(update == 1){
                def commodity = commonDBFunctions.getDBRecordById(commodityId, 'commodity', 'id')
                def message = "Success"
                def commodityResponse = ObjectUtils.mapToSnakeCaseMap(commodity)
                response.data = commodityResponse
                response.status = HttpStatus.OK.value()
                response.message = message
            }else{
                response.status = HttpStatus.BAD_REQUEST.value()
                response.message = "Update failed!!"
            }
        }else{
            response.status = HttpStatus.BAD_REQUEST.value()
            response.message = 'Commodity with id ('+ commodityId +') does not exist!'
        }

        return response
    }

    def BaseApiResponse updateCommodityStatus(long commodityId, def body) {
        Sql sql = new Sql(dataSource)
        BaseApiResponse response = new BaseApiResponse([], 200, "")
        def params = ObjectUtils.asMap(body)
        params.id = commodityId
        def newStatus = (params.status.toString().toLowerCase() == 'active')
        params.status = newStatus

        def isCommodityExist = commonDBFunctions.getDBRecordById(commodityId, 'commodity', 'id')
        if(isCommodityExist){
            def update = sql.executeUpdate "UPDATE commodity set is_active = ?.status WHERE id = ?.id", params
            sql.close()
            def statusMessage = params.status ? 'activated' : 'deactivated'

            if(update == 1){
                def commodity = commonDBFunctions.getDBRecordById(commodityId, 'commodity', 'id')
                def message = "Commodity ("+ commodity.name+ ") " + statusMessage
                def commodityResponse = ObjectUtils.mapToSnakeCaseMap(commodity)
                response.data = commodityResponse
                response.status = HttpStatus.OK.value()
                response.message = message
            }else{
                response.status = HttpStatus.BAD_REQUEST.value()
                response.message = "Update failed!!"
            }
        }else{
            response.status = HttpStatus.BAD_REQUEST.value()
            response.message = 'Commodity with id ('+ commodityId +') does not exist!'
        }
        response
    }

    def BaseApiResponse updateCommodityImage(long commodityId, def body, String userName){
        Sql sql = new Sql(dataSource)
        BaseApiResponse response = new BaseApiResponse(null,200,"")
        def params = ObjectUtils.asMap(body)
        def isCommodityExist = commonDBFunctions.getDBRecordById(commodityId, 'commodity','id' )
        if(isCommodityExist){
            String commodityImage = params.image
            if(commodityImage){
                String imageURL = fileUtils.saveFile(userName, commodityId, COMMODITY_BUCKET_NAME, commodityImage, storageApi)
                Map updateParams = [id: commodityId, url: imageURL]
                int updateRes = sql.executeUpdate("UPDATE commodity SET image_url = ?.url WHERE id = ?.id", updateParams)
                if(updateRes){
                    def commodity = commonDBFunctions.getDBRecordById(commodityId, 'commodity','id' )
                    def message = "Commodity ("+ commodity.name+ ") Image Updated "
                    def commodityResponse = ObjectUtils.mapToSnakeCaseMap(commodity)
                    response.data = commodityResponse
                    response.status = HttpStatus.OK.value()
                    response.message = message
                }else{
                    response.status = HttpStatus.BAD_REQUEST.value()
                    response.message = "failed to update image!!"
                }
            }
        }else{
            response.status = HttpStatus.BAD_REQUEST.value()
            response.message = 'Commodity with id ('+ commodityId +') does not exist!'
        }

        sql.close()
        response
    }
}
