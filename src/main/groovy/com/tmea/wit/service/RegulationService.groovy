package com.tmea.wit.service

import com.tmea.wit.model.BaseApiResponse
import com.tmea.wit.model.GeneralApiListResponse
import com.tmea.wit.model.dto.FieldErrorDto
import com.tmea.wit.model.dto.response.ApiCommodityResponseDto
import com.tmea.wit.repository.UserRepository
import com.tmea.wit.util.CommonDBFunctions
import com.tmea.wit.util.ObjectUtils
import groovy.json.JsonOutput
import groovy.sql.Sql
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service

import javax.sql.DataSource

@Service
class RegulationService {
    DataSource dataSource
    UserRepository userRepository
    CommonDBFunctions commonDBFunctions
    @Autowired
    UserService userService

    @Autowired
    RegulationService(DataSource dataSource, UserRepository userRepository,
                      CommonDBFunctions commonDBFunctions) {
        this.dataSource = dataSource
        this.userRepository = userRepository
        this.commonDBFunctions = commonDBFunctions
    }

    public BaseApiResponse addRegulation(def body, String userName) {
        Sql sql = new Sql(dataSource)
        BaseApiResponse response = new BaseApiResponse([], 400, "")

        List<FieldErrorDto> errors = []
        Map requestParams = ObjectUtils.asMap(body)
        Long addedBy = userRepository.findUserIdByDetails(userName);

        //check if added by exist
        if (addedBy) {
            requestParams.addedBy = addedBy
            //check if trade operation exist
            def tradeOperation = commonDBFunctions.getDBRecordById(requestParams.tradeOperation, "trade_operation", "id")
            if (tradeOperation) {
                //check if sub category exist
                def subCategory = commonDBFunctions.getDBRecordById(requestParams.subCategory, "commodity_sub_category", "id")
                if (subCategory) {
                    def countryTo = commonDBFunctions.getDBRecordById(requestParams.countryTo, "country", "id")
                    def countryFrom = commonDBFunctions.getDBRecordById(requestParams.countryFrom, "country", "id")
                    // check if country from and country to exist
                    if (countryFrom && countryTo) {
                        //check if primary key fields exists
                        def regulationExist = sql.firstRow("""SELECT * FROM regulation 
                                                                   WHERE trade_operation_id=?.tradeOperation
                                                                   AND sub_category_id =?.subCategory
                                                                   AND country_to =?.countryTo
                                                                   AND country_from =?.countryFrom
                                                                   AND is_active""", requestParams)
                        if (!regulationExist) {
                            def insertRecord = sql.executeInsert("""INSERT INTO regulation (sub_category_id, trade_operation_id, country_from, country_to, is_active, added_by)
                                                                       VALUES (?.subCategory,?.tradeOperation,?.countryFrom,?.countryTo,TRUE,?.addedBy)""", requestParams)
                            if (insertRecord) {
                                def insertedRecord = sql.firstRow("""SELECT * FROM regulation WHERE id =?""", insertRecord.get(0).get(0))
                                response.message = 'record inserted successfully'
                                response.status = HttpStatus.OK.value()
                                response.data = ObjectUtils.mapToSnakeCaseMap(insertedRecord)
                            } else {
                                response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
                                response.message = 'an error occurred while inserting record'
                            }

                        } else {
                            response.message = 'regulation with similar details exist'
                            response.data = ObjectUtils.mapToSnakeCaseMap(regulationExist)
                        }

                    } else {
                        response.message = 'provided country from or country to does not exist'
                    }

                } else {
                    response.message = 'commodity sub category id ' + requestParams.subCategory + " does not exist"
                }
            } else {
                response.message = 'trade operation id ' + requestParams.tradeOperation + " does not exist"

            }


        } else {
            response.message = "user does not exist"
        }
        return response
    }

    public BaseApiResponse editRegulation(def body, regulationId, String username) {
        Sql sql = Sql.newInstance(dataSource)
        BaseApiResponse response = new BaseApiResponse([], 400, "")
        Map requestParams = ObjectUtils.asMap(body)
        Long userId = userRepository.findUserIdByDetails(username);
        List<FieldErrorDto> errorDto = new ArrayList<>()
        println("request params" + requestParams)
        if (userId) {
            if (requestParams) {
                def isRegulationExist = commonDBFunctions.getDBRecordByIdAndStatus(regulationId, "regulation", "id")
                if (isRegulationExist) {
                    def updateRecords = sql.executeUpdate("""UPDATE regulation 
                                                                 SET sub_category_id =?.subCategory,
                                                                 trade_operation_id =?.tradeOperation,
                                                                 country_from =?.countryFrom,
                                                                 country_to =?.countryTo""", requestParams)
                    if (updateRecords) {
                        def updatedRecord = sql.firstRow("""SELECT * FROM regulation WHERE id=?""", regulationId)
                        response.message = "record updated successfully"
                        response.status = HttpStatus.OK.value()
                        response.data = ObjectUtils.mapToSnakeCaseMap(updatedRecord)
                    } else {
                        response.message = "an error occurred while updating record"
                        response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
                    }


                } else {
                    response.message = 'regulation does not exist'
                }

            } else {
                response.message = "could not process empty pay load"
            }

        } else {
            response.message = "user does not exist"
        }
        sql.close()
        return response
    }

    public BaseApiResponse updateRegulationStatus(long regulationId, def body) {
        Sql sql = new Sql(dataSource)
        BaseApiResponse response = new BaseApiResponse([], 400, "")
        def params = ObjectUtils.asMap(body)
        params.regulationId = regulationId
        def newStatus
        if (params.status.toLowerCase() == 'active') {
            newStatus = true;
        }
        if (params.status.toLowerCase() == 'inactive') {
            newStatus = false;
        }
        params.status = newStatus
        def statusMessage = params.status ? 'activated' : 'deactivated'
        def isRegulationExist = commonDBFunctions.getDBRecordById(regulationId, 'regulation', 'id')
        if (isRegulationExist) {
            def updateStatus = sql.executeUpdate("""UPDATE regulation SET is_active =?.status WHERE id=?.regulationId""", params)
            if (updateStatus) {
                def updatedRecord = sql.firstRow("""SELECT * FROM regulation WHERE id=?""", regulationId)
                response.message = "regulation(" + statusMessage + ") successfully"
                response.status = HttpStatus.OK.value()
                response.data = ObjectUtils.mapToSnakeCaseMap(updatedRecord)
            } else {
                response.message = "an error occurred while updating record"
                response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
            }


        } else {
            response.message = "regulation does not exist"

        }
        sql.close()
        response

    }

    public GeneralApiListResponse getRegulation(Map parameterMap, boolean isAdmin) {
        Sql sql = new Sql(dataSource)
        GeneralApiListResponse response
        List<ApiCommodityResponseDto> regulationList = new ArrayList<>()
        List<FieldErrorDto> errors = []

        def params = ObjectUtils.flattenListParam(parameterMap)
        def status = params.status
        def query = params.query
        def subCategory = params.subCategoryId
        def statusFilter = ""
        def searchFilter = ""
        def queryFilters = ""
        def subCategoryFilter = ""

        def sqlParams = [start: 0, limit: 25]

        //OverRide Status Filter if not Admin
        status = isAdmin ? status : "active"
        if (status) {
            def booleanStatus = CommonDBFunctions.getStatusAsBoolean(status)
            if (booleanStatus != null) {
                sqlParams.status = booleanStatus
                statusFilter = " AND regulation.is_active =?.status"
            } else {
                errors.add(new FieldErrorDto("status", "Invalid value passed. Check Documentation"))
                response = new GeneralApiListResponse([], 0, HttpStatus.BAD_REQUEST.value(), "Invalid status value", errors)
                return response
            }
        }

        if (subCategory) {
            sqlParams.put("subCategory", subCategory)
            subCategoryFilter = " AND commodity_sub_category.id =?.category::bigint"
        }

        if (query) {

            String querySearch = "%$query%"
            sqlParams.put("query", querySearch)
            searchFilter = """ AND (
                                    trade_operation.name ILIKE ?.query OR
                                    commodity_sub_category.name ILIKE ?.query OR
                                    countryTo.name ILIKE ?.query OR
                                    countryFrom.name ILIKE ?.query
                                  )
                          """

        }

        def adminQuery = ""

        if (isAdmin) {
            adminQuery = 'regulation.added_by,regulation.date_added, regulation.is_active,'

        }
        //Pagination parameters & logic
        sqlParams = commonDBFunctions.paginationSqlParams(sqlParams, params)

        def fetchRegulationQuery = fetchRegulation(adminQuery)
        def countQuery = """SELECT  COUNT (*)
                        FROM trade_operation,commodity_sub_category,country AS CountryFrom,country AS countryTo,regulation
                        WHERE  regulation.trade_operation_id = trade_operation.id
                        AND    regulation.sub_category_id = commodity_sub_category.id
                        AND    regulation.country_to = countryTo.id
                        AND    regulation.country_from = CountryFrom.id"""
        queryFilters = statusFilter + subCategoryFilter + searchFilter
        def data = sql.rows(fetchRegulationQuery + queryFilters + " ORDER BY regulation.id ASC" + commonDBFunctions.getLimitClause(), sqlParams)
        def total = queryFilters ? sql.firstRow(countQuery + queryFilters, sqlParams).get("count") : sql.firstRow(countQuery).get("count")
        if (data) {

            response = new GeneralApiListResponse(ObjectUtils.mapToSnakeCaseArrayMap(data), total as Long, HttpStatus.OK.value(), "Success")
        } else {
            response = new GeneralApiListResponse([], total as Long, HttpStatus.NO_CONTENT.value(), "No records found")
        }

    }

    public BaseApiResponse getRegulationDetail(id, boolean isAdmin) {
        Sql sql = new Sql(dataSource)
        BaseApiResponse response = new BaseApiResponse([], 400, "")
        List<FieldErrorDto> errors = []
        Long regulationId = id
        def fetchRegulationQuery = fetchRegulation("") + (""" AND regulation.id=?""")
        def regulation = sql.firstRow(fetchRegulationQuery, regulationId)
        def procedureRequirements = []
        if(regulation){
            def isAdminProcedureFilter = isAdmin?""",is_active,date_added""":""
            def procedureStatusFilter = isAdmin?"":" AND is_active"
            def procedures = sql.rows("""SELECT id,title,description,regulation_id,is_apply_online,application_url,cost,time_frame,regulation_id,priority_number,payment_process $isAdminProcedureFilter FROM regulation_procedure WHERE regulation_id =? $procedureStatusFilter ORDER BY priority_number""", regulationId)
            def adminRequirementFilter = isAdmin?""",regulation_procedure_requirement.is_active,regulation_procedure_requirement.added_by,regulation_procedure_requirement.date_added""":""
            def requirementStatusFilter = isAdmin?"":""" AND regulation_procedure_requirement.is_active"""
            def allProcedureReq = sql.rows("""SELECT regulation_procedure_requirement.id,regulation_procedure_requirement.requirement,
                                                         regulation_procedure_requirement.regulation_procedure_id,
                                                         regulation_procedure_requirement.regulation_procedure_requirement_type_id, 
                                                         regulation_procedure_requirement_type.name AS regulation_requirement_type_name
                                                         $adminRequirementFilter 
                                                  FROM   regulation_procedure_requirement,
                                                         regulation_procedure_requirement_type,regulation_procedure
                                                  WHERE  regulation_procedure_requirement.regulation_procedure_id = regulation_procedure.id 
                                                  $requirementStatusFilter
                                                  AND    regulation_procedure.is_active AND regulation_procedure.regulation_id = ?""", regulationId)
            if (procedures) {
                procedures.each {
                    def procedure = it;
                    Long procedureId = procedure.get("id")
                    def currentProcedureReq = allProcedureReq.findAll({
                        return it.get("regulation_procedure_id") == procedureId
                    })

                    procedure.put("requirements", ObjectUtils.mapToSnakeCaseArrayMap(currentProcedureReq) )
                    procedureRequirements.add(procedure)
                }
            }
            regulation.put("procedures",ObjectUtils.mapToSnakeCaseArrayMap(procedureRequirements) )
        }else{
            response.message = 'regulation id ('+ id +') does not exist'
             sql.close()
            return response;
        }

        if (regulation) {
            response.message = "records found"
            response.data = ObjectUtils.mapToSnakeCaseMap(regulation)
            response.status = HttpStatus.OK.value()
        } else {
            response.data = []
            response.status = HttpStatus.NO_CONTENT.value()
            response.message = "record not found"
        }
        sql.close()
        return response
    }

    public String fetchRegulation(adminQuery) {
        def fetchQuery = """SELECT regulation.id,trade_operation.name AS trade_operation,$adminQuery commodity_sub_category.name AS commodity_sub_category,countryFrom.name AS country_from,countryTo.name AS country_to
                        FROM trade_operation,commodity_sub_category,country AS CountryFrom,country AS countryTo,regulation
                        WHERE  regulation.trade_operation_id = trade_operation.id
                        AND    regulation.sub_category_id = commodity_sub_category.id
                        AND    regulation.country_to = countryTo.id
                        AND    regulation.country_from = CountryFrom.id"""
    }

}



