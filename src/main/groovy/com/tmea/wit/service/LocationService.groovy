package com.tmea.wit.service

import com.tmea.wit.model.BaseApiResponse
import com.tmea.wit.model.GeneralApiListResponse
import com.tmea.wit.model.dto.FieldErrorDto
import com.tmea.wit.model.dto.response.ApiLocationResponseDto
import com.tmea.wit.repository.UserRepository
import com.tmea.wit.util.CommonDBFunctions
import com.tmea.wit.util.ObjectUtils
import groovy.sql.Sql
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service

import javax.sql.DataSource

@Service
class LocationService {
    DataSource dataSource
    UserRepository userRepository
    CommonDBFunctions commonDBFunctions

    @Autowired
    LocationService(DataSource dataSource, UserRepository userRepository, CommonDBFunctions commonDBFunctions) {
        this.dataSource = dataSource
        this.userRepository = userRepository
        this.commonDBFunctions = commonDBFunctions
    }

    public BaseApiResponse addLocation(def body){
        BaseApiResponse response = new BaseApiResponse([], 200, "", [])
        List<FieldErrorDto> errors = []
        Sql sql = new Sql(dataSource)
        def params = ObjectUtils.asMap(body)
        Authentication auth = SecurityContextHolder.getContext().getAuthentication()
        def addedBy = userRepository.findUserIdByDetails(auth.getName())
        if(addedBy){
            params.put("addedBy", addedBy)
            def isNameExist = commonDBFunctions.getDBRecordByTableColumn(params.name, 'location', 'name')
            if(isNameExist){
                response.status = HttpStatus.BAD_REQUEST.value()
                response.message = 'Location name ('+ params.name +') already exist!'
            }else{
                def isLocationLevelExist = commonDBFunctions.getDBRecordById(params.levelId,"location_level","id")
                if(isLocationLevelExist){
                    def query = """
                                INSERT INTO location (name, code_name, level_id, added_by)
                                VALUES (?.name, ?.codeName, ?.levelId, ?.addedBy) 
                            """
                    def insertLocation = sql.executeInsert query, params
                    if(insertLocation){
                        Long locationId = insertLocation?.get(0)?.get(0)
                        def location = commonDBFunctions.getDBRecordById(locationId, 'location', 'id')
                        def locationResponse = ObjectUtils.snakeCaseMapToPojo(location, new ApiLocationResponseDto())
                        response.data = locationResponse
                        response.status = HttpStatus.OK.value()
                        response.message = 'Success'
                    }else{
                        response.data = []
                        response.message = "Failed to add location"
                        response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
                    }
                }else
                {

                    response.status = HttpStatus.BAD_REQUEST.value()
                    response.message =  "Location Level Id "+ params.levelId + " does not exist"
                }

            }
        }else{
            errors.add(new FieldErrorDto("userId", "Failed to fetch user details of the user adding the location"))
            response.status = HttpStatus.BAD_REQUEST.value()
            response.message = "Failed to add location"
        }

        response
    }

    public GeneralApiListResponse getLocations(Map parameterMap, boolean isAdmin){
        Sql sql = new Sql(dataSource)
        GeneralApiListResponse response
        List<ApiLocationResponseDto> locationList = new ArrayList<>()
        List<FieldErrorDto> errors = []
        def params = ObjectUtils.flattenListParam(parameterMap)
        def status = params.status
        def sqlParams = [start: 0, limit: 25, levelId: params.levelId]
        def queryFilter = ""
        def limitFilter
        def queryFilterPrefix = ""
        def levelFilter = ""
        def statusFilter = ""
        def searchFilter = ""
        boolean whereIncluded = false;
        //OverRide Status Filter if not Admin
        status = isAdmin ? status: "active"
        if(status){
            def booleanStatus = CommonDBFunctions.getStatusAsBoolean(status)
            if(booleanStatus != null){
                sqlParams.status = booleanStatus
                statusFilter = " WHERE is_active = ?.status "
                whereIncluded = true
            }else{
                errors.add(new FieldErrorDto("status", "Invalid value passed. Check Documentation"))
                response = new GeneralApiListResponse([], 0, HttpStatus.BAD_REQUEST.value(), "Invalid status value", errors)
                return response
            }
        }

        if(params.levelId)
        {
            def levelClause = " level_id = ?.levelId::bigint"
            levelFilter= whereIncluded ? " AND "+levelClause : " WHERE "+levelClause
            whereIncluded = true
        }
        if(params.query)
        {
            String querySearch = "%$params.query%"
            sqlParams.put("query",querySearch)
            def queryFilterClause = """ location.name ILIKE ?.query OR 
                                        location.code_name ILIKE ?.query """
            searchFilter = whereIncluded ? " AND "+queryFilterClause : " WHERE "+queryFilterClause
        }

        //Pagination parameters & logic
        sqlParams = commonDBFunctions.paginationSqlParams(sqlParams, params)
        queryFilter = statusFilter + levelFilter + searchFilter
        def fetchLocationQuery = isAdmin? "SELECT * FROM location": "SELECT id,name,code_name FROM location"
        def data = sql.rows fetchLocationQuery+queryFilter+ "ORDER BY name ASC" +commonDBFunctions.getLimitClause(), sqlParams
        String countQuery = "SELECT count(*) FROM location"+ queryFilter
        def total
        if(queryFilter){
            total = sql.firstRow(countQuery, sqlParams)?.get('count')
        }else{
            total = sql.firstRow(countQuery)?.get('count')
        }
        !total ? total = 0 : total
        sql.close()



        if(data){
            response = new GeneralApiListResponse(ObjectUtils.mapToSnakeCaseArrayMap(data), total as Long, HttpStatus.OK.value(), "Success")
        }else{
            response = new GeneralApiListResponse(null, total as Long, HttpStatus.NO_CONTENT.value(), "No records found")
        }
        return response
    }

    public BaseApiResponse updateLocationStatus(Long locationId, def body){
        BaseApiResponse response = new BaseApiResponse([], 200, "")
        Sql sql = new Sql(dataSource)
        def params = ObjectUtils.asMap(body)
        params.id = locationId
        def newStatus = (params.status == 'ACTIVE' || params.status == 'active')
        params.status = newStatus

        def isLocationExist = commonDBFunctions.getDBRecordById(locationId, 'location', 'id')
        if(isLocationExist){
            def update = sql.executeUpdate "UPDATE location set is_active = ?.status WHERE id = ?.id", params
            sql.close()
            def statusMessage= newStatus ? 'activated' : 'deactivated'
            if(update == 1){
                def location = commonDBFunctions.getDBRecordById(locationId, 'location', 'id')
                def message = "Location ("+ location.name+ ") " + statusMessage
                def locationResponse = ObjectUtils.snakeCaseMapToPojo(location, new ApiLocationResponseDto())
                response.data = locationResponse
                response.status = HttpStatus.OK.value()
                response.message = message
            }else{
                response.status = HttpStatus.BAD_REQUEST.value()
                response.message = "Update failed!!"
            }
        }else{
            response.status = HttpStatus.BAD_REQUEST.value()
            response.message = 'Location with id ('+ locationId +') does not exist!'
        }

        response

    }


    def BaseApiResponse updateLocation(long locationId, def body) {
        Sql sql = new Sql(dataSource)
        BaseApiResponse response = new BaseApiResponse([], 200, "")
        def params = ObjectUtils.asMap(body)
        params.id = locationId

        def isLocationExist = commonDBFunctions.getDBRecordById(locationId, 'location', 'id')
        if(isLocationExist){
            def query = """
                            UPDATE location set name = ?.name, code_name = ?.codeName, level_id = ?.levelId
                             WHERE id = ?.id
                        """
            def update = sql.executeUpdate query, params
            sql.close()

            if(update == 1){
                def location = commonDBFunctions.getDBRecordById(locationId, 'location', 'id')
                def message = "Success"
                def locationResponse = ObjectUtils.snakeCaseMapToPojo(location, new ApiLocationResponseDto())
                response.data = locationResponse
                response.status = HttpStatus.OK.value()
                response.message = message
            }else{
                response.status = HttpStatus.BAD_REQUEST.value()
                response.message = "Update failed!!"
            }

        }else{
            response.status = HttpStatus.BAD_REQUEST.value()
            response.message = 'Location with id ('+ locationId +') does not exist!'
        }
        response
    }
}
