package com.tmea.wit.service

import com.tmea.wit.model.BaseApiResponse
import com.tmea.wit.model.dto.FieldErrorDto
import com.tmea.wit.model.dto.request.ApiPasswordResetDto
import com.tmea.wit.model.dto.request.ApiPasswordResetRequestDto
import com.tmea.wit.util.ObjectUtils
import groovy.sql.Sql
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service

import javax.sql.DataSource

@Service
class PasswordResetService {

    private final DataSource dataSource;
    private final SmsVerificationService smsVerificationService;

    @Autowired
    PasswordResetService(DataSource dataSource, SmsVerificationService smsVerificationService) {
        this.dataSource = dataSource;
        this.smsVerificationService = smsVerificationService;
    }

    public BaseApiResponse requestPhonePasswordReset(ApiPasswordResetRequestDto apiPasswordResetRequestDto){

        BaseApiResponse response = new BaseApiResponse();
        Sql sql = Sql.newInstance(dataSource);

        Map userDetails = sql.firstRow("""SELECT 
              id,
              phone_number
          FROM "user"
          WHERE phone_number ILIKE ?.phoneNumber
        """,apiPasswordResetRequestDto);

        if(userDetails){
            Long userId = userDetails.get("id");
            boolean smsSent = smsVerificationService.sendAccountVerificationSms(userId);
            if(smsSent){
                response.status = HttpStatus.OK.value();
                response.setData(userDetails);
                response.setMessage("Password reset code sent");
            }else{
                response.status = HttpStatus.INTERNAL_SERVER_ERROR.value();
                response.setData(null);
                response.setMessage("Could not reset password. Try again later");
            }
        }else{
            response.status = HttpStatus.BAD_REQUEST.value();
            response.setData(null);
            response.setMessage("Phone number not registered");
        }


        sql.close();

        return response;

    }

    public BaseApiResponse resetPhonePassword(ApiPasswordResetDto apiPasswordResetDto){

        Map requestParams = ObjectUtils.asMap(apiPasswordResetDto);
        BaseApiResponse response = new BaseApiResponse();

        Sql sql = Sql.newInstance(dataSource);
        def codeDetails = sql.firstRow("SELECT *,current_timestamp > user_account_verification.expiry_time AS expired FROM user_account_verification WHERE user_id = ?.userId AND code=?.code",requestParams);
        if(codeDetails){
            if(codeDetails.get("is_active")){
                if(codeDetails.get("expired")){
                    response.setStatus(HttpStatus.BAD_REQUEST.value());
                    response.setData(null);
                    response.setMessage("Invalid verification code");

                }else{
                    if(codeDetails.get("used")){
                        response.setStatus(HttpStatus.BAD_REQUEST.value());
                        response.setData(null);
                        response.setMessage("Invalid verification code");
                    }else {
                        if(requestParams.get("password") == requestParams.get("confirmPassword")){

                            BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder(12);
                            requestParams.put("encodedPassword",bCryptPasswordEncoder.encode(requestParams.get("password")));

                            int passwordUpdateAffectedRows = sql.executeUpdate("""UPDATE "user" SET password = ?.encodedPassword WHERE id = ?.userId""",requestParams);
                            if(passwordUpdateAffectedRows){
                                sql.executeUpdate("UPDATE user_account_verification SET used = TRUE WHERE id=?",codeDetails.get("id"));
                                response.setStatus(HttpStatus.OK.value());
                                response.setData(null);
                                response.setMessage("Password changed");

                            }else {
                                response.data = null;
                                response.status = HttpStatus.INTERNAL_SERVER_ERROR.value();
                                response.message = "Error. Could not update password";


                            }

                        }else{
                            response.data = null;
                            response.status = HttpStatus.BAD_REQUEST.value();
                            response.message = "Password Mismatch";
                            List<FieldErrorDto> errors = [];
                            errors.add(new FieldErrorDto("confirmPassword","Does not match password"))
                            response.setErrors(errors);
                        }

                    }
                }
            }else {
                response.setStatus(HttpStatus.BAD_REQUEST.value());
                response.setData(null);
                response.setMessage("Invalid verification code");
            }

        }else {
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            response.setData(null);
            response.setMessage("Invalid verification code");
        }


        sql.close();
        return response;
    }
}
