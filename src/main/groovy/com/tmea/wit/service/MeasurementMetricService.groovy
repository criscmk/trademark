package com.tmea.wit.service

import com.tmea.wit.model.BaseApiResponse
import com.tmea.wit.model.GeneralApiListResponse
import com.tmea.wit.model.dto.FieldErrorDto
import com.tmea.wit.model.dto.response.ApiLanguageResponseDto
import com.tmea.wit.model.dto.response.ApiMeasurementMetricResponseDto
import com.tmea.wit.repository.UserRepository
import com.tmea.wit.util.CommonDBFunctions
import com.tmea.wit.util.ObjectUtils
import groovy.sql.Sql
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service

import javax.sql.DataSource

@Service
class MeasurementMetricService {

    DataSource dataSource
    UserRepository userRepository
    CommonDBFunctions commonDBFunctions

    @Autowired
    MeasurementMetricService(DataSource dataSource, UserRepository userRepository, CommonDBFunctions commonDBFunctions) {
        this.dataSource = dataSource
        this.userRepository = userRepository
        this.commonDBFunctions = commonDBFunctions
    }

    public BaseApiResponse addMeasurementMetric(def body){
        Sql sql = new Sql(dataSource)
        BaseApiResponse response = new BaseApiResponse()
        List<FieldErrorDto> errors = []
        Map requestParams = ObjectUtils.asMap(body)
        Authentication auth = SecurityContextHolder.getContext().getAuthentication()
        String userName = auth.getName()
        Long addedBy = userRepository.findUserIdByDetails(userName);
        if(addedBy){
            requestParams.put("addedBy", addedBy)
            def isNameExist = commonDBFunctions.getDBRecordByTableColumn(requestParams.name, 'measurement_metric', 'name')
            if(isNameExist){
                response.status = HttpStatus.BAD_REQUEST.value()
                response.message = 'Measurement Metric name ('+ requestParams.name +') already exist!'
            }else{
                def query = """
                            INSERT INTO measurement_metric (name, description, code, added_by)
                            VALUES (?.name, ?.description, ?.code, ?.addedBy)
                        """
                def insertMetric = sql.executeInsert(query, requestParams)
                if(insertMetric){
                    Long metricId = insertMetric.get(0).get(0)
                    def category = commonDBFunctions.getDBRecordById(metricId, 'measurement_metric', 'id')
                    def data = ObjectUtils.snakeCaseMapToPojo(category, new ApiMeasurementMetricResponseDto())
                    response.data = data
                    response.message = "Success"
                    response.status = HttpStatus.OK.value()
                }else{
                    response.data = []
                    response.message = "Failed to add measurement metric"
                    response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
                }
            }
        }else{
            errors.add(new FieldErrorDto("userId", "Failed to fetch user details of the user adding the measurement metric"))
            response.data = []
            response.message = "Failed to add measurement metric"
            response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
            response.errors = errors
        }
        response
    }

    public GeneralApiListResponse getMeasurementMetrics(Map parameterMap){
        Sql sql = new Sql(dataSource)
        GeneralApiListResponse response

        List<FieldErrorDto> errors = []

        def params = ObjectUtils.flattenListParam(parameterMap)
        def status = params.status
        def code = params.get("code")
        def query = params.get("query")
        def sqlParams = [start: 0, limit: 25]
        boolean whereIncluded = false;

        def nameFilter = ""
        def codeFilter = ""
        def statusFilter = ""
        if(status){
            def booleanStatus = CommonDBFunctions.getStatusAsBoolean(status)
            if(booleanStatus != null){
                sqlParams.status = booleanStatus
                statusFilter = " WHERE is_active = ?.status"
                whereIncluded = true
            }else{
                errors.add(new FieldErrorDto("status", "Invalid value passed. Check Documentation"))
                response = new GeneralApiListResponse([], 0, HttpStatus.BAD_REQUEST.value(), "Invalid status value", errors)
                return response
            }
        }

        if(query)
        {

            String querySearch = "%$query%"
            sqlParams.put("query",querySearch)
            def queryFilterClause = """ measurement_metric.name ILIKE ?.query """
            nameFilter = whereIncluded ? " AND "+queryFilterClause : " WHERE "+queryFilterClause
        }

        if (code){
            String codeSearch = "%$code%"
            sqlParams.put("code",codeSearch)
            def codeFilterClause = """ measurement_metric.code_name ILIKE ?.code """
            codeFilter = whereIncluded ? " AND "+codeFilterClause : " WHERE "+codeFilterClause
        }

        //Pagination parameters & logic
        sqlParams = commonDBFunctions.paginationSqlParams(sqlParams, params)
        def queryFilters = statusFilter+nameFilter+codeFilter
        def fetchLanguagesQuery = "SELECT * FROM measurement_metric" + queryFilters + " ORDER BY name ASC "+ CommonDBFunctions.getLimitClause()
        def countQuery = "SELECT count(*) FROM measurement_metric" + queryFilters

        def data = sql.rows(fetchLanguagesQuery, sqlParams)
        def total = queryFilters ? sql.firstRow(countQuery,sqlParams).get("count") : sql.firstRow(countQuery).get("count")
        sql.close()

        if(data){
            response = new GeneralApiListResponse(ObjectUtils.snakeCaseArrayMapToListPojos(data, ApiMeasurementMetricResponseDto), total as Long, HttpStatus.OK.value(), "Success")
        }else{
            response = new GeneralApiListResponse([], total as Long, HttpStatus.OK.value(), "No records found")
        }
        return response
    }

    public BaseApiResponse updateMeasurementMetricStatus(Long metricId, def body){
        Sql sql = new Sql(dataSource)
        BaseApiResponse response = new BaseApiResponse([], 200, "")
        def params = ObjectUtils.asMap(body)
        params.id = metricId
        def newStatus = (params.status == 'ACTIVE' || params.status == 'active')
        params.status = newStatus

        def isMetricExist = commonDBFunctions.getDBRecordById(metricId, 'measurement_metric', 'id')
        if(isMetricExist){
            def update = sql.executeUpdate "UPDATE measurement_metric set is_active = ?.status WHERE id = ?.id", params
            sql.close()
            def statusMessage= params.status ? 'activated' : 'deactivated'

            if(update == 1){
                def metric = commonDBFunctions.getDBRecordById(metricId, 'measurement_metric', 'id')
                def message = "Metric ("+ metric.name+ ") " + statusMessage
                def metricResponse = ObjectUtils.snakeCaseMapToPojo(metric, new ApiMeasurementMetricResponseDto())
                response.data = metricResponse
                response.status = HttpStatus.OK.value()
                response.message = message
            }else{
                response.status = HttpStatus.BAD_REQUEST.value()
                response.message = "Update failed!!"
            }
        }else{
            response.status = HttpStatus.BAD_REQUEST.value()
            response.message = 'Measurement Metric with id ('+ metricId +') does not exist!'
        }

        response
    }

    def BaseApiResponse updateMeasurementMetric(long metricId, def body) {
        Sql sql = new Sql(dataSource)
        BaseApiResponse response = new BaseApiResponse([], 200, "")
        def params = ObjectUtils.asMap(body)
        params.id = metricId

        def isMetricExist = commonDBFunctions.getDBRecordById(metricId, 'measurement_metric', 'id')
        if(isMetricExist){
            def query = "UPDATE measurement_metric set name = ?.name, description = ?.description, code = ?.code WHERE id = ?.id"
            def update = sql.executeUpdate query, params
            sql.close()

            if(update == 1){
                def metric = commonDBFunctions.getDBRecordById(metricId, 'measurement_metric', 'id')
                def message = "Success"
                def metricResponse = ObjectUtils.snakeCaseMapToPojo(metric, new ApiMeasurementMetricResponseDto())
                response.data = metricResponse
                response.status = HttpStatus.OK.value()
                response.message = message
            }else{
                response.status = HttpStatus.BAD_REQUEST.value()
                response.message = "Update failed!!"
            }
        }else{
            response.status = HttpStatus.BAD_REQUEST.value()
            response.message = 'Measurement Metric with id ('+ metricId +') does not exist!'
        }
        response
    }
}
