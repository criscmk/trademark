package com.tmea.wit.service

import com.tmea.wit.model.BaseApiResponse
import com.tmea.wit.model.dto.FieldErrorDto
import com.tmea.wit.repository.UserRepository
import com.tmea.wit.util.CommonDBFunctions
import com.tmea.wit.util.ObjectUtils
import groovy.sql.Sql
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service

import javax.sql.DataSource

@Service
class RegulationProcedureService {
    DataSource dataSource
    UserRepository userRepository
    CommonDBFunctions commonDBFunctions
    @Autowired
    RegulationProcedureService(DataSource dataSource, UserRepository userRepository,
                              CommonDBFunctions commonDBFunctions) {
        this.dataSource = dataSource
        this.userRepository = userRepository
        this.commonDBFunctions = commonDBFunctions
    }
    public BaseApiResponse addRegulationProcedure(def body, String userName) {
        Sql sql = new Sql(dataSource)
        BaseApiResponse response = new BaseApiResponse([], 400, "")
        List<FieldErrorDto> errors = []
        Map requestParams = ObjectUtils.asMap(body)
        Long addedBy = userRepository.findUserIdByDetails(userName);
        //check if added by exist
        if (addedBy) {
            //check if regulation exist
            def regulationExist = commonDBFunctions.getDBRecordByIdAndStatus(requestParams.regulation,"regulation","id")
            if(regulationExist)
            {
                //check if regulation category exist
                def regulationCategoryExist = commonDBFunctions.getDBRecordByIdAndStatus(requestParams.regulationCategory,"regulation_category","id")
                if(regulationCategoryExist)
                {
                    //check if regulation procedure exist
                    def regulationProcedureExist = sql.firstRow("""SELECT * FROM regulation_procedure WHERE title ILIKE ?.title""",requestParams)

                    if(!regulationProcedureExist)
                    {
                        def insertRecord = sql.executeInsert("""INSERT INTO regulation_procedure ( regulation_id, title, description, is_apply_online, application_url, time_frame, cost, payment_process, priority_number, added_by, is_active, "regulation_category_id ") 
                                                            VALUES (?.regulation,?.title,?.description,?.applyOnline,?.applicationUrl,?.timeFrame,?.cost,?.paymentProcess,?.priority,?.addedBy,TRUE,?.regulationCategory)""",requestParams)
                        if(insertRecord)
                        {
                            def insertedRecord = sql.firstRow("""SELECT * FROM regulation_procedure WHERE id =?""",insertRecord.get(0).get(0))
                            response.message = "record inserted successfully"
                            response.status = HttpStatus.OK.value()
                            response.data = insertedRecord

                        }else
                        {
                            response.message = "an error occurred while inserting record"
                            response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
                        }
                    }
                    else
                    {
                        def  status = regulationProcedureExist.is_active
                        def responseMessage = status?"regulatory procedure "+ "($requestParams.title)" + " exist":"regulatory procedure "+ "($requestParams.title)" + " exist but is de activated"
                        response.data = ObjectUtils.mapToSnakeCaseMap(regulationProcedureExist)
                        response.message = responseMessage
                    }

                }else{

                    response.message = 'regulation category ' + requestParams.regulationCategory + " does not exist"
                }
            }else{
                response.message = "regulation "+ requestParams.regulation +" does not exist"

            }

        }else
        {
            response.message = "user does not exist"
        }
        sql.close()
         return  response
        }
    public BaseApiResponse editRegulationProcedure(def body, regulationProcedureId, String username) {
        Sql sql = Sql.newInstance(dataSource)
        BaseApiResponse response = new BaseApiResponse([], 400, "")
        Map requestParams = ObjectUtils.asMap(body)
        Long userId = userRepository.findUserIdByDetails(username);
        List<FieldErrorDto> errorDto = new ArrayList<>()

        if (userId) {
            if (requestParams) {
                def isRegulationProcedureExist = commonDBFunctions.getDBRecordByIdAndStatus(regulationProcedureId, "regulation_procedure", "id")
                if (isRegulationProcedureExist) {
                    requestParams.id = regulationProcedureId
                    def updateRecord = sql.executeUpdate("""UPDATE regulation_procedure 
                                                                 SET regulation_id =?.regulation,
                                                                 title =?.title,
                                                                 description =?.description,
                                                                 application_url =?.applicationUrl,
                                                                 cost=?.cost,
                                                                 is_apply_online =?.applyOnline,
                                                                 payment_process =?.paymentProcess,
                                                                 priority_number =?.priority,
                                                                 "regulation_category_id "=?.regulationCategoryId,
                                                                 time_frame =?.timeFrame
                                                                 WHERE id=?.id
                                                                 """, requestParams)
                    if (updateRecord) {
                        def updatedRecord = sql.firstRow("""SELECT * FROM regulation_procedure WHERE id =?""", regulationProcedureId)
                        response.message = 'record updated successfully'
                        response.status = HttpStatus.OK.value()
                        response.data = ObjectUtils.mapToSnakeCaseMap(updatedRecord)

                    }else {
                        response.message = " failed to update regulation procedure"
                        response.data = requestParams
                        response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
                    }

                }
                else {
                    response.message = "regulation procedure  does not exist"
                }


            }else{
                response.message = "cannot update empty payload"
            }

        }else{

            response.message = 'could not update record,user does not exist'
        }
        sql.close()
        response
    }

    public BaseApiResponse updateRegulationProcedureStatus(long regulationProcedureId, def body) {
        Sql sql = new Sql(dataSource)
        BaseApiResponse response = new BaseApiResponse([], 400, "")
        def params = ObjectUtils.asMap(body)
        params.regulationId = regulationProcedureId
        def newStatus=""
        if (params.status.toLowerCase() == 'active') {
            newStatus = true;
        }
        if (params.status.toLowerCase() == 'inactive') {
            newStatus = false;
        }
        params.status = newStatus
        def statusMessage = params.status ? 'activated' : 'deactivated'
        def isRegulationExist = commonDBFunctions.getDBRecordById(regulationProcedureId, 'regulation', 'id')
        if (isRegulationExist) {
            def updateStatus = sql.executeUpdate("""UPDATE regulation_procedure SET is_active =?.status WHERE id=?.regulationId""", params)
            if (updateStatus) {
                def updatedRecord = sql.firstRow("""SELECT * FROM regulation_procedure WHERE id=?""", regulationProcedureId)
                response.message = "regulation procedure ("+statusMessage+")  successfully"
                response.status = HttpStatus.OK.value()
                response.data = ObjectUtils.mapToSnakeCaseMap(updatedRecord)
            } else {
                response.message = "an error occurred while updating record"
                response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
            }


        } else {
            response.message = "regulation procedure id ("+ regulationProcedureId+")does not exist"

        }
        sql.close()
        response

    }
    public BaseApiResponse linkRegulationProcedureRequirement(id,def body, String userName) {
        Sql sql = new Sql(dataSource)
        BaseApiResponse response = new BaseApiResponse([], 400, "")

        List<FieldErrorDto> errors = []
        Map requestParams = ObjectUtils.asMap(body)
        Long addedBy = userRepository.findUserIdByDetails(userName);
        requestParams.regulationProcedure =id
        //check if added by exist
        if (addedBy) {
            requestParams.addedBy = addedBy
            def isRegulationProcedureRequirementTypeExist = commonDBFunctions.getDBRecordByIdAndStatus(requestParams.regulationProcedureRequirementType, "regulation_procedure_requirement_type", "id")
            if (isRegulationProcedureRequirementTypeExist) {

                def isRegulationProcedureExist = commonDBFunctions.getDBRecordByIdAndStatus(requestParams.regulationProcedure, "regulation_procedure", "id")
                if (isRegulationProcedureExist){
                    //check if record exist
                    def recordExist = sql.firstRow("""SELECT * FROM  regulation_procedure_requirement 
                                                          WHERE regulation_procedure_id =?.regulationProcedure
                                                          AND regulation_procedure_requirement_type_id =?.regulationProcedureRequirementType""",requestParams )
                    if(!recordExist)
                    {
                        def insertRecord = sql.executeInsert("""INSERT INTO regulation_procedure_requirement (regulation_procedure_requirement_type_id, is_active, added_by, requirement,regulation_procedure_id) 
                                                            VALUES (?.regulationProcedureRequirementType,TRUE,?.addedBy,?.text,?.regulationProcedure)""", requestParams)

                        if (insertRecord) {
                            def insertedRecord = sql.firstRow("""SELECT * FROM  regulation_procedure_requirement WHERE id =?""", insertRecord.get(0).get(0))
                            response.message = "Regulation Category added successfully"
                            response.status = HttpStatus.OK.value()
                            response.data = ObjectUtils.mapToSnakeCaseMap(insertedRecord)
                        } else {
                            response.message = "an error occurred while inserting record"
                            response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
                        }
                    }
                    else{
                        response.data = ObjectUtils.mapToSnakeCaseMap(recordExist)
                        response.message ='regulation procedure requirement exist'
                    }


                } else {
                    response.message = "regulation procedure " + "'$requestParams.regulationProcedure'" + " does not exist"
                }

            } else {
                response.message = "Regulation Procedure Requirement type" + "'$requestParams.regulationProcedureRequirementType'" + " doest not exist"
            }

        }

        else {
            response.message = 'user does not exist'
        }
        sql.close ( )
        response
    }
}
