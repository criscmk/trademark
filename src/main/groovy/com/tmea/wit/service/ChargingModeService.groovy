package com.tmea.wit.service

import com.tmea.wit.model.BaseApiResponse
import com.tmea.wit.model.GeneralApiListResponse
import com.tmea.wit.model.dto.FieldErrorDto
import com.tmea.wit.util.CommonDBFunctions
import com.tmea.wit.util.ObjectUtils
import groovy.sql.Sql
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service

import javax.sql.DataSource

@Service
class ChargingModeService {
    @Autowired
    DataSource dataSource
    CommonDBFunctions commonDBFunctions

    ChargingModeService(DataSource dataSource, CommonDBFunctions commonDBFunctions){
        this.dataSource = dataSource
        this.commonDBFunctions = commonDBFunctions

    }
    public BaseApiResponse addChargingMode(def body)
    {
        Sql sql = new Sql(dataSource)
        BaseApiResponse response = new BaseApiResponse()
        List<FieldErrorDto> errors = []
        Map requestParams = ObjectUtils.asMap(body)

        def isChargingModeExist = commonDBFunctions.getDBRecordByTableColumn(requestParams.get("name"),"charge_mode","name")
        if(!isChargingModeExist)
        {
            def insert = sql.executeInsert("""INSERT INTO charge_mode (name) VALUES (?.name)""",requestParams)
            if(isChargingModeExist)
            {
                def insertedRecord = sql.rows("""SELECT * FROM  charge_mode WHERE id ?""",insert.get(0).get(0))
                response.status = HttpStatus.OK.value()
                response.message = "Charge mode added successfully"
                response.data = insertedRecord
            }
            else
            {
                response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
                response.message = "An error occurred while inserting record"
                response.data = null
            }
        }else
        {
            response.status = HttpStatus.BAD_REQUEST.value()
            response.message = "Charging Mode Exists"
            response.data = ObjectUtils.mapToSnakeCaseMap(isChargingModeExist)
        }
        sql.close()
        return  response
    }
    public GeneralApiListResponse getChargingMode(Map parameterMap)
    {
        Sql sql = new Sql(dataSource)
        List<FieldErrorDto> errors = []
        GeneralApiListResponse response = new GeneralApiListResponse(null,null,400,"");
        def params = ObjectUtils.flattenListParam(parameterMap)
        response.message = "cmk"
        response.data =params
        def fetchChargeModeQuery = sql.rows("""SELECT  id,name FROM charge_mode """)
        def total = sql.firstRow("""SELECT count(1) FROM  charge_mode""").get("count")
        if(fetchChargeModeQuery)
        {
            response.status = HttpStatus.OK.value()
            response.message = "records found"
            response.data = fetchChargeModeQuery
            response.total = total
        }else
        {
            response.status = HttpStatus.NO_CONTENT.value()
            response.message = "no records found"
            response.data = fetchChargeModeQuery
        }
        sql.close()
        return  response

    }

}
