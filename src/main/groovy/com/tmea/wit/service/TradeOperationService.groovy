package com.tmea.wit.service

import com.tmea.wit.model.BaseApiResponse
import com.tmea.wit.model.GeneralApiListResponse
import com.tmea.wit.model.dto.FieldErrorDto
import com.tmea.wit.model.dto.response.ApiGenderResponseDto
import com.tmea.wit.model.dto.response.ApiLocationResponseDto
import com.tmea.wit.util.CommonDBFunctions
import com.tmea.wit.util.ObjectUtils
import groovy.sql.Sql
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service
import org.springframework.web.bind.annotation.GetMapping

import javax.sql.DataSource

@Service
class TradeOperationService {
    @Autowired
    DataSource dataSource
    CommonDBFunctions commonDBFunctions

    TradeOperationService(DataSource dataSource, CommonDBFunctions commonDBFunctions){
        this.dataSource = dataSource
        this.commonDBFunctions = commonDBFunctions

    }
    public BaseApiResponse addTradeOperation(def body)
    {
        Sql sql = new Sql(dataSource)
        BaseApiResponse response = new BaseApiResponse()
        List<FieldErrorDto> errors = []
        Map requestParams = ObjectUtils.asMap(body)

        def isTradeOperationExist = commonDBFunctions.getDBRecordByNameAndStatus(requestParams.get("name"),"trade_operation","name")
        if(isTradeOperationExist)
        {
            response.data = ObjectUtils.mapToSnakeCaseMap(isTradeOperationExist)
            response.status = HttpStatus.BAD_REQUEST.value()
            response.setMessage("trade operation " + requestParams.get("name") +  " already exist")
            return  response
        }else
        {
            def insertTradeOperation = sql.executeInsert("""INSERT INTO trade_operation ( name, description, is_active)
                                                            VALUES (?.name,?.description,TRUE)""",requestParams)
            if(insertTradeOperation)
            {
                def insertedRecord = sql.firstRow("""SELECT  * FROM trade_operation WHERE  id = ?""",insertTradeOperation.get(0).get(0))
                response.message = "record inserted successfully"
                response.status = HttpStatus.OK.value()
                response.data = ObjectUtils.mapToSnakeCaseMap(insertedRecord)
            }else
            {
                response.message = "an error occurred while inserting record"
                response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
            }
            sql.close()
            return response
        }

    }

    public GeneralApiListResponse getTradeOperation(Map parameterMap) {
        Sql sql = new Sql(dataSource)
    List<FieldErrorDto> errors = []
    GeneralApiListResponse response = new GeneralApiListResponse(null,null,400,"");
    def params = ObjectUtils.flattenListParam(parameterMap)
    response.message = "cmk"
    response.data =params
    def fetchTradeOperationQuery = sql.rows("""SELECT id,name,description FROM trade_operation WHERE is_active""")
    def total = sql.firstRow("""SELECT count(1) FROM  trade_operation WHERE is_active""").get("count")
    if(fetchTradeOperationQuery)
    {
        response.status = HttpStatus.OK.value()
        response.message = "records found"
        response.data = fetchTradeOperationQuery
        response.total = total
    }else
    {
        response.status = HttpStatus.NO_CONTENT.value()
        response.message = "no records found"
        response.data = fetchTradeOperationQuery
    }
    sql.close()
    return  response

}

    public BaseApiResponse updateTradeOperationStatus(Long tradeOperationId, def body){
        Sql sql = new Sql(dataSource)
        BaseApiResponse response = new BaseApiResponse([], 200, "")
        def params = ObjectUtils.asMap(body)
        params.id = tradeOperationId
        def newStatus = (params.status == 'ACTIVE' || params.status == 'active')
        params.status = newStatus
        def isTradeOperation = commonDBFunctions.getDBRecordById(tradeOperationId, 'trade_operation', 'id')
        if(isTradeOperation){
            def update = sql.executeUpdate "UPDATE trade_operation set is_active = ?.status WHERE id = ?.id", params
            sql.close()
            def statusMessage= newStatus ? 'activated' : 'deactivated'
            if(update == 1){
                def record = commonDBFunctions.getDBRecordById(tradeOperationId, 'trade_operation', 'id')
                def message = "Trade Operation (" + record.name + ") " + statusMessage
                def genderResponse = ObjectUtils.mapToSnakeCaseMap(record)
                response.data = genderResponse
                response.status = HttpStatus.OK.value()
                response.message = message
            }else{
                response.status = HttpStatus.BAD_REQUEST.value()
                response.message = "Update failed!!"
            }
        }else{
            response.status = HttpStatus.BAD_REQUEST.value()
            response.message = 'Trade Operation with id ('+ tradeOperationId +') does not exist!'
        }

        response
    }
}
