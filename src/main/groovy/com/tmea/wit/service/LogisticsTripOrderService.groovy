package com.tmea.wit.service

import com.tmea.wit.model.BaseApiResponse
import com.tmea.wit.model.GeneralApiListResponse
import com.tmea.wit.model.dto.FieldErrorDto
import com.tmea.wit.model.dto.response.ApiFetchTripOrderResponseDto
import com.tmea.wit.repository.UserRepository
import com.tmea.wit.util.CommonDBFunctions
import com.tmea.wit.util.ObjectUtils
import groovy.sql.Sql
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service

import javax.sql.DataSource

@Service
class LogisticsTripOrderService {

    DataSource dataSource
    UserRepository userRepository
    CommonDBFunctions commonDBFunctions
    LogisticsTripService logisticsTripService

    @Autowired
    LogisticsTripService(DataSource dataSource, UserRepository userRepository, CommonDBFunctions commonDBFunctions,LogisticsTripService logisticsTripService) {
        this.dataSource = dataSource
        this.userRepository = userRepository
        this.commonDBFunctions = commonDBFunctions
        this.logisticsTripService = logisticsTripService
    }

    public BaseApiResponse assignOrderToVehicle(def body) {
        BaseApiResponse response = new BaseApiResponse();
        List<FieldErrorDto> errors = []
        Sql sql = new Sql(dataSource)
        def queryParams = ObjectUtils.asMap(body)
        Authentication auth = SecurityContextHolder.getContext().getAuthentication()
        def vehicleId = queryParams.get("vehicleId")
        def userId = userRepository.findUserIdByDetails(auth.getName())
        queryParams.put("userId", userId)


        def ifTripScheduled = logisticsTripService.scheduledTrip(queryParams)
        if (ifTripScheduled) {

            def orderId = queryParams.get("orderId")
            // Check if the order id is valid(Exists and is active)
            def orderActive = sql.firstRow("""SELECT id FROM  logistics_service_order WHERE is_active AND id=?""",orderId)
            if(orderActive)
            {
                //check if order already assigned to another trip
                def activeOrderExist = sql.firstRow("""SELECT 
                                    logistics_trip_order.id 
                                    FROM 
                                    logistics_trip_order,
                                    logistics_trip
                                   
                    WHERE
                     logistics_trip_order.logistics_trip_id = logistics_trip.id
                    AND logistics_trip_order.is_active
                    AND logistics_trip.is_active
                    AND logistics_order_id = ?
                               """, orderId)

                if (!activeOrderExist) {
                    //check if order is assigned to a vehicle
                    def orderAssignedToVehicle = commonDBFunctions.getDBRecordByTableColumn(orderId, "logistics_trip_order", "logistics_order_id")
                    if (!orderAssignedToVehicle) {
                        def assignOrder = sql.executeInsert("""INSERT INTO logistics_trip_order 
                                                   (logistics_trip_id, logistics_order_id, date_created, assigned_by) 
                                                   VALUES (?.tripId, ?.orderId,current_timestamp,?.userId)""", queryParams)
                        if (assignOrder) {
                            def assignedRecordId = assignOrder.get(0).get(0)
                            def assignedRecord = commonDBFunctions.getDBRecordByTableColumn(assignedRecordId, "logistics_trip_order", "id")
                            response.setData(assignedRecord)
                            response.setStatus(HttpStatus.OK.value())
                            response.setMessage("Order assigned to the trip")

                        } else {
                            response.setMessage("An error occurred while inserting record")
                            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value())
                        }
                    } else {
                        response.setMessage("Order has already been assigned to a vehicle")
                        response.setStatus(HttpStatus.BAD_REQUEST.value())
                    }

                } else {
                    response.setMessage("order has already been assigned to a trip")
                    response.setStatus(HttpStatus.BAD_REQUEST.value())
                }
            }
            else
            {
                response.setMessage("Order is not active")
                response.setStatus(HttpStatus.BAD_REQUEST.value())
            }


        } else {
            response.setMessage("trip is not scheduled")
            response.setStatus(HttpStatus.BAD_REQUEST.value())
        }
        sql.close()
        response
    }

    public GeneralApiListResponse getLogisticsTripOrder(Long tripId) {
        Sql sql = new Sql(dataSource)
        GeneralApiListResponse response = new GeneralApiListResponse();
        List<FieldErrorDto> errors = []
        Authentication auth = SecurityContextHolder.getContext().getAuthentication()
        def userId = userRepository.findUserIdByDetails(auth.getName())
        def queryParams = [:]
        queryParams.put("userId", userId)
        queryParams.put("tripId", tripId)

        //check if trip order exist and is active
        def tripOrderExist = sql.firstRow("""SELECT  id from logistics_trip
                                                  WHERE  logistics_trip.id = ?.tripId""",queryParams)
        if(tripOrderExist)
        {
            def assignedOrders = sql.rows("""SELECT  
                                            logistics_trip.id,
                                            logistics_trader_vehicle.plate_number AS assigned_vehicle,
                                            logistics_service_order.order_number,
                                            logistics_trip_status.name AS trip_status
                                           FROM 
                                           logistics_trip,
                                           logistics_trip_order,
                                           logistics_service_order,
                                           logistics_trader_vehicle,
                                           logistics_trip_status
                                           WHERE
                                            logistics_trip.id = logistics_trip_order.logistics_trip_id
                                            AND
                                            logistics_trip_order.logistics_order_id = logistics_service_order.id
                                            AND
                                           logistics_trip.vehicle_id = logistics_trader_vehicle.id
                                            AND
                                            logistics_trip.logistics_trip_status_id = logistics_trip_status.id
                                             AND 
                                             logistics_trip_order.is_active    
                                           AND
                                           logistics_trip_order.logistics_trip_id = ?.tripId      
                                           """, queryParams)

            def countQuery = """ SELECT count(*)   FROM 
                                          
                                           logistics_trip,
                                           logistics_trip_order,
                                           logistics_service_order,
                                           logistics_trader_vehicle,
                                           logistics_trip_status
                                           WHERE
                                            logistics_trip.id = logistics_trip_order.logistics_trip_id
                                            AND
                                            logistics_trip_order.logistics_order_id = logistics_service_order.id
                                            AND
                                           logistics_trip.vehicle_id = logistics_trader_vehicle.id
                                            AND
                                            logistics_trip.logistics_trip_status_id = logistics_trip_status.id
                                             AND 
                                             logistics_trip_order.is_active    
                                           AND
                                           logistics_trip_order.logistics_trip_id = ?.tripId   
                                       """
            def total = 0
            total = sql.firstRow(countQuery, queryParams)?.get('count')

            if (assignedOrders) {
                List<ApiFetchTripOrderResponseDto> fetchTripOrderResponseDtoList;
                fetchTripOrderResponseDtoList = ObjectUtils.snakeCaseArrayMapToListPojos(assignedOrders, ApiFetchTripOrderResponseDto);
                response.setData(fetchTripOrderResponseDtoList)
                response.setTotal(total)
                response.setMessage("records found")
                response.setStatus(HttpStatus.OK.value())
            } else {
                response.setMessage("no records found")
                response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value())
            }

        }
        else{
            response.setMessage("trip id "+tripId +" does not exist or is inactive")
            response.setStatus(HttpStatus.BAD_REQUEST.value())
        }

        sql.close()
        response
    }

    public BaseApiResponse cancelTripOrder(Long tripOrderId)
    {
        Sql sql = new Sql(dataSource)
        BaseApiResponse response = new BaseApiResponse();
        List<FieldErrorDto> errors = []
        Authentication auth = SecurityContextHolder.getContext().getAuthentication()
        def userId = userRepository.findUserIdByDetails(auth.getName())
        def queryParams = [:]
        queryParams.put("userId", userId)
        queryParams.put("tripId", tripOrderId)

        def tripOrderExists = sql.firstRow("""SELECT * FROM logistics_trip_order WHERE is_active AND id =? """,tripOrderId)
        if(tripOrderExists)
        {
            def deActivateTripOrder = sql.executeUpdate("""UPDATE  logistics_trip_order SET is_active=FALSE WHERE id=?""",tripOrderId)
            if(deActivateTripOrder)
            {
                response.setStatus(HttpStatus.OK.value())
                response.setMessage("Trip order has been cancelled successfully")
            }else{
                response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value())
                response.setMessage("trip order failed to cancel")
            }

        }else{
            response.setMessage("Trip Order with "+ tripOrderId +" does not exists")
            response.setStatus(HttpStatus.BAD_REQUEST.value())
        }
        sql.close()
        response

    }


}
