package com.tmea.wit.service

import ch.qos.logback.core.rolling.helper.FileStoreUtil
import com.tmea.wit.core.storage.StorageApi
import com.tmea.wit.model.BaseApiResponse
import com.tmea.wit.model.GeneralApiListResponse
import com.tmea.wit.model.dto.FieldErrorDto
import com.tmea.wit.model.dto.response.ApiCommodityCategoryResponseDto
import com.tmea.wit.model.dto.response.ApiCommoditySubCategoryResponseDto
import com.tmea.wit.repository.UserRepository
import com.tmea.wit.util.CommonDBFunctions
import com.tmea.wit.util.FileUtils
import com.tmea.wit.util.ObjectUtils
import groovy.sql.Sql
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service

import javax.sql.DataSource

@Service
class CommoditySubCategoryService {
    DataSource dataSource
    UserRepository userRepository
    CommonDBFunctions commonDBFunctions
    StorageApi storageApi
    FileUtils fileUtils
    @Value('${services.storage.endpoint}')
    String fileStorageEndpoint

    final String SUB_CATEGORY_BUCKET_NAME = "sub-category";

    @Autowired
    CommoditySubCategoryService(DataSource dataSource, UserRepository userRepository,
                                CommonDBFunctions commonDBFunctions, StorageApi storageApi, FileUtils fileUtils) {
        this.dataSource = dataSource
        this.userRepository = userRepository
        this.commonDBFunctions = commonDBFunctions
        this.storageApi = storageApi
        this.fileUtils = fileUtils
    }

    public BaseApiResponse addCommoditySubCategory(def body, String userName){
        Sql sql = new Sql(dataSource)
        BaseApiResponse response = new BaseApiResponse()
        List<FieldErrorDto> errors = []
        Map requestParams = ObjectUtils.asMap(body)
        Long addedBy = userRepository.findUserIdByDetails(userName)
        if(addedBy){
            requestParams.put("addedBy", addedBy)
            def isNameExist = commonDBFunctions.getDBRecordByTableColumn(requestParams.name, 'commodity_sub_category', 'name')
            if(isNameExist){
                response.status = HttpStatus.BAD_REQUEST.value()
                response.message = 'Sub-Category name ('+ requestParams.name +') already exist!'
            }else{
                def query = """
                            INSERT INTO commodity_sub_category (name, description, commodity_category_id,added_by)
                            VALUES (?.name, ?.description, ?.commodityCategoryId, ?.addedBy)
                        """
                def insertSubCategory = sql.executeInsert(query, requestParams)
                if(insertSubCategory){
                    Long subCategoryId = insertSubCategory.get(0).get(0)
                    String subCategoryImage = requestParams.image
                    if(subCategoryImage){
                        try{
                            String imageURL = fileUtils.saveFile(userName, subCategoryId, SUB_CATEGORY_BUCKET_NAME, subCategoryImage, storageApi)
                            Map updateParams = [id: subCategoryId, url: imageURL]
                            sql.executeUpdate("UPDATE commodity_sub_category SET image_url = ?.url WHERE id = ?.id", updateParams)
                            response.message = "Success"
                        }catch(ConnectException e){
                            response.message = "Sub-Category Saved but Image could not be updated"
                        }
                    }
                    def category = commonDBFunctions.getDBRecordById(subCategoryId, 'commodity_sub_category', 'id')
                    def data = ObjectUtils.mapToSnakeCaseMap(category)
                    response.data = data

                    response.status = HttpStatus.OK.value()
                }else{
                    response.data = []
                    response.message = "Failed to add sub-category"
                    response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
                }
            }
        }else{
            errors.add(new FieldErrorDto("userId", "Failed to fetch user details of the user adding the sub-category"))
            response.data = []
            response.message = "Failed to add sub-category"
            response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
            response.errors = errors
        }
        sql.close()
        response
    }

    public GeneralApiListResponse getCommoditySubCategories(Map parameterMap,boolean isAdmin){
        Sql sql = new Sql(dataSource)
        GeneralApiListResponse response
        List<ApiCommoditySubCategoryResponseDto> subCategoryList = new ArrayList<>()
        List<FieldErrorDto> errors = []

        def params = ObjectUtils.flattenListParam(parameterMap)
        def status = params.status
        def query = params.get("query")
        def category = params.get("categoryId")
        def sqlParams = [start: 0, limit: 25]
        def queryFilters = ""
        def statusFilter = ""
        def searchFilter = ""
        def categoryFilter = ""
        //Overide Status Filter if not Admin
        status = isAdmin ? status: "active"
        if(status){
            def booleanStatus = CommonDBFunctions.getStatusAsBoolean(status)
            if(booleanStatus != null){
                sqlParams.status = booleanStatus
                statusFilter = " AND commodity_sub_category.is_active = ?.status"

            }else{
                errors.add(new FieldErrorDto("status", "Invalid value passed. Check Documentation"))
                response = new GeneralApiListResponse([], 0, HttpStatus.BAD_REQUEST.value(), "Invalid status value", errors)
                return response
            }
        }
        if(category)
        {

            sqlParams.put("category",category.toInteger())
            categoryFilter = " AND commodity_sub_category.commodity_category_id = ?.category"
        }



        if(query)
        {

            String querySearch = "%$query%"
            sqlParams.put("query",querySearch)
            searchFilter = """ AND (
                                    commodity_sub_category.name ILIKE ?.query OR
                                    commodity_category.name ILIKE ?.query
                                  )
                          """

        }

        //Pagination parameters & logic
        sqlParams = CommonDBFunctions.paginationSqlParams(sqlParams, params)

        queryFilters = statusFilter+ categoryFilter + searchFilter
        def adminQuery = ""
        if(isAdmin)
        {
            adminQuery = ',commodity_sub_category.added_by,  commodity_sub_category.is_active,commodity_sub_category.date_added'
        }

        def fetchCommoditySubCategoryQuery = """ SELECT 
                          commodity_sub_category.id,
                          commodity_sub_category.name,
                          commodity_sub_category.description,
                          commodity_category.name as commodity_category_name,
                          commodity_sub_category.commodity_category_id,
                          commodity_sub_category.image_url
                         
                          $adminQuery                        
                          FROM   
                          commodity_sub_category,
                          commodity_category
                          WHERE 
                          commodity_sub_category.commodity_category_id = commodity_category.id """

        def countQuery = """SELECT count(*) FROM commodity_sub_category,commodity_category
                                            WHERE commodity_sub_category.commodity_category_id = commodity_category.id """ + queryFilters




        def data = sql.rows(fetchCommoditySubCategoryQuery+ queryFilters + " ORDER BY commodity_sub_category.name ASC" +commonDBFunctions.getLimitClause(), sqlParams)
        def total = queryFilters ? sql.firstRow(countQuery,sqlParams).get("count") : sql.firstRow(countQuery).get("count")

        sql.close()

        if(data){
            response = new GeneralApiListResponse(ObjectUtils.mapToSnakeCaseArrayMap(data), total as Long, HttpStatus.OK.value(), "Success")
        }else{
            response = new GeneralApiListResponse([], total as Long, HttpStatus.NO_CONTENT.value(), "No records found")
        }
        return response
    }

    public BaseApiResponse updateCommoditySubCategory(Long subCategoryId, def body, String userName){
        Sql sql = new Sql(dataSource)
        BaseApiResponse response = new BaseApiResponse([], 200, "")
        def params = ObjectUtils.asMap(body)
        params.id = subCategoryId

        def isSubCategoryExist = commonDBFunctions.getDBRecordById(subCategoryId, 'commodity_sub_category', 'id')
        if(isSubCategoryExist){
            String subCategoryImage = params.image
            String imageUrl = null
            if(subCategoryImage){
                imageUrl = fileUtils.saveFile(userName, subCategoryId, SUB_CATEGORY_BUCKET_NAME, subCategoryImage, storageApi)
            }else{
                imageUrl = isSubCategoryExist.get("image_url")
            }
            params.put("url", imageUrl)
            def query = """UPDATE commodity_sub_category set name = ?.name, description = ?.description,
                       commodity_category_id = ?.commodityCategoryId, image_url = ?.url WHERE id = ?.id"""
            def update = sql.executeUpdate query, params
            sql.close()

            if(update == 1){
                def commoditySubCategory = commonDBFunctions.getDBRecordById(subCategoryId, 'commodity_sub_category', 'id')
                def message = "Success"
                def commoditySubCategoryResponse = ObjectUtils.mapToSnakeCaseMap(commoditySubCategory)
                response.data = commoditySubCategoryResponse
                response.status = HttpStatus.OK.value()
                response.message = message
            }else{
                response.status = HttpStatus.BAD_REQUEST.value()
                response.message = "Update failed!!"
            }
        }else{
            response.status = HttpStatus.BAD_REQUEST.value()
            response.message = 'Sub-Category with id ('+ subCategoryId +') does not exist!'
        }

        response
    }

    def BaseApiResponse updateCommoditySubCategoryStatus(Long subCategoryId, def body) {
        Sql sql = new Sql(dataSource)
        BaseApiResponse response = new BaseApiResponse([], 200, "")
        def params = ObjectUtils.asMap(body)
        params.id = subCategoryId
        def newStatus = (params.status.toString().toLowerCase() == 'active')
        params.status = newStatus

        def isSubCategoryExist = commonDBFunctions.getDBRecordById(subCategoryId, 'commodity_sub_category', 'id')
        if(isSubCategoryExist){
            def update = sql.executeUpdate "UPDATE commodity_sub_category set is_active = ?.status WHERE id = ?.id", params
            sql.close()
            def statusMessage= newStatus ? 'activated' : 'deactivated'

            if(update == 1){
                def commoditySubCategory = commonDBFunctions.getDBRecordById(subCategoryId, 'commodity_sub_category', 'id')
                def message = "Sub-Category ("+ commoditySubCategory.name+ ") " + statusMessage
                def commoditySubCategoryResponse = ObjectUtils.mapToSnakeCaseMap(commoditySubCategory)
                response.data = commoditySubCategoryResponse
                response.status = HttpStatus.OK.value()
                response.message = message
            }else{
                response.status = HttpStatus.BAD_REQUEST.value()
                response.message = "Update failed!!"
            }
        }else{
            response.status = HttpStatus.BAD_REQUEST.value()
            response.message = 'Sub-Category with id ('+ subCategoryId +') does not exist!'
        }
        return response
    }

    def BaseApiResponse updateCommoditySubCategoryImage(long subCategoryId, def body, String userName){
        Sql sql = new Sql(dataSource)
        BaseApiResponse response = new BaseApiResponse(null,200,"")
        def params = ObjectUtils.asMap(body)
        def isSubCategoryExist = commonDBFunctions.getDBRecordById(subCategoryId, 'commodity_sub_category','id' )
        if(isSubCategoryExist){
            String subCategoryImage = params.image
            if(subCategoryImage){
                String imageURL = fileUtils.saveFile(userName, subCategoryId, SUB_CATEGORY_BUCKET_NAME, subCategoryImage, storageApi)
                Map updateParams = [id: subCategoryId, url: imageURL]
                int updateRes = sql.executeUpdate("UPDATE commodity_sub_category SET image_url = ?.url WHERE id = ?.id", updateParams)
                if(updateRes){
                    def commoditySubCategory = commonDBFunctions.getDBRecordById(subCategoryId, 'commodity_sub_category','id' )
                    def message = "Sub Category ("+ commoditySubCategory.name+ ") Image Updated "
                    def commodityCategoryResponse = ObjectUtils.mapToSnakeCaseMap(commoditySubCategory)
                    response.data = commodityCategoryResponse
                    response.status = HttpStatus.OK.value()
                    response.message = message
                }else{
                    response.status = HttpStatus.BAD_REQUEST.value()
                    response.message = "failed to update image!!"
                }
            }
        }else{
            response.status = HttpStatus.BAD_REQUEST.value()
            response.message = 'Category with id ('+ subCategoryId +') does not exist!'
        }

        sql.close()
        response
    }
}
