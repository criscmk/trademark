package com.tmea.wit.service

import com.tmea.wit.model.BaseApiResponse
import com.tmea.wit.model.GeneralApiListResponse
import com.tmea.wit.model.dto.FieldErrorDto
import com.tmea.wit.model.dto.response.ApiFetchMarketPriceResponseDto
import com.tmea.wit.repository.UserRepository
import com.tmea.wit.util.CommonDBFunctions
import com.tmea.wit.util.ObjectUtils
import groovy.sql.Sql
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service

import javax.sql.DataSource

@Service
class MarketPriceService {
    @Autowired
    DataSource dataSource
    @Autowired
    UserService userService
    CommonDBFunctions commonDBFunctions
    UserRepository userRepository
    @Autowired
    GlobalConfigService globalConfigService;

    MarketPriceService(DataSource dataSource, UserRepository userRepository, CommonDBFunctions commonDBFunctions) {
        this.dataSource = dataSource
        this.commonDBFunctions = commonDBFunctions
        this.userRepository = userRepository
    }

    public BaseApiResponse postMarketPrice(def marketPriceDto, String username) {
        Map requestParams = ObjectUtils.asMap(marketPriceDto);
        BaseApiResponse response = new BaseApiResponse();
        Sql sql = Sql.newInstance(dataSource);
        List<FieldErrorDto> errors = [];
        Long userId = userService.getUserIdFromUsername(username);
        //check if the market price already exists
        def exitingPrice = sql.firstRow(checkIfMarketPriceExists(), requestParams);

        if (exitingPrice) {

            response.data = ObjectUtils.mapToSnakeCaseMap(exitingPrice)
            response.status = HttpStatus.BAD_REQUEST.value();
            response.setErrors(errors);
            response.setMessage("Commodity market price with similar details already exists");

        } else {
            def marketPriceCurrency = globalConfigService.getConfigValue("default_currency_config");
            if (marketPriceCurrency) {
                def measurementUnit = getCommodityMarketPriceMeasurementUnit(requestParams.get("commodity"))
                if (measurementUnit) {
                    Integer currency = Integer.parseInt(marketPriceCurrency);
                    requestParams.put("currency", currency);
                    requestParams.put("isActive", true);
                    requestParams.put("addedBy", userId);
                    requestParams.put("measurementUnit", measurementUnit);

                    def insertMarketPrice = sql.executeInsert(insertCommodityMarketPrice(), requestParams);
                    if (insertMarketPrice) {
                        def addedRecord = commonDBFunctions.getDBRecordById(insertMarketPrice.get(0).get(0), "market_price", "id")
                        response.status = HttpStatus.OK.value();
                        response.data = ObjectUtils.mapToSnakeCaseMap(addedRecord);
                        response.setMessage("Commodity market price has been inserted successfully");
                    } else {
                        response.status = HttpStatus.BAD_REQUEST.value();
                        response.setErrors(errors);
                        response.data = requestParams;
                        response.setMessage("An error occurred while inserting commodity market price ");
                    }
                } else {
                    response.status = HttpStatus.INTERNAL_SERVER_ERROR.value();
                    response.setErrors(errors);
                    response.data = requestParams;
                    response.setMessage("Measurement unit of commodity does not exist");

                }
            } else {
                response.status = HttpStatus.INTERNAL_SERVER_ERROR.value();
                response.setErrors(errors);
                response.data = requestParams;
                response.setMessage("Currency configuration needs to be defined to add a market price");

            }

        }

        return response
    }

    //update commodity market price
    public BaseApiResponse updateMarketPrice(def marketPriceUpdateDto, Long id, String username) {
        Map requestParams = ObjectUtils.asMap(marketPriceUpdateDto);
        BaseApiResponse response = new BaseApiResponse();
        Sql sql = Sql.newInstance(dataSource)
        List<FieldErrorDto> errors = [];
        Long userId = userService.getUserIdFromUsername(username);
        requestParams.put("id", id);
        requestParams.put("isActive", true)
        //check if market price exists
        def marketPriceExists = sql.firstRow(""" SELECT  * FROM market_price WHERE id = ?.id AND is_active """, requestParams);
        if (marketPriceExists) {
            Map queryParams = [:];
            queryParams.put("commodity", marketPriceExists.get("commodity_id"));
            queryParams.put("location", marketPriceExists.get("location_id"));
            queryParams.put("price", requestParams.get("price"));
            queryParams.put("date", marketPriceExists.get("date"));
            queryParams.put("currency", marketPriceExists.get("currency_id"));
            queryParams.put("isActive", true);
            queryParams.put("measurementUnit", marketPriceExists.get("measurement_unit_id"));
            queryParams.put("marketPriceId", marketPriceExists.get("id"));
            queryParams.put("addedBy", userId);
            //deactivate the commodity market price
            sql.withTransaction {
                def updateStatus = sql.executeUpdate("""UPDATE market_price SET is_active=false  WHERE id =? """, id);
                def insertNewMarketPrice = sql.executeInsert(insertCommodityMarketPrice(), queryParams);

                if (insertNewMarketPrice) {
                    def updatedRecord = sql.firstRow("""SELECT  * FROM  market_price WHERE  id =? """, insertNewMarketPrice.get(0).get(0))
                    response.status = HttpStatus.OK.value();
                    response.data = updatedRecord
                    response.setMessage("Commodity market price has been updated successfully");

                } else {
                    response.setErrors(errors);
                    response.setData();
                    response.status = HttpStatus.BAD_REQUEST.value();
                    response.setMessage("An error occurred while updating market price ");
                }
            }

        } else {
            response.status = HttpStatus.BAD_REQUEST.value();
            response.setErrors(errors);
            response.setData();
            response.setMessage("commodity market price does not exists or its already updated");
        }
        return response

    }

    //get market prices
    public GeneralApiListResponse getCommodityMarketPrice(Long commodity, Long location, String date, Long category, Long subCategory, String dateFrom, String dateTo, String query, String aggregator, Integer page, Integer size) {
        def requestParams = [:];
        def commodityFilter = "";
        requestParams.page = page
        requestParams.size = size
        if (commodity) {
            commodityFilter = " AND market_price.commodity_id  = ?.commodity ";
            requestParams.put("commodity", commodity);
        }


        def locationFilter = "";
        if (location) {
            locationFilter = " AND market_price.location_id = ?.location ";
            requestParams.put("location", location);
        }

        def dateFilter = "";
        if (date) {
            dateFilter = ' AND market_price."date" = ?.date::date ';
            requestParams.put("date", date);
        }

        def categoryFilter = "";
        if (category) {
            categoryFilter = " AND commodity_category.id = ?.category ";
            requestParams.put("category", category);
        }

        def subCategoryFilter = "";
        if (subCategory) {
            subCategoryFilter = " AND commodity_sub_category.id = ?.subCategory ";
            requestParams.put("subCategory", subCategory);
        }


        def dateRangeFilter = "";
        if (dateFrom && dateTo) {
            dateFilter = "";
            dateRangeFilter = ' AND market_price."date" BETWEEN date(?.dateFrom) AND date(?.dateTo)';
            requestParams.put("dateFrom", dateFrom);
            requestParams.put("dateTo", dateTo);
        }
        def queryFilter = "";
        if (query) {
            commodityFilter = "";
            categoryFilter = "";
            subCategoryFilter = "";

            String formattedQuery = "%$query%";
            queryFilter = """ AND (
                                    commodity.name ILIKE ?.query OR
                                    commodity_sub_category.name ILIKE ?.query OR
                                    commodity_category.name ILIKE ?.query
                                  )
                          """;
            requestParams.put("query", formattedQuery);

        }

        def annualAggregator = ""
        def annualGroupBy = ""
        def monthAggregator = ""
        def monthGroupBy = ""
        def commodityAggregator = "";
        def commodityGroupBy = "";
        def categoryAggregator = "";
        def categoryGroupBy = "";
        def subCategoryAggregator = "";
        def subCategoryGroupBy = "";
        def aggregatorFilter = "";
        def commaGroupByFilter = "";
        def groupByFilter = "";
        def monthString = "";
        def yearString = "";
        if (aggregator) {
            String[] myAggregator = aggregator.split(",");
            Map aggregatorsMap = [:];
            for (int i = 0; i <= myAggregator.size() - 1; i++) {
                aggregatorsMap.put(myAggregator.getAt(i), myAggregator.getAt(i));
            }

            if (aggregatorsMap.get("annual")) {
                annualAggregator = "prices.year,"
                annualGroupBy = "year,"
            }

            if (aggregatorsMap.get("month")) {
                monthAggregator = "prices.month,"
                monthGroupBy = "month,"
                annualAggregator = "prices.year,"
                annualGroupBy = "year,"
            }

            if (aggregatorsMap.get("annual")) {
                annualAggregator = "prices.year,"
                annualGroupBy = "year,"
            }

            if (aggregatorsMap.get("commodity")) {
                commodityAggregator = "prices.commodity,"
                commodityGroupBy = "commodity,"

            }
            if (aggregatorsMap.get("category")) {
                categoryAggregator = "prices.commodity_category,"
                categoryGroupBy = "commodity_category,"

            }
            if (aggregatorsMap.get("subCategory")) {

                subCategoryAggregator = "prices.commodity_sub_category,"
                subCategoryGroupBy = "commodity_sub_category,"
                categoryAggregator = "prices.commodity_category,"
                categoryGroupBy = "commodity_category,"
            }
            aggregatorFilter = annualAggregator + monthAggregator + commodityAggregator + categoryAggregator + subCategoryAggregator;
            commaGroupByFilter = annualGroupBy + monthGroupBy + commodityGroupBy + categoryGroupBy + subCategoryGroupBy;
            groupByFilter = commaGroupByFilter.substring(0, commaGroupByFilter.length() - 1);
        }

        def filterQueryStr = commodityFilter + locationFilter + subCategoryFilter + categoryFilter + dateFilter + queryFilter + dateRangeFilter;

        GeneralApiListResponse response = new GeneralApiListResponse();
        Sql sql = Sql.newInstance(dataSource);
        List<FieldErrorDto> errors = [];
        if (!aggregatorFilter) {
            Map paginateParams = [start: 0, limit: 25]
            def paginateOffsetLimit = commonDBFunctions.paginationSqlParams(paginateParams, requestParams)
            requestParams = requestParams + paginateOffsetLimit
        }
        if (aggregator) {
            monthString = """trim(TO_CHAR(market_price.date,'Month')) AS "month","""
            yearString = """(extract('year' FROM market_price.date))::int AS "year" """ + ",";

        }

        def marketPrice = """
               SELECT
               commodity.name AS commodity,
               location.name AS location,
               market_price.id,
               to_char(market_price.date,'YYYY-MM-DD') AS date,
               market_price.price,
               commodity_category.name As commodity_category,
               commodity_sub_category.name As commodity_sub_category,""" + monthString + yearString + """currency.name AS currency,
               currency.currency_symbol AS currency_symbol,
               measurement_unit.name AS measurement_unit          
             FROM
             market_price,
             commodity,
             commodity_category,
             commodity_sub_category,
             location,
             currency,
             measurement_unit
            WHERE 
            market_price.commodity_id = commodity."id"
            AND commodity.commodity_sub_category_id = commodity_sub_category.id
            AND commodity_sub_category.commodity_category_id = commodity_category.id
            AND market_price.location_id = location."id" 
            AND market_price.currency_id = currency."id" 
            AND market_price.measurement_unit_id = measurement_unit."id"
            AND market_price.is_active         
        """ + filterQueryStr;
        def marketPriceStatsQuery = [:]
        def total = filterQueryStr ? sql.firstRow(filterAllMarketPricesCount() + filterQueryStr, requestParams).get("count") : sql.firstRow(filterAllMarketPricesCount()).get("count")

        if (aggregator) {
            if (!filterQueryStr) {
                requestParams = [:]
            }
            total = null
            marketPriceStatsQuery = sql.rows("""
                                                SELECT 
                                                ${aggregatorFilter}
                                               COUNT(prices.price),
                                               MAX(prices.price) AS highest,
                                               MIN(prices.price) AS lowest,
                                               ROUND(cast(AVG(prices.price) as numeric),2) AS AVG
                                               FROM(${marketPrice}) AS prices
                                               GROUP BY ${groupByFilter}
                                                """, requestParams);
            response.data = ObjectUtils.mapToSnakeCaseArrayMap(marketPriceStatsQuery)
        } else {
            marketPriceStatsQuery = sql.rows(marketPrice + " ORDER BY market_price.date DESC" + commonDBFunctions.getLimitClause(), requestParams);
            response.data = ObjectUtils.snakeCaseArrayMapToListPojos(marketPriceStatsQuery, ApiFetchMarketPriceResponseDto);

        }

        if (marketPriceStatsQuery) {
            response.status = HttpStatus.OK.value();
            response.total = total;
            response.setMessage("Market Price Found");
        } else {
            response.status = HttpStatus.NO_CONTENT.value();
            response.setData(marketPriceStatsQuery);
            response.setMessage("No Market Price Found");
        }

        return response;
    }

    //insert into commodity market price table function
    public String insertCommodityMarketPrice() {
        def insertQuery = ("""
                        INSERT INTO "market_price"
                        (commodity_id, location_id, price, currency_id,date, is_active,added_by,date_added,measurement_unit_id)
                        VALUES (?.commodity,?.location,?.price,?.currency,?.date::date ,?.isActive,?.addedBy,current_timestamp,?.measurementUnit)
                        """);
        return insertQuery;
    }

    //check if price commodity exists
    public String checkIfMarketPriceExists() {
        def marketPriceExists = """SELECT *
                          
           FROM market_price
            WHERE 
            commodity_id = ?.commodity AND 
            location_id = ?.location AND 
            "date" = ?.date::date AND
            is_active
             """

    }

    public String filterAllMarketPricesCount(String selectType) {
        def marketPriceQuery = ("""SELECT COUNT(1) 
                        
             FROM
             market_price,
             commodity,
             commodity_category,
             commodity_sub_category,
             location,
             currency,
             measurement_unit
            WHERE 
            market_price.commodity_id = commodity."id"
            AND commodity.commodity_sub_category_id = commodity_sub_category.id
            AND commodity_sub_category.commodity_category_id = commodity_category.id
            AND market_price.location_id = location."id" 
            AND market_price.currency_id = currency."id" 
            AND market_price.measurement_unit_id = measurement_unit."id" 
            AND market_price.is_active        
        """);
    }

    public Long getCommodityMarketPriceMeasurementUnit(Long commodityId) {
        Sql sql = Sql.newInstance(dataSource);
        Long measurementUnitId = sql.firstRow("SELECT market_price_measurement_unit_id FROM commodity WHERE id = ?", commodityId).get("market_price_measurement_unit_id");
        sql.close();
        return measurementUnitId;
    }
}
