package com.tmea.wit.service

import com.tmea.wit.model.BaseApiResponse
import com.tmea.wit.model.GeneralApiListResponse
import com.tmea.wit.model.dto.FieldErrorDto
import com.tmea.wit.model.dto.response.ApiCommodityResponseDto
import com.tmea.wit.repository.UserRepository
import com.tmea.wit.util.CommonDBFunctions
import com.tmea.wit.util.ObjectUtils
import groovy.sql.Sql
import org.springframework.http.HttpStatus
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service

import javax.sql.DataSource

@Service
class StandardService {
    DataSource dataSource
    UserRepository userRepository
    CommonDBFunctions commonDBFunctions
    StandardService(DataSource dataSource, UserRepository userRepository, CommonDBFunctions commonDBFunctions) {
        this.dataSource = dataSource
        this.userRepository = userRepository
        this.commonDBFunctions = commonDBFunctions
    }
    public BaseApiResponse addStandard(def body) {
        Sql sql = new Sql(dataSource)
        BaseApiResponse response = new BaseApiResponse([], 400, "")
        List<FieldErrorDto> errors = []
        Map requestParams = ObjectUtils.asMap(body)
        Authentication auth = SecurityContextHolder.getContext().getAuthentication()
        String userName = auth.getName()
        Long addedBy = userRepository.findUserIdByDetails(userName)
        if (addedBy) {
            requestParams.put("addedBy", addedBy)
            def isRecordExist = sql.firstRow("""SELECT id,title,description FROM standard WHERE commodity_id =?.commodity AND import_location=?.importLocation
                                                    AND export_location=?.exportLocation AND trade_operation_id=?.tradeOperation""", requestParams)
            if (!isRecordExist) {
                def isCommodityExist = commonDBFunctions.getDBRecordByIdAndStatus(requestParams.commodity,"commodity","id")
                if(isCommodityExist){
                    def isImportLocationExist = commonDBFunctions.getDBRecordByIdAndStatus(requestParams.importLocation,"import_export_locations","id")
                    def isExportLocationExist = commonDBFunctions.getDBRecordByIdAndStatus(requestParams.exportLocation,"import_export_locations","id")

                    if(isImportLocationExist && isExportLocationExist)
                    {
                        def isTradeOperationExist =commonDBFunctions.getDBRecordByIdAndStatus(requestParams.tradeOperation,"trade_operation","id")
                        if(isTradeOperationExist){
                            def insertRecord = sql.executeInsert("""INSERT INTO standard (title, description, trade_operation_id, commodity_id, import_location, export_location, is_active, added_by) 
                                                                        VALUES (?.title,?.description,?.tradeOperation,?.commodity,?.importLocation,?.exportLocation,TRUE,?.addedBy)""",requestParams)
                            if(insertRecord){
                                def insertedRecord = sql.firstRow("""SELECT * FROM standard where id=? """, insertRecord.get(0).get(0))
                                response.message = "record added successfully"
                                response.status = HttpStatus.OK.value()
                                response.data = ObjectUtils.mapToSnakeCaseMap(insertedRecord)

                            }else {
                                response.message = 'an error occurred while inserting record'
                                response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
                            }

                        }else {
                            response.message = "trade operation does not exist or is inactive"
                        }


                    }else{
                        response.message = "import location or export location does not exist or is inactive"
                    }


                }else
                {
                    response.message = "commodity id ("+ requestParams.commodity +") does not exist"
                }

            }else{
                response.message = 'standard with similar details exist  '
                response.data = ObjectUtils.mapToSnakeCaseMap(isRecordExist)

            }
        }else{
            response.message = 'could not find logged in user'
        }
        sql.close()
        return  response
    }
    public BaseApiResponse updateStandardStatus(Long id,def body) {
        Sql sql = new Sql(dataSource)
        BaseApiResponse response = new BaseApiResponse([], 400, "")
        List<FieldErrorDto> errors = []
        Map requestParams = ObjectUtils.asMap(body)
        requestParams.id = id
        def newStatus = ""
        Authentication auth = SecurityContextHolder.getContext().getAuthentication()
        String userName = auth.getName()
        Long updatedBy = userRepository.findUserIdByDetails(userName)
        if (requestParams.status.toLowerCase() == 'active') {
            requestParams.newStatus = true;
        } else if (requestParams.status.toLowerCase() == 'inactive') {
            requestParams.newStatus = false;
        }
        if (updatedBy) {
            def responseMessage = requestParams.newStatus ? "activated" : "deactivated"
            def recordExist = commonDBFunctions.getDBRecordById(id, "standard", "id")
            if (recordExist) {
                def updateQuery = sql.executeUpdate("""UPDATE standard SET is_active =?.newStatus WHERE id=?.id""", requestParams)

                if (updateQuery) {
                    def updatedRecord = sql.firstRow("""SELECT * FROM standard where id=? """, requestParams.id)
                    response.message = "record ("+responseMessage +") successfully"
                    response.status = HttpStatus.OK.value()
                    response.data = ObjectUtils.mapToSnakeCaseMap(updatedRecord)
                }else
                {
                    response.message = "an error occurred while updating status"
                    response.status =HttpStatus.INTERNAL_SERVER_ERROR.value()
                }
            }else {
                response.message = 'record with id ('+id +') does not exist'
            }
        }else{
            response.message = 'could not find logged in user'
        }
        sql.close()
        return  response

    }
    public BaseApiResponse updateStandardDetails(Long id,def body) {
        Sql sql = new Sql(dataSource)
        BaseApiResponse response = new BaseApiResponse([], 400, "")
        List<FieldErrorDto> errors = []
        Map requestParams = ObjectUtils.asMap(body)
        Authentication auth = SecurityContextHolder.getContext().getAuthentication()
        String userName = auth.getName()
        Long updatedBy = userRepository.findUserIdByDetails(userName)
        if(updatedBy) {
            if (requestParams) {
                //check if product exists
                def recordExist = commonDBFunctions.getDBRecordByIdAndStatus(id, "standard", "id")
                if (recordExist) {
                    requestParams.id = id
                    def updateQuery = sql.executeUpdate(
                            """UPDATE standard SET title =?.title,description =?.description,commodity_id =?.commodity,
                                                trade_operation_id =?.tradeOperation,import_location=?.importLocation,
                                                export_location=?.exportLocation            
                                   WHERE id = ?.id""", requestParams)
                    if (updateQuery == 1) {
                        def updatedRecord =  commonDBFunctions.getDBRecordByIdAndStatus(id,"standard","id")
                        response.status = HttpStatus.OK.value()
                        response.data = ObjectUtils.mapToSnakeCaseMap(updatedRecord)
                        response.message = "record updated successfully"
                    } else {
                        response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
                        response.message = "failed to update record details"
                    }

                }else
                {
                    response.message ="record does not exist"

                }
            }
        }
        sql.close()
        return  response
    }
    public GeneralApiListResponse getStandard(Map parameterMap, boolean isAdmin) {
        Sql sql = new Sql(dataSource)
        GeneralApiListResponse response
        List<FieldErrorDto> errors = []

        def params = ObjectUtils.flattenListParam(parameterMap)
        def status = params.status
        def query = params.query
        def commodity = params.commodity
        def tradeOperation = params.tradeOperation
        def importLocation = params.importLocation
        def exportLocation = params.exportLocation
        def statusFilter = ""
        def searchFilter = ""
        def queryFilters = ""
        def commodityFilter = ""
        def tradeOperationFilter = ""
        def importLocationFilter = ""
        def exportLocationFilter = ""
        def sqlParams = [start: 0, limit: 25]
        //OverRide Status Filter if not Admin
        status = isAdmin ? status : "active"
        if (status) {
            def booleanStatus = CommonDBFunctions.getStatusAsBoolean(status)
            if (booleanStatus != null) {
                sqlParams.status = booleanStatus
                statusFilter = " AND standard.is_active =?.status"
            } else {
                errors.add(new FieldErrorDto("status", "Invalid value passed. Check Documentation"))
                response = new GeneralApiListResponse([], 0, HttpStatus.BAD_REQUEST.value(), "Invalid status value", errors)
                return response
            }
        }
        if (commodity) {
            sqlParams.put("commodity", commodity)
            commodityFilter = " AND commodity.id =?.commodity::BigInt"
        }

        if (tradeOperation) {
            sqlParams.put("tradeOperation", tradeOperation)
            tradeOperationFilter = " AND trade_operation.id =?.tradeOperation::BigInt"
        }

        if (exportLocation) {
            sqlParams.put("exportLocation", exportLocation)
            exportLocationFilter = " AND export_location.id =?.exportLocation::BigInt"
        }

        if (importLocation) {
            sqlParams.put("importLocation", importLocation)
            importLocationFilter = " AND import_location.id =?.importLocation::BigInt"
        }

        if (query) {
            String querySearch = "%$query%"
            sqlParams.put("query", querySearch)
            searchFilter = """ AND (
                                    trade_operation.name ILIKE ?.query OR
                                    commodity.name ILIKE ?.query OR
                                    import_location.name ILIKE ?.query OR
                                    export_location.name ILIKE ?.query
                                  )
                          """

        }
        def adminQuery = ""
        if (isAdmin) {
            adminQuery = ',standard.is_active,standard.added_by,standard.date_added'

        }

        sqlParams = commonDBFunctions.paginationSqlParams(sqlParams, params)
        queryFilters = statusFilter + commodityFilter + tradeOperationFilter + importLocationFilter + exportLocationFilter + searchFilter
        //Pagination parameters & logic
        def fetchStandardQuery = fetchStandard(adminQuery)
        def countQuery = """SELECT  COUNT (*)
                                    FROM   standard,import_export_locations AS export_location,
                                           import_export_locations AS import_location,
                                           commodity,trade_operation
                                    WHERE  standard.export_location = export_location.id
                                    AND    standard.import_location = import_location.id
                                    AND    standard.trade_operation_id = trade_operation.id
                                    AND    standard.commodity_id = commodity.id"""
        def data = sql.rows(fetchStandardQuery + queryFilters + " ORDER BY standard.id ASC" + commonDBFunctions.getLimitClause(), sqlParams)
        def total = queryFilters ? sql.firstRow(countQuery + queryFilters, sqlParams).get("count") : sql.firstRow(countQuery).get("count")
        if (data) {

            response = new GeneralApiListResponse(ObjectUtils.mapToSnakeCaseArrayMap(data), total as Long, HttpStatus.OK.value(), "Success")
        } else {
            response = new GeneralApiListResponse([], total as Long, HttpStatus.NO_CONTENT.value(), "No records found")
        }


    }
    public String fetchStandard(adminQuery) {
        def fetchQuery = """SELECT standard.id,standard.title,standard.description,
                                   commodity.name AS commodity,
                                   import_location.name AS import_location,
                                   export_location.name AS export_location,
                                   trade_operation.name AS trade_operation
                                   $adminQuery
                            FROM   standard,
                                   import_export_locations AS import_location,
                                   import_export_locations AS export_location,
                                   commodity,trade_operation
                            WHERE  standard.export_location = export_location.id
                            AND    standard.import_location = import_location.id
                            AND    standard.trade_operation_id = trade_operation.id
                            AND    standard.commodity_id = commodity.id"""
    }
    public BaseApiResponse getStandardDetails(Long id, boolean isAdmin) {
        Sql sql = new Sql(dataSource)
        BaseApiResponse response = new BaseApiResponse([], 400, "")
        List<FieldErrorDto> errors = []
        Long standardId = id
        def adminQuery = ""
        if (isAdmin) {
            adminQuery = ',standard.is_active,standard.added_by,standard.date_added'
        }
        def fetchStandardQuery = fetchStandard(adminQuery) + (""" AND standard.id=?""")
        def standard = sql.firstRow(fetchStandardQuery, standardId)
        if(standard)
        {
            adminQuery = isAdmin?"standard_specification.*":"standard_specification.id,standard_specification.title,standard_specification.description,standard_specification.standard_id AS standard_id,standard_specification_type.id AS specification_type_id,standard_specification_type.name AS specification_type"
            def standardSpecifications = sql.rows("""SELECT $adminQuery  FROM standard_specification,standard_specification_type WHERE standard_id=? AND standard_specification.is_active ORDER BY standard_specification.id DESC """,standardId)
            standard.put("standardSpecifications",ObjectUtils.mapToSnakeCaseArrayMap(standardSpecifications))
            response.message = "success"
            response.data = standard
            response.status = HttpStatus.OK.value()

        }else {
            response.message = "record id (" +id+") does not exist"
        }
        sql.close()
        return response
    }

}
