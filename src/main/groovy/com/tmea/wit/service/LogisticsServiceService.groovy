package com.tmea.wit.service

import com.tmea.wit.model.BaseApiResponse
import com.tmea.wit.model.GeneralApiListResponse
import com.tmea.wit.model.dto.FieldErrorDto
import com.tmea.wit.model.dto.request.*
import com.tmea.wit.model.dto.response.*
import com.tmea.wit.util.CommonDBFunctions
import com.tmea.wit.util.ObjectUtils
import groovy.sql.Sql
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service

import javax.sql.DataSource

@Service
class LogisticsServiceService {
    private DataSource dataSource
    CommonDBFunctions commonDBFunctions

    @Autowired
    LogisticsServiceService(DataSource dataSource, CommonDBFunctions commonDBFunctions) {
        this.dataSource = dataSource
        this.commonDBFunctions = commonDBFunctions
    }

    @Autowired
    UserService userService

    BaseApiResponse addNewService(ApiLogisticServicePostDto body, String name) {
        Sql sql = Sql.newInstance(dataSource)

        BaseApiResponse response = new BaseApiResponse()
        def params = ObjectUtils.asMap(body)
        Long userId = userService.getUserIdFromUsername(name)
        params.userId = userId

        if (userId) {
            def serviceNameExist = getLogisticsServiceByName(params.serviceName as String)

            if (serviceNameExist) {
                response.message = 'Logistics service (' + params.serviceName + ') already exist!'
                response.status = HttpStatus.BAD_REQUEST.value()
            } else {

                Map tariffParams = params.serviceTariff as Map
                Integer minimumOrder = tariffParams.get("minimumOrder")
                Integer maximumOrder = tariffParams.get("maximumOrder")
                if ( minimumOrder > maximumOrder) {
                    response.status = HttpStatus.BAD_REQUEST.value()
                    response.message = "Minimum order cannot be greater than maximum order"
                } else {

                    def insertServiceResult
                    def insertAddedCharges
                    def insertTariff
                    def insertTariffCharges
                    Long returnedServiceId
                    Long returnedTariffId
                    def additionalCharges = params.addedCharges
                    def tariffCharges = params.serviceTariff.serviceTariffCharges
                    sql.withTransaction {

                        insertServiceResult = sql.executeInsert(
                                """
                                INSERT INTO logistics_service (name, description, location_from, location_to, is_active, provider_user_id)
                                    VALUES (?.serviceName, ?.description, ?.locationFromId, ?.locationToId, true, ?.userId)
                            """, params)
                        returnedServiceId = insertServiceResult.get(0).get(0)


                        if (returnedServiceId) {

                            //insert added charges if any
                            def query = """
                                INSERT INTO logistics_service_additional_charge (logistics_service_id, logistics_service_type_id, amount, is_active)
                                VALUES(?, ?, ?, true)
                                """
                            insertAddedCharges = sql.withBatch(query) {
                                additionalCharges.each { addition ->
                                    it.addBatch(returnedServiceId, addition.serviceTypeId, addition.amount)
                                }
                            }

                            tariffParams.put("serviceId", returnedServiceId)

                            //insert tariff
                            insertTariff = sql.executeInsert(
                                    """INSERT INTO logistics_service_tariff (logistics_service_id, charging_mode_id, currency_id, measurement_unit_id, minimum_order, is_active, maximum_order)
                                       VALUES (?.serviceId, ?.chargingModeId, ?.currencyId, ?.measurementUnitId, ?.minimumOrder, true, ?.maximumOrder)
                                     """, tariffParams)

                            returnedTariffId = insertTariff.get(0).get(0)

                            //insert tariff charges
                            if (returnedTariffId) {

                                def queryTariffCharges = """INSERT INTO logistics_service_tariff_charge(logistics_service_tariff_id, from_unit, to_unit, charge)
                                                        VALUES(?, ?, ?, ?)"""
                                insertTariffCharges = sql.withBatch(queryTariffCharges) {
                                    tariffCharges.each { charge ->
                                        it.addBatch(returnedTariffId, charge.fromUnit, charge.toUnit, charge.charge)
                                    }
                                }

                            }
                        }
                    }


                    if (returnedTariffId) {
                        def service = getLogisticsServiceById(returnedServiceId)
                        def addedCharges = getServiceAdditionalCharges(returnedServiceId)
                        def tariff = getServiceTariff(returnedServiceId)
                        Long tariffId = tariff.get("id")
                        def tariffChargesRes = getTariffCharges(tariffId)

                        ApiLogisticsServiceResponseDto data = ObjectUtils.snakeCaseMapToPojo(service, new ApiLogisticsServiceResponseDto())
                        additionalCharges = ObjectUtils.snakeCaseArrayMapToListPojos(addedCharges, ApiLogisticsServiceAddedChargesResponseDto)
                        ApiLogisticsServiceTariffResponseDto serviceTariff = ObjectUtils.snakeCaseMapToPojo(tariff, new ApiLogisticsServiceTariffResponseDto())
                        def serviceTariffCharges = ObjectUtils.snakeCaseArrayMapToListPojos(tariffChargesRes, ApiLogisticsServiceTariffChargesResponseDto)
                        serviceTariff.setTariffCharges(serviceTariffCharges);
                        data.setAddedCharges(additionalCharges)
                        data.setTariff(serviceTariff)

                        response.status = HttpStatus.OK.value()
                        response.data = data
                        response.message = "Service added successfully"

                    } else {
                        response.message = "Could not add service"
                        response.status = HttpStatus.BAD_REQUEST.value()
                    }
                }
            }

        }

        sql.close()
        return response
    }

    BaseApiResponse editService(ApiLogisticsServiceUpdatePostDto body, Long serviceId, String name) {
        Sql sql = Sql.newInstance(dataSource)
        BaseApiResponse res = new BaseApiResponse()
        Map params = ObjectUtils.asMap(body);

        Long userId = userService.getUserIdFromUsername(name);
        List<FieldErrorDto> errors = []

        params.put("id", serviceId);
        params.put("userId", userId)


        if (userId) {
            if (body) {
                def serviceExist = getLogisticsServiceById(serviceId)

                if (serviceExist) {
                    Map updateParams = ObjectUtils.rowAsMap(serviceExist);

                    ObjectUtils.copyMapProperties(params, updateParams);

                    def affectedRows = sql.executeUpdate(
                            """UPDATE logistics_service
                                    SET name = ?.name,
                                        description = ?.description
                                    WHERE id = ?.id
                                      """, updateParams)

                    if (affectedRows == 1) {
                        def service = sql.firstRow(
                                """SELECT id, name, description, is_active
                                        FROM logistics_service WHERE id = ?""", serviceId
                        )

                        ApiLogisticServiceUpdateResponseDto data = ObjectUtils.snakeCaseMapToPojo(service, new ApiLogisticServiceUpdateResponseDto())

                        res.data = data
                        res.message = affectedRows + " record updated successfully"
                        res.status = HttpStatus.OK.value()
                    } else {
                        res.data = null
                        res.message = "Failed to edit logistic service"
                        res.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
                    }

                } else {
                    res.status = HttpStatus.BAD_REQUEST.value()
                    res.message = "Record not found!"
                }
            } else {
                res.status = HttpStatus.BAD_REQUEST.value()
                res.message = "Update data cannot be null!"
            }
        } else {
            errors.add(new FieldErrorDto("userId", "Failed to fetch user details of the user editing logistics service"))
            res.data = null
            res.message = "Failed to edit logistic service"
            res.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
            res.errors = errors
        }

        sql.close()
        return res
    }

    BaseApiResponse editTariff(ApiLogisticServiceTariffPostDto apiLogisticServiceTariffPostDto, Long serviceId, String name) {
        Sql sql = Sql.newInstance(dataSource)
        BaseApiResponse response = new BaseApiResponse()
        Map requestParams = ObjectUtils.asMap(apiLogisticServiceTariffPostDto)
        List<FieldErrorDto> errorDtoList = new ArrayList<>()

        def user = userService.getUserIdFromUsername(name)

        if (user) {
            def tariffExist = getServiceTariff(serviceId)
            Long tariffId = tariffExist.get("id")
            requestParams.put("serviceId", serviceId)
            if (tariffExist) {
                def insertTariffCharges
                Long newTariffId
                sql.withTransaction {
                    def disableExist = sql.executeUpdate(
                            """UPDATE logistics_service_tariff 
                                    SET is_active = false 
                                    WHERE id = ?""", tariffId
                    )

                    if (disableExist > 0) {
                        Integer minimumOrder = requestParams.get("minimumOrder")
                        Integer maximumOrder = requestParams.get("maximumOrder")
                        if (minimumOrder > maximumOrder) {
                            response.status = HttpStatus.BAD_REQUEST.value()
                            response.message = "Minimum order cannot be greater than maximum order"
                        } else {

                            def insertTariff = sql.executeInsert(
                                    """INSERT INTO logistics_service_tariff (logistics_service_id, charging_mode_id, currency_id, measurement_unit_id, minimum_order, is_active, maximum_order)
                                       VALUES (?.serviceId, ?.chargingModeId, ?.currencyId, ?.measurementUnitId, ?.minimumOrder, true, ?.maximumOrder)
                                     """, requestParams)

                            newTariffId = insertTariff.get(0).get(0)

                            def tariffCharges = requestParams.serviceTariffCharges
                            if (newTariffId) {

                                def queryTariffCharges = """INSERT INTO logistics_service_tariff_charge(logistics_service_tariff_id, from_unit, to_unit, charge)
                                                        VALUES(?, ?, ?, ?)"""
                                insertTariffCharges = sql.withBatch(queryTariffCharges) {
                                    tariffCharges.each { charge ->
                                        it.addBatch(newTariffId, charge.fromUnit, charge.toUnit, charge.charge)
                                    }
                                }

                            }
                        }
                    }
                }

                if (newTariffId) {
                    def tariff = getServiceTariff(serviceId)
                    def tariffChargesRes = getTariffCharges(newTariffId)

                    ApiLogisticsServiceTariffResponseDto tariffResponseDto = ObjectUtils.snakeCaseMapToPojo(tariff, new ApiLogisticsServiceTariffResponseDto())
                    def serviceTariffCharges = ObjectUtils.snakeCaseArrayMapToListPojos(tariffChargesRes, ApiLogisticsServiceTariffChargesResponseDto)
                    tariffResponseDto.setTariffCharges(serviceTariffCharges);

                    response.status = HttpStatus.OK.value()
                    response.data = tariffResponseDto
                    response.message = "Service tariff update successfully"
                } else {
                    response.status = HttpStatus.BAD_REQUEST.value()
                    response.message = "Failed to update service tariff"
                }

            } else {
                response.status = HttpStatus.BAD_REQUEST.value()
                response.message = "Record not found"
            }
        } else {
            errorDtoList.add(new FieldErrorDto("userId", "Failed to fetch user details of the user editing logistics service tariff"))
            response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
            response.errors = errorDtoList
            response.message = "Failed to edit logistic service tariff"
        }

        sql.close()
        return response
    }

    GeneralApiListResponse getAllLogisticsServices(Map parameterMap, boolean isForCurrentUser) {
        Sql sql = Sql.newInstance(dataSource)
        GeneralApiListResponse response = new GeneralApiListResponse()
        List<ApiLogisticsServiceResponseDto> serviceList = new ArrayList<>()
        List<FieldErrorDto> errors = []
        def params = ObjectUtils.flattenListParam(parameterMap)
        def status = params.status
        Map sqlParams = [start: 0, limit: 25]

        def locationFrom = params.get("locationFrom")?.toInteger()
        def locationTo = params.get("locationTo")?.toInteger()
        def provider = params.get("providerId")?.toInteger()

        def statusFilter = "";

        if (status) {
            def booleanStatus = CommonDBFunctions.getStatusAsBoolean(status)

            if (booleanStatus != null) {
                sqlParams.status = booleanStatus
                statusFilter = " AND logistics_service.is_active = ?.status ";
            } else {
                errors.add(new FieldErrorDto("status", "Invalid value passed. Check Documentation"))
                response.data = []
                response.total = 0
                response.status = HttpStatus.BAD_REQUEST.value()
                response.message = "Invalid status value"
                return response

            }
        }

        def query = """SELECT logistics_service.id,
                            logistics_service.name,
                            logistics_service.description,
                            location_from.name   AS location_from,
                            location_to.name   AS location_to,
                            CASE
                            WHEN "user".user_type_id = 1 THEN (SELECT user_individual_detail.first_name||' '||user_individual_detail.middle_name||' '||user_individual_detail.last_name  FROM user_individual_detail WHERE "user".id = user_individual_detail.user_id)
                            WHEN "user".user_type_id = 2 THEN (SELECT user_organization_detail.organization_name FROM user_organization_detail WHERE "user".id = user_organization_detail.user_id)
                            END AS provider_name,
                            logistics_service.is_active
                            
                        FROM logistics_service,
                             location AS location_from,
                             location AS location_to,
                             user_individual_detail,
                             "user"

                        WHERE logistics_service.location_from = location_from.id
                        AND   logistics_service.location_to = location_to.id 
                        AND   user_individual_detail.user_id = "user".id """;

        def countQuery = """SELECT count(1) FROM logistics_service,
                                                 location AS location_from,
                                                 location AS location_to
                                            WHERE logistics_service.location_from = location_from.id
                                            AND   logistics_service.location_to = location_to.id """;

        def locationFromFilter = "";
        if (locationFrom) {
            locationFromFilter = " AND location_from = ?.locationFrom ";
            sqlParams.put("locationFrom", locationFrom);
        }

        def locationToFilter = "";
        if (locationTo) {
            locationToFilter = " AND location_to = ?.locationTo ";
            sqlParams.put("locationTo", locationTo);
        }

        def providerFilter = ""
        if (provider) {
            providerFilter = " AND provider_user_id = ?.provider ";
            sqlParams.put("provider", provider);

        }

        if (isForCurrentUser) {
            Authentication auth = SecurityContextHolder.getContext().getAuthentication()
            String userName = auth.getName()
            Long currentUserId = userService.getUserIdFromUsername(userName);
            providerFilter = " AND provider_user_id = ?.provider ";
            sqlParams.put("provider", currentUserId);
        }

        //Pagination parameters & logic
        sqlParams = commonDBFunctions.paginationSqlParams(sqlParams, params)

        String limitClause = " limit ?.limit offset ?.start";
        String queryFilter =  locationFromFilter + locationToFilter + providerFilter + statusFilter + limitClause;

        String filteredSelectQuery = query + queryFilter

        String filteredCountQuery = countQuery + queryFilter


        def data = sql.rows(filteredSelectQuery, sqlParams);
        def total;

        if (queryFilter) {
            total = sql.firstRow(filteredCountQuery, sqlParams).get("count")
        } else {
            total = sql.firstRow(filteredCountQuery).get("count")
        }

        sql.close();

        if (data) {
            data.each {
                Long serviceId = it.get("id")

                def addedCharges = getServiceAdditionalCharges(serviceId)

                def tariff = getServiceTariff(serviceId)

                def tariffCharges
                if (tariff){
                    Long tariffId = tariff.get("id")
                    tariffCharges = getTariffCharges(tariffId)
                }


                ApiLogisticsServiceResponseDto apiLogisticsServiceResponse = ObjectUtils.snakeCaseMapToPojo(it, new ApiLogisticsServiceResponseDto())
                def additionalCharges = ObjectUtils.snakeCaseArrayMapToListPojos(addedCharges, ApiLogisticsServiceAddedChargesResponseDto)
                ApiLogisticsServiceTariffResponseDto serviceTariff = ObjectUtils.snakeCaseMapToPojo(tariff, new ApiLogisticsServiceTariffResponseDto())
                def serviceTariffCharges = ObjectUtils.snakeCaseArrayMapToListPojos(tariffCharges, ApiLogisticsServiceTariffChargesResponseDto)
                serviceTariff.setTariffCharges(serviceTariffCharges);

                apiLogisticsServiceResponse.setAddedCharges(additionalCharges)
                apiLogisticsServiceResponse.setTariff(serviceTariff)

                serviceList.add(apiLogisticsServiceResponse)
            }

            response = new GeneralApiListResponse(serviceList, total as Long, HttpStatus.OK.value(), "Success")
        } else {
            response = new GeneralApiListResponse([], total as Long, HttpStatus.OK.value(), "No records found")
        }

        return response
    }

    BaseApiResponse getServiceDetails(Long id) {
        Sql sql = Sql.newInstance(dataSource)

        BaseApiResponse response = new BaseApiResponse()
        def requestParams = [:]
        requestParams.id = id
        def service
        def additionalCharges
        def tariff
        def tariffCharges
        def tariffId
        if (id) {
            sql.withTransaction {
                service = getLogisticsServiceById(id)
                additionalCharges = getServiceAdditionalCharges(id)
                tariff = getServiceTariff(id)
                tariffId = tariff.get("id")
                tariffCharges = getTariffCharges(tariffId)

            }


            ApiLogisticsServiceResponseDto data = ObjectUtils.snakeCaseMapToPojo(service, new ApiLogisticsServiceResponseDto())
            def addedCharges = ObjectUtils.snakeCaseArrayMapToListPojos(additionalCharges, ApiLogisticsServiceAddedChargesResponseDto)
            ApiLogisticsServiceTariffResponseDto serviceTariff = ObjectUtils.snakeCaseMapToPojo(tariff, new ApiLogisticsServiceTariffResponseDto())
            def serviceTariffCharges = ObjectUtils.snakeCaseArrayMapToListPojos(tariffCharges, ApiLogisticsServiceTariffChargesResponseDto)
            serviceTariff.setTariffCharges(serviceTariffCharges);

            data.setAddedCharges(addedCharges)
            data.setTariff(serviceTariff)

            response.status = HttpStatus.OK.value()
            response.data = data
            response.message = "Success!!!"

        }

        sql.close()
        return response

    }

    BaseApiResponse calculateEstimateServiceCost(Map parameterMap) {
        Sql sql = Sql.newInstance(dataSource)

        BaseApiResponse response = new BaseApiResponse()
        Map params = ObjectUtils.flattenListParam(parameterMap);
        def serviceId = params.serviceId as int
        Double quantity = params.quantity as double

        //check if service id exist
        if (!serviceId || !quantity) {
            response.message = "Missing value service id or quantity"
            response.status = HttpStatus.BAD_REQUEST.value()
        } else {
            def tariffDetails = sql.firstRow("""SELECT *
                            FROM logistics_service_tariff
                            WHERE logistics_service_id = ?.serviceId::INTEGER
                            AND is_active
                            """, params)

            def tariffId = tariffDetails.get("id")
            def chargingMode = tariffDetails.get("charging_mode_id")
            Map tariffChargesParams = [tariffId: tariffId, quantity: quantity];

            if (quantity >= tariffDetails.get("minimum_order") && quantity <= tariffDetails.get("maximum_order")) {
                def tariffCharge = sql.firstRow("""SELECT * 
                                                FROM logistics_service_tariff_charge 
                                                WHERE  logistics_service_tariff_id= ?.tariffId 
                                                  AND  ?.quantity::INTEGER BETWEEN from_unit AND to_unit""", tariffChargesParams);

                def additionalChargeAmount = sql.rows(
                        """SELECT SUM (amount) AS total
                                FROM logistics_service_additional_charge
                                WHERE logistics_service_id = ?
                               """, serviceId
                )

                Double amountToCharge = 0;

                //Per unit = 1 flat-rate = 2
                if (chargingMode == 1) {
                    amountToCharge = (tariffCharge.get("charge") * Double.parseDouble(params.quantity));
                } else if (chargingMode == 2) {
                    amountToCharge = tariffCharge.get("charge");
                }

                def res = [:]
                res.serviceCost = amountToCharge
                res.additionalChargesCost = additionalChargeAmount.get(0).get("total")
                res.totalCost = amountToCharge + additionalChargeAmount.get(0).get("total")

                response.data = res
                response.status = HttpStatus.OK.value()
                response.message = "Success!"
            } else {
                response.status = HttpStatus.BAD_REQUEST.value()
                response.message = "Quantity out of range"
            }


        }

        sql.close()
        return response


    }

    GeneralApiListResponse fetchServiceAddedCharges(Long serviceId) {
        Sql sql = Sql.newInstance(dataSource)
        GeneralApiListResponse response = new GeneralApiListResponse()

        if (serviceId) {
            def additionalCharges = getServiceAdditionalCharges(serviceId)

            if (additionalCharges) {
                def count = sql.firstRow(
                        """SELECT count(1) 
                                FROM logistics_service_additional_charge 
                                WHERE is_active AND logistics_service_id =?""", serviceId
                ).get("count")
                def addedCharges = ObjectUtils.snakeCaseArrayMapToListPojos(additionalCharges, ApiLogisticsServiceAddedChargesResponseDto)

                response.data = addedCharges
                response.total = count
                response.status = HttpStatus.OK.value()
                response.message = "Success!"
            } else {
                response.status = HttpStatus.BAD_REQUEST.value()
                response.message = "Record not found"
            }
        } else {
            response.status = HttpStatus.BAD_REQUEST.value()
            response.message = "Missing service id"
        }

        sql.close()
        return response

    }

    BaseApiResponse fetchServiceTariff(Long serviceId) {
        Sql sql = Sql.newInstance(dataSource)

        BaseApiResponse response = new BaseApiResponse()

        //check if record exists
        def tariffExists = getServiceTariff(serviceId)

        if (tariffExists) {
            def tariffId = tariffExists.get("id")

            def tariffCharges = getTariffCharges(tariffId)
            if (tariffCharges) {
                ApiLogisticsServiceTariffResponseDto tariff = ObjectUtils.snakeCaseMapToPojo(tariffExists, new ApiLogisticsServiceTariffResponseDto())
                def tariffChargesData = ObjectUtils.snakeCaseArrayMapToListPojos(tariffCharges, ApiLogisticsServiceTariffChargesResponseDto)
                tariff.setTariffCharges(tariffChargesData)

                response.status = HttpStatus.OK.value()
                response.data = tariff
                response.message = "Success!!!"
            }
        } else {
            response.status = HttpStatus.BAD_REQUEST.value()
            response.message = "Record not found"
        }

        sql.close()
        return response
    }

    BaseApiResponse updateServiceStatus(ApiStatusRequestDto body, Long serviceId, String name) {
        Sql sql = Sql.newInstance(dataSource)

        BaseApiResponse response = new BaseApiResponse()
        Map params = ObjectUtils.asMap(body);
        Long user = userService.getUserIdFromUsername(name);

        params.put("id", serviceId);
        params.put("userId", user)
        def newStatus = (params.status == 'ACTIVE' || params.status == 'active')
        params.status = newStatus

        if (user) {
            def serviceExist = getLogisticsServiceById(serviceId)
            if (serviceExist) {
                def query
                sql.withTransaction {
                    query = sql.executeUpdate(
                            """UPDATE logistics_service
                        SET is_active = ?.status
                        WHERE id = ?.id
                        """, params)
                }

                def statusMsg = newStatus ? 'activated' : 'deactivated'
                if (query != 1) {
                    response.status = HttpStatus.BAD_REQUEST.value()
                    response.message = "Update failed!!"
                } else {

                    def service = sql.firstRow(
                            """SELECT id, name, description, is_active
                                        FROM logistics_service WHERE id = ?""", serviceId
                    )
                    def message = "Logistics service (" + service.name + ") " + statusMsg
                    def serviceData = ObjectUtils.snakeCaseMapToPojo(service, new ApiLogisticServiceUpdateResponseDto())
                    response.data = serviceData
                    response.status = HttpStatus.OK.value()
                    response.message = message
                }

            } else {
                response.status = HttpStatus.BAD_REQUEST.value()
                response.message = "Record not found"
            }

        } else {
            response.data = null
            response.message = "Failed to edit logistic service status"
            response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
        }


        sql.close()
        return response
    }

    BaseApiResponse removeAddedCharges(Long addedChargeId) {
        Sql sql = Sql.newInstance(dataSource)
        def response = new BaseApiResponse()

        // check if Additional charge exist
        def isRecordExist = sql.firstRow(
                """SELECT * FROM logistics_service_additional_charge WHERE id = ? AND is_active""", addedChargeId
        )

        def updateQuery
        if (isRecordExist) {
            sql.withTransaction {
                updateQuery = sql.executeUpdate("""UPDATE logistics_service_additional_charge SET is_active = false
                                                        WHERE logistics_service_additional_charge.id = ?
                                                        """, addedChargeId)
            }


            if (updateQuery == 1) {
                response.message = "Added charge removed successfully"
                response.status = HttpStatus.OK.value()
            } else {
                response.message = "Failed to remove record"
                response.status = HttpStatus.BAD_REQUEST.value()
            }

        } else {
            response.status = HttpStatus.BAD_REQUEST.value()
            response.message = "Record not found"
        }

        sql.close()
        return response
    }

    BaseApiResponse addAdditionalCharge(ApiLogisticServiceAddedChargesPostDto body, Long serviceId, String username) {
        Sql sql = Sql.newInstance(dataSource)
        def response = new BaseApiResponse()
        Map requestParams = ObjectUtils.asMap(body)
        requestParams.serviceId = serviceId
        def userId = userService.getUserIdFromUsername(username)

        List<FieldErrorDto> errorDtoList = new ArrayList<>()

        if (userId) {
            // check if service exist
            def isRecordExist = sql.firstRow(
                    """SELECT * FROM logistics_service_additional_charge 
                            WHERE logistics_service_additional_charge.logistics_service_id = ?.serviceId
                              AND logistics_service_additional_charge.logistics_service_type_id = ?.serviceTypeId
                              AND is_active""", requestParams
            )

            def insertQuery
            if (isRecordExist) {
                response.status = HttpStatus.BAD_REQUEST.value()
                response.message = "A similar record exist"
            } else {
                sql.withTransaction {
                    insertQuery = sql.executeInsert(
                            """INSERT INTO logistics_service_additional_charge(logistics_service_id, logistics_service_type_id, amount, is_active) 
                                VALUES (?.serviceId, ?.serviceTypeId, ?.amount, true)""", requestParams
                    )

                }

                if (insertQuery) {
                    Long addedChargeId = insertQuery.get(0).get(0)

                    def addedCharge = sql.firstRow(
                            """SELECT id, amount, is_active FROM logistics_service_additional_charge WHERE id = ? AND is_active""", addedChargeId
                    )

                    if (addedCharge) {
                        response.status = HttpStatus.OK.value()
                        response.data = addedCharge
                        response.message = "Additional charge added successfully"
                    }
                } else {
                    response.status = HttpStatus.BAD_REQUEST.value()
                    response.message = "Failed to add additional charge"
                }

            }
        } else {
            errorDtoList.add(new FieldErrorDto("userId", "Failed to fetch user details of the logged in user"))
            response.data = null
            response.message = "Failed to edit logistic vehicle type"
            response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
            response.errors = errorDtoList
        }

        sql.close()
        return response
    }


    Map getLogisticsServiceById(Long id) {
        Sql sql = Sql.newInstance(dataSource)
        def result = sql.firstRow """
                            SELECT 
                                logistics_service.id,
                                logistics_service.name,
                                logistics_service.description,
                                location_from.name   AS location_from,
                                location_to.name   AS location_to,
                                CASE
                                WHEN "user".user_type_id = 1 THEN (SELECT user_individual_detail.first_name||' '||user_individual_detail.middle_name||' '||user_individual_detail.last_name  FROM user_individual_detail WHERE "user".id = user_individual_detail.user_id)
                                WHEN "user".user_type_id = 2 THEN (SELECT user_organization_detail.organization_name FROM user_organization_detail WHERE "user".id = user_organization_detail.user_id)
                                END AS provider_name,
                                logistics_service.is_active
                                
                            FROM logistics_service,
                                 location AS location_from,
                                 location AS location_to,
                                 "user"
                            WHERE logistics_service.location_from = location_from.id
                            AND   logistics_service.location_to = location_to.id
                            AND   logistics_service.id = ?""", id
        sql.close()
        return result
    }

    Map getLogisticsServiceByName(String serviceName) {
        Sql sql = Sql.newInstance(dataSource)
        def result = sql.firstRow "SELECT * FROM logistics_service WHERE name = ?", serviceName

        sql.close()
        return result
    }

    List<Map> getServiceAdditionalCharges(Long id) {
        Sql sql = Sql.newInstance(dataSource)
        def result = sql.rows(
                """SELECT logistics_service_additional_charge.id,
                              logistics_service_additional_charge.logistics_service_id,
                              logistics_service_type.name AS service_type,
                              logistics_service_additional_charge.amount
        
                        FROM 
                            logistics_service_additional_charge,
                            logistics_service_type
                        WHERE logistics_service_additional_charge.logistics_service_type_id = logistics_service_type.id
                          AND logistics_service_additional_charge.is_active
                        AND logistics_service_additional_charge.logistics_service_id = ?
                         """, id)

        sql.close()
        return result

    }

    Map getServiceTariff(Long id) {
        Sql sql = Sql.newInstance(dataSource)
        def result = sql.firstRow(
                """
            SELECT logistics_service_tariff.id,
                   logistics_service_tariff.logistics_service_id,
                   charge_mode.name    AS charging_mode,
                   currency.name         AS currency,
                   measurement_unit.name AS measurement_unit,
                   logistics_service_tariff.minimum_order,
                   logistics_service_tariff.maximum_order
            FROM charge_mode,
                 currency,
                 measurement_unit,
                 logistics_service_tariff
            WHERE logistics_service_tariff.charging_mode_id = charge_mode.id
              AND logistics_service_tariff.currency_id = currency.id
              AND logistics_service_tariff.measurement_unit_id = measurement_unit.id
              AND logistics_service_tariff.is_active
              AND logistics_service_tariff.logistics_service_id = ?
            """, id)

        sql.close()
        return result
    }

    List<Map> getTariffCharges(Long tariffId) {
        Sql sql = Sql.newInstance(dataSource)
        def result = sql.rows(
                """
         SELECT *
            FROM logistics_service_tariff_charge
            WHERE logistics_service_tariff_charge.logistics_service_tariff_id = ?
         """, tariffId)

        sql.close()
        return result
    }

}
