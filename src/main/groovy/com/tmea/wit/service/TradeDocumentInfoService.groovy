package com.tmea.wit.service

import com.tmea.wit.core.storage.StorageApi
import com.tmea.wit.model.BaseApiResponse
import com.tmea.wit.model.GeneralApiListResponse
import com.tmea.wit.model.dto.FieldErrorDto
import com.tmea.wit.model.dto.request.ApiEditDocumentInfoPostDto
import com.tmea.wit.model.dto.request.ApiStatusRequestDto
import com.tmea.wit.model.dto.request.ApiTradeDocumentInfoPostDto
import com.tmea.wit.model.dto.request.ApiTradeDocumentSamplePostDto
import com.tmea.wit.util.CommonDBFunctions
import com.tmea.wit.util.FileUtils
import com.tmea.wit.util.ObjectUtils
import groovy.sql.Sql
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service

import javax.sql.DataSource

@Service
class TradeDocumentInfoService {
    private DataSource dataSource
    CommonDBFunctions commonDBFunctions
    StorageApi storageApi
    FileUtils fileUtils

    final String PRODUCT_BUCKET_NAME = "trade-document-samples"

    @Autowired
    UserService userService

    TradeDocumentInfoService(DataSource dataSource, CommonDBFunctions commonDBFunctions, StorageApi storageApi, FileUtils fileUtils) {
        this.dataSource = dataSource
        this.commonDBFunctions = commonDBFunctions
        this.storageApi = storageApi
        this.fileUtils = fileUtils
    }

    BaseApiResponse addDocumentInfo(ApiTradeDocumentInfoPostDto body, String name) {
        Sql sql = new Sql(dataSource)
        BaseApiResponse response = new BaseApiResponse()
        Long userId = userService.getUserIdFromUsername(name)
        Map params = ObjectUtils.asMap(body)
        params.put("isActive", true)
        params.put("addedBy", userId)

        if (userId) {
            def recordExist = sql.firstRow(
                    """SELECT * FROM trade_document_information WHERE name ILIKE ?.name AND commodity_id = ?.commodityId""", params
            )

            def insertQuery = ""
            Long tradeDocInfoId
            if (!recordExist) {
                sql.withTransaction {
                    insertQuery = sql.executeInsert(
                            """INSERT INTO trade_document_information(name, description, trade_document_type_id, trade_operation_id, country_from, country_to, commodity_id, is_active, added_by, date_added) 
                                    VALUES (?.name, ?.description, ?.documentTypeId, ?.tradeOperationId, ?.countryFromId, ?.countryToId, ?.commodityId, ?.isActive, ?.addedBy, current_timestamp)""", params
                    )

                    tradeDocInfoId = insertQuery.get(0).get(0) as Long

                    def docSamples = params.documentSamples

                    if (tradeDocInfoId) {
                        def insertSampleQuery = """INSERT INTO trade_document_sample(name, document_information_id, path, is_active)
                                                        VALUES(?, ?, ?, ?)"""

                        sql.withBatch(insertSampleQuery) {
                            docSamples.each { sample ->

                                String imageBase64File = sample.path
                                String sampleName = sample.name
                                String sampleURL = fileUtils.saveFile(name, tradeDocInfoId, PRODUCT_BUCKET_NAME, imageBase64File, storageApi)
                                it.addBatch(sampleName, tradeDocInfoId, sampleURL, true)
                            }
                        }
                    }

                }

                if (insertQuery) {
                    def tradeDocInfo = getTradeDocumentInfoById(tradeDocInfoId)
                    def samples = getDocumentSamples(tradeDocInfoId, true)
                    tradeDocInfo.put("samples_documents", samples)

                    def data = ObjectUtils.mapToSnakeCaseMap(tradeDocInfo)

                    response.status = HttpStatus.OK.value()
                    response.data = data
                    response.message = "Success"
                } else {
                    response.message = "Operation failed"
                    response.status = HttpStatus.BAD_REQUEST.value()
                }
            } else {
                response.status = HttpStatus.BAD_REQUEST.value()
                response.message = "A record with similar details already exist. Do you wish to edit?"
            }

        } else {
            List<FieldErrorDto> errors = new ArrayList<>()
            errors.add(new FieldErrorDto("userId", "Failed to fetch user details"))
            response.data = null
            response.message = "Failed to add trade document information"
            response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
            response.errors = errors
        }

        sql.close()
        return response
    }

    BaseApiResponse editDocumentInfo(ApiEditDocumentInfoPostDto body, Long id, String name) {
        Sql sql = new Sql(dataSource)
        BaseApiResponse response = new BaseApiResponse()
        Map params = ObjectUtils.asMap(body)

        Long userId = userService.getUserIdFromUsername(name)

        if (userId) {
            def recordExist = sql.firstRow(
                    """SELECT * FROM trade_document_information WHERE id = ?""", id
            )

            if (recordExist) {
                Map updateParams = ObjectUtils.rowAsMap(recordExist)
                updateParams = ObjectUtils.mapToSnakeCaseMap(updateParams) as Map
                ObjectUtils.copyMapProperties(params, updateParams)
                updateParams.put("id", id)

                def editQuery = sql.executeUpdate(
                        """UPDATE trade_document_information 
                                    SET name = ?.name, 
                                        description = ?.description,
                                        trade_document_type_id = ?.tradeDocumentTypeId,
                                        trade_operation_id = ?.tradeOperationId,
                                        country_from = ?.countryFrom,
                                        country_to = ?.countryTo,
                                        commodity_id = ?.commodityId
                                                                    
                                      WHERE id = ?.id""", updateParams
                )

                def sampleDocs = params.get("documentSamples")

                if (sampleDocs) {
                    def insertSampleQuery = """INSERT INTO trade_document_sample(name, document_information_id, path, is_active)
                                                        VALUES(?, ?, ?, ?)"""

                    sql.withBatch(insertSampleQuery) {
                        sampleDocs.each { sample ->

                            String imageBase64File = sample.path
                            String sampleName = sample.name
                            String sampleURL = fileUtils.saveFile(name, id, PRODUCT_BUCKET_NAME, imageBase64File, storageApi)
                            it.addBatch(sampleName, id, sampleURL, true)
                        }
                    }
                }

                if (editQuery == 1) {
                    def documentType = getTradeDocumentInfoById(id)
                    def samples = getDocumentSamples(id, true)
                    documentType.put("samples_documents", samples)

                    def data = ObjectUtils.mapToSnakeCaseMap(documentType)

                    response.status = HttpStatus.OK.value()
                    response.data = data
                    response.message = "Success"
                } else {
                    response.status = HttpStatus.BAD_REQUEST.value()
                    response.message = "Operation failed"
                }

            } else {
                response.status = HttpStatus.NOT_FOUND.value()
                response.message = "Record not found"
            }

        } else {
            List<FieldErrorDto> errors = new ArrayList<>()
            errors.add(new FieldErrorDto("userId", "Failed to fetch user details"))
            response.data = null
            response.message = "Failed to edit trade document information"
            response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
            response.errors = errors
        }

        sql.close()
        return response
    }

    GeneralApiListResponse getAllTradeDocumentInfo(Map mapParams, Boolean isAdmin) {
        Sql sql = Sql.newInstance(dataSource)
        GeneralApiListResponse response = new GeneralApiListResponse()
        Map params = ObjectUtils.flattenListParam(mapParams)

        Map requestParams = [start: 0, limit: 25]
        def query = params.get("query")
        def status = params.get("status")
        Long category = params.get("category") as Long
        Long subCategory = params.get("subCategory") as Long
        Long commodity = params.get("commodity") as Long
        Long tradeOperation = params.get("tradeOperation") as Long
        Long countryFrom = params.get("countryFrom") as Long
        Long countryTo = params.get("countryTo") as Long
        Long documentCategory = params.get("documentCategory") as Long
        Long documentType = params.get("documentType") as Long

        def statusFilter = ""
        if (status) {
            def booleanStatus = CommonDBFunctions.getStatusAsBoolean(status)

            if (booleanStatus != null) {
                requestParams.status = booleanStatus
                statusFilter = " AND trade_document_information.is_active = ?.status "
            } else {
                List<FieldErrorDto> errors = new ArrayList<>()
                errors.add(new FieldErrorDto("status", "Invalid value passed. Check Documentation"))
                response.data = []
                response.total = 0
                response.status = HttpStatus.BAD_REQUEST.value()
                response.message = "Invalid status value"
                return response

            }
        }

        def documentTypeFilter = ""
        if (documentType) {
            documentTypeFilter = " AND trade_document_information.trade_document_type_id  = ?.documentType "
            requestParams.put("documentType", documentType)
        }

        def documentCategoryFilter = ""
        if (documentCategory) {
            documentCategoryFilter = " AND trade_document_category.id  = ?.documentCategory "
            requestParams.put("documentCategory", documentCategory)
        }

        def commodityFilter = ""
        if (commodity) {
            commodityFilter = " AND trade_document_information.commodity_id  = ?.commodity "
            requestParams.put("commodity", commodity as Long);

        }


        def tradeOperationFilter = "";
        if (tradeOperation) {
            tradeOperationFilter = " AND trade_document_information.trade_operation_id  = ?.tradeOperation ";
            requestParams.put("tradeOperation", tradeOperation);
        }

        def countryFromFilter = "";
        if (countryFrom) {
            countryFromFilter = " AND trade_document_information.country_from = ?.countryFrom ";
            requestParams.put("countryFrom", countryFrom);
        }

        def countryToFilter = "";
        if (countryTo) {
            countryToFilter = ' AND trade_document_information.country_to = ?.countryTo ';
            requestParams.put("countryTo", countryTo);
        }

        def categoryFilter = "";
        if (category) {
            categoryFilter = " AND commodity_category.id = ?.category ";
            requestParams.put("category", category);
        }

        def subCategoryFilter = "";
        if (subCategory) {
            subCategoryFilter = " AND commodity_sub_category.id = ?.subCategory ";
            requestParams.put("subCategory", subCategory);
        }


        def queryFilter = "";
        if (query) {
            commodityFilter = "";
            categoryFilter = "";
            subCategoryFilter = "";
            documentCategoryFilter = ""
            tradeOperationFilter = ""
            documentTypeFilter = ""

            String formattedQuery = "%$query%";
            queryFilter = """ AND (
                        trade_document_information.name ILIKE ?.query OR 
                       commodity.name ILIKE ?.query OR 
                       commodity_sub_category.name ILIKE ?.query OR 
                       commodity_category.name ILIKE ?.query OR 
                       trade_document_type.name ILIKE ?.query OR 
                       trade_document_category.name ILIKE ?.query OR 
                       trade_operation.name ILIKE ?.query
                     )
             """;
            requestParams.put("query", formattedQuery);

        }

        if (!isAdmin){
            requestParams.status = true
            statusFilter = " AND trade_document_information.is_active = ?.status "
        }

        def adminSelectQuery = """
                       SELECT trade_document_information.id,
                              trade_document_information.name,
                              trade_document_information.description,
                              trade_document_type.name   AS document_type,
                              trade_operation.name       AS  trade_operation,
                              country_from.name          AS  country_from,
                              country_to.name            AS  country_to,
                              commodity.name             AS  commodity_name,
                              trade_document_information.is_active,
                              trade_document_information.date_added
                              
                       FROM trade_document_information,
                            trade_document_type,
                            trade_document_category,
                            country AS country_from,
                            country AS country_to,
                            trade_operation,
                            commodity,
                            commodity_sub_category,
                            commodity_category
                            
                       WHERE trade_document_information.trade_document_type_id = trade_document_type.id 
                       AND trade_document_type.trade_document_category_id = trade_document_category.id 
                       AND trade_document_information.trade_operation_id = trade_operation.id 
                       AND trade_document_information.country_from = country_from.id 
                       AND trade_document_information.country_to = country_to.id 
                       AND trade_document_information.commodity_id = commodity.id 
                       AND commodity.commodity_sub_category_id = commodity_sub_category.id 
                       AND commodity_sub_category.commodity_category_id = commodity_category.id """

        def selectQuery = """
                       SELECT trade_document_information.id,
                              trade_document_information.name,
                              trade_document_information.description,
                              trade_document_type.name   AS document_type,
                              trade_operation.name       AS  trade_operation,
                              country_from.name          AS  country_from,
                              country_to.name            AS  country_to,
                              commodity.name             AS  commodity_name
                              
                       FROM trade_document_information,
                            trade_document_type,
                            trade_document_category,
                            country AS country_from,
                            country AS country_to,
                            trade_operation,
                            commodity,
                            commodity_sub_category,
                            commodity_category
                            
                       WHERE trade_document_information.trade_document_type_id = trade_document_type.id 
                       AND trade_document_type.trade_document_category_id = trade_document_category.id 
                       AND trade_document_information.trade_operation_id = trade_operation.id 
                       AND trade_document_information.country_from = country_from.id 
                       AND trade_document_information.country_to = country_to.id 
                       AND trade_document_information.commodity_id = commodity.id 
                       AND commodity.commodity_sub_category_id = commodity_sub_category.id 
                       AND commodity_sub_category.commodity_category_id = commodity_category.id """

        def countQuery = """SELECT COUNT(1) 
                        FROM trade_document_information,
                            trade_document_type,
                            trade_document_category,
                            country AS country_from,
                            country AS country_to,
                            trade_operation,
                            commodity,
                            commodity_sub_category,
                            commodity_category
                            
                       WHERE trade_document_information.trade_document_type_id = trade_document_type.id 
                       AND trade_document_type.trade_document_category_id = trade_document_category.id 
                       AND trade_document_information.trade_operation_id = trade_operation.id 
                       AND trade_document_information.country_from = country_from.id 
                       AND trade_document_information.country_to = country_to.id 
                       AND trade_document_information.commodity_id = commodity.id 
                       AND commodity.commodity_sub_category_id = commodity_sub_category.id 
                       AND commodity_sub_category.commodity_category_id = commodity_category.id """

        //Pagination parameters
        requestParams = commonDBFunctions.paginationSqlParams(requestParams, params)

        String limitClause = " limit ?.limit offset ?.start"
        String filter = commodityFilter + categoryFilter + subCategoryFilter + documentCategoryFilter + tradeOperationFilter +
                documentTypeFilter + countryFromFilter + countryToFilter + queryFilter + statusFilter + limitClause

        def filteredQuery = isAdmin ? adminSelectQuery + filter : selectQuery + filter
        def count = countQuery + filter

        def data = sql.rows(filteredQuery, requestParams)
        def total

        if (filter) {
            total = sql.firstRow(count, requestParams).get("count")
        } else {
            total = sql.firstRow(count).get("count")
        }

        if (data) {

            def responseData = ObjectUtils.mapToSnakeCaseArrayMap(data)

            response.status = HttpStatus.OK.value()
            response.data = responseData
            response.total = total
            response.message = "Success"

        } else {
            response.message = "No records found"
            response.status = HttpStatus.BAD_REQUEST.value()
        }


        sql.close()
        return response
    }

    GeneralApiListResponse getAllTradeDocInfoSampleDocs(Long id, Map parameters, Boolean isAdmin) {
        Sql sql = Sql.newInstance(dataSource)
        GeneralApiListResponse response = new GeneralApiListResponse()
        Map params = ObjectUtils.flattenListParam(parameters)

        Map requestParams = [start: 0, limit: 25]
        requestParams.id = id

        def query = params.get("query")
        def status = params.get("status")

        def statusFilter = ""
        if (status) {
            def booleanStatus = CommonDBFunctions.getStatusAsBoolean(status)

            if (booleanStatus != null) {
                requestParams.status = booleanStatus
                statusFilter = " AND trade_document_sample.is_active = ?.status "
            } else {
                List<FieldErrorDto> errors = new ArrayList<>()
                errors.add(new FieldErrorDto("status", "Invalid value passed. Check Documentation"))
                response.data = []
                response.total = 0
                response.status = HttpStatus.BAD_REQUEST.value()
                response.message = "Invalid status value"
                return response

            }
        }

        def queryFilter = ""
        if (query) {
            String formattedQuery = "%$query%"
            queryFilter = """ AND trade_document_sample.name ILIKE ?.query """
            requestParams.put("query", formattedQuery)
        }

        if (!isAdmin) {
            requestParams.status = true
            statusFilter = " AND trade_document_sample.is_active = ?.status "
        }

        def adminSelectQuery = """
                        SELECT id, 
                                name, 
                                path, 
                                is_active 
                        FROM trade_document_sample 
                        WHERE document_information_id = ?.id """

        def selectQuery = """
                        SELECT id, 
                                name, 
                                path 
                                
                        FROM trade_document_sample 
                        WHERE document_information_id = ?.id"""

        def countQuery = """SELECT COUNT(1)
                         FROM trade_document_sample 
                        WHERE document_information_id = ?.id """

        //Pagination parameters
        requestParams = commonDBFunctions.paginationSqlParams(requestParams, params)

        String limitClause = " limit ?.limit offset ?.start"
        String filter = queryFilter + statusFilter + limitClause

        def filteredQuery = isAdmin ? adminSelectQuery + filter : selectQuery + filter
        def count = countQuery + filter

        def data = sql.rows(filteredQuery, requestParams);
        def total

        if (filter) {
            total = sql.firstRow(count, requestParams).get("count")
        } else {
            total = sql.firstRow(count, requestParams).get("count")
        }

        if (data) {
            def sampleDocs = ObjectUtils.mapToSnakeCaseArrayMap(data)

            response.status = HttpStatus.OK.value()
            response.data = sampleDocs
            response.total = total
            response.message = "Success"
        } else {
            response.message = "No records found"
            response.status = HttpStatus.BAD_REQUEST.value()
        }

        sql.close()
        return response
    }

    BaseApiResponse activateDeactivateRecord(Long id, ApiStatusRequestDto body, String name, Boolean isSampleDocs) {
        Sql sql = new Sql(dataSource)
        BaseApiResponse response = new BaseApiResponse()
        Map params = ObjectUtils.asMap(body)

        Long userId = userService.getUserIdFromUsername(name)

        def statusMsg
        def tableName = isSampleDocs ? "trade_document_sample" : "trade_document_information"
        if (userId) {
            def recordExist = sql.firstRow(
                    """SELECT * FROM ${tableName} WHERE id = ?""", id
            )

            if (recordExist) {
                def status = CommonDBFunctions.getStatusAsBoolean(params.get("status"))

                params.replace("status", status)
                params.put("id", id)

                def affectedRow = sql.executeUpdate(
                        """UPDATE ${tableName} SET is_active = ?.status WHERE id = ?.id""", params
                )

                statusMsg = status ? "activated" : "deactivated"
                if (affectedRow == 1) {
                    def recordData
                    if (isSampleDocs) {
                        recordData = sql.firstRow(
                                """SELECT id, name, document_information_id, path, is_active FROM ${tableName} WHERE id = ?""", id
                        )
                    } else {
                        recordData = sql.firstRow(
                                """SELECT id, name, description, is_active FROM ${tableName} WHERE id = ?""", id
                        )
                    }


                    if (recordData) {
                        def data = ObjectUtils.mapToSnakeCaseMap(recordData)

                        response.status = HttpStatus.OK.value()
                        response.data = data
                        response.message = "Record " + statusMsg + " successfully"
                    }
                } else {
                    response.message = "Failed to " + statusMsg + " record"
                    response.status = HttpStatus.BAD_REQUEST.value()
                }
            } else {
                response.status = HttpStatus.BAD_REQUEST.value()
                response.message = "Record not found"
            }

        } else {
            List<FieldErrorDto> errors = new ArrayList<>()
            errors.add(new FieldErrorDto("userId", "Failed to fetch user details"))
            response.data = null
            response.message = "Failed to " + statusMsg + " document"
            response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
            response.errors = errors
        }

        sql.close()
        return response
    }

    Map getTradeDocumentInfoById(Long documentInfoId) {
        Sql sql = Sql.newInstance(dataSource)
        def docInfo = sql.firstRow(
                """SELECT trade_document_information.id,
                              trade_document_information.name,
                              trade_document_information.description,
                              trade_document_type.name   AS document_type,
                              trade_operation.name       AS  trade_operation,
                              country_from.name          AS  country_from,
                              country_to.name            AS  country_to,
                              commodity.name             AS  commodity_name,
                              trade_document_information.is_active,
                              trade_document_information.date_added
                       
                       FROM trade_document_information,
                            trade_document_type,
                            country AS country_from,
                            country AS country_to,
                            trade_operation,
                            commodity
                       WHERE trade_document_information.trade_document_type_id = trade_document_type.id 
                       AND trade_document_information.trade_operation_id = trade_operation.id 
                       AND trade_document_information.country_from = country_from.id 
                       AND trade_document_information.country_to = country_to.id 
                       AND trade_document_information.commodity_id = commodity.id
                       AND trade_document_information.id = ?
                    """, documentInfoId
        )

        sql.close()
        return docInfo
    }

    List<Map> getDocumentSamples(Long documentInfoId, boolean status) {
        Sql sql = Sql.newInstance(dataSource)
        Map params = new HashMap()
        params.put("id", documentInfoId)
        params.put("isActive", status)
        def docInfoSamples = sql.rows(
                """SELECT id, name, path, is_active 
                        FROM trade_document_sample 
                        WHERE is_active = ?.isActive 
                          AND document_information_id = ?.id""", params
        )

        sql.close()
        return docInfoSamples
    }
}
