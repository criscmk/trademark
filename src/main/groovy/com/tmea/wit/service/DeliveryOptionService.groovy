package com.tmea.wit.service

import com.tmea.wit.model.BaseApiResponse
import com.tmea.wit.model.GeneralApiListResponse
import com.tmea.wit.model.dto.FieldErrorDto
import com.tmea.wit.model.dto.response.ApiDeliveryOptionResponseDto
import com.tmea.wit.model.dto.response.ApiLanguageResponseDto
import com.tmea.wit.repository.UserRepository
import com.tmea.wit.util.CommonDBFunctions
import com.tmea.wit.util.ObjectUtils
import groovy.sql.Sql
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service

import javax.sql.DataSource

@Service
class DeliveryOptionService {

    DataSource dataSource
    UserRepository userRepository
    CommonDBFunctions commonDBFunctions

    @Autowired
    DeliveryOptionService(DataSource dataSource, UserRepository userRepository, CommonDBFunctions commonDBFunctions) {
        this.dataSource = dataSource
        this.userRepository = userRepository
        this.commonDBFunctions = commonDBFunctions
    }


    public BaseApiResponse addDeliveryOption(def body){
        Sql sql = new Sql(dataSource)
        BaseApiResponse response = new BaseApiResponse()
        List<FieldErrorDto> errors = []
        Map requestParams = ObjectUtils.asMap(body)
        Authentication auth = SecurityContextHolder.getContext().getAuthentication()
        String userName = auth.getName()
        Long addedBy = userRepository.findUserIdByDetails(userName);
        if(addedBy){
            requestParams.put("addedBy", addedBy)
            def isNameExist = commonDBFunctions.getDBRecordByTableColumn(requestParams.name, 'delivery_option', 'name')
            if(isNameExist){
                response.status = HttpStatus.BAD_REQUEST.value()
                response.message = 'Delivery Option name ('+ requestParams.name +') already exist!'
            }else{
                def query = """
                            INSERT INTO delivery_option (name, description, added_by)
                            VALUES (?.name, ?.description, ?.addedBy)
                        """
                def insertDeliveryOption = sql.executeInsert(query, requestParams)
                if(insertDeliveryOption){
                    Long deliveryOptionId = insertDeliveryOption.get(0).get(0)
                    def deliveryOption = commonDBFunctions.getDBRecordById(deliveryOptionId, 'delivery_option', 'id')
                    def data = ObjectUtils.snakeCaseMapToPojo(deliveryOption, new ApiDeliveryOptionResponseDto())
                    response.data = data
                    response.message = "Success"
                    response.status = HttpStatus.OK.value()
                }else{
                    response.data = []
                    response.message = "Failed to add delivery option"
                    response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
                }
            }
        }else{
            errors.add(new FieldErrorDto("userId", "Failed to fetch user details of the user adding the delivery option"))
            response.data = []
            response.message = "Failed to add delivery option"
            response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
            response.errors = errors
        }
        response
    }

    public GeneralApiListResponse getDeliveryOptions(Map parameterMap){
        Sql sql = new Sql(dataSource)
        GeneralApiListResponse response
        List<FieldErrorDto> errors = []

        def params = ObjectUtils.flattenListParam(parameterMap)
        def status = params.get("status")
        def query = params.get("query")
        def sqlParams = [start: 0, limit: 25]
        boolean whereIncluded = false;

        def nameFilter = ""
        def statusFilter = ""
        if(status){
            def booleanStatus = CommonDBFunctions.getStatusAsBoolean(status)
            if(booleanStatus != null){
                sqlParams.status = booleanStatus
                statusFilter = " WHERE is_active = ?.status"
                whereIncluded = true
            }else{
                errors.add(new FieldErrorDto("status", "Invalid value passed. Check Documentation"))
                response = new GeneralApiListResponse([], 0, HttpStatus.BAD_REQUEST.value(), "Invalid status value", errors)
                return response
            }
        }

        if(query)
        {

            String querySearch = "%$query%"
            sqlParams.put("query",querySearch)
            def queryFilterClause = """ delivery_option.name ILIKE ?.query """
            nameFilter = whereIncluded ? " AND "+queryFilterClause : " WHERE "+queryFilterClause
        }

        //Pagination parameters & logic
        sqlParams = commonDBFunctions.paginationSqlParams(sqlParams, params)
        def queryFilters = statusFilter+nameFilter
        def fetchLanguagesQuery = "SELECT * FROM delivery_option" + queryFilters + " ORDER BY name ASC "+ CommonDBFunctions.getLimitClause()
        def countQuery = "SELECT count(*) FROM delivery_option" + queryFilters

        def data = sql.rows(fetchLanguagesQuery, sqlParams)
        def total = queryFilters ? sql.firstRow(countQuery,sqlParams).get("count") : sql.firstRow(countQuery).get("count")
        sql.close()

        if(data){
            response = new GeneralApiListResponse(ObjectUtils.snakeCaseArrayMapToListPojos(data, ApiDeliveryOptionResponseDto), total as Long, HttpStatus.OK.value(), "Success")
        }else{
            response = new GeneralApiListResponse([], total as Long, HttpStatus.OK.value(), "No records found")
        }
        return response
    }

    public BaseApiResponse updateDeliveryOptionStatus(Long deliveryOptionId, def body){
        Sql sql = new Sql(dataSource)
        BaseApiResponse response = new BaseApiResponse([], 200, "")
        def params = ObjectUtils.asMap(body)
        params.id = deliveryOptionId
        def newStatus = (params.status == 'ACTIVE' || params.status == 'active')
        params.status = newStatus

        def isDeliveryOptionExist = commonDBFunctions.getDBRecordById(deliveryOptionId, 'delivery_option', 'id')
        if(isDeliveryOptionExist){
            def update = sql.executeUpdate "UPDATE delivery_option set is_active = ?.status WHERE id = ?.id", params
            sql.close()
            def statusMessage= newStatus ? 'activated' : 'deactivated'

            if(update == 1){
                def deliveryOption = commonDBFunctions.getDBRecordById(deliveryOptionId, 'delivery_option', 'id')
                def message = "Category ("+ deliveryOption.name+ ") " + statusMessage
                def deliveryOptionResponse = ObjectUtils.snakeCaseMapToPojo(deliveryOption, new ApiDeliveryOptionResponseDto())
                response.data = deliveryOptionResponse
                response.status = HttpStatus.OK.value()
                response.message = message
            }else{
                response.status = HttpStatus.BAD_REQUEST.value()
                response.message = "Update failed!!"
            }
        }else{
            response.status = HttpStatus.BAD_REQUEST.value()
            response.message = 'Delivery Option with id ('+ deliveryOptionId +') does not exist!'
        }

        response
    }

    def BaseApiResponse updateDeliveryOption(long deliveryOptionId, def body) {
        Sql sql = new Sql(dataSource)
        BaseApiResponse response = new BaseApiResponse([], 200, "")
        def params = ObjectUtils.asMap(body)
        params.id = deliveryOptionId

        def isDeliveryOptionExist = commonDBFunctions.getDBRecordById(deliveryOptionId, 'delivery_option', 'id')
        if(isDeliveryOptionExist){
            def update = sql.executeUpdate "UPDATE delivery_option set name = ?.name, description = ?.description WHERE id = ?.id", params
            sql.close()

            if(update == 1){
                def deliveryOption = commonDBFunctions.getDBRecordById(deliveryOptionId, 'delivery_option', 'id')
                def message = "Success"
                def deliveryOptionResponse = ObjectUtils.snakeCaseMapToPojo(deliveryOption, new ApiDeliveryOptionResponseDto())
                response.data = deliveryOptionResponse
                response.status = HttpStatus.OK.value()
                response.message = message
            }else{
                response.status = HttpStatus.BAD_REQUEST.value()
                response.message = "Update failed!!"
            }

        }else{
            response.status = HttpStatus.BAD_REQUEST.value()
            response.message = 'Delivery Option with id ('+ deliveryOptionId +') does not exist!'
        }
        response
    }
}
