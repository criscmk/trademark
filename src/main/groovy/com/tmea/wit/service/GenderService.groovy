package com.tmea.wit.service

import com.tmea.wit.model.BaseApiResponse
import com.tmea.wit.model.GeneralApiListResponse
import com.tmea.wit.model.dto.FieldErrorDto
import com.tmea.wit.model.dto.response.ApiGenderResponseDto
import com.tmea.wit.util.CommonDBFunctions
import com.tmea.wit.util.ObjectUtils
import groovy.sql.Sql
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service

import javax.sql.DataSource

@Service
class GenderService {
    DataSource dataSource
    CommonDBFunctions commonDBFunctions

    @Autowired
    GenderService(DataSource dataSource, CommonDBFunctions commonDBFunctions) {
        this.dataSource = dataSource
        this.commonDBFunctions = commonDBFunctions
    }

    public GeneralApiListResponse getGenders(Map parameterMap){
        Sql sql = new Sql(dataSource)
        GeneralApiListResponse response
        List<ApiGenderResponseDto> genderList = new ArrayList<>()
        List<FieldErrorDto> errors = []

        def params = ObjectUtils.flattenListParam(parameterMap)
        def status = params.status
        def sqlParams = [start: 0, limit: 25]
        if(status){
            def booleanStatus = CommonDBFunctions.getStatusAsBoolean(status)
            if(booleanStatus != null){
                sqlParams.status = booleanStatus
            }else{
                errors.add(new FieldErrorDto("status", "Invalid value passed. Check Documentation"))
                response = new GeneralApiListResponse([], 0, HttpStatus.BAD_REQUEST.value(), "Invalid status value", errors)
                return response
            }
        }

        //Pagination parameters & logic
        sqlParams = commonDBFunctions.paginationSqlParams(sqlParams, params)

        def query = "SELECT * FROM user_gender"
        def countQuery = "SELECT count(*) FROM user_gender"
        Map queryMap = CommonDBFunctions.addQueryFilters(status, query, countQuery )

        String filteredSelectQuery = queryMap.selectQuery
        String filteredCountQuery = queryMap.countQuery
        String queryFilter = queryMap.queryFilter

        def data = sql.rows(filteredSelectQuery, sqlParams)
        def total = commonDBFunctions.getTotalCountBasedOnFilters(queryFilter, filteredCountQuery, sqlParams)
        sql.close()
        data.each {
            def apiGenderResponse = ObjectUtils.snakeCaseMapToPojo(it, new ApiGenderResponseDto()) as ApiGenderResponseDto
            genderList.add(apiGenderResponse)
        }
        if(data){
            response = new GeneralApiListResponse(genderList, total as Long, HttpStatus.OK.value(), "Success")
        }else{
            response = new GeneralApiListResponse([], total as Long, HttpStatus.OK.value(), "No records found")
        }
        return response
    }

    public BaseApiResponse updateGenderStatus(Long genderId, def body){
        Sql sql = new Sql(dataSource)
        BaseApiResponse response = new BaseApiResponse([], 200, "")
        def params = ObjectUtils.asMap(body)
        params.id = genderId
        def newStatus = (params.status == 'ACTIVE' || params.status == 'active')
        params.status = newStatus

        def isGenderExist = commonDBFunctions.getDBRecordById(genderId, 'user_gender', 'id')
        if(isGenderExist){
            def update = sql.executeUpdate "UPDATE user_gender set is_active = ?.status WHERE id = ?.id", params
            sql.close()
            def statusMessage= newStatus ? 'activated' : 'deactivated'
            if(update == 1){
                def gender = commonDBFunctions.getDBRecordById(genderId, 'user_gender', 'id')
                def message = "Gender (" + gender.name + ") " + statusMessage
                def genderResponse = ObjectUtils.snakeCaseMapToPojo(gender, new ApiGenderResponseDto())
                response.data = genderResponse
                response.status = HttpStatus.OK.value()
                response.message = message
            }else{
                response.status = HttpStatus.BAD_REQUEST.value()
                response.message = "Update failed!!"
            }
        }else{
            response.status = HttpStatus.BAD_REQUEST.value()
            response.message = 'Country with id ('+ genderId +') does not exist!'
        }

         response
    }

}
