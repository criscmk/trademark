package com.tmea.wit.service

import com.tmea.wit.model.BaseApiResponse
import com.tmea.wit.model.GeneralApiListResponse
import com.tmea.wit.model.dto.FieldErrorDto
import com.tmea.wit.model.dto.response.APIFetchMarketPriceInquiryResponseDto
import com.tmea.wit.model.dto.response.APiFetchAllMarketPriceInquiryResponseDto
import com.tmea.wit.util.CommonDBFunctions
import com.tmea.wit.util.ObjectUtils
import groovy.sql.Sql
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service

import javax.sql.DataSource

@Service
class MarketPriceInquiryService {
    @Autowired
    DataSource dataSource;
    @Autowired
    UserService userService;
    @Autowired
    MarketPriceService marketPriceService;
    @Autowired
    GlobalConfigService globalConfigService;


    public BaseApiResponse getCommodityMarketInquiry(Long commodity, Long location, String date, String username) {
        Map requestParams = [:];
        BaseApiResponse response = new BaseApiResponse();
        requestParams.put("commodity", commodity);
        requestParams.put("location", location);
        requestParams.put("date", date);
        Sql sql = Sql.newInstance(dataSource);
        List<FieldErrorDto> errors = [];
        Long userId = userService.getUserIdFromUsername(username);
        def inquiredPrice = sql.firstRow("""
            SELECT 
            commodity.name AS commodity,
            location.name AS location,
            market_price.id,
            market_price.date,
            market_price.price,
            currency.name AS currency,
            
            commodity_category.name As commodity_category,
            commodity_sub_category.name As commodity_sub_category,
            currency.currency_symbol AS currency_symbol,
            measurement_unit.name AS measurement_unit                   
            FROM
             market_price,
             commodity,
             location,
             currency,
             commodity_category,
             commodity_sub_category,
             measurement_unit
            WHERE 
             market_price.commodity_id = commodity."id"
            AND market_price.location_id = location."id" 
            AND market_price.currency_id = currency."id" 
            AND commodity.commodity_sub_category_id = commodity_sub_category.id
            AND commodity_sub_category.commodity_category_id = commodity_category.id
            AND market_price.measurement_unit_id = measurement_unit."id"
            AND market_price.commodity_id = ?.commodity
            AND  market_price.location_id = ?.location
             AND market_price."date" = ?.date::date """, requestParams)

             if (inquiredPrice) {
            response.status = HttpStatus.OK.value();
            response.setErrors(errors);
            response.setData(ObjectUtils.snakeCaseMapToPojo(inquiredPrice, new APIFetchMarketPriceInquiryResponseDto()));
            response.setMessage("Inquired Commodity market price found");
        } else {
            response.status = HttpStatus.BAD_REQUEST.value();
            response.setErrors(errors);
            response.data = ObjectUtils.mapToSnakeCaseMap(requestParams)
            response.setMessage("Inquired Commodity market price does not exists");
        }
        sql.close()
        return response
    }

    public BaseApiResponse postCommodityMarketPriceInquiry(def marketPriceInquiryDto, String username) {
        Map requestParams = ObjectUtils.asMap(marketPriceInquiryDto);
        BaseApiResponse response = new BaseApiResponse();
        Sql sql = Sql.newInstance(dataSource);
        List<FieldErrorDto> errors = [];
        Long userId = userService.getUserIdFromUsername(username);
        sql.close();
        requestParams.put("userId", userId);
        //check if the market price exists
        def exitingPrice = sql.firstRow(marketPriceService.checkIfMarketPriceExists(), requestParams);
        if(!exitingPrice)
        {
            //check if a similar post by the same user exists
            def existingInquiredPrice = sql.firstRow("""SELECT *
                           
            FROM
             market_price_inquiry
            WHERE 
            market_price_inquiry.commodity_id = ?.commodity AND 
            market_price_inquiry.location_id = ?.location AND 
            market_price_inquiry."date" = ?.date::date AND 
            market_price_inquiry."inquired_by" = ?.userId   
                       
                                 
        """, requestParams);
            if (existingInquiredPrice) {
                response.data = requestParams
                response.status = HttpStatus.BAD_REQUEST.value();
                response.setErrors(errors);
                response.setMessage("Inquiry already exists");
                return  response
            } else {
                def insertMarketPriceInquiry = sql.executeInsert("""
                        INSERT INTO "market_price_inquiry"(commodity_id, location_id,
                                           date,date_inquired,is_answered,inquired_by)
                        VALUES (?.commodity,?.location,?.date::date,current_timestamp,'false',?.userId)
                        """, requestParams);
                if (insertMarketPriceInquiry) {
                 // def insertedRecord = sql.firstRow("""SELECT * FROM  market_price_inquiry WHERE id =? """,insertMarketPriceInquiry.get(0).get(0))
                    response.status = HttpStatus.OK.value();
                    response.data = null
                    response.setMessage("Commodity market price Inquiry has been inserted successfully");
                } else {
                    response.status = HttpStatus.INTERNAL_SERVER_ERROR.value();
                    response.setErrors(errors);
                    response.setData(insertMarketPriceInquiry);
                    response.setMessage("An error occurred while inserting commodity market price ");
                }
            }
        }
        else
        {

            response.data = requestParams
            response.status = HttpStatus.BAD_REQUEST.value();
            response.message="Inquired market price already exists";
        }



        sql.close();
        return response
    }

    public BaseApiResponse answerMarketPriceInquiry(def answerMarketPriceInquiryDto, String username) {
        Map requestParams = ObjectUtils.asMap(answerMarketPriceInquiryDto);
        BaseApiResponse response = new BaseApiResponse();
        Sql sql = Sql.newInstance(dataSource);
        List<FieldErrorDto> errors = [];
        Long userId = userService.getUserIdFromUsername(username);
        sql.close();
        //check if inquiry exists
        def inquiryExists = sql.firstRow(""" SELECT *  FROM market_price_inquiry WHERE id=?.inquiry """, requestParams);
        if (inquiryExists) {
            def queryParams = [:];
            def measurementUnit = marketPriceService.getCommodityMarketPriceMeasurementUnit(inquiryExists.get("commodity_id"))
            if(measurementUnit) {
                queryParams.put("commodity", inquiryExists.get("commodity_id"));
                queryParams.put("location", inquiryExists.get("location_id"));
                queryParams.put("date", inquiryExists.get("date"));
                queryParams.put("measurementUnit", measurementUnit);
                queryParams.put("price", requestParams.get("price"));
                queryParams.put("isActive", true);
                queryParams.put("addedBy", userId);
            }else
            {
                response.setStatus(HttpStatus.BAD_REQUEST.value())
                response.message = "Commodity does not have a measurement unit "
                response.data = inquiryExists
                return response
            }


            //check if inquiry is answered
            def marketPriceCurrency = globalConfigService.getConfigValue("default_currency_config");
            if(marketPriceCurrency) {
                Integer currency = Integer.parseInt(marketPriceCurrency);
                queryParams.put("currency", currency);
                def marketPriceExist = sql.firstRow(marketPriceService.checkIfMarketPriceExists(), queryParams);
                if (!marketPriceExist) {
                  
                    def insertMarketPrice = sql.executeInsert(marketPriceService.insertCommodityMarketPrice(), queryParams);
                    if (insertMarketPrice) {
                        //update inquiry status
                        queryParams.put("isAnswered", true);
                        def markInquiryAnswered = sql.executeUpdate("""UPDATE 
                               market_price_inquiry SET is_answered =?.isAnswered,
                               date_answered=current_timestamp
                                WHERE
                                commodity_id = ?.commodity AND 
                                location_id = ?.location AND 
                                "date" = ?.date::date """, queryParams);
                        if(markInquiryAnswered)
                        {

                            response.data = requestParams
                            response.setStatus(HttpStatus.OK.value());
                            response.message = "Inquiry has been answered successfully";
                        }else
                        {
                            response.data = requestParams
                            response.status = HttpStatus.BAD_REQUEST.value();
                            response.setErrors(errors);
                            response.setMessage("An error occurred while marking inquiry as answered ");
                        }

                    } else {
                        response.data = requestParams
                        response.status = HttpStatus.BAD_REQUEST.value();
                        response.setErrors(errors);
                        response.setMessage("An error occurred while inserting commodity market price ");
                    }
                } else {
                    response.data = requestParams
                    response.status = HttpStatus.BAD_REQUEST.value();
                    response.setErrors(errors);
                    response.setMessage("Market Price Inquiry have  Already been answered");
                }
            }
            else
            {
                response.status = HttpStatus.INTERNAL_SERVER_ERROR.value();
                response.setErrors(errors);
                response.data = requestParams;
                response.setMessage("Currency configuration needs to be defined to add a market price");
            }

        } else {
            response.status = HttpStatus.BAD_REQUEST.value();
            response.setErrors(errors);
            response.setMessage("Inquiry does not exists");
        }


        return response;
    }

    public GeneralApiListResponse getAllMarketPriceInquiries(String isAnswered,String date, String dateFrom, String dateTo, Integer page,Integer size) {
        GeneralApiListResponse response = new GeneralApiListResponse();
        Sql sql = Sql.newInstance(dataSource);
        List<FieldErrorDto> errors = [];
        def requestParams = [:];
        def dateFilter=""
        def dateRangeFilter =""
        def queryFilter = ""
        def paginate = ""
        if (page && size) {
            int offset = (page - 1) * size;
            int limit = size;
            paginate = " LIMIT ?.limit OFFSET ?.offset "
            requestParams.put("limit", limit);
            requestParams.put("offset", offset);
        }
        def answeredFilter = ""
        if (isAnswered) {
            answeredFilter = " AND is_answered =?.isAnswered "
            requestParams.put("isAnswered", Boolean.parseBoolean(isAnswered))

        }
        if (date) {

            dateFilter = ' AND market_price_inquiry.date::date  = ?.date::date '
            requestParams.put("date", date);
        }
        if (dateFrom && dateTo) {
            dateFilter = ""
            dateRangeFilter = " AND market_price_inquiry.date::date BETWEEN date(?.dateFrom::date) AND date(?.dateTo::date)";
            requestParams.put("dateFrom", dateFrom);
            requestParams.put("dateTo", dateTo);

        }

        def countQuery = """ SELECT count(1)
                                FROM
                                 market_price_inquiry,
                                 commodity,
                                 location,
                                 commodity_category,
                                 commodity_sub_category,
                                 user_individual_detail,
                                 "user"
                                WHERE 
                                market_price_inquiry.commodity_id = commodity.id  
                                AND market_price_inquiry.location_id = location."id" 
                                AND commodity.commodity_sub_category_id = commodity_sub_category.id
                                AND commodity_sub_category.commodity_category_id = commodity_category.id
                                AND market_price_inquiry.inquired_by = "user".id
                                AND user_individual_detail.user_id = "user".id   
                                                               
                                """
        def total = 0
        queryFilter = answeredFilter + dateFilter + dateRangeFilter
        if(queryFilter)
        {
            total = sql.firstRow(countQuery + queryFilter, requestParams).get('count')

        }
        else
        {
            total = sql.firstRow(countQuery)?.get('count')
        }


        def allInquiries = sql.rows("""SELECT 
                                market_price_inquiry.id,
                                to_char(market_price_inquiry.date,'YYYY-MM-DD') As market_price_date,
                                to_char(market_price_inquiry.date_inquired,'YYYY-MM-DD')  As date_inquired,
                                market_price_inquiry.is_answered As is_answered,
                                commodity.name AS commodity,
                                location.name AS location,
                                "user".phone_number AS inquired_by_phone,
                                "user".email AS inquired_by_email,
                                commodity_sub_category.name AS commodity_sub_category,
                                market_price_inquiry.comment,
                                commodity_category.name AS commodity_category,
                                 CASE
                                 WHEN "user".user_type_id = 1 THEN (SELECT user_individual_detail.first_name||' '||user_individual_detail.middle_name||' '||user_individual_detail.last_name  FROM user_individual_detail WHERE "user".id = user_individual_detail.user_id)
                                 WHEN "user".user_type_id = 2 THEN (SELECT user_organization_detail.organization_name FROM user_organization_detail WHERE "user".id = user_organization_detail.user_id)
                                 END AS inquired_by_name              
                                 FROM
                                 market_price_inquiry,
                                 commodity,
                                 location,
                                 commodity_sub_category,
                                 commodity_category,
                                 "user"
                                 WHERE 
                                 commodity.id = market_price_inquiry.commodity_id
                                 AND
                                 market_price_inquiry.location_id = location.id
                                 AND
                                 market_price_inquiry.inquired_by = "user".id
                                 AND
                                 commodity.commodity_sub_category_id = commodity_sub_category.id
                                 AND
                                 commodity_category.id = commodity_sub_category.commodity_category_id
                                                                                                    
                                """ + queryFilter + "ORDER BY id  DESC" + paginate, requestParams);

        List<APiFetchAllMarketPriceInquiryResponseDto> allMarketPriceInquiries = ObjectUtils.snakeCaseArrayMapToListPojos(allInquiries, APiFetchAllMarketPriceInquiryResponseDto);
        if (allInquiries) {


            response.status = HttpStatus.OK.value()
            response.data = ObjectUtils.snakeCaseArrayMapToListPojos(allInquiries, APiFetchAllMarketPriceInquiryResponseDto );
            response.total = total
            response.setMessage("market Price Found")
        } else {
            response.status = HttpStatus.NO_CONTENT.value();
            response.setMessage("No market price inquiry found");

        }
        sql.close();
        return response;
    }

}
