package com.tmea.wit.service

import com.tmea.wit.core.storage.StorageApi
import com.tmea.wit.model.BaseApiResponse
import com.tmea.wit.model.GeneralApiListResponse
import com.tmea.wit.model.dto.FieldErrorDto
import com.tmea.wit.model.dto.request.ApiAddProductImagePostDto
import com.tmea.wit.model.dto.request.ApiImageUploadRequestDto
import com.tmea.wit.model.dto.request.ApiProductPostDto
import com.tmea.wit.model.dto.request.ApiProductStatusUpdateRequestDto
import com.tmea.wit.model.dto.request.ApiProductUpdateRequestDto
import com.tmea.wit.model.dto.request.ApiStatusRequestDto
import com.tmea.wit.model.dto.response.ApiFetchAllProductsResponseDto
import com.tmea.wit.model.dto.response.ApiProductDetailsResponseDto
import com.tmea.wit.model.dto.response.ApiProductResponseDto
import com.tmea.wit.model.dto.response.ApiProductUpdateStatusResponseDto
import com.tmea.wit.model.dto.response.ApiUserIndividualResponseDto
import com.tmea.wit.model.dto.response.ApiUserOrganizationResponseDto
import com.tmea.wit.util.CommonDBFunctions
import com.tmea.wit.util.FileUtils
import com.tmea.wit.util.ObjectUtils
import groovy.sql.Sql
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service

import javax.sql.DataSource
import java.math.RoundingMode

@Service
class ProductService {
    private DataSource dataSource
    CommonDBFunctions commonDBFunctions
    StorageApi storageApi
    FileUtils fileUtils

    final String PRODUCT_BUCKET_NAME = "product"

    @Autowired
    GlobalConfigService globalConfigService
    @Autowired
    UserService userService

    @Autowired
    ProductService(DataSource dataSource, CommonDBFunctions commonDBFunctions, StorageApi storageApi, FileUtils fileUtils) {
        this.dataSource = dataSource
        this.commonDBFunctions = commonDBFunctions
        this.storageApi = storageApi
        this.fileUtils = fileUtils
    }

    BaseApiResponse addProduct(ApiProductPostDto body, String name) {
        Map requestParams = ObjectUtils.asMap(body)
        BaseApiResponse response = new BaseApiResponse()
        Sql sql = Sql.newInstance(dataSource)
        List<FieldErrorDto> errors = new ArrayList<>()
        Long userId = userService.getUserIdFromUsername(name)
        requestParams.put("traderId", userId)

        if (userId) {

            // check if product of the same name exists
            def isNameExist = getProductByName(requestParams.get("name"))
            if (isNameExist) {
                response.status = HttpStatus.BAD_REQUEST.value()
                response.message = "Product with the same name already exists"
            } else {
                Boolean isApprovalNeeded = Boolean.valueOf(globalConfigService.getConfigValue("product_approval"))
                BigDecimal price = requestParams.get("price") as BigDecimal
                def toTwoDecimalPrice = price.setScale(2, RoundingMode.CEILING)
                requestParams.put("price", toTwoDecimalPrice)
                requestParams.put("isFeatured", false)

                if (isApprovalNeeded) {
                    requestParams.put("isActive", false)
                    requestParams.put("isApproved", false)
                } else {
                    requestParams.put("isActive", true)
                    requestParams.put("isApproved", true)

                }

                def insertProductQuery
                Long productId
                sql.withTransaction {
                    insertProductQuery = sql.executeInsert(
                            """INSERT INTO product(commodity_id, name, description, price, trader_id, minimum_order_quantity, measurement_unit_id, is_post_to_inventory, is_update_sales, is_approved, date_added, is_in_stock, is_published, language_id, is_featured, is_active) 
                                            VALUES (?.commodityId, ?.name, ?.description, ?.price, ?.traderId, ?.minimumOrderQuantity, ?.measurementUnitId, false, false, ?.isApproved, current_timestamp, ?.isInStock, ?.isPublished, ?.languageId, ?.isFeatured, ?.isActive)""", requestParams
                    )

                    productId = insertProductQuery.get(0).get(0)
                    requestParams.put("productId", productId)
                    def prodImages = requestParams.images
                    String primaryImage = requestParams.get("primaryImage")

                    if (productId) {
                        String primaryImageUrl = fileUtils.saveFile(name, productId, PRODUCT_BUCKET_NAME, primaryImage, storageApi)
                        requestParams.replace("primaryImage", primaryImageUrl)

                        sql.executeInsert(
                                """INSERT INTO product_image(product_id, url, is_approved, is_active, date_added, primary_image) 
                                        VALUES(?.productId, ?.primaryImage, true, true, current_timestamp, true) """, requestParams
                        )

                        if (prodImages) {
                            def imageQuery = """INSERT INTO product_image(product_id, url, is_approved, is_active, date_added, primary_image)
                                                        VALUES(?, ?, ?, ?, current_timestamp, ?)"""

                            sql.withBatch(imageQuery) {
                                prodImages.each { image ->

                                    String imageBase64File = image.image

                                    String imageURL = fileUtils.saveFile(name, productId, PRODUCT_BUCKET_NAME, imageBase64File, storageApi)
                                    it.addBatch(productId, imageURL, true, true, false)
                                }
                            }
                        }
                    }

                }


                if (insertProductQuery) {
                    def product = getProductById(productId)
                    product.replace("price", toTwoDecimalPrice)
                    def data = ObjectUtils.snakeCaseMapToPojo(product, new ApiProductResponseDto())

                    response.data = data
                    response.status = HttpStatus.OK.value()
                    response.message = "Product added successfully"

                } else {
                    response.status = HttpStatus.BAD_REQUEST.value()
                    response.message = "Failed to add product"
                }
            }

        } else {
            errors.add(new FieldErrorDto("userId", "Failed to fetch user details"))
            response.data = null
            response.message = "Failed to add product"
            response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
            response.errors = errors
        }

        sql.close()
        return response

    }

    BaseApiResponse editProduct(ApiProductUpdateRequestDto body, Long id, String name) {
        Sql sql = Sql.newInstance(dataSource)

        BaseApiResponse response = new BaseApiResponse()
        Map params = ObjectUtils.asMap(body)
        Long userId = userService.getUserIdFromUsername(name)
        List<FieldErrorDto> errorDto = new ArrayList<>()

        if (userId) {
            if (params) {
                //check if product exists
                def productExists = sql.firstRow(
                        """SELECT * FROM product WHERE id = ?""", id
                )

                if (productExists) {
                    Map updateParams = ObjectUtils.rowAsMap(productExists)
                    ObjectUtils.copyMapProperties(params, updateParams)

                    BigDecimal price = updateParams.get("price") as BigDecimal
                    def toTwoDecimalPrice = price.setScale(2, RoundingMode.CEILING)
                    updateParams.put("price", toTwoDecimalPrice)

                    def updateProductQuery = sql.executeUpdate(
                            """UPDATE product 
                                SET name = ?.name, 
                                    description = ?.description, 
                                    price = ?.price, 
                                    minimum_order_quantity = ?.minimum_order_quantity, 
                                    measurement_unit_id = ?.measurement_unit_id
                                    WHERE id = ?.id""", updateParams
                    )

                    if (updateProductQuery > 0) {
                        def updatedProduct = sql.firstRow(
                                """SELECT product.id,
                                            product.name,
                                            product.description,
                                            product.price,
                                            product.minimum_order_quantity,
                                            measurement_unit.name         AS measurement_unit,
                                            product_image.url             AS primary_image
                                    FROM product,
                                         measurement_unit, 
                                         product_image
                                     WHERE product.measurement_unit_id = measurement_unit.id
                                       AND product_image.product_id = product.id
                                       AND product_image.primary_image
                                     AND product.id = ?""", id
                        )

                        if (updatedProduct) {
                            updatedProduct.replace("price", toTwoDecimalPrice)
                        }

                        response.status = HttpStatus.OK.value()
                        response.data = ObjectUtils.mapToSnakeCaseMap(updatedProduct)
                        response.message = updateProductQuery + " updated successfully!"
                    } else {
                        response.status = HttpStatus.BAD_REQUEST.value()
                        response.message = "Failed to edit product"
                    }
                } else {
                    response.status = HttpStatus.BAD_REQUEST.value()
                    response.message = "Record not found"
                }
            } else {
                response.status = HttpStatus.BAD_REQUEST.value()
                response.message = "Payload cannot be empty"
            }

        } else {
            errorDto.add(new FieldErrorDto("userId", "Failed to fetch user details"))
            response.data = null
            response.message = "Failed to edit product"
            response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
            response.errors = errorDto
        }

        sql.close()
        return response
    }

    BaseApiResponse addProductImage(ApiAddProductImagePostDto body, Long id, String name) {
        Sql sql = Sql.newInstance(dataSource)
        BaseApiResponse response = new BaseApiResponse()
        Map params = ObjectUtils.asMap(body)
        Long userId = userService.getUserIdFromUsername(name)

        List<FieldErrorDto> errors = new ArrayList<>()

        if (userId) {
            def productExist = getProductById(id)

            if (productExist) {
                def productImages = params.get("images")

                if (productImages) {
                    def imageQuery = """INSERT INTO product_image(product_id, url, is_approved, is_active, date_added)
                                                        VALUES(?, ?, ?, ?, current_timestamp)"""

                    def query = sql.withBatch(imageQuery) {
                        productImages.each { image ->

                            String imageBase64File = image.image

                            String imageURL = fileUtils.saveFile(name, id, PRODUCT_BUCKET_NAME, imageBase64File, storageApi)
                            it.addBatch(id, imageURL, true, true)
                        }
                    }

                    if (query.size() > 0) {
                        response.status = HttpStatus.OK.value()
                        response.message = "Images added "
                    } else {
                        response.status = HttpStatus.BAD_REQUEST.value()
                        response.message = "Image upload failed!"
                    }
                }
            } else {
                response.status = HttpStatus.NOT_FOUND.value()
                response.message = "Record not found"
            }

        } else {
            errors.add(new FieldErrorDto("userId", "Failed to fetch user details"))
            response.data = null
            response.message = "Failed to add image"
            response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
            response.errors = errors
        }

        sql.close()
        return response

    }

    BaseApiResponse removeProductImage(Long id, String name) {
        Sql sql = Sql.newInstance(dataSource)
        BaseApiResponse response = new BaseApiResponse()
        Long userId = userService.getUserIdFromUsername(name)

        List<FieldErrorDto> errorDto = new ArrayList<>()

        if (userId) {
            def imageExist = sql.firstRow(
                    """SELECT * FROM product_image WHERE id = ?""", id
            )

            if (imageExist) {
                def updateQuery = sql.executeUpdate(
                        """UPDATE product_image SET is_active = false WHERE id = ?""", id
                )

                if (updateQuery > 0) {
                    response.status = HttpStatus.OK.value()
                    response.message = "Image deleted successfully"
                } else {
                    response.status = HttpStatus.BAD_REQUEST.value()
                    response.message = "Failed to delete image"
                }

            } else {
                response.status = HttpStatus.NOT_FOUND.value()
                response.message = "Record not found!"
            }

        } else {
            errorDto.add(new FieldErrorDto("userId", "Failed to fetch user details"))
            response.data = null
            response.message = "Failed to delete image"
            response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
            response.errors = errorDto
        }

        sql.close()
        return response
    }

    BaseApiResponse getProductDetails(Long id) {
        Sql sql = Sql.newInstance(dataSource)
        BaseApiResponse response = new BaseApiResponse()

        def productDetails = getProductDetailsById(id)
        def productImages = getProductImages(id)

        if (productDetails) {
            BigDecimal productPrice = productDetails.get("price")
            productDetails.replace("price", (productPrice.setScale(2, RoundingMode.CEILING)))
            ApiProductDetailsResponseDto productDetailsResponseDto = ObjectUtils.snakeCaseMapToPojo(productDetails, new ApiProductDetailsResponseDto()) as ApiProductDetailsResponseDto
            productDetailsResponseDto.setImages(productImages)

            response.status = HttpStatus.OK.value()
            response.data = productDetailsResponseDto
            response.message = "Success"
        } else {
            response.message = "Record not found"
            response.status = HttpStatus.BAD_REQUEST.value()
        }

        sql.close()
        return response
    }

    GeneralApiListResponse getAllProducts(Map parameterMap, boolean isForCurrentUser, boolean isAdmin) {
        Sql sql = Sql.newInstance(dataSource)
        Authentication auth = SecurityContextHolder.getContext().getAuthentication()
        String userName = auth.getName()
        Long currentUserId = userService.getUserIdFromUsername(userName)
        GeneralApiListResponse response = new GeneralApiListResponse()
        List<ApiFetchAllProductsResponseDto> productsResponseDtoList = new ArrayList<>()
        List<FieldErrorDto> errors = new ArrayList<>()

        def sqlParams = ObjectUtils.flattenListParam(parameterMap)
        def status = sqlParams.status
        Map requestParams = [start: 0, limit: 25]

        def commodity = sqlParams.get("commodityId")?.toInteger()
        def category = sqlParams.get("categoryId")?.toInteger()
        def subCategory = sqlParams.get("subCategoryId")?.toInteger()
        def trader = sqlParams.get("traderId")?.toInteger()
        def isPublished = sqlParams.get("isPublished")
        def isApproved = sqlParams.get("isApproved")
        def isInStock = sqlParams.get("isInStock")
        def isFeatured = sqlParams.get("isFeatured")
        def query = sqlParams.get("query")

        def statusFilter = ""
        if (status) {
            def booleanStatus = CommonDBFunctions.getStatusAsBoolean(status)

            if (booleanStatus != null) {
                requestParams.status = booleanStatus
                statusFilter = " AND product.is_active = ?.status "
            } else {
                errors.add(new FieldErrorDto("status", "Invalid value passed. Check Documentation"))
                response.data = []
                response.total = 0
                response.status = HttpStatus.BAD_REQUEST.value()
                response.message = "Invalid status value"
                return response

            }
        }

        def isPublishedFilter = ""
        if (isPublished) {
            def publishedStatus = CommonDBFunctions.getStatusAsBoolean(isPublished)

            if (publishedStatus != null) {
                requestParams.isPublished = publishedStatus
                isPublishedFilter = " AND product.is_published = ?.isPublished "
            } else {
                errors.add(new FieldErrorDto("isPublished", "Invalid value passed. Check Documentation"))
                response.data = []
                response.total = 0
                response.status = HttpStatus.BAD_REQUEST.value()
                response.message = "Invalid isPublished value"
                return response

            }
        }

        def isApprovedFilter = ""
        if (isApproved) {
            def approvedStatus = CommonDBFunctions.getStatusAsBoolean(isApproved)

            if (approvedStatus != null) {
                requestParams.isApproved = approvedStatus
                isApprovedFilter = " AND product.is_approved = ?.isApproved "
            } else {
                errors.add(new FieldErrorDto("isApproved", "Invalid value passed. Check Documentation"))
                response.data = []
                response.total = 0
                response.status = HttpStatus.BAD_REQUEST.value()
                response.message = "Invalid isApproved value"
                return response

            }
        }

        def isFeaturedFilter = ""
        if (isFeatured) {
            def featureStatus = CommonDBFunctions.getStatusAsBoolean(isFeatured)

            if (featureStatus != null) {
                requestParams.isFeatured = featureStatus
                isFeaturedFilter = " AND product.is_featured = ?.isFeatured "
            } else {
                errors.add(new FieldErrorDto("isFeatured", "Invalid value passed. Check Documentation"))
                response.data = []
                response.total = 0
                response.status = HttpStatus.BAD_REQUEST.value()
                response.message = "Invalid isFeatured value"
                return response

            }
        }

        def isInStockFilter = ""
        if (isFeatured) {
            def inStockStatus = CommonDBFunctions.getStatusAsBoolean(isInStock)

            if (inStockStatus != null) {
                requestParams.isInStock = inStockStatus
                isInStockFilter = " AND product.is_in_stock = ?.isInStock "
            } else {
                errors.add(new FieldErrorDto("isInStock", "Invalid value passed. Check Documentation"))
                response.data = []
                response.total = 0
                response.status = HttpStatus.BAD_REQUEST.value()
                response.message = "Invalid isInStock value"
                return response

            }
        }

        def commodityFilter = "";
        if (commodity) {
            commodityFilter = " AND product.commodity_id  = ?.commodity "
            requestParams.put("commodity", commodity)
        }

        def categoryFilter = ""
        if (category) {
            categoryFilter = " AND commodity_category.id = ?.category "
            requestParams.put("category", category)
        }

        def subCategoryFilter = ""
        if (subCategory) {
            subCategoryFilter = " AND commodity_sub_category.id = ?.subCategory "
            requestParams.put("subCategory", subCategory)
        }

        def traderFilter = ""
        if (trader) {
            traderFilter = " AND product.trader_id = ?.traderId "
            requestParams.put("traderId", trader)
        }

        def queryFilter = ""
        if (query) {
            commodityFilter = ""
            categoryFilter = ""
            subCategoryFilter = ""

            String formattedQuery = "%$query%"
            queryFilter = """ AND (
                       product.name ILIKE ?.query OR
                       commodity.name ILIKE ?.query OR
                       commodity_sub_category.name ILIKE ?.query OR
                       commodity_category.name ILIKE ?.query
                     )
             """;
            requestParams.put("query", formattedQuery)

        }

        if (isForCurrentUser && !isAdmin) {

            traderFilter = " AND product.trader_id = ?.traderId ";
            requestParams.put("traderId", currentUserId);

            statusFilter = " AND product.is_active = ?.status "
            requestParams.put("status", true);

        }

        if (!isAdmin && !isForCurrentUser) {

            statusFilter = " AND product.is_active = ?.status "
            requestParams.put("status", true);

            requestParams.isPublished = true
            isPublishedFilter = " AND product.is_published = ?.isPublished "

            requestParams.isApproved = true
            isApprovedFilter = " AND product.is_approved = ?.isApproved "

        }


        def isAdminQuery = """
                    SELECT 
                               product.id,
                               product.name,
                               product.description,
                               product.price,
                               product.minimum_order_quantity,
                               commodity.id AS commodity_id,
                               commodity_category.id AS category_id,
                               commodity_sub_category.id AS sub_category_id,
                               commodity.name                      AS commodity_name,
                               commodity_category.name             AS category_name,
                               commodity_sub_category.name         AS sub_category_name,
                               measurement_unit.name               AS measurement_unit,
                               (SELECT url FROM product_image WHERE product.id = product_id AND product_image.is_approved AND product_image.is_active AND product_image.primary_image ) AS primary_image,
                               language.name                       AS language,
                               CASE
                                WHEN "user".user_type_id = 1 THEN (SELECT user_individual_detail.first_name||' '||user_individual_detail.middle_name||' '||user_individual_detail.last_name  FROM user_individual_detail WHERE "user".id = user_individual_detail.user_id)
                                WHEN "user".user_type_id = 2 THEN (SELECT user_organization_detail.organization_name FROM user_organization_detail WHERE "user".id = user_organization_detail.user_id)
                                END AS trader_name,
                               product.trader_id,
                               product.is_post_to_inventory,
                               product.is_update_sales,
                               product.is_approved,
                               product.date_added,
                               product.is_in_stock,
                               product.is_published,
                               product.is_featured,
                               product.is_active    
                        FROM 
                             product,
                             commodity,
                             commodity_category,
                             commodity_sub_category,
                             measurement_unit,
                             language,
                             "user"
                        WHERE product.commodity_id = commodity.id
                        AND commodity.commodity_sub_category_id = commodity_sub_category.id
                        AND commodity_sub_category.commodity_category_id = commodity_category.id
                        AND  product.measurement_unit_id = measurement_unit.id
                        AND  product.language_id = language.id
                        AND  product.trader_id = "user".id 
                    """

        def selectQuery = """
                            SELECT 
                               product.id,
                               product.name,
                               product.description,
                               product.price,
                               product.minimum_order_quantity,
                               commodity.id AS commodity_id,
                               commodity_category.id AS category_id,
                               commodity_sub_category.id AS sub_category_id,
                               commodity.name                      AS commodity_name,
                               commodity_category.name             AS category_name,
                               commodity_sub_category.name         AS sub_category_name,
                               measurement_unit.name               AS measurement_unit,
                               (SELECT url FROM product_image WHERE product.id = product_id AND product_image.is_approved AND product_image.is_active AND product_image.primary_image ) AS primary_image,
                               product.trader_id,                              
                               CASE
                                WHEN "user".user_type_id = 1 THEN (SELECT user_individual_detail.first_name||' '||user_individual_detail.middle_name||' '||user_individual_detail.last_name  FROM user_individual_detail WHERE "user".id = user_individual_detail.user_id)
                                WHEN "user".user_type_id = 2 THEN (SELECT user_organization_detail.organization_name FROM user_organization_detail WHERE "user".id = user_organization_detail.user_id)
                                END AS trader_name
                                
                            FROM 
                                 product,
                                 commodity,
                                 commodity_category,
                                 commodity_sub_category,
                                 measurement_unit,
                                 "user"
                            WHERE product.commodity_id = commodity.id
                                AND commodity.commodity_sub_category_id = commodity_sub_category.id
                                AND commodity_sub_category.commodity_category_id = commodity_category.id
                                AND  product.measurement_unit_id = measurement_unit.id
                                AND  product.trader_id = "user".id 
                        """

        def countQuery = """SELECT count(1) FROM  product,
                             commodity,
                             commodity_category,
                             commodity_sub_category,
                             measurement_unit,
                             language,
                             user_individual_detail,
                             "user"
                        WHERE product.commodity_id = commodity.id
                        AND commodity.commodity_sub_category_id = commodity_sub_category.id
                        AND commodity_sub_category.commodity_category_id = commodity_category.id
                        AND  product.measurement_unit_id = measurement_unit.id
                        AND  product.language_id = language.id
                        AND  user_individual_detail.user_id = "user".id""";

        //Pagination parameters & logic
        requestParams = commonDBFunctions.paginationSqlParams(requestParams, sqlParams)

        String limitClause = " limit ?.limit offset ?.start";
        String filterQuery = commodityFilter + categoryFilter + subCategoryFilter + traderFilter + queryFilter + isApprovedFilter +
                isPublishedFilter + isInStockFilter + isFeaturedFilter + statusFilter + limitClause;

        def filteredSelectQuery = isAdmin || isForCurrentUser ? isAdminQuery : selectQuery
        def filterProductQuery = filteredSelectQuery + filterQuery
        def filteredCountQuery = countQuery + filterQuery

        def data = sql.rows(filterProductQuery, requestParams);
        def total;

        if (filterQuery) {
            total = sql.firstRow(filteredCountQuery, requestParams).get("count")
        } else {
            total = sql.firstRow(filteredCountQuery).get("count")
        }

        if (data) {

            data.each {
                BigDecimal price = it.get("price") as BigDecimal
                it.replace("price", (price.setScale(2, RoundingMode.CEILING)))

                def productResponseDto = ObjectUtils.mapToSnakeCaseMap(it)
                productsResponseDtoList.add(productResponseDto)
            }
            response.status = HttpStatus.OK.value()
            response.data = productsResponseDtoList
            response.total = total as Long
            response.message = "success"
        } else {
            response.message = "No records found"
        }


        sql.close()
        return response
    }

    BaseApiResponse updateProductStatus(ApiProductStatusUpdateRequestDto apiProductStatusUpdateRequestDto, Long id, String name,String statusToUpdate) {
        Sql sql = Sql.newInstance(dataSource)
        BaseApiResponse response = new BaseApiResponse()
        Long userId = userService.getUserIdFromUsername(name)

        List<FieldErrorDto> errorList = new ArrayList<>()

        if (userId) {

            //check if record exists
            def recordExists = getProductDetailsById(id)

            if (recordExists) {

                def updateQuery
                def statusMsg = ""
                String statusColumn = ""

                if(statusToUpdate =="active" || statusToUpdate=="instock" || statusToUpdate=="featured" || statusToUpdate=="published"){
                    if (statusToUpdate == "instock") {
                        statusColumn = "is_in_stock"
                    }else if (statusToUpdate == "featured") {
                        statusColumn = "is_featured"
                    }else if (statusToUpdate == "published") {
                        statusToUpdate = "isPublished"
                        statusColumn = "is_published"
                    }else if (statusToUpdate == "active") {

                        if (!recordExists.get("is_approved")) {
                            response.message = "Cannot update status, product has not been approved"
                            response.status = HttpStatus.BAD_REQUEST.value()
                            sql.close()
                            return response
                        } else {
                            statusColumn = "is_active"
                        }

                    }
                    boolean statusToUpdateValue = apiProductStatusUpdateRequestDto.getStatus()

                    Map statusUpdateMap = [id:id,status:statusToUpdateValue]
                    def updateQueryStm = """UPDATE product
                                    SET $statusColumn = ?.status
                                    WHERE id = ?.id"""

                    updateQuery = sql.executeUpdate(updateQueryStm, statusUpdateMap)

                    statusMsg = "Product $statusToUpdate set to $statusToUpdateValue successfully"

                    if (updateQuery > 0) {
                        def product = sql.firstRow(
                                """SELECT id, name, description, $statusColumn
                                        FROM product WHERE id = ?""", id
                        )
                        def productResDto = ObjectUtils.mapToSnakeCaseMap(product)

                        response.status = HttpStatus.OK.value()
                        response.data = productResDto
                        response.message = statusMsg
                    } else {
                        response.message = "Could not update product"
                        response.status = HttpStatus.BAD_REQUEST.value()
                    }
                }else{
                    response.status = HttpStatus.BAD_REQUEST.value()
                    response.message = "Invalid Status"
                }

            } else {
                response.status = HttpStatus.NOT_FOUND.value()
                response.message = "Record not found"
            }


        } else {
            errorList.add(new FieldErrorDto("userId", "Failed to fetch user details"))
            response.data = null
            response.message = "Failed to update product!"
            response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
            response.errors = errorList
        }

        sql.close()
        return response
    }


    Map getProductById(Long id) {
        Sql sql = Sql.newInstance(dataSource)
        def prodResult = sql.firstRow(
                """SELECT 
                               product.id,
                               product.name,
                               product.description,
                               product.price,
                               product.minimum_order_quantity,
                               product_image.url     AS primary_image
                        FROM 
                             product,
                             product_image
                        WHERE product_image.product_id = product.id
                          AND product_image.primary_image
                           AND  product.id = ?""", id
        )

        sql.close()
        return prodResult
    }

    Map getProductDetailsById(Long id) {
        Sql sql = Sql.newInstance(dataSource)
        def prodResult = sql.firstRow(
                """SELECT 
                               product.id,
                               product.name,
                               product.description,
                               product.price,
                               product.minimum_order_quantity,
                               commodity.name                  AS commodity_name,
                               measurement_unit.name           AS measurement_unit,
                               language.name                   AS language,
                               (SELECT url FROM product_image WHERE product.id = product_id AND product_image.is_approved AND product_image.is_active AND product_image.primary_image ) AS primary_image,
                                CASE
                                WHEN "user".user_type_id = 1 THEN (SELECT user_individual_detail.first_name||' '||user_individual_detail.middle_name||' '||user_individual_detail.last_name  FROM user_individual_detail WHERE "user".id = user_individual_detail.user_id)
                                WHEN "user".user_type_id = 2 THEN (SELECT user_organization_detail.organization_name FROM user_organization_detail WHERE "user".id = user_organization_detail.user_id)
                                END AS trader_name,
                               product.trader_id,
                               commodity.id AS commodity_id,
                               commodity_category.id AS category_id,
                               commodity_sub_category.id AS sub_category_id,
                               commodity_category.name             AS category_name,
                               commodity_sub_category.name         AS sub_category_name,
                               product.is_post_to_inventory,
                               product.is_update_sales,
                               product.is_approved,
                               product.date_added,
                               product.is_in_stock,
                               product.is_published,
                               product.is_featured,
                               product.is_active    
                        FROM 
                             product,
                             commodity,
                             commodity_category,
                             commodity_sub_category,
                             measurement_unit,
                             language,
                             "user"
                        WHERE product.commodity_id = commodity.id
                        AND commodity.commodity_sub_category_id = commodity_sub_category.id
                        AND commodity_sub_category.commodity_category_id = commodity_category.id
                        AND   product.measurement_unit_id = measurement_unit.id
                        AND   product.language_id = language.id
                        AND   product.trader_id = "user".id
                        AND   product.id = ?""", id

        )

        sql.close()
        return prodResult
    }

    BaseApiResponse setProductPrimaryImage(long id, String name) {
        Sql sql = Sql.newInstance(dataSource)
        BaseApiResponse response = new BaseApiResponse()
        Long userId = userService.getUserIdFromUsername(name)

        List<FieldErrorDto> errorList = new ArrayList<>()

        if (userId) {
            def imageExist = sql.firstRow("""SELECT * FROM product_image WHERE id = ?""", id)

            if (imageExist) {
                Long productId = imageExist.get("product_id")
                def activePrimaryImage = sql.firstRow("""SELECT * FROM product_image WHERE primary_image AND product_id = ?""", productId)
                Long primaryImageId = activePrimaryImage.get("id")

                def setImage = ""
                def deactivateCurrentPrimaryImage = ""
                if (activePrimaryImage) {
                    sql.withTransaction {
                        deactivateCurrentPrimaryImage = sql.executeUpdate("""
                                                        UPDATE product_image SET primary_image = false WHERE id = ?
                                                        """, primaryImageId)

                        setImage = sql.executeUpdate("""UPDATE product_image SET primary_image = true WHERE id = ?""", id)
                    }
                } else {
                    setImage = sql.executeUpdate("""UPDATE product_image SET primary_image = true WHERE id = ?""", id)
                }


                if (setImage > 0) {
                    response.status = HttpStatus.OK.value()
                    response.message = "Product primary image update successfully"
                } else {
                    response.status = HttpStatus.BAD_REQUEST.value()
                    response.message = "Operation failed"
                }

            } else {
                response.status = HttpStatus.NOT_FOUND.value()
                response.message = "Image not found"
            }

        } else {
            errorList.add(new FieldErrorDto("userId", "Failed to fetch user details"))
            response.data = null
            response.message = "Failed to update product!"
            response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
            response.errors = errorList
        }

        sql.close()
        return response
    }

    BaseApiResponse getTraderDetails(Long userId) {
        Sql sql = Sql.newInstance(dataSource)
        BaseApiResponse response
pres
        def userType = sql.firstRow('SELECT user_type_id FROM "user" WHERE id = ?', userId).get("user_type_id");
        if (userType) {
            def userDetailsResponse;
            BigDecimal rating
            def traderDetails = userService.fetchUserDetails(userId, userType as Long)
            if (traderDetails) {
                rating = userService.getUserRating(userId)
                if (userType == 1) {
                    userDetailsResponse = ObjectUtils.snakeCaseMapToPojo(traderDetails, new ApiUserIndividualResponseDto()) as ApiUserIndividualResponseDto;
                } else if (userType == 2) {
                    userDetailsResponse = ObjectUtils.snakeCaseMapToPojo(traderDetails, new ApiUserOrganizationResponseDto()) as ApiUserOrganizationResponseDto;
                }
                userDetailsResponse.setRating(rating)

                response = new BaseApiResponse(userDetailsResponse, HttpStatus.OK.value(), "Success");
            }

        } else {
            response = new BaseApiResponse(null, HttpStatus.BAD_REQUEST.value(), "Unrecognized user");
        }

        sql.close();

        return response;
    }


    List<Map> getProductImages(Long id) {
        Sql sql = new Sql(dataSource)
        def images = sql.rows(
                """SELECT id, url FROM product_image WHERE is_active AND is_approved AND NOT primary_image AND product_id = ?""", id
        )

        sql.close()
        return images
    }

    Map getProductByName(String name) {
        Sql sql = Sql.newInstance(dataSource)
        def prodResult = sql.firstRow(
                """SELECT * FROM product WHERE name ILIKE ?""", name
        )

        sql.close()
        return prodResult
    }

}
