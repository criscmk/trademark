package com.tmea.wit.service

import com.tmea.wit.core.storage.PolicyTypes
import com.tmea.wit.core.storage.StorageApi
import com.tmea.wit.model.BaseApiResponse
import com.tmea.wit.model.GeneralApiListResponse
import com.tmea.wit.model.dto.FieldErrorDto
import com.tmea.wit.model.dto.response.ApiCommodityCategoryResponseDto
import com.tmea.wit.repository.UserRepository
import com.tmea.wit.util.CommonDBFunctions
import com.tmea.wit.util.FileUtils
import com.tmea.wit.util.ObjectUtils
import groovy.sql.Sql
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service

import javax.sql.DataSource

@Service
class CommodityCategoryService {

    DataSource dataSource
    UserRepository userRepository
    CommonDBFunctions commonDBFunctions
    StorageApi storageApi
    FileUtils fileUtils
    @Value('${services.storage.endpoint}')
    String fileStorageEndpoint


    final String CATEGORY_BUCKET_NAME = "category";

    @Autowired
    CommodityCategoryService(DataSource dataSource, UserRepository userRepository, CommonDBFunctions commonDBFunctions,StorageApi storageApi,FileUtils fileUtils) {
        this.dataSource = dataSource
        this.userRepository = userRepository
        this.commonDBFunctions = commonDBFunctions
        this.storageApi = storageApi
        this.fileUtils = fileUtils
    }

    public BaseApiResponse addCommodityCategory(def body, String userName){
        Sql sql = new Sql(dataSource)
        BaseApiResponse response = new BaseApiResponse()
        List<FieldErrorDto> errors = []
        Map requestParams = ObjectUtils.asMap(body)
        Long addedBy = userRepository.findUserIdByDetails(userName)

        if(addedBy){
            requestParams.put("addedBy", addedBy)
            def isNameExist = commonDBFunctions.getDBRecordByTableColumn(requestParams.name, 'commodity_category','name' )
            if(isNameExist){
                response.status = HttpStatus.BAD_REQUEST.value()
                response.message = 'Category name ('+ requestParams.name +') already exist!'
            }else{

                def query = """
                            INSERT INTO commodity_category (name, description, added_by)
                            VALUES (?.name, ?.description, ?.addedBy)
                        """
                def insertCategory = sql.executeInsert(query, requestParams)
                if(insertCategory){
                    Long categoryId = insertCategory.get(0).get(0)
                    String categoryImage = requestParams.image
                    if(categoryImage){
                        try{
                            String imageURL = fileUtils.saveFile(userName, categoryId, CATEGORY_BUCKET_NAME, categoryImage, storageApi)
                            Map updateParams = [id: categoryId, url: imageURL]
                            sql.executeUpdate("UPDATE commodity_category SET image_url = ?.url WHERE id = ?.id", updateParams)
                            response.message = "Success"
                        }catch(ConnectException e){
                            response.message = "Category Saved but Image could not be updated"
                        }

                    }

                    def category = commonDBFunctions.getDBRecordById(categoryId, 'commodity_category','id' )
                    def data = ObjectUtils.snakeCaseMapToPojo(category, new ApiCommodityCategoryResponseDto())
                    response.data = data

                    response.status = HttpStatus.OK.value()
                }else{
                    response.data = []
                    response.message = "Failed to add category"
                    response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
                }
            }
        }else{
            errors.add(new FieldErrorDto("userId", "Failed to fetch user details of the user adding the category"))
            response.data = []
            response.message = "Failed to add category"
            response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
            response.errors = errors
        }
        sql.close()
        response
    }

    public GeneralApiListResponse getCommodityCategories(Map parameterMap,boolean isAdmin){
        Sql sql = new Sql(dataSource)
        GeneralApiListResponse response
        List<ApiCommodityCategoryResponseDto> categoryList = new ArrayList<>()
        List<FieldErrorDto> errors = []

        def params = ObjectUtils.flattenListParam(parameterMap)
        def status = params.status
        def query = params.get("query")

        def queryFilters = ""
        def statusFilter = ""
        def nameFilter = ""

        Map sqlParams = [start: 0, limit: 25]

        boolean whereIncluded = false;


        //Override Status Filter if not Admin
        status = isAdmin ? status: "active"
        if(status){
            def booleanStatus = CommonDBFunctions.getStatusAsBoolean(status)
            if(booleanStatus != null){
                sqlParams.status = booleanStatus
                statusFilter = " WHERE is_active = ?.status"
                whereIncluded = true
            }else{
                errors.add(new FieldErrorDto("status", "Invalid value passed. Check Documentation"))
                response = new GeneralApiListResponse([], 0, HttpStatus.BAD_REQUEST.value(), "Invalid status value", errors)
                return response
            }
        }

        if(query)
        {

            String querySearch = "%$query%"
            sqlParams.put("query",querySearch)
            def queryFilterClause = """ commodity_category.name ILIKE ?.query """
            nameFilter = whereIncluded ? " AND "+queryFilterClause : " WHERE "+queryFilterClause
        }

        //Pagination parameters & logic
        sqlParams = CommonDBFunctions.paginationSqlParams(sqlParams, params)

        queryFilters = statusFilter+nameFilter

        def querySuffix = queryFilters+ " ORDER BY name ASC "+ CommonDBFunctions.getLimitClause()

        def fetchCommoditySelectQuery = isAdmin ? "SELECT * FROM commodity_category ":
                "SELECT id,name,description,image_url from commodity_category "

        def fetchCommodityCategoryQuery = fetchCommoditySelectQuery+querySuffix

        def countQuery = "SELECT count(*) FROM commodity_category"+queryFilters

        def data = sql.rows(fetchCommodityCategoryQuery, sqlParams)

        def total = queryFilters ? sql.firstRow(countQuery,sqlParams).get("count") : sql.firstRow(countQuery).get("count")

        if(data){
            response = new GeneralApiListResponse(ObjectUtils.mapToSnakeCaseArrayMap(data), total as Long, HttpStatus.OK.value(), "Success")
        }else{
            response = new GeneralApiListResponse([], total as Long, HttpStatus.NO_CONTENT.value(), "No records found")
        }
        sql.close()
        response
    }

    public BaseApiResponse updateCommodityCategoryStatus(Long categoryId, def body){
        Sql sql = new Sql(dataSource)
        BaseApiResponse response = new BaseApiResponse([], 200, "")
        def params = ObjectUtils.asMap(body)
        params.id = categoryId
        def newStatus = (params.status.toString().toLowerCase() == 'active')
        params.status = newStatus

        def isCategoryExist = commonDBFunctions.getDBRecordById(categoryId, 'commodity_category','id' )
        if(isCategoryExist){
            def update = sql.executeUpdate "UPDATE commodity_category set is_active = ?.status WHERE id = ?.id", params
            sql.close()
            def statusMessage= newStatus ? 'activated' : 'deactivated'

            if(update == 1){
                def commodityCategory = commonDBFunctions.getDBRecordById(categoryId, 'commodity_category','id' )
                def message = "Category ("+ commodityCategory.name+ ") " + statusMessage
                def commodityCategoryResponse = ObjectUtils.mapToSnakeCaseMap(commodityCategory)
                response.data = commodityCategoryResponse
                response.status = HttpStatus.OK.value()
                response.message = message
            }else{
                response.status = HttpStatus.BAD_REQUEST.value()
                response.message = "Update failed!!"
            }
        }else{
            response.status = HttpStatus.BAD_REQUEST.value()
            response.message = 'Category with id ('+ categoryId +') does not exist!'
        }

        response
    }

    def BaseApiResponse updateCommodityCategoryImage(long categoryId, def body, String userName){
        Sql sql = new Sql(dataSource)
        BaseApiResponse response = new BaseApiResponse(null,200,"")
        def params = ObjectUtils.asMap(body)
        def isCategoryExist = commonDBFunctions.getDBRecordById(categoryId, 'commodity_category','id' )
        if(isCategoryExist){
            String categoryImage = params.image
            if(categoryImage){
                String imageURL = fileUtils.saveFile(userName, categoryId, CATEGORY_BUCKET_NAME, categoryImage, storageApi)
                Map updateParams = [id: categoryId, url: imageURL]
                int updateRes = sql.executeUpdate("UPDATE commodity_category SET image_url = ?.url WHERE id = ?.id", updateParams)
                if(updateRes){
                    def commodityCategory = commonDBFunctions.getDBRecordById(categoryId, 'commodity_category','id' )
                    def message = "Category ("+ commodityCategory.name+ ") Image Updated "
                    def commodityCategoryResponse = ObjectUtils.mapToSnakeCaseArrayMap(commodityCategory)
                    response.data = commodityCategoryResponse
                    response.status = HttpStatus.OK.value()
                    response.message = message
                }else{
                    response.status = HttpStatus.BAD_REQUEST.value()
                    response.message = "failed to update image!!"
                }
            }
        }else{
            response.status = HttpStatus.BAD_REQUEST.value()
            response.message = 'Category with id ('+ categoryId +') does not exist!'
        }

        sql.close()
        response
    }

    def BaseApiResponse updateCommodityCategory(long categoryId, def body, String userName) {
        Sql sql = new Sql(dataSource)
        BaseApiResponse response = new BaseApiResponse([], 200, "")
        def params = ObjectUtils.asMap(body)
        params.id = categoryId

        def isCategoryExist = commonDBFunctions.getDBRecordById(categoryId, 'commodity_category','id' )
        if(isCategoryExist){
            String categoryImage = params.image
            String imageUrl = null
            if(categoryImage){
                imageUrl = fileUtils.saveFile(userName, categoryId, CATEGORY_BUCKET_NAME, categoryImage, storageApi)
            }else{
                imageUrl = isCategoryExist.get("image_url")
            }
            params.put("url", imageUrl)
            def update = sql.executeUpdate "UPDATE commodity_category set name = ?.name, description = ?.description, image_url = ?.url WHERE id = ?.id", params
            sql.close()

            if(update == 1){
                def commodityCategory = commonDBFunctions.getDBRecordById(categoryId, 'commodity_category','id' )
                def message = "Success"
                def commodityCategoryResponse = ObjectUtils.mapToSnakeCaseMap(commodityCategory)
                response.data = commodityCategoryResponse
                response.status = HttpStatus.OK.value()
                response.message = message
            }else{
                response.status = HttpStatus.BAD_REQUEST.value()
                response.message = "Update failed!!"
            }

        }else{
            response.status = HttpStatus.BAD_REQUEST.value()
            response.message = 'Category with id ('+ categoryId +') does not exist!'
        }
        response
    }
}
