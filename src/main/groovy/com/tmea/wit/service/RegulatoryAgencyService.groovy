package com.tmea.wit.service

import com.tmea.wit.core.storage.StorageApi
import com.tmea.wit.model.BaseApiResponse
import com.tmea.wit.model.GeneralApiListResponse
import com.tmea.wit.model.dto.FieldErrorDto
import com.tmea.wit.model.dto.request.ApiAddRegulatoryAgencyPostDto
import com.tmea.wit.model.dto.request.ApiEditRegulatoryAgencyPostDto
import com.tmea.wit.model.dto.request.ApiStatusRequestDto
import com.tmea.wit.model.dto.response.ApiRegulatoryAgencyResponseDto
import com.tmea.wit.util.CommonDBFunctions
import com.tmea.wit.util.FileUtils
import com.tmea.wit.util.ObjectUtils
import groovy.sql.Sql
import kotlin.reflect.jvm.internal.impl.resolve.constants.LongValue
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service

import javax.sql.DataSource

@Service
class RegulatoryAgencyService {
    DataSource dataSource
    CommonDBFunctions commonDBFunctions
    StorageApi storageApi
    FileUtils fileUtils

    final String PRODUCT_BUCKET_NAME = "regulatory-agency-logo"

    @Autowired
    UserService userService

    @Autowired
    RegulatoryAgencyService(DataSource dataSource, CommonDBFunctions commonDBFunctions, StorageApi storageApi, FileUtils fileUtils) {
        this.dataSource = dataSource
        this.commonDBFunctions = commonDBFunctions
        this.storageApi = storageApi
        this.fileUtils = fileUtils
    }


    BaseApiResponse addNewRegulatoryAgency(ApiAddRegulatoryAgencyPostDto body, String name) {
        Sql sql = Sql.newInstance(dataSource)
        BaseApiResponse response = new BaseApiResponse()
        Long userId = userService.getUserIdFromUsername(name)
        List<FieldErrorDto> errors = new ArrayList<>()
        Map params = ObjectUtils.asMap(body)
        params.put("userId", userId)

        if (userId) {
            def isNameExisting = getAgencyByName(params.get("agencyName") as String)

            if (!isNameExisting) {

                def insertQuery = sql.executeInsert(
                        """INSERT INTO regulatory_agency(code, name, description, country_id, email_address, phone_number, website_url, postal_address, physical_address, is_active, added_by) 
                                                     VALUES (?.code, ?.agencyName, ?.description, ?.countryId, ?.email, ?.phoneNumber, ?.websiteUrl, ?.postalAddress, ?.physicalAddress, true, ?.userId)""", params
                )

                if (insertQuery) {
                    def imageBase64File = params.get("logoUrl")
                    Long agencyId = insertQuery.get(0).get(0) as Long
                    String logoURL = fileUtils.saveFile(name, agencyId, PRODUCT_BUCKET_NAME, imageBase64File, storageApi)

                    sql.executeUpdate(
                            """UPDATE regulatory_agency SET logo_url = ?""", logoURL
                    )

                    def agency = getAgencyById(agencyId)

                    def data = ObjectUtils.snakeCaseMapToPojo(agency, new ApiRegulatoryAgencyResponseDto())

                    response.status = HttpStatus.OK.value()
                    response.data = data
                    response.message = "Regulatory agency added successfully"
                } else {
                    response.status = HttpStatus.BAD_REQUEST.value()
                    response.message = "Failed to add regulatory agency."
                }
            } else {
                response.status = HttpStatus.BAD_REQUEST.value()
                response.message = "A record with the same name exists. Do you wish to update record?"
            }

        } else {
            errors.add(new FieldErrorDto("userId", "Failed to fetch user details"))
            response.data = null
            response.message = "Failed to add regulatory agency"
            response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
            response.errors = errors
        }

        sql.close()
        return response
    }

    BaseApiResponse editAgency(Long agencyId, ApiEditRegulatoryAgencyPostDto body, String name) {
        Sql sql = Sql.newInstance(dataSource)
        BaseApiResponse response = new BaseApiResponse()
        Map editParams = ObjectUtils.asMap(body)
        List<FieldErrorDto> errors = new ArrayList<>()
        Long userId = userService.getUserIdFromUsername(name)

        editParams.put("id", agencyId)

        if (userId) {
            def agencyExist = getAgencyById(agencyId)

            if (agencyExist) {
                String imageBase64File = editParams.get("logoUrl")

                if (!imageBase64File.isEmpty()) {
                    String logoURL = fileUtils.saveFile(name, agencyId, PRODUCT_BUCKET_NAME, imageBase64File, storageApi)
                    editParams.replace("logoUrl", logoURL)
                }

                Map updateParams = ObjectUtils.rowAsMap(agencyExist)
                updateParams = ObjectUtils.mapToSnakeCaseMap(updateParams) as Map
                ObjectUtils.copyMapProperties(editParams, updateParams)

                def update = sql.executeUpdate(
                        """UPDATE regulatory_agency 
                                SET code = ?.code, 
                                name = ?.name, 
                                description = ?.description, 
                                logo_url = ?.logoUrl,
                                email_address = ?.emailAddress,
                                phone_number = ?.phoneNumber,
                                website_url = ?.websiteUrl,
                                postal_address = ?.postalAddress,
                                physical_address = ?.physicalAddress
                                WHERE id = ?.id""", updateParams
                )

                if (update == 1) {
                    def updatedAgency = getAgencyById(agencyId)

                    ApiRegulatoryAgencyResponseDto data = ObjectUtils.snakeCaseMapToPojo(updatedAgency, new ApiRegulatoryAgencyResponseDto()) as ApiRegulatoryAgencyResponseDto

                    response.status = HttpStatus.OK.value()
                    response.data = data
                    response.message = update + " record updated successfully"
                } else {
                    response.status = HttpStatus.BAD_REQUEST.value()
                    response.message = "Failed to edit record"
                }
            } else {
                response.status = HttpStatus.NOT_FOUND.value()
                response.message = "Record not found"
            }

        } else {
            errors.add(new FieldErrorDto("userId", "Failed to fetch user details"))
            response.data = null
            response.message = "Failed to edit regulatory agency"
            response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
            response.errors = errors
        }

        sql.close()
        return response

    }

    GeneralApiListResponse fetchAllRegulatoryAgency(Map parameterMap, boolean isAdmin) {
        Sql sql = new Sql(dataSource)
        GeneralApiListResponse response = new GeneralApiListResponse()
        List<FieldErrorDto> errors = new ArrayList<>()

        def parameterList = ObjectUtils.flattenListParam(parameterMap)
        Map requestParams = [start: 0, limit: 25]

        def status = parameterList.get("status")
        def country = parameterList.get("countryId")
        def query = parameterList.get("query")

        def statusFilter = ""
        if (status) {
            def booleanStatus = CommonDBFunctions.getStatusAsBoolean(status)

            if (booleanStatus != null) {
                requestParams.status = booleanStatus
                statusFilter = " AND regulatory_agency.is_active = ?.status "
            } else {
                errors.add(new FieldErrorDto("status", "Invalid value passed. Check Documentation"))
                response.data = []
                response.total = 0
                response.status = HttpStatus.BAD_REQUEST.value()
                response.message = "Invalid status value"
                return response

            }
        }

        def countryFilter = ""
        if (country) {
            countryFilter = " AND regulatory_agency.country_id = ?.countryId "
            requestParams.put("countryId", country as long)
        }

        def queryFilter = ""
        if (query) {
            countryFilter = ""

            String formattedQuery = "%$query%"
            queryFilter = """ AND (
                       regulatory_agency.name ILIKE ?.query OR
                       regulatory_agency.code ILIKE ?.query OR
                       regulatory_agency.description ?.query
                     )
             """
            requestParams.put("query", formattedQuery)
        }

        if (!isAdmin) {
            requestParams.status = true
            statusFilter = " AND regulatory_agency.is_active = ?.status "
        }

        def selectQuery = """SELECT regulatory_agency.id,
                              regulatory_agency.name,
                              regulatory_agency.code,
                              regulatory_agency.description,
                              country.name            AS country,
                              regulatory_agency.email_address,
                               regulatory_agency.logo_url,
                               regulatory_agency.phone_number,
                               regulatory_agency.website_url,
                               regulatory_agency.postal_address,
                               regulatory_agency.physical_address,
                               regulatory_agency.is_active
                       FROM regulatory_agency,
                            country
                       WHERE regulatory_agency.country_id = country.id """

        def countQuery = """SELECT COUNT(1) 
                        FROM regulatory_agency,
                            country
                        WHERE regulatory_agency.country_id = country.id """

        requestParams = commonDBFunctions.paginationSqlParams(requestParams, parameterList)

        String limitClause = " limit ?.limit offset ?.start"

        String filterQuery = countryFilter + queryFilter + statusFilter + limitClause

        def filteredSelectQuery = selectQuery + filterQuery
        def count = countQuery + filterQuery

        def data = sql.rows(filteredSelectQuery, requestParams);
        def total;

        if (filterQuery) {
            total = sql.firstRow(count, requestParams).get("count")
        } else {
            total = sql.firstRow(count).get("count")
        }

        if (data) {
            List<ApiRegulatoryAgencyResponseDto> regulatoryAgencyResponseDto = ObjectUtils.snakeCaseArrayMapToListPojos(data, ApiRegulatoryAgencyResponseDto)

            response.status = HttpStatus.OK.value()
            response.data = regulatoryAgencyResponseDto
            response.message = "Success"
            response.total = total
        } else {
            response.message = "No records found"
        }

        sql.close()
        return response
    }


    BaseApiResponse updateAgencyStatus(Long agencyId, ApiStatusRequestDto body, String name) {
        Sql sql = Sql.newInstance(dataSource)
        BaseApiResponse response = new BaseApiResponse()
        Map params = ObjectUtils.asMap(body)
        List<FieldErrorDto> errors = new ArrayList<>()

        Long userId = userService.getUserIdFromUsername(name)
        def statusMsg
        if (userId) {
            def recordExist = getAgencyById(agencyId)

            if (recordExist) {
                def status = CommonDBFunctions.getStatusAsBoolean(params.get("status"))

                params.replace("status", status)
                params.put("id", agencyId)

                def affectedRow = sql.executeUpdate(
                        """UPDATE regulatory_agency SET is_active = ?.status WHERE id = ?.id""", params
                )

                statusMsg = status ? "activated" : "deactivated"
                if (affectedRow == 1) {
                    def agency = sql.firstRow(
                            """SELECT id, name, description, code, is_active FROM regulatory_agency WHERE id = ?""", agencyId
                    )

                    if (agency) {
                        def data = ObjectUtils.mapToSnakeCaseMap(agency)

                        response.status = HttpStatus.OK.value()
                        response.data = data
                        response.message = "Record " + statusMsg + " successfully"
                    }
                } else {
                    response.message = "Failed to edit record status"
                    response.status = HttpStatus.BAD_REQUEST.value()
                }
            } else {
                response.status = HttpStatus.BAD_REQUEST.value()
                response.message = "Record not found"
            }

        } else {
            errors.add(new FieldErrorDto("userId", "Failed to fetch user details"))
            response.data = null
            response.message = "Failed to " + statusMsg + " regulatory agency"
            response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
            response.errors = errors
        }

        sql.close()
        return response

    }

    Map getAgencyByName(String name) {
        Sql sql = Sql.newInstance(dataSource)

        def agency = sql.firstRow(
                """SELECT *
                       FROM regulatory_agency
                       WHERE name ilike ?""", name
        )

        sql.close()
        return agency
    }

    Map getAgencyById(Long id) {
        Sql sql = Sql.newInstance(dataSource)

        def agency = sql.firstRow(
                """SELECT regulatory_agency.id,
                              regulatory_agency.name,
                              regulatory_agency.code,
                              regulatory_agency.description,
                              country.name            AS country,
                              regulatory_agency.email_address,
                               regulatory_agency.logo_url,
                               regulatory_agency.phone_number,
                               regulatory_agency.website_url,
                               regulatory_agency.postal_address,
                               regulatory_agency.physical_address,
                               regulatory_agency.is_active
                       FROM regulatory_agency,
                            country
                       WHERE regulatory_agency.country_id = country.id
                         AND regulatory_agency.id = ?""", id
        )

        sql.close()
        return agency
    }
}
