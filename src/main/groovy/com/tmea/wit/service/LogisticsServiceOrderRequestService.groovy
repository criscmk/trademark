package com.tmea.wit.service

import com.tmea.wit.model.BaseApiResponse
import com.tmea.wit.model.GeneralApiListResponse
import com.tmea.wit.model.dto.FieldErrorDto
import com.tmea.wit.model.dto.request.ApiLogisticsServiceOrderRequestDto
import com.tmea.wit.model.dto.response.ApiFetchLogisticsServiceOrderResponseDto
import com.tmea.wit.model.dto.response.ApiLocationResponseDto
import com.tmea.wit.model.dto.response.ApiFetchAllLogisticsServiceOrderRequestResponseDto
import com.tmea.wit.model.dto.response.ApiLogisticsVehiclesResponseDto
import com.tmea.wit.util.CommonDBFunctions
import com.tmea.wit.util.ObjectUtils
import groovy.sql.Sql
import org.hashids.Hashids
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service

import javax.sql.DataSource

@Service
class LogisticsServiceOrderRequestService {
    @Value('${hashids.ordernumber.salt}')
    private String orderNumberHashIdSalt;
    @Value('${wit.ordernumber.seperator}')
    private String OrderNumberSeparator;
    @Autowired
    DataSource dataSource
    @Autowired
    UserService userService
    @Autowired
    GlobalConfigService globalConfigService
    CommonDBFunctions commonDBFunctions

    LogisticsServiceOrderRequestService(DataSource dataSource, CommonDBFunctions commonDBFunctions) {
        this.dataSource = dataSource
        this.commonDBFunctions = commonDBFunctions
    }

    public BaseApiResponse addLogisticsServiceOrderRequest(def body, String username) {
        Map requestParams = ObjectUtils.asMap(body)
        BaseApiResponse response = new BaseApiResponse()
        Sql sql = Sql.newInstance(dataSource)
        List<FieldErrorDto> errors = []

        Long userId = userService.getUserIdFromUsername(username)

        def serviceExists = sql.firstRow("""SELECT * FROM logistics_service WHERE is_active=TRUE AND id = ?.logisticsService""", requestParams)

        if (serviceExists) {
            //get the active service tariff
            def activeServiceTariff = currentActiveTariff(requestParams.get("logisticsService"))
            if (activeServiceTariff) {
                //get status id
                def status = "pending"
                Long tariffId = activeServiceTariff.get("id")
                def statusData = getApprovalStatusByName(status)
                //check if market order exist
                if (requestParams.marketOrderId) {
                    def marketOrderExist = sql.firstRow("""SELECT id FROM market_order WHERE  id =?.marketOrderId AND is_active""", requestParams)
                    if (!marketOrderExist) {
                        response.data = ObjectUtils.mapToSnakeCaseMap(requestParams)
                        response.message = "Market order " + requestParams.marketOrderId + " does not exist"
                        response.status = HttpStatus.BAD_REQUEST.value()
                        return response
                    }
                }
                if (statusData) {
                    requestParams.put("tariffId", tariffId)
                    requestParams.put("approvalStatusId", statusData.get("id"))
                    requestParams.put("userId", userId)

                    sql.withTransaction {
                        def query = """
                               INSERT INTO logistics_service_order_request
                                (logistics_service_id,market_order_id,description,ordered_by,quantity,logistics_service_tariff_id,price,approval_status_id,date_requested)
                                VALUES (?.logisticsService, ?.marketOrderId,?.description, ?.userId, ?.quantity, ?.tariffId, ?.price, ?.approvalStatusId ,current_timestamp) 
                            """
                        def insertLogisticsServiceOrderRequest = sql.executeInsert query, requestParams
                        if (insertLogisticsServiceOrderRequest) {
                            Long orderRequestId = insertLogisticsServiceOrderRequest?.get(0)?.get(0)
                            requestParams.put("orderRequestId", orderRequestId)
                            //update the logistics service order request price updates
                            def insertPrice = sql.executeInsert(""" Insert Into logistics_service_order_request_price_update
                                                                       (logistics_service_order_request_id, updated_price, is_active, date_updated, updated_by)
                                                                       VALUES (?.orderRequestId, ?.price,TRUE, CURRENT_TIMESTAMP, ?.userId)""", requestParams)

                            //def insertedRecord = getOrderRequestById(orderRequestId)
                            def insertedRecord = sql.firstRow("""SELECT  * FROM logistics_service_order_request WHERE  id = ? """, orderRequestId)

                            response.status = HttpStatus.OK.value()
                            response.setData(ObjectUtils.mapToSnakeCaseMap(insertedRecord));
                            response.setMessage("Request added successfully")
                        } else {
                            response.setData(requestParams)
                            response.setMessage("Failed to add request")
                            response.setMessage(HttpStatus.INTERNAL_SERVER_ERROR.value())
                        }
                    }

                } else {
                    response.data = ObjectUtils.mapToSnakeCaseMap(requestParams)
                    response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value())
                    response.setMessage(' Status ' + status + ' doesnt exists')
                }


            } else {
                response.data = ObjectUtils.mapToSnakeCaseMap(requestParams)
                response.setStatus(HttpStatus.BAD_REQUEST.value())
                response.setMessage('No Active tariff found for  service Id ' + requestParams.get('logisticsService'))
            }
        } else {
            response.data = ObjectUtils.mapToSnakeCaseMap(requestParams)
            response.setStatus(HttpStatus.BAD_REQUEST.value())
            response.setMessage('service id ' + requestParams.get('logisticsService') + ' does not exists')
        }
        sql.close()
        response
    }

    public GeneralApiListResponse getLogisticsServiceOrderRequest(Map parameterMap, String username) {
        Sql sql = new Sql(dataSource)
        GeneralApiListResponse response = new GeneralApiListResponse();

        List<FieldErrorDto> errors = []
        def params = ObjectUtils.flattenListParam(parameterMap)
        def page = params.get("page")
        def size = params.get("size")
        def providerId = params.get("providerId")
        def queryParams = params
        def filterStatusQuery = ""
        def counterStatusQuery = ""
        def loggedInProviderFilter = ""
        def providerIdFilter = ""
        def query = params.query
        def searchFilter = ""
        def orderedByFilter = ""
        def logisticsServiceFilter = ""
        //filter by logged in user
        if (username) {
            Long userId = userService.getUserIdFromUsername(username)
            queryParams.put("userId", userId)
            loggedInProviderFilter = " AND logistics_service.provider_user_id = ?.userId::BigInt"
        }
        //Admin filter by specific provider
        if (!username && params.providerId) {
            providerIdFilter = " AND logistics_service.provider_user_id = ?.providerId::BigInt"
        }
        // filter orderedBy
        if (params.orderedById) {
            orderedByFilter = " AND logistics_service_order_request.ordered_by = ?.orderedById::BigInt"
        }
        //filter by logistics service
        if (params.logisticsServiceId) {
            logisticsServiceFilter = " AND logistics_service_order_request.logistics_service_id = ?.logisticsServiceId::Bigint"
        }
        //search by name / description
        if (query) {
            String querySearch = "%$query%"
            queryParams.put("query", querySearch)
            searchFilter = """ AND (
                                    logistics_service.name ILIKE ?.query OR
                                    logistics_service.description ILIKE ?.query 
                                   
                                  )
                          """
        }

        if (params.status) {
            String status = params.status.toLowerCase()

            if (status == 'pending' || status == 'approved' || status == 'rejected') {
                def filterStatus = getApprovalStatusByName(status).get("id")
                if (filterStatus) {
                    queryParams.put("statusId", filterStatus)
                    filterStatusQuery = " AND logistics_service_order_request.approval_status_id = ?.statusId "

                }
            } else {
                response.setMessage("Invalid status value")
                response.setData(null)
                response.setStatus(HttpStatus.BAD_REQUEST.value())
                sql.close()
                return response
            }
        }
        //pagination
        Map paginateParams = [start: 0, limit: 25]
        def paginate = commonDBFunctions.paginationSqlParams(paginateParams, params)
        def paginationFilter = " LIMIT ?.limit OFFSET ?.start"
        if (page && size) {
            queryParams = queryParams + paginate

        }
        def queryFilter = filterStatusQuery + loggedInProviderFilter + providerIdFilter + searchFilter + orderedByFilter + logisticsServiceFilter
        def getAllLogisticsOrdersRequestQuery = getLogisticsOrderRequest() + queryFilter + " ORDER BY id DESC" + paginationFilter
        def allLogisticsOrderService = sql.rows(getAllLogisticsOrdersRequestQuery, queryParams)
        String countQuery = """SELECT count(*) 
                                 FROM
                                   logistics_service_order_request,
                                   logistics_service,
                                   approval_status,
                                   user_individual_detail,
                                   "user"
                                   WHERE 
                                   logistics_service_order_request.logistics_service_id  = logistics_service.id
                                   AND
                                   logistics_service_order_request.approval_status_id = approval_status.id
                                   AND
                                   logistics_service_order_request.ordered_by = "user".id
                                   AND
                                   user_individual_detail.user_id = "user".id
                                    """

        def total = 0

        if (queryFilter) {
            total = sql.firstRow(countQuery + queryFilter, queryParams)?.get('count')

        } else {
            total = sql.firstRow(countQuery)?.get('count')
        }

        if (allLogisticsOrderService) {
            response.setData(ObjectUtils.snakeCaseArrayMapToListPojos(allLogisticsOrderService, ApiFetchAllLogisticsServiceOrderRequestResponseDto))
            response.setMessage("records found")
            response.setTotal(total)
            response.setStatus(HttpStatus.OK.value())
        } else {
            response.setData(null)
            response.setTotal(total)
            response.setMessage("No records found")
            response.setStatus(HttpStatus.NO_CONTENT.value())
        }
        sql.close()
        response
    }

    public BaseApiResponse getLogisticsServiceOrderRequestById(Long requestId, String username) {
        Sql sql = new Sql(dataSource)
        BaseApiResponse response = new BaseApiResponse(null, 400, "")
        List<FieldErrorDto> errors = []
        Long userId = userService.getUserIdFromUsername(username)
        def queryParams = [:]
        queryParams.put("userId", userId)
        queryParams.put("requestId", requestId)
        println(queryParams)
        if (requestId) {
            def query = " AND logistics_service_order_request.id = ?.requestId "
            def getSpecificLogisticsOrdersRequestQuery = getLogisticsOrderRequest() + query
            def allLogisticsOrderService = sql.firstRow(getSpecificLogisticsOrdersRequestQuery, queryParams)
            if (allLogisticsOrderService) {
                def requestResponse = ObjectUtils.snakeCaseMapToPojo(allLogisticsOrderService, new ApiFetchAllLogisticsServiceOrderRequestResponseDto())
                response.setStatus(HttpStatus.OK.value())
                response.setData(requestResponse)
                println("data" + allLogisticsOrderService)
                response.setMessage("Record found")
            } else {
                response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value())
                response.setData(null)
                response.setMessage("No Record found")
            }

        } else {
            response.setMessage("Request Id is required")
            response.setStatus(HttpStatus.BAD_REQUEST.value())

        }
        sql.close()
        response

    }

    public BaseApiResponse approveLogisticsServiceOrderRequest(def body, Long logisticsServiceOrderId, String userName) {
        BaseApiResponse response = new BaseApiResponse(null, 200, "")
        Sql sql = new Sql(dataSource)
        def params = ObjectUtils.asMap(body)
        Long userId = userService.getUserIdFromUsername(userName)
        def queryParams = [:];
        def status = params.approval.toLowerCase()
        def approvalStatus = getApprovalStatusByName(params.approval)

        queryParams.put("userId", userId)
        queryParams.put("comment", params.get("comment"))
        queryParams.put("logisticsServiceOrderId", logisticsServiceOrderId)

        def logisticsServiceOrderExists = getOrderRequestById(logisticsServiceOrderId)
        if (userId) {

            if (logisticsServiceOrderExists) {
                //check if approval status exists
                if (approvalStatus) {
                    def statusId = approvalStatus.get("id")
                    queryParams.put("status", statusId)
                    //check if the logistics service order request has been approved
                    def logisticsServiceRequestApprovalStatusId = logisticsServiceOrderExists.get("approval_status_id")
                    def existingStatus = getApprovalStatusById(logisticsServiceRequestApprovalStatusId).get("code")
                    existingStatus ? existingStatus.toLowerCase() : existingStatus
                    if (existingStatus == "pending" || existingStatus == "rejected") {
                        String prefix = globalConfigService.getConfigValue("logistics_order_number_prefix")
                        if(prefix){
                        sql.withTransaction {
                            def approve = sql.executeUpdate("""UPDATE logistics_service_order_request set approval_status_id = ?.status WHERE id=?.logisticsServiceOrderId""", queryParams)
                            def updatedDetails = getOrderRequestById(logisticsServiceOrderId)
                            println("updated details" + updatedDetails)
                            if (approve) {
                                def approvedOrderRequestPrice = logisticsServiceOrderExists.get("price")
                                queryParams.put("approvedOrderRequestPrice", approvedOrderRequestPrice)
                                //insert approval  logistics service order
                                def logisticsServiceOrderApproval = sql.executeInsert("""
                               INSERT INTO logistics_service_order_request_approval
                                (logistics_service_order_request_id,approved_by,comment,date_approved)
                                VALUES (?.logisticsServiceOrderId, ?.userId,?.comment,current_timestamp) 
                            """, queryParams)

                                if (logisticsServiceOrderApproval) {
                                    if (status == "approved") {

                                        //create logistics service order
                                        def getInitialLogisticsOrderStatus = sql.firstRow("""SELECT id FROM logistics_service_order_status WHERE name ILIKE ? """, "pending")
                                        if (getInitialLogisticsOrderStatus) {
                                            def initialStatus = getInitialLogisticsOrderStatus.get("id")
                                            queryParams.put("initialStatus", initialStatus)
                                        } else {

                                            response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
                                            response.data = params
                                            response.message = "Logistics service order status is not configured"
                                            return response
                                        }

                                        def logisticsServiceOrder = sql.executeInsert("""
                               INSERT INTO logistics_service_order
                                (logistics_service_order_request_id,is_payment_complete,price,logistics_service_order_status_id,is_active,date_added)
                                VALUES (?.logisticsServiceOrderId, false, ?.initialStatus, ?.status, true, current_timestamp) 
                            """, queryParams)
                                        if (logisticsServiceOrder) {
                                            //update order number
                                            def orderId = logisticsServiceOrder.get(0).get(0)
                                             prefix = prefix + OrderNumberSeparator

                                            Hashids hashids = new Hashids(orderNumberHashIdSalt, 6)
                                            String orderNumber = prefix + hashids.encode(orderId).toUpperCase();
                                            queryParams.put("orderId", orderId)
                                            queryParams.put("orderNumber", orderNumber)
                                            def updateOrderId = sql.executeUpdate("""UPDATE logistics_service_order set order_number = ?.orderNumber WHERE id= ?.orderId""", queryParams)

                                            response.status = HttpStatus.OK.value()
                                            response.setData(updatedDetails);
                                            response.setMessage("Request approved successfully")
                                        } else {
                                            response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
                                            response.setData(null);
                                            response.setMessage("Request has not been  approved ")
                                        }
                                    } else {
                                        response.status = HttpStatus.OK.value()
                                        response.setData(updatedDetails);
                                        response.setMessage("Request has been  " + params.approval)
                                    }

                                } else {
                                    response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
                                    response.setData(null);
                                    response.setMessage("Request could not be approved")
                                }
                            } else {
                                response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
                                response.setData(null);
                                response.setMessage("Request failed to be " + params.approval)
                            }
                        }
                        }else {

                            response.message = "logistics order number prefix is not configured"
                            response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()


                          }


                    } else {
                        response.status = HttpStatus.BAD_REQUEST.value()
                        response.setData(ObjectUtils.mapToSnakeCaseMap(logisticsServiceOrderExists));
                        response.setMessage("Request has already been " + existingStatus)
                        return response
                    }
                } else {
                    response.message = "approval status " + params.approval + " not exists"
                    response.data = ObjectUtils.mapToSnakeCaseMap(logisticsServiceOrderExists)
                    response.status = HttpStatus.BAD_REQUEST.value()
                    return response

                }
            } else {
                response.setMessage("Logistics Order Service id " + logisticsServiceOrderId + " Does not exists")
                response.setData(params)
                response.setStatus(HttpStatus.BAD_REQUEST.value())
                return response
            }
        } else {
            response.message = "user does not exists"
            response.data = params
            response.status = HttpStatus.BAD_REQUEST.value()
        }
        sql.close()
        return response

    }

    public BaseApiResponse updateLogisticsServiceOrderPrice(def body, Long logisticsServiceOrderRequestId, String userName) {
        BaseApiResponse response = new BaseApiResponse(null, 200, "")
        Sql sql = new Sql(dataSource)
        def requestParams = ObjectUtils.asMap(body)
        Long userId = userService.getUserIdFromUsername(userName)

        requestParams.put("logisticsServiceOrderRequestId", logisticsServiceOrderRequestId)
        requestParams.put("userId", userId)

        //check if order exists
        def orderExists = sql.firstRow("""SELECT * FROM logistics_service_order_request,approval_status
                                              WHERE logistics_service_order_request.approval_status_id = approval_status.id
                                              AND logistics_service_order_request.id =?""",logisticsServiceOrderRequestId);


        if (!orderExists) {
            response.setMessage("Order id " + logisticsServiceOrderRequestId + " does not exists")
            return response
        } else {

            //update new price
            //check if order request is approved
            if (orderExists.get("code") == "pending") {
                def updateOrderRequestPrice = sql.executeUpdate(""" UPDATE logistics_service_order_request SET price =?.price  WHERE id = ?.logisticsServiceOrderRequestId""", requestParams)
                def deActivatePrice = sql.executeUpdate(""" UPDATE  logistics_service_order_request_price_update SET is_active = FALSE WHERE logistics_service_order_request_id=?.logisticsServiceOrderRequestId""", requestParams)
                def insertNewPrice = logNewPrices(requestParams)
                def updatedRecord = sql.firstRow("""SELECT * FROM logistics_service_order_request WHERE id =?""", logisticsServiceOrderRequestId)

                if (updateOrderRequestPrice && deActivatePrice && insertNewPrice) {
                    response.data = updatedRecord
                    response.setMessage(("Order request price updated successfully"))
                } else {
                    response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value())
                    response.setMessage("Order request price could not be updated")
                }

            }else
            {
                response.data = ObjectUtils.mapToSnakeCaseMap(orderExists)
                response.message = "Logistics service order request has already been " + orderExists.get("name")
                response.status = HttpStatus.BAD_REQUEST.value()
            }
        }
        sql.close()
        response
    }

    public def currentActiveTariff(Long serviceId) {
        Sql sql = new Sql(dataSource)
        def tarrif = sql.firstRow "SELECT * FROM logistics_service_tariff WHERE is_active=TRUE AND id = ?", serviceId
        sql.close()
        tarrif
    }

    public def getApprovalStatusByName(String statusName) {
        Sql sql = new Sql(dataSource)
        def status = sql.firstRow "SELECT * FROM approval_status WHERE is_active=TRUE AND code ILIKE ?", statusName
        sql.close()
        status
    }

    public def getApprovalStatusById(Long statusId) {
        Sql sql = new Sql(dataSource)
        def vehicle = sql.firstRow "SELECT * FROM approval_status WHERE is_active=TRUE AND id = ?", statusId
        sql.close()
        vehicle
    }

    public def getOrderRequestById(Long orderRequestId) {
        Sql sql = new Sql(dataSource)
        def insertedRequestRecord = sql.firstRow("""SELECT  * FROM logistics_service_order_request WHERE  id = ? """, orderRequestId)
        sql.close()
        insertedRequestRecord
    }

    public def logNewPrices(Map requestParams) {
        Sql sql = new Sql(dataSource)
        def logPrice = sql.executeInsert(""" Insert Into logistics_service_order_request_price_update
                                           (logistics_service_order_request_id, updated_price, is_active, date_updated, updated_by)
                                            VALUES (?.logisticsServiceOrderRequestId, ?.price,TRUE, CURRENT_TIMESTAMP, ?.userId)""", requestParams)
        sql.close()
        logPrice
    }

    public String getLogisticsOrderRequest() {
        def getAllLogisticsOrdersRequestQuery = """SELECT 
                   logistics_service_order_request.id,
                   logistics_service_order_request.quantity,
                   logistics_service_order_request.price,
                   logistics_service_order_request.market_order_id,
                   logistics_service_order_request.date_requested,
                   logistics_service.name AS logistics_service_name,
                   approval_status.name AS status,
                   "user".phone_number AS requested_by_phone,
                    "user".email AS requested_by_email,
                   
                    CASE
                    WHEN "user".user_type_id = 1 THEN (SELECT user_individual_detail.first_name||' '||user_individual_detail.middle_name||' '||user_individual_detail.last_name  FROM user_individual_detail WHERE "user".id = user_individual_detail.user_id)
                    WHEN "user".user_type_id = 2 THEN (SELECT user_organization_detail.organization_name FROM user_organization_detail WHERE "user".id = user_organization_detail.user_id)
                    END AS requested_by_name
                    FROM
                   logistics_service_order_request,
                   logistics_service,
                   approval_status,
                
                   "user"
                   WHERE 
                   logistics_service_order_request.logistics_service_id  = logistics_service.id
                   AND
                   logistics_service_order_request.approval_status_id = approval_status.id
                   AND
                  logistics_service_order_request.ordered_by = "user".id
                    
                    """
    }
}
