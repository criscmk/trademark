package com.tmea.wit.service

import com.tmea.wit.model.BaseApiResponse
import com.tmea.wit.model.dto.FieldErrorDto
import com.tmea.wit.repository.UserRepository
import com.tmea.wit.util.CommonDBFunctions
import com.tmea.wit.util.ObjectUtils
import groovy.sql.Sql
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service

import javax.sql.DataSource

@Service
class RegulationRegulatoryAgencyService {
    DataSource dataSource
    UserRepository userRepository
    CommonDBFunctions commonDBFunctions

    @Autowired
    RegulationRegulatoryAgencyService(DataSource dataSource, UserRepository userRepository,
                                      CommonDBFunctions commonDBFunctions) {
        this.dataSource = dataSource
        this.userRepository = userRepository
        this.commonDBFunctions = commonDBFunctions
    }

    public BaseApiResponse addRegulationRegulatoryAgency(def body, String userName) {
        Sql sql = new Sql(dataSource)
        BaseApiResponse response = new BaseApiResponse([], 400, "")
        List<FieldErrorDto> errors = []
        Map requestParams = ObjectUtils.asMap(body)
        Long addedBy = userRepository.findUserIdByDetails(userName)
        //check if added by exist
        if (addedBy) {
            //check if agency exist
            def isRegulatoryAgencyExist = commonDBFunctions.getDBRecordById(requestParams.regulatoryAgency, "regulatory_agency", "id")
            if (isRegulatoryAgencyExist) {
                def isRegulationExist = commonDBFunctions.getDBRecordByIdAndStatus(requestParams.regulation, "regulation", "id")
                if (isRegulationExist) {
                    //check if record exist
                    def isRecordExist = sql.firstRow("""SELECT * FROM regulation_regulatory_agency WHERE regulation_id =?.regulation AND regulatory_agency_id=?.regulatoryAgency""",requestParams)
                    if(!isRecordExist){
                        def insertRecord = sql.executeInsert("""INSERT INTO regulation_regulatory_agency(regulatory_agency_id, regulation_id, is_active ) 
                                                               VALUES (?.regulatoryAgency`,?.regulation,TRUE)""",requestParams)

                        if(insertRecord)
                        {
                            def insertedRecord = sql.firstRow("""SELECT * FROM regulation_regulatory_agency WHERE id =?""",insertRecord.get(0).get(0))
                            response.message = "record inserted successfully"
                            response.status = HttpStatus.OK.value()
                            response.data = ObjectUtils.mapToSnakeCaseMap(insertedRecord)

                        }else
                        {
                            response.message = "an error occurred while inserting record"
                            response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
                        }

                    }else{
                        def recordStatus = isRecordExist.is_active
                        def responseMessage=recordStatus?"regulation regulatory agency already exist":"regulation regulatory agency already exist but is deactivated.activate the record"
                        response.data = ObjectUtils.mapToSnakeCaseMap(isRecordExist)
                        response.message = responseMessage
                    }



                } else {
                    response.message = "regulation id " + "'$requestParams.regulation'" + " does not exist"
                }
            } else {
                response.message = "regulation agency id " + "'$requestParams.agency'" + " does not exist"
            }
        } else {

            response.message = "user does not exist"

        }
        sql.close()
        return  response

    }

}
