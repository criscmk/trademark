package com.tmea.wit.service

import com.tmea.wit.model.BaseApiResponse
import com.tmea.wit.model.dto.FieldErrorDto
import com.tmea.wit.repository.UserRepository
import com.tmea.wit.util.CommonDBFunctions
import com.tmea.wit.util.ObjectUtils
import groovy.sql.Sql
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service

import javax.sql.DataSource
@Service
class RegulationProcedureRequirementTypeService {
    DataSource dataSource
    UserRepository userRepository
    CommonDBFunctions commonDBFunctions
    @Autowired
    RegulationProcedureRequirementTypeService(DataSource dataSource, UserRepository userRepository,
                                              CommonDBFunctions commonDBFunctions) {
        this.dataSource = dataSource
        this.userRepository = userRepository
        this.commonDBFunctions = commonDBFunctions
    }
    public BaseApiResponse addRegulationProcedureRequirementType(def body, String userName) {
        Sql sql = new Sql(dataSource)
        BaseApiResponse response = new BaseApiResponse([], 400, "")
        List<FieldErrorDto> errors = []
        Map requestParams = ObjectUtils.asMap(body)
        Long addedBy = userRepository.findUserIdByDetails(userName);
        //check if added by exist
        if (addedBy) {

            //check if RegulatoryProcedureRequirementType exist
            def isRegulatoryProcedureRequirementTypeExist = commonDBFunctions.getDBRecordByTableColumn(requestParams.name,"regulation_procedure_requirement_type","name")
            if(!isRegulatoryProcedureRequirementTypeExist)
            {
                def insertRecord = sql.executeInsert("""INSERT INTO regulation_procedure_requirement_type ( name, description, is_active) 
                                                            VALUES (?.name,?.description,TRUE)""",requestParams)
                if(insertRecord)
                {
                    def insertedRecord = sql.firstRow("""SELECT * FROM regulation_procedure_requirement_type WHERE id =?""",insertRecord.get(0).get(0))
                    response.message = "record inserted successfully"
                    response.status = HttpStatus.OK.value()
                    response.data = ObjectUtils.mapToSnakeCaseMap(insertedRecord)

                }else
                {
                    response.message = "an error occurred while inserting record"
                    response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
                }
            }else
            {

                def recordStatus = isRegulatoryProcedureRequirementTypeExist.is_active
                def message = recordStatus?"regulation procedure requirement type " +"'$requestParams.name'" + " exist":"regulation procedure requirement type " +"'$requestParams.name'" + " exist but is de activated"
                response.message = message
                response.data = ObjectUtils.mapToSnakeCaseMap(isRegulatoryProcedureRequirementTypeExist)
            }

        }else
        {
            response.message = "user does not exist"
        }
        sql.close()
        return  response
        }
    public BaseApiResponse editRegulationProcedureRequirementType(def body, Id, String username) {
        Sql sql = Sql.newInstance(dataSource)
        BaseApiResponse response = new BaseApiResponse([], 400, "")
        Map requestParams = ObjectUtils.asMap(body)
        Long userId = userRepository.findUserIdByDetails(username);
        List<FieldErrorDto> errorDto = new ArrayList<>()

        if (userId) {
            if (requestParams) {
                def isRegulationProcedureRequirementTypeExist = commonDBFunctions.getDBRecordByIdAndStatus(Id, "regulation_procedure_requirement_type", "id")
                if (isRegulationProcedureRequirementTypeExist) {
                    requestParams.Id =Id
                    def updateRecord = sql.executeUpdate(""" UPDATE regulation_procedure_requirement_type 
                                                                 SET name = ?.name, description =?.description
                                                                 WHERE id=?.Id
                                                                 """, requestParams)
                    if (updateRecord) {
                        def updatedRecord = sql.firstRow("""SELECT * FROM regulation_procedure_requirement_type WHERE id =?""", Id)
                        response.message = 'regulation procedure requirement type updated successfully'
                        response.status = HttpStatus.OK.value()
                        response.data = ObjectUtils.mapToSnakeCaseMap(updatedRecord)

                    }else {
                        response.message = " failed to update regulation procedure requirement type"
                        response.data = requestParams
                        response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
                    }

                }
                else {
                    response.message = "regulation procedure requirement type id("+Id+") does not exist"
                }


            }else{
                response.message = "cannot update empty payload"
            }

        }else{

            response.message = 'could not update record,user does not exist'
        }
        sql.close()
        response
    }
    public BaseApiResponse updateRegulationProcedureRequirementTypeStatus(long Id, def body,String userName) {
        Sql sql = new Sql(dataSource)
        BaseApiResponse response = new BaseApiResponse([], 400, "")
        def params = ObjectUtils.asMap(body)
        Long userId = userRepository.findUserIdByDetails(userName);
        params.id = Id
        def newStatus=""
        if (params.status.toLowerCase() == 'active') {
            newStatus = true;
        }
        if (params.status.toLowerCase() == 'inactive') {
            newStatus = false;
        }
        params.status = newStatus
        def statusMessage = params.status ? 'activated' : 'deactivated'
        if(userId) {
            def isRecordExist = commonDBFunctions.getDBRecordById(Id, 'regulation_procedure_requirement_type', 'id')
            if (isRecordExist) {
                def updateStatus = sql.executeUpdate("""UPDATE regulation_procedure_requirement_type SET is_active =?.status WHERE id=?.id""", params)
                if (updateStatus) {
                    def updatedRecord = sql.firstRow("""SELECT * FROM regulation_procedure_requirement_type WHERE id=?""", Id)
                    response.message = "regulation procedure requirement type (" + statusMessage + ")  successfully"
                    response.status = HttpStatus.OK.value()
                    response.data = ObjectUtils.mapToSnakeCaseMap(updatedRecord)
                } else {
                    response.message = "an error occurred while updating record regulation procedure requirement type "
                    response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
                }


            } else {
                response.message = "regulation procedure requirement type  id (" + Id + ")does not exist"

            }
        }else{
            response.message ="could not update record status, user does not exist"
        }
        sql.close()
        response

    }


}
