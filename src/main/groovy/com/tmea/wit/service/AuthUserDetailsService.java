package com.tmea.wit.service;

import com.tmea.wit.auth.UserPrincipal;
import com.tmea.wit.entity.PermissionEntity;
import com.tmea.wit.entity.UserEntity;
import com.tmea.wit.repository.PermissionRepository;
import com.tmea.wit.repository.UserRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class AuthUserDetailsService implements UserDetailsService {

    private final UserRepository userRepository;
    private final PermissionRepository userRolePermissionRepository;

    public AuthUserDetailsService(UserRepository userRepository, PermissionRepository userRolePermissionRepository){
        super();
        this.userRepository = userRepository;
        this.userRolePermissionRepository = userRolePermissionRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserEntity user = this.userRepository.findUserByDetails(username);

        if(null==user){
            throw new UsernameNotFoundException("cannot find username: " + username);
        }
        List<PermissionEntity> authGroups = this.userRolePermissionRepository.findByRoleByUsername(username);
        return new UserPrincipal(user, authGroups);
    }
}
