package com.tmea.wit.service

import com.tmea.wit.model.BaseApiResponse
import com.tmea.wit.model.GeneralApiListResponse
import com.tmea.wit.model.dto.FieldErrorDto
import com.tmea.wit.model.dto.response.ApiFetchMarketOrderResponseDto
import com.tmea.wit.repository.UserRepository
import com.tmea.wit.util.CommonDBFunctions
import com.tmea.wit.util.ObjectUtils
import groovy.sql.Sql
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service

import javax.sql.DataSource

@Service
class MarketOrderService {
    @Autowired
    DataSource dataSource
    @Autowired
    UserService userService
    @Autowired
    GlobalConfigService globalConfigService
    CommonDBFunctions commonDBFunctions
    UserRepository userRepository

    MarketOrderService(DataSource dataSource, UserRepository userRepository, CommonDBFunctions commonDBFunctions) {
        this.dataSource = dataSource
        this.commonDBFunctions = commonDBFunctions
        this.userRepository = userRepository
    }

    public BaseApiResponse cancelMarketOrder(def body, Long marketOrderId) {
        BaseApiResponse response = new BaseApiResponse(null, 400, "", [])
        List<FieldErrorDto> errors = []
        Sql sql = new Sql(dataSource)
        def queryParams = ObjectUtils.asMap(body)
        Authentication auth = SecurityContextHolder.getContext().getAuthentication()
        def cancelledBy = userRepository.findUserIdByDetails(auth.getName())
        queryParams.put("cancelledBy", cancelledBy)
        queryParams.put("marketOrderId", marketOrderId)
        def cancelledOrderStatusId = commonDBFunctions.getDBRecordByNameAndStatus("cancelled", "market_order_status", "name").get("id")
        if (marketOrderExist(marketOrderId)) {
            def isMarketOrderCancelled = commonDBFunctions.getDBRecordById(marketOrderId, "market_order_cancellation", "market_order_id")
            if (!isMarketOrderCancelled) {
                if (cancelledOrderStatusId) {
                    queryParams.put("cancelledOrderStatusId", cancelledOrderStatusId)
                    def updateMarketOrder
                    def logCancelledOrder
                    sql.withTransaction {
                        updateMarketOrder = sql.executeUpdate("""UPDATE market_order SET market_order_status_id = ?.cancelledOrderStatusId,is_active = FALSE WHERE  id =?.marketOrderId""", queryParams)
                        logCancelledOrder = sql.executeInsert("""INSERT INTO market_order_cancellation 
                                                                  (market_order_id, cancelled_by, reason_for_cancellation, date_cancelled) 
                                                                  VALUES (?.marketOrderId, ?.cancelledBy, ?.reason,CURRENT_TIMESTAMP)""", queryParams)

                    }
                    if (updateMarketOrder && logCancelledOrder) {
                        def cancelledOrder = commonDBFunctions.getDBRecordById(marketOrderId, "market_order", "id")
                        response.message = "Order has been cancelled successfully"
                        response.status = HttpStatus.OK.value()
                        response.data = cancelledOrder
                    }
                } else {
                    response.message = "Cancelled status does not exist"
                    response.status = HttpStatus.BAD_REQUEST.value()
                }
            } else {
                response.message = "Order Have already been cancelled"
            }
        } else {
            response.message = "market order with id " + marketOrderId + " does not exists"
        }

        sql.close()
        response


    }

    public BaseApiResponse updateMarketOrderStatus(def body, Long marketOrderId) {
        BaseApiResponse response = new BaseApiResponse(null, 400, "", [])
        List<FieldErrorDto> errors = []
        Sql sql = new Sql(dataSource)
        def queryParams = ObjectUtils.asMap(body)
        Authentication auth = SecurityContextHolder.getContext().getAuthentication()
        def cancelledBy = userRepository.findUserIdByDetails(auth.getName())
        queryParams.put("updatedBy", cancelledBy)
        queryParams.put("marketOrderId", marketOrderId)
        //check if market order exists
        if (marketOrderExist(marketOrderId)) {
            def status = queryParams.get("status")
            //check if the status exist
            def updatesStatusId = commonDBFunctions.getDBRecordByNameAndStatus(status, "market_order_status", "name")
            if (updatesStatusId) {
                queryParams.put("updatesStatusId", updatesStatusId.get("id"))
                def updateMarketOrderStatus = sql.executeInsert("""UPDATE market_order SET market_order_status_id =?.updatesStatusId WHERE id=?.marketOrderId""", queryParams)
                if (updateMarketOrderStatus) {
                    def updatedOrder = commonDBFunctions.getDBRecordById(marketOrderId, "market_order", "id")
                    response.status = HttpStatus.OK.value()
                    response.message = "record updated successfully"
                    response.data = updatedOrder
                } else {
                    response.message = "record failed to update"
                }
            } else {
                response.message = " status " + status + " does not exist"
            }
        } else {
            response.message = "Market Order Does Not Exist"
        }
        sql.close()
        response
    }

    public GeneralApiListResponse getAllMarketOrder(def parameterMap, userType) {
        Sql sql = new Sql(dataSource)
        GeneralApiListResponse response = new GeneralApiListResponse();
        def queryParams = ObjectUtils.flattenListParam(parameterMap)
        Authentication auth = SecurityContextHolder.getContext().getAuthentication()
        def userId = userRepository.findUserIdByDetails(auth.getName())
        def page = queryParams.get("page")
        def size = queryParams.get("size")
        def isActive = queryParams.get("isActive")
        def sellerId = queryParams.get("seller")
        def buyerId = queryParams.get("buyer")
        def dateTo = queryParams.get("dateTo")
        def dateFrom = queryParams.get("dateFrom")
        def date = queryParams.get("date")
        def commodityId = queryParams.get("commodity")
        def filterStatusQuery = ""
        def providerFilter = ""
        def isActiveFilter = ""
        def filterBySeller = ""
        def filterByBuyer = ""
        def dateRangeFilter = ""
        def dateFilter = ""
        def commodityFilter = ""
        if (userType == 1) {
            providerFilter = " AND product.trader_id = ?.userId"
            queryParams.put("userId", userId)
        }
        if (userType == 2) {
            providerFilter = " AND market_order.ordered_by = ?.userId"
            queryParams.put("userId", userId)
        }
        if (queryParams.status) {
            String status = queryParams.status.toLowerCase()
            if (status) {
                def filterStatus = commonDBFunctions.getDBRecordByNameAndStatus(status, "market_order_status", "name")
                if (filterStatus) {
                    filterStatus = filterStatus.get("id")
                    queryParams.put("statusId", filterStatus)
                    filterStatusQuery = " AND market_order.market_order_status_id = ?.statusId "

                } else {
                    response.setMessage("Invalid status value")
                    response.setData(null)
                    response.setStatus(HttpStatus.BAD_REQUEST.value())
                    sql.close()
                    return response
                }
            }

        }
        if (isActive) {
            isActive = Boolean.parseBoolean(isActive)
            queryParams.put("isActive", isActive)
            isActiveFilter = " AND market_order.is_active = ?.isActive"
        }
        if (date) {

                dateFilter = ' AND market_order.date_ordered::date  = ?.date::date '
                queryParams.put("date", date);


        }
        if (dateFrom && dateTo) {

                    dateFilter = "";
                    println("dateFrom" + dateFrom)
                    dateRangeFilter = " AND market_order.date_ordered::date BETWEEN date(?.dateFrom::date) AND date(?.dateTo::date)";
                    queryParams.put("dateFrom", dateFrom);
                    queryParams.put("dateTo", dateTo);

        }
        if (commodityId) {
            commodityId = Integer.parseInt(String.valueOf(commodityId));
            commodityFilter = " AND product.commodity_id =?.commodityId"
            queryParams.put("commodityId", commodityId)
        }
        //filter restricted only to the admin
        if (userType == 0) {
            //filter by seller
            if (sellerId) {
                sellerId = Integer.parseInt(String.valueOf(sellerId));
                filterBySeller = " AND product.trader_id = ?.sellerId"
                queryParams.put("sellerId", sellerId)
            }
            //filter by buyer
            if (buyerId) {
                buyerId = Integer.parseInt(String.valueOf(buyerId));
                filterByBuyer = " AND market_order.ordered_by = ?.buyerId"
                queryParams.put("buyerId", buyerId)
            }
        }

        //pagination
        Map paginateParams = [start: 0, limit: 25]
        def paginate = commonDBFunctions.paginationSqlParams(paginateParams, queryParams)
        def paginationFilter = ""
        if (page && size) {
            paginationFilter = " LIMIT ?.limit OFFSET ?.start"
            queryParams = queryParams + paginate

        }
        String countQuery = """                   SELECT count(*) 
                                                  FROM 
                                                  product,
                                                  market_order,
                                                  market_order_request,
                                                  measurement_unit,
                                                  "user",
                                                  user_individual_detail
                                                  WHERE
                                                  
                                                  product.id = market_order_request.product_id                                              
                                                  AND
                                                  market_order_request.id = market_order.market_order_request_id
                                                  AND
                                                  measurement_unit.id = product.measurement_unit_id
                                                  AND
                                                  market_order_request.ordered_by = "user".id 
                                                  AND
                                                  "user".id = user_individual_detail.user_id     
                                              
                                                 
                                    """
        def queryFilter = filterStatusQuery + providerFilter + isActiveFilter + filterByBuyer + filterBySeller + dateFilter + dateRangeFilter + commodityFilter
        def allMarketOrders = sql.rows(getMarketOrder() + queryFilter +" ORDER BY id DESC"+  paginationFilter , queryParams)
        def total = 0
        if (queryFilter) {
            total = sql.firstRow(countQuery + queryFilter, queryParams)?.get('count')

        } else {
            total = sql.firstRow(countQuery)?.get('count')
        }
        if (allMarketOrders) {
            List<ApiFetchMarketOrderResponseDto> apiFetchMarketOrderResponseDtoList;
            apiFetchMarketOrderResponseDtoList = ObjectUtils.snakeCaseArrayMapToListPojos(allMarketOrders, ApiFetchMarketOrderResponseDto);
            response.data = apiFetchMarketOrderResponseDtoList
            response.message = "records found"
            response.status = HttpStatus.OK.value()
            response.total = total
        } else {
            response.message = "no records found"
            response.total = total
            response.status = HttpStatus.NO_CONTENT.value()
        }

        sql.close()
        response
    }

    public BaseApiResponse getMarketOrderById(Long marketOrderId) {
        Sql sql = new Sql(dataSource)
        BaseApiResponse response = new BaseApiResponse(null, 400, "")
        def queryParams = [:]
        queryParams.put("marketRequestId", marketOrderId)
        def filterById = " AND market_order.id = ?.marketRequestId "
        def marketOrderById = sql.firstRow(getMarketOrder() + filterById, queryParams)
        if (marketOrderById) {
            def responseData = ObjectUtils.snakeCaseMapToPojo(marketOrderById, new ApiFetchMarketOrderResponseDto())
            response.data = responseData
            response.message = "record found"
            response.status = HttpStatus.OK.value()
        } else {
            response.message = "no record found"
            response.status = HttpStatus.NO_CONTENT.value()

        }
        sql.close()
        response

    }

    public BaseApiResponse MarketOrderRating(def body, Long marketOrderId) {
        BaseApiResponse response = new BaseApiResponse(null, 400, "", [])
        List<FieldErrorDto> errors = []
        Sql sql = new Sql(dataSource)
        def queryParams = ObjectUtils.asMap(body)
        Authentication auth = SecurityContextHolder.getContext().getAuthentication()
        def ratedBy = userRepository.findUserIdByDetails(auth.getName())
        queryParams.put("ratedBy", ratedBy)
        queryParams.put("markerOrderId", marketOrderId)
        def userRatingExist = sql.firstRow("""SELECT * FROM market_order_rating WHERE market_order_id=?.markerOrderId AND rated_by =?.ratedBy""", queryParams)
   
        //check if user has already submitted rating
        if (!userRatingExist) {
            if (ratedBy) {
                //check if order exists
                def orderExist = sql.firstRow("""SELECT  id FROM  market_order WHERE is_active AND id=?""", marketOrderId)

                if (orderExist) {
                    def ratingUserTypeExist = commonDBFunctions.getDBRecordByNameAndStatus(queryParams.get("ratingUserType"), "rating_user_type", "name")

                    //check if ratingUserTypeExist
                    if (ratingUserTypeExist) {
                        queryParams.put("ratingUserTypeId", ratingUserTypeExist.get("id"))


                        def insertRating = sql.executeInsert("""INSERT INTO  market_order_rating  
                                                         (market_order_id, rating, rated_by, rating_user_type_id, date_rated) 
                                                         VALUES (?.markerOrderId,?.rating,?.ratedBy,?.ratingUserTypeId,CURRENT_TIMESTAMP)""", queryParams)
                        if (insertRating) {
                            def insertedRecord = sql.firstRow("""SELECT * FROM  market_order_rating WHERE id =?""", insertRating.get(0).get(0))
                            response.message = "rating done successfully"
                            response.status = HttpStatus.OK.value()
                            response.data = insertedRecord
                        } else {
                            response.message = "an error occurred during rating"
                        }
                    } else {
                        response.message = "rating user type" + queryParams.get("ratingUserType") + " does not exist"
                    }

                } else {
                    response.message = "market order id " + marketOrderId + " does not exist"
                }


            } else {
                response.message = "user does not exist"
            }
        } else {
            response.message = queryParams.get("ratingUserType") + " has already rated the market order"
        }
        sql.close()
        response

    }


    public def marketOrderExist(Long marketOrderId) {
        Sql sql = new Sql(dataSource)
        def marketOrder = sql.firstRow "SELECT * FROM market_order WHERE id = ?", marketOrderId
        sql.close()
        marketOrder
    }

    public String getMarketOrder() {
        def getAllMarketOrderQuery = """          SELECT
                                                        market_order.id AS id,
                                                        product.name AS product,
                                                        commodity.name AS commodity,
                                                        product.description,
                                                        market_order_request.unit_price,
                                                        market_order_request.quantity_ordered,
                                                        market_order_request.total_price,
                                                        market_order_request.customer_comments,
                                                        market_order_request.date_requested,
                                                        market_order_status.name AS market_order_status,
                                                        measurement_unit.code AS measurement_unit,
                                                        "buyer".phone_number AS requested_by_phone,
                                                        "buyer".email AS requested_by_email,
                                                    
                                                        CASE
                                                            WHEN "buyer".user_type_id = 1 THEN (SELECT user_individual_detail.first_name||' '||user_individual_detail.middle_name||' '||user_individual_detail.last_name  FROM user_individual_detail WHERE "buyer".id = user_individual_detail.user_id)
                                                            WHEN "buyer".user_type_id = 2 THEN (SELECT user_organization_detail.organization_name FROM user_organization_detail WHERE "buyer".id = user_organization_detail.user_id)
                                                            END AS requested_by_name,
                                                        "seller".phone_number AS sold_by_phone,
                                                        "seller".email AS sold_by_email,
                                                        CASE
                                                            WHEN "seller".user_type_id = 1 THEN (SELECT user_individual_detail.first_name||' '||user_individual_detail.middle_name||' '||user_individual_detail.last_name  FROM user_individual_detail WHERE "seller".id = user_individual_detail.user_id)
                                                            WHEN "seller".user_type_id = 2 THEN (SELECT user_organization_detail.organization_name FROM user_organization_detail WHERE "seller".id = user_organization_detail.user_id)
                                                            END AS sold_by_name
                                                    FROM
                                                        product,
                                                        market_order_request,
                                                        market_order,
                                                        measurement_unit,
                                                        market_order_status,
                                                        "user" AS buyer,
                                                        "user" AS seller,
                                                        commodity
                                                    WHERE
                                                    market_order_request.id = market_order.market_order_request_id
                                                    AND
                                                       product.id = market_order_request.product_id
                                                    AND
                                                        commodity.id = product.commodity_id
                                                    AND    
                                                        measurement_unit.id = product.measurement_unit_id
                                                    AND
                                                          market_order_status.id = market_order.market_order_status_id
                                                    AND
                                                            market_order_request.ordered_by = "buyer".id
                                                    AND
                                                            product.trader_id = "seller".id
    
                                                       """
    }


}
