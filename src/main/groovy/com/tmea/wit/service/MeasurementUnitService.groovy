package com.tmea.wit.service

import com.tmea.wit.model.BaseApiResponse
import com.tmea.wit.model.GeneralApiListResponse
import com.tmea.wit.model.dto.FieldErrorDto
import com.tmea.wit.model.dto.response.ApiMeasurementMetricResponseDto
import com.tmea.wit.model.dto.response.ApiMeasurementUnitResponseDto
import com.tmea.wit.repository.UserRepository
import com.tmea.wit.util.CommonDBFunctions
import com.tmea.wit.util.ObjectUtils
import groovy.sql.Sql
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service

import javax.sql.DataSource

@Service
class MeasurementUnitService {
    DataSource dataSource
    UserRepository userRepository
    CommonDBFunctions commonDBFunctions

    @Autowired
    MeasurementUnitService(DataSource dataSource, UserRepository userRepository, CommonDBFunctions commonDBFunctions) {
        this.dataSource = dataSource
        this.userRepository = userRepository
        this.commonDBFunctions = commonDBFunctions
    }

    public BaseApiResponse addMeasurementUnit(def body){
        Sql sql = new Sql(dataSource)
        BaseApiResponse response = new BaseApiResponse()
        List<FieldErrorDto> errors = []
        Map requestParams = ObjectUtils.asMap(body)
        Authentication auth = SecurityContextHolder.getContext().getAuthentication()
        String userName = auth.getName()
        Long addedBy = userRepository.findUserIdByDetails(userName);
        if(addedBy){
            requestParams.put("addedBy", addedBy)
            def isNameExist = commonDBFunctions.getDBRecordByTableColumn(requestParams.name, 'measurement_unit', 'name')
            if(isNameExist){
                response.status = HttpStatus.BAD_REQUEST.value()
                response.message = 'Measurement Unit name ('+ requestParams.name +') already exist!'
            }else{
                def query = """
                            INSERT INTO measurement_unit (name, description, code, measurement_metric_id, added_by)
                            VALUES (?.name, ?.description, ?.code, ?.measurementMetricId, ?.addedBy)
                        """
                def insertUnit = sql.executeInsert(query, requestParams)
                if(insertUnit){
                    Long unitId = insertUnit.get(0).get(0)
                    def unit = commonDBFunctions.getDBRecordById(unitId, 'measurement_unit', 'id')
                    def data = ObjectUtils.snakeCaseMapToPojo(unit, new ApiMeasurementUnitResponseDto())
                    response.data = data
                    response.message = "Success"
                    response.status = HttpStatus.OK.value()
                }else{
                    response.data = []
                    response.message = "Failed to add measurement unit"
                    response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
                }
            }
        }else{
            errors.add(new FieldErrorDto("userId", "Failed to fetch user details of the user adding the measurement unit"))
            response.data = []
            response.message = "Failed to add measurement unit"
            response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
            response.errors = errors
        }

        response
    }

    public GeneralApiListResponse getMeasurementUnits(Map parameterMap){
        Sql sql = new Sql(dataSource)
        GeneralApiListResponse response
        List<FieldErrorDto> errors = []

        def params = ObjectUtils.flattenListParam(parameterMap)
        def status = params.status
        def code = params.get("code")
        def metricId = params.get("metricId")
        def query = params.get("query")
        def sqlParams = [start: 0, limit: 25]
        boolean whereIncluded = false;

        def nameFilter = ""
        def metricIdFilter = ""
        def codeFilter = ""
        def statusFilter = ""
        if(status){
            def booleanStatus = CommonDBFunctions.getStatusAsBoolean(status)
            if(booleanStatus != null){
                sqlParams.status = booleanStatus
                statusFilter = " WHERE is_active = ?.status"
                whereIncluded = true
            }else{
                errors.add(new FieldErrorDto("status", "Invalid value passed. Check Documentation"))
                response = new GeneralApiListResponse([], 0, HttpStatus.BAD_REQUEST.value(), "Invalid status value", errors)
                return response
            }
        }

        if(query)
        {

            String querySearch = "%$query%"
            sqlParams.put("query",querySearch)
            def queryFilterClause = """ measurement_unit.name ILIKE ?.query """
            nameFilter = whereIncluded ? " AND "+queryFilterClause : " WHERE "+queryFilterClause
        }

        if (code){
            String codeSearch = "%$code%"
            sqlParams.put("code",codeSearch)
            def codeFilterClause = """ measurement_unit.code_name ILIKE ?.code """
            codeFilter = whereIncluded ? " AND "+codeFilterClause : " WHERE "+codeFilterClause
        }

        if (metricId){
            sqlParams.put("metricId", metricId)
            def metricIdFilterClause = """ measurement_unit.measurement_metric_id = ?.metricId """
            metricIdFilter = whereIncluded ? " AND "+metricIdFilterClause : " WHERE "+metricIdFilterClause
        }

        //Pagination parameters & logic
        sqlParams = commonDBFunctions.paginationSqlParams(sqlParams, params)
        def queryFilters = statusFilter + nameFilter + codeFilter + metricIdFilter
        def fetchLanguagesQuery = "SELECT * FROM measurement_unit" + queryFilters + " ORDER BY name ASC "+ CommonDBFunctions.getLimitClause()
        def countQuery = "SELECT count(*) FROM measurement_unit" + queryFilters

        def data = sql.rows(fetchLanguagesQuery, sqlParams)
        def total = queryFilters ? sql.firstRow(countQuery,sqlParams).get("count") : sql.firstRow(countQuery).get("count")
        sql.close()

        if(data){
            response = new GeneralApiListResponse(ObjectUtils.snakeCaseArrayMapToListPojos(data, ApiMeasurementUnitResponseDto), total as Long, HttpStatus.OK.value(), "Success")
        }else{
            response = new GeneralApiListResponse([], total as Long, HttpStatus.OK.value(), "No records found")
        }
        return response
    }

    public BaseApiResponse updateMeasurementUnit(Long unitId, def body){
        Sql sql = new Sql(dataSource)
        BaseApiResponse response = new BaseApiResponse([], 200, "")
        def params = ObjectUtils.asMap(body)
        params.id = unitId

        def isUnitExist = commonDBFunctions.getDBRecordById(unitId, 'measurement_unit', 'id')
        if(isUnitExist){
            def query = """UPDATE measurement_unit set name = ?.name, description = ?.description, code = ?.code,
                       measurement_metric_id = ?.measurementMetricId WHERE id = ?.id"""
            def update = sql.executeUpdate query, params
            sql.close()

            if(update == 1){
                def unit = commonDBFunctions.getDBRecordById(unitId, 'measurement_unit', 'id')
                def message = "Success"
                def unitResponse = ObjectUtils.snakeCaseMapToPojo(unit, new ApiMeasurementUnitResponseDto())
                response.data = unitResponse
                response.status = HttpStatus.OK.value()
                response.message = message
            }else{
                response.status = HttpStatus.BAD_REQUEST.value()
                response.message = "Update failed!!"
            }
        }else{
            response.status = HttpStatus.BAD_REQUEST.value()
            response.message = 'Measurement Unit with id ('+ unitId +') does not exist!'
        }
        return response
    }

    def BaseApiResponse updateMeasurementUnitStatus(long unitId, def body) {
        Sql sql = new Sql(dataSource)
        BaseApiResponse response = new BaseApiResponse([], 200, "")
        def params = ObjectUtils.asMap(body)
        params.id = unitId
        def newStatus = (params.status == 'ACTIVE' || params.status == 'active')
        params.status = newStatus

        def isUnitExist = commonDBFunctions.getDBRecordById(unitId, 'measurement_unit', 'id')
        if(isUnitExist){
            def update = sql.executeUpdate "UPDATE measurement_unit set is_active = ?.status WHERE id = ?.id", params
            sql.close()
            def statusMessage= params.status ? 'activated' : 'deactivated'

            if(update == 1){
                def unit = commonDBFunctions.getDBRecordById(unitId, 'measurement_unit', 'id')
                def message = "Measurement Unit ("+ unit.name+ ") " + statusMessage
                def unitResponse = ObjectUtils.snakeCaseMapToPojo(unit, new ApiMeasurementUnitResponseDto())
                response.data = unitResponse
                response.status = HttpStatus.OK.value()
                response.message = message
            }else{
                response.status = HttpStatus.BAD_REQUEST.value()
                response.message = "Update failed!!"
            }
        }else{
            response.status = HttpStatus.BAD_REQUEST.value()
            response.message = 'Measurement Unit with id ('+ unitId +') does not exist!'
        }
        return response
    }
}
