package com.tmea.wit.service

import com.tmea.wit.model.BaseApiResponse
import com.tmea.wit.model.dto.FieldErrorDto
import com.tmea.wit.repository.UserRepository
import com.tmea.wit.util.CommonDBFunctions
import com.tmea.wit.util.ObjectUtils
import groovy.sql.Sql
import org.springframework.http.HttpStatus
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service

import javax.sql.DataSource

@Service
class StandardSpecificationService {
    DataSource dataSource
    UserRepository userRepository
    CommonDBFunctions commonDBFunctions
    StandardSpecificationService(DataSource dataSource, UserRepository userRepository, CommonDBFunctions commonDBFunctions) {
        this.dataSource = dataSource
        this.userRepository = userRepository
        this.commonDBFunctions = commonDBFunctions
    }
    public BaseApiResponse addStandardSpecification(def body) {
        Sql sql = new Sql(dataSource)
        BaseApiResponse response = new BaseApiResponse([], 400, "")
        List<FieldErrorDto> errors = []
        Map requestParams = ObjectUtils.asMap(body)
        Authentication auth = SecurityContextHolder.getContext().getAuthentication()
        String userName = auth.getName()
        Long addedBy = userRepository.findUserIdByDetails(userName)
        if (addedBy) {
            requestParams.put("addedBy", addedBy)
            def isSpecificationExist = sql.firstRow("""SELECT id,description FROM standard_specification WHERE standard_id=?.standard AND standard_specification_type_id=?.standardSpecificationType  AND title=?.title""", requestParams)
            if (!isSpecificationExist) {
            def isStandardExist = commonDBFunctions.getDBRecordByIdAndStatus(requestParams.standard, "standard", "id")
            if (isStandardExist) {
                def isSpecificationTypeExist = commonDBFunctions.getDBRecordByIdAndStatus(requestParams.standardSpecificationType,"standard_specification_type","id")
                if(isSpecificationTypeExist){
                    def insertRecordQuery = sql.executeInsert("""INSERT INTO standard_specification(standard_id,standard_specification_type_id, title, description, added_by, is_active)
                                                                 VALUES (?.standard,?.standardSpecificationType,?.title,?.description,?.addedBy,TRUE)""", requestParams)
                    if (insertRecordQuery) {
                        def insertedRecord = sql.firstRow("""SELECT * FROM standard_specification where id=? """, insertRecordQuery.get(0).get(0))
                        response.message = "record added successfully"
                        response.status = HttpStatus.OK.value()
                        response.data = ObjectUtils.mapToSnakeCaseMap(insertedRecord)
                    } else {
                        response.message = "an error occurred while updating record"
                        response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
                    }
                }else {
                    response.message = "standard specification type ("+ requestParams.standardSpecificationType +") does not exist"

                }

            } else {
                response.message = "standard  id ("+ requestParams.standard +") does not exist"
            }
        }else
            {
                response.data = ObjectUtils.mapToSnakeCaseMap(isSpecificationExist)
                response.message = "record with similar details exist"
            }
        }else{
            response.message = "could bot find logged in user"
        }
        sql.close()
        return response
    }
    public BaseApiResponse updateStandardSpecificationStatus(Long id,def body) {
        Sql sql = new Sql(dataSource)
        BaseApiResponse response = new BaseApiResponse([], 400, "")
        List<FieldErrorDto> errors = []
        Map requestParams = ObjectUtils.asMap(body)
        requestParams.id = id
        def newStatus = ""
        Authentication auth = SecurityContextHolder.getContext().getAuthentication()
        String userName = auth.getName()
        Long updatedBy = userRepository.findUserIdByDetails(userName)
        if (requestParams.status.toLowerCase() == 'active') {
            requestParams.newStatus = true;
        } else if (requestParams.status.toLowerCase() == 'inactive') {
            requestParams.newStatus = false;
        }
        if (updatedBy) {
            def responseMessage = requestParams.newStatus ? "activated" : "deactivated"
            def recordExist = commonDBFunctions.getDBRecordById(id, "standard_specification", "id")
            if(recordExist)
            {

                def updateStatusQuery = sql.executeUpdate("""UPDATE standard_specification SET is_active=?.newStatus WHERE id=?.id""",requestParams)
                if (updateStatusQuery) {
                    def updatedRecord = sql.firstRow("""SELECT * FROM standard_specification where id=? """, requestParams.id)
                    response.message = "record ("+responseMessage +") successfully"
                    response.status = HttpStatus.OK.value()
                    response.data = ObjectUtils.mapToSnakeCaseMap(updatedRecord)
                }else
                {
                    response.message = "an error occurred while updating status"
                    response.status =HttpStatus.INTERNAL_SERVER_ERROR.value()
                }
            }else {
                response.message = "specification id ("+id+") does not exist"
            }
        }else
        {
            response.message ="could not find logged in user"
        }
         sql.close()
        return response
    }
    public BaseApiResponse updateStandardSpecificationDetails(Long id,def body) {
        Sql sql = new Sql(dataSource)
        BaseApiResponse response = new BaseApiResponse([], 400, "")
        List<FieldErrorDto> errors = []
        Map requestParams = ObjectUtils.asMap(body)
        Authentication auth = SecurityContextHolder.getContext().getAuthentication()
        String userName = auth.getName()
        Long updatedBy = userRepository.findUserIdByDetails(userName)
        requestParams.id = id
        if (updatedBy) {
            if (requestParams) {
                //check if product exists
                def recordExist = sql.firstRow("""SELECT id FROM standard_specification WHERE id=?""",id)
                println("record"+recordExist)
                println("record"+id)
                if (recordExist) {
                    def updateRecordQuery = sql.executeUpdate("""UPDATE standard_specification SET title=?.title,
                                                                            description=?.description,standard_id=?.standard WHERE id=?.id""",requestParams)
                    println("update"+updateRecordQuery)
                    if(updateRecordQuery==1){
                    def updatedRecord =   sql.firstRow("""SELECT * FROM standard_specification WHERE id=?""",id)
                    response.status = HttpStatus.OK.value()
                    response.data = ObjectUtils.mapToSnakeCaseMap(updatedRecord)
                    response.message = "record updated successfully"
                } else {
                    response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
                    response.message = "Operation failed"
                }
                }else
                {
                    response.message = "record id ("+id+") does not exist"
                }
            }else{
                response.message = 'cannot update an empty payload'
            }
        }else {
            response.message = 'could not get logged in user'
        }
        sql.close()
        return  response
    }
}
