package com.tmea.wit.service

import com.tmea.wit.model.BaseApiResponse
import com.tmea.wit.model.GeneralApiListResponse
import com.tmea.wit.model.dto.FieldErrorDto
import com.tmea.wit.model.dto.response.ApiDeliveryOptionResponseDto
import com.tmea.wit.model.dto.response.ApiTaxResponseDto
import com.tmea.wit.repository.UserRepository
import com.tmea.wit.util.CommonDBFunctions
import com.tmea.wit.util.ObjectUtils
import groovy.sql.Sql
import org.springframework.http.HttpStatus
import org.springframework.security.core.Authentication
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service

import javax.sql.DataSource

@Service
class TaxTypeService {

    DataSource dataSource
    UserRepository userRepository
    CommonDBFunctions commonDBFunctions

    TaxTypeService(DataSource dataSource, UserRepository userRepository, CommonDBFunctions commonDBFunctions) {
        this.dataSource = dataSource
        this.userRepository = userRepository
        this.commonDBFunctions = commonDBFunctions
    }

    public BaseApiResponse addTaxType(def body){
        Sql sql = new Sql(dataSource)
        BaseApiResponse response = new BaseApiResponse()
        List<FieldErrorDto> errors = []
        Map requestParams = ObjectUtils.asMap(body)
        Authentication auth = SecurityContextHolder.getContext().getAuthentication()
        String userName = auth.getName()
        Long addedBy = userRepository.findUserIdByDetails(userName)
        if(addedBy){
            requestParams.put("addedBy", addedBy)
            def isNameExist = commonDBFunctions.getDBRecordByTableColumn(requestParams.name, 'tax', 'name')
            if(isNameExist){
                response.status = HttpStatus.BAD_REQUEST.value()
                response.message = 'Tax Type name ('+ requestParams.name +') already exist!'
            }else{
                def query = """
                            INSERT INTO tax (name, value, charge_mode_id, added_by)
                            VALUES (?.name, ?.value, ?.chargeModeId, ?.addedBy)
                        """
                def insertTaxType = sql.executeInsert(query, requestParams)
                if(insertTaxType){
                    Long taxTypeId = insertTaxType.get(0).get(0)
                    def taxType = commonDBFunctions.getDBRecordById(taxTypeId, 'tax', 'id')
                    def data = ObjectUtils.snakeCaseMapToPojo(taxType, new ApiTaxResponseDto())
                    response.data = data
                    response.message = "Success"
                    response.status = HttpStatus.OK.value()
                }else{
                    response.data = []
                    response.message = "Failed to add tax type"
                    response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
                }
            }
        }else{
            errors.add(new FieldErrorDto("userId", "Failed to fetch user details of the user adding the tax type"))
            response.data = []
            response.message = "Failed to add tax type"
            response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
            response.errors = errors
        }
        response
    }

    public GeneralApiListResponse getTaxTypes(Map parameterMap){
        Sql sql = new Sql(dataSource)
        GeneralApiListResponse response
        List<ApiTaxResponseDto> taxTypeList = new ArrayList<>()
        List<FieldErrorDto> errors = []

        def params = ObjectUtils.flattenListParam(parameterMap)
        def status = params.status
        def query = params.get("query")
        def sqlParams = [start: 0, limit: 25]
        boolean whereIncluded = false;

        def nameFilter = ""
        def statusFilter = ""
        if(status){
            def booleanStatus = CommonDBFunctions.getStatusAsBoolean(status)
            if(booleanStatus != null){
                sqlParams.status = booleanStatus
                statusFilter = " WHERE is_active = ?.status"
                whereIncluded = true
            }else{
                errors.add(new FieldErrorDto("status", "Invalid value passed. Check Documentation"))
                response = new GeneralApiListResponse([], 0, HttpStatus.BAD_REQUEST.value(), "Invalid status value", errors)
                return response
            }
        }

        if(query)
        {

            String querySearch = "%$query%"
            sqlParams.put("query",querySearch)
            def queryFilterClause = """ tax.name ILIKE ?.query """
            nameFilter = whereIncluded ? " AND "+queryFilterClause : " WHERE "+queryFilterClause
        }

        //Pagination parameters & logic
        sqlParams = commonDBFunctions.paginationSqlParams(sqlParams, params)
        def queryFilters = statusFilter+nameFilter
        def fetchLanguagesQuery = "SELECT * FROM tax" + queryFilters + " ORDER BY name ASC "+ CommonDBFunctions.getLimitClause()
        def countQuery = "SELECT count(*) FROM tax" + queryFilters

        def data = sql.rows(fetchLanguagesQuery, sqlParams)
        def total = queryFilters ? sql.firstRow(countQuery,sqlParams).get("count") : sql.firstRow(countQuery).get("count")
        sql.close()

        if(data){
            response = new GeneralApiListResponse(ObjectUtils.snakeCaseArrayMapToListPojos(data, ApiTaxResponseDto), total as Long, HttpStatus.OK.value(), "Success")
        }else{
            response = new GeneralApiListResponse([], total as Long, HttpStatus.OK.value(), "No records found")
        }
        return response
    }

    public BaseApiResponse updateTaxTypeStatus(Long taxTypeId, def body){
        Sql sql = new Sql(dataSource)
        BaseApiResponse response = new BaseApiResponse([], 200, "")
        def params = ObjectUtils.asMap(body)
        params.id = taxTypeId
        def newStatus = (params.status == 'ACTIVE' || params.status == 'active')
        params.status = newStatus

        def isTaxTypeExist = commonDBFunctions.getDBRecordById(taxTypeId, 'tax', 'id')
        if(isTaxTypeExist){
            def update = sql.executeUpdate "UPDATE tax set is_active = ?.status WHERE id = ?.id", params
            sql.close()
            def statusMessage= newStatus ? 'activated' : 'deactivated'

            if(update == 1){
                def taxType = commonDBFunctions.getDBRecordById(taxTypeId, 'tax', 'id')
                def message = "Tax Type ("+ taxType.name+ ") " + statusMessage
                def taxTypeResponse = ObjectUtils.snakeCaseMapToPojo(taxType, new ApiTaxResponseDto())
                response.data = taxTypeResponse
                response.status = HttpStatus.OK.value()
                response.message = message
            }else{
                response.status = HttpStatus.BAD_REQUEST.value()
                response.message = "Update failed!!"
            }
        }else{
            response.status = HttpStatus.BAD_REQUEST.value()
            response.message = 'Tax Type with id ('+ taxTypeId +') does not exist!'
        }

        response
    }

    def BaseApiResponse updateTaxType(long taxTypeId, def body) {
        Sql sql = new Sql(dataSource)
        BaseApiResponse response = new BaseApiResponse([], 200, "")
        def params = ObjectUtils.asMap(body)
        params.id = taxTypeId

        def isTaxTypeExist = commonDBFunctions.getDBRecordById(taxTypeId, 'tax', 'id')
        if(isTaxTypeExist){
            def query = """
                            UPDATE tax set name = ?.name, value = ?.value, charge_mode_id = ?.chargeModeId 
                            WHERE id = ?.id
                        """
            def update = sql.executeUpdate query, params
            sql.close()

            if(update == 1){
                def taxType = commonDBFunctions.getDBRecordById(taxTypeId, 'tax', 'id')
                def message = "Success"
                def taxTypeResponse = ObjectUtils.snakeCaseMapToPojo(taxType, new ApiTaxResponseDto())
                response.data = taxTypeResponse
                response.status = HttpStatus.OK.value()
                response.message = message
            }else{
                response.status = HttpStatus.BAD_REQUEST.value()
                response.message = "Update failed!!"
            }

        }else{
            response.status = HttpStatus.BAD_REQUEST.value()
            response.message = 'Tax Type with id ('+ taxTypeId +') does not exist!'
        }
        response
    }
}
