package com.tmea.wit.service

import com.tmea.wit.core.storage.StorageApi
import com.tmea.wit.model.BaseApiResponse
import com.tmea.wit.model.GeneralApiListResponse
import com.tmea.wit.model.dto.FieldErrorDto
import com.tmea.wit.model.dto.request.ApiProductUpdateRequestDto
import com.tmea.wit.repository.UserRepository
import com.tmea.wit.util.CommonDBFunctions
import com.tmea.wit.util.FileUtils
import com.tmea.wit.util.ObjectUtils
import groovy.sql.Sql
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service

import javax.sql.DataSource

@Service
class RegulationCategoryService {
    DataSource dataSource
    UserRepository userRepository
    CommonDBFunctions commonDBFunctions
    @Autowired
    UserService userService
    @Autowired
    RegulationCategoryService(DataSource dataSource, UserRepository userRepository,
                              CommonDBFunctions commonDBFunctions) {
        this.dataSource = dataSource
        this.userRepository = userRepository
        this.commonDBFunctions = commonDBFunctions
    }

    public BaseApiResponse addRegulatoryCategory(def body, String userName) {
        Sql sql = new Sql(dataSource)
        BaseApiResponse response = new BaseApiResponse([], 400, "")
        List<FieldErrorDto> errors = []
        Map requestParams = ObjectUtils.asMap(body)
        Long addedBy = userRepository.findUserIdByDetails(userName);
        //check if added by exist
        if (addedBy) {
            requestParams.addedBy = addedBy
            //check if regulation category exist
            def isRegulationCategoryExist = sql.firstRow("""SELECT * FROM regulation_category WHERE title ILIKE ?""", requestParams.title)
            if (!isRegulationCategoryExist) {
                def insertRecord = sql.executeInsert("""INSERT INTO regulation_category (title, description, is_active, added_by) 
                                                            VALUES (?.title,?.description,TRUE,?.addedBy)""", requestParams)
                if (insertRecord) {
                    def insertedRecord = sql.firstRow("""SELECT * FROM  regulation_category WHERE id =?""", insertRecord.get(0).get(0))
                    response.message = "Regulation Category added successfully"
                    response.status = HttpStatus.OK.value()
                    response.data = ObjectUtils.mapToSnakeCaseMap(insertedRecord)
                } else {
                    response.message = "an error occurred while inserting record"
                    response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
                }

            } else {
                def recordStatus = isRegulationCategoryExist.is_active
                def responseMessage = recordStatus?"regulation category" + " '$requestParams.title'" + " exist":"regulation category" + " '$requestParams.title'" + " exist but is deActivated."
                response.message = responseMessage
                response.data = ObjectUtils.mapToSnakeCaseMap(isRegulationCategoryExist)
            }
        } else {

            response.message = "User does not exist"
        }
        sql.close()
        return response;
    }

    BaseApiResponse editRegulationCategory(def body, Long id, String name) {
        Sql sql = Sql.newInstance(dataSource)
        BaseApiResponse response = new BaseApiResponse([], 400, "")
        Map requestParams = ObjectUtils.asMap(body)
        Long userId = userService.getUserIdFromUsername(name)
        List<FieldErrorDto> errorDto = new ArrayList<>()

        if (userId) {
            if (requestParams) {
                def categoryExist = sql.firstRow("""SELECT * FROM regulation_category WHERE id =?""", id)
                if (categoryExist) {

                    def updateParams = ObjectUtils.copyMapProperties(requestParams, categoryExist)
                    updateParams.id = id
                    def updateCategoryQuery = sql.executeUpdate(
                            """UPDATE regulation_category 
                                   SET title = ?.title, 
                                       description = ?.description
                                   WHERE id = ?.id""", updateParams
                    )
                    if (updateCategoryQuery) {
                        def updatedRecord = sql.firstRow("""SELECT * FROM regulation_category WHERE id =?""", id)
                        response.message = 'record updated successfully'
                        response.status = HttpStatus.OK.value()
                        response.data = ObjectUtils.mapToSnakeCaseMap(updatedRecord)

                    }else {
                        response.message = "record failed to update"
                        response.data = requestParams
                        response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
                    }


                } else {
                    response.message = "regulation category does not exist"
                }


            }else{
                response.message = "cannot update empty payload"
            }

        }else{
            response.message = 'user does not exist'
        }
        sql.close()
        response

    }
     BaseApiResponse updateRegulationCategoryStatus(long id, def body) {
        Sql sql = new Sql(dataSource)
        BaseApiResponse response = new BaseApiResponse([], 400, "")
        def params = ObjectUtils.asMap(body)
        params.id = id
        def newStatus
         if (params.status.toLowerCase() == 'active') {
             newStatus = true;
         }
         if (params.status.toLowerCase() ==  'inactive') {
             newStatus = false;
         }
         params.status = newStatus

        def isRegulationCategoryExist = commonDBFunctions.getDBRecordById(id, 'regulation_category', 'id')
         if(isRegulationCategoryExist) {
             def update = sql.executeUpdate "UPDATE regulation_category set is_active = ?.status WHERE id = ?.id", params

             def statusMessage = params.status ? 'activated' : 'deactivated'
             if(update){
                 def updatedRecord = sql.firstRow("""SELECT  * FROM regulation_category WHERE id=?""",id)
                 response.message = "Regulatory category has been " +statusMessage
                 response.status = HttpStatus.OK.value()
                 response.data = ObjectUtils.mapToSnakeCaseMap(updatedRecord)

             }else {
                 response.message = "an error occurred while (" + statusMessage +") regulation category"
                 response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
             }
         }else{
             response.message = "regulatory category does id(" +id +") not exist"

         }
         sql.close()
         return response
    }
    public GeneralApiListResponse getRegulationCategory(Map parameterMap , Boolean isAdmin){
        Sql sql = new Sql(dataSource)
        GeneralApiListResponse response
        //List<ApiRegulationProcedureResponseDto> RegulationCategoryList = new ArrayList<>()
        List<FieldErrorDto> errors = []
        def params = ObjectUtils.flattenListParam(parameterMap)
        def status = params.status
        def query = params.query
        def statusFilter = ""
        def sqlParams = [start: 0, limit: 25]
        boolean whereIncluded = false;
        //OverRide Status Filter if not Admin
        status = isAdmin ? status : "active"
        if(status){
            def booleanStatus = CommonDBFunctions.getStatusAsBoolean(status)
            if(booleanStatus != null){
                sqlParams.status = booleanStatus
                statusFilter = " WHERE is_active = ?.status"
                whereIncluded = true
            }else{
                errors.add(new FieldErrorDto("status", "Invalid value passed. Check Documentation"))
                response = new GeneralApiListResponse([], 0, HttpStatus.BAD_REQUEST.value(), "Invalid status value", errors)
                return response
            }
        }
        def searchFilter=""
        if(query)
        {

            String querySearch = "%$query%"
            sqlParams.put("query",querySearch)
            def queryFilterClause = """title ILIKE ?.query OR
                                       description ILIKE ?.query
                                       """
            searchFilter = whereIncluded ? " AND "+queryFilterClause : " WHERE "+queryFilterClause
        }

        //Pagination parameters & logic
        sqlParams = commonDBFunctions.paginationSqlParams(sqlParams,params)
        def countQuery = """SELECT count(*) FROM regulation_category"""
        def queryFilters = statusFilter  + searchFilter
        def fetchQuery= isAdmin?"""SELECT * FROM regulation_category """:"""SELECT id,title,description FROM regulation_category """
        def data = sql.rows(fetchQuery + queryFilters +  commonDBFunctions.getLimitClause(), sqlParams)

        def total = queryFilters ? sql.firstRow(countQuery + queryFilters,sqlParams).get("count") : sql.firstRow(countQuery).get("count")

        if(data){

            response = new GeneralApiListResponse(  ObjectUtils.mapToSnakeCaseArrayMap(data), total as Long, HttpStatus.OK.value(), "Success")
        }else{
            response = new GeneralApiListResponse([], total as Long, HttpStatus.NO_CONTENT.value(), "No records found")
        }
        sql.close()
        return  response
    }
}
