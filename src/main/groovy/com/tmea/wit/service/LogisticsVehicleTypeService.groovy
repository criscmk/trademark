package com.tmea.wit.service

import com.tmea.wit.model.BaseApiResponse
import com.tmea.wit.model.GeneralApiListResponse
import com.tmea.wit.model.dto.FieldErrorDto
import com.tmea.wit.model.dto.request.ApiLogisticsVehicleTypePostDto
import com.tmea.wit.model.dto.request.ApiStatusRequestDto
import com.tmea.wit.model.dto.response.ApiLogisticsServiceTypeResponseDto
import com.tmea.wit.model.dto.response.ApiLogisticsVehicleTypeResponseDto
import com.tmea.wit.util.ObjectUtils
import groovy.sql.Sql
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service

import javax.sql.DataSource

@Service
class LogisticsVehicleTypeService {

    private DataSource dataSource
    UserService userService

    @Autowired
    LogisticsVehicleTypeService(DataSource dataSource,UserService userService) {
        this.dataSource = dataSource
        this.userService = userService
    }

    BaseApiResponse addNewVehicle(ApiLogisticsVehicleTypePostDto apiLogisticsVehicleTypePostDto, String username) {
        Sql sql = new Sql(dataSource)
        BaseApiResponse response = new BaseApiResponse()
        def params = ObjectUtils.asMap(apiLogisticsVehicleTypePostDto)
        def userId = userService.getUserIdFromUsername(username)
        List<FieldErrorDto> errors = []
        params.userId = userId

        if (userId) {
            def isNameExist = getLogisticsVehicleTypeByName(params.vehicleTypeName)

            if (isNameExist) {
                response.status = HttpStatus.BAD_REQUEST.value()
                response.message = 'Logistics vehicle type (' + params.vehicleTypeName + ') already exist!'
            } else {
                def insertRes
                sql.withTransaction {
                    insertRes = sql.executeInsert("""
                                INSERT INTO logistics_vehicle_type(name, is_active, added_by)
                                VALUES (?.vehicleTypeName, true, ?.userId)
                                """, params)
                }

                if (insertRes) {
                    Long vehicleTypeId = insertRes.get(0).get(0)
                    def vehicleType = getLogisticsVehicleTypeById(vehicleTypeId)

                    def data = ObjectUtils.snakeCaseMapToPojo(vehicleType, new ApiLogisticsVehicleTypeResponseDto())
                    response.data = data
                    response.message = "Success"
                    response.status = HttpStatus.OK.value()
                } else {
                    response.data = null
                    response.message = "Failed to add logistic vehicle type"
                    response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
                }

            }

        } else {
            errors.add(new FieldErrorDto("userId", "Failed to fetch user details of the logged in user"))
            response.data = null
            response.message = "Failed to add logistic vehicle type"
            response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
            response.errors = errors
        }

        sql.close()
        return response

    }

    BaseApiResponse editLogisticsVehicle(ApiLogisticsVehicleTypePostDto apiLogisticsVehicleTypePostDto, Long id, String username) {
        Sql sql = Sql.newInstance(dataSource)

        BaseApiResponse response = new BaseApiResponse()
        def sqlParams = ObjectUtils.asMap(apiLogisticsVehicleTypePostDto)
        def user = userService.getUserIdFromUsername(username)
        sqlParams.userId = user
        sqlParams.id = id
        List<FieldErrorDto> errorDtoList = []

        if (user){
            def recordExist = getLogisticsVehicleTypeById(id)

            if (recordExist){
                def updateQuery
                sql.withTransaction {
                    updateQuery = sql.executeUpdate(
                            """UPDATE logistics_vehicle_type
                            SET name = ?.vehicleTypeName,
                                added_by = ?.userId
                            WHERE id = ?.id
                            AND name != ?.vehicleTypeName
                            """, sqlParams
                    )
                }

                if (updateQuery > 0){
                    def vehicleType = getLogisticsVehicleTypeById(id)
                    def logisticsVehicleTypeResponse = ObjectUtils.snakeCaseMapToPojo(vehicleType, new ApiLogisticsVehicleTypeResponseDto())
                    response.data = logisticsVehicleTypeResponse
                    response.message = updateQuery + " record updated successfully"
                    response.status = HttpStatus.OK.value()

                } else {
                    response.data = null
                    response.message = "Failed to edit logistic vehicle type"
                    response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
                }

            } else {
                response.status = HttpStatus.BAD_REQUEST.value()
                response.message = 'Record not found!'
            }

        } else {
            errorDtoList.add(new FieldErrorDto("userId", "Failed to fetch user details of the logged in user"))
            response.data = null
            response.message = "Failed to edit logistic vehicle type"
            response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
            response.errors = errorDtoList
        }


        sql.close()
        return response
    }

    BaseApiResponse updateLogisticsVehicleStatus(Long id, ApiStatusRequestDto body, String username) {
        Sql sql = Sql.newInstance(dataSource)

        BaseApiResponse response = new BaseApiResponse()
        Map requestParams = ObjectUtils.asMap(body);
        Long user = userService.getUserIdFromUsername(username);

        requestParams.put("id", id);
        requestParams.put("userId", user)
        def newStatus = (requestParams.status == 'ACTIVE' || requestParams.status == 'active')
        requestParams.status = newStatus

        if (user) {
            def vehicleTypeExist = getLogisticsVehicleTypeById(id)
            if (vehicleTypeExist) {
                def updateRes
                sql.withTransaction {
                    updateRes = sql.executeUpdate(
                            """UPDATE logistics_vehicle_type
                        SET is_active = ?.status
                        WHERE id = ?.id
                        """, requestParams)
                }

                def statusMessage = newStatus ? 'activated' : 'deactivated'
                if (updateRes == 1) {
                    def logisticVehicleType = getLogisticsVehicleTypeById(id)
                    def message = "Logistics vehicle type (" + logisticVehicleType.name + ") " + statusMessage
                    def logisticVehicleTypeResponse = ObjectUtils.snakeCaseMapToPojo(logisticVehicleType, new ApiLogisticsServiceTypeResponseDto())
                    response.data = logisticVehicleTypeResponse
                    response.status = HttpStatus.OK.value()
                    response.message = message
                } else {
                    response.status = HttpStatus.BAD_REQUEST.value()
                    response.message = "Update failed!!"
                }

            } else {
                response.status = HttpStatus.BAD_REQUEST.value()
                response.message = "Record not found"
            }

        } else {
            response.data = null
            response.message = "Failed to edit logistic vehicle type status"
            response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
        }


        sql.close()
        return response
    }

    GeneralApiListResponse fetchAllLogisticsVehicles(Map parameterMap) {
        Sql sql = new Sql(dataSource)
        GeneralApiListResponse response = new GeneralApiListResponse()
        List<ApiLogisticsVehicleTypeResponseDto> vehicleTypes = new ArrayList<>()
        List<FieldErrorDto> errorList = []

        def params = ObjectUtils.flattenListParam(parameterMap)
        def status = params.status
        def sqlParams = [start: 0, limit: 25]
        if (status) {
            if (params.status == 'active' || params.status == 'ACTIVE') {
                sqlParams.status = true
            } else if (params.status == 'inactive' || params.status == 'INACTIVE') {
                sqlParams.status = false
            } else {
                response.status = HttpStatus.BAD_REQUEST.value()
                response.message = "Invalid status value"
            }
        }

        def query
        def queryFilter = ""
        def limitFilter

        //Pagination parameters & logic
        def page = params.page?.toInteger() ? params.page?.toInteger() : 0
        def size = params.size?.toInteger() ? params.size?.toInteger() : 25
        if (page == 0) {
            sqlParams.limit = 1 * size
            sqlParams.start = sqlParams.limit - size
        } else {
            sqlParams.limit = page * size
            sqlParams.start = sqlParams.limit - size
        }

        query = "SELECT * FROM logistics_vehicle_type"
        if (status) {
            queryFilter = " WHERE is_active = ?.status"
            query += queryFilter
        }
        limitFilter = " LIMIT ?.limit OFFSET ?.start"
        query += limitFilter

        def data = sql.rows(query, sqlParams)
        String countQuery = "SELECT count(*) FROM logistics_vehicle_type" + queryFilter

        def total
        if (queryFilter) {
            total = sql.firstRow(countQuery, sqlParams)?.get('count')
        } else {
            total = sql.firstRow(countQuery)?.get('count')
        }

        !total ? total = 0 : total

        data.each {
            def apiLogisticsVehicleResponse = ObjectUtils.snakeCaseMapToPojo(it, new ApiLogisticsVehicleTypeResponseDto()) as ApiLogisticsVehicleTypeResponseDto
            vehicleTypes.add(apiLogisticsVehicleResponse)
        }
        if (data) {
            response = new GeneralApiListResponse(vehicleTypes, total as Long, HttpStatus.OK.value(), "Success")
        } else {
            response = new GeneralApiListResponse(null, total as Long, HttpStatus.OK.value(), "No records found")
        }

        sql.close()
        return response

    }

    Map getLogisticsVehicleTypeById(Long id) {
        Sql sql = Sql.newInstance(dataSource)
        def result = sql.firstRow "SELECT * FROM logistics_vehicle_type WHERE id = ?", id
        sql.close()
        return result
    }

    Map getLogisticsVehicleTypeByName(String vehicleTypeName) {
        Sql sql = Sql.newInstance(dataSource)
        def result = sql.firstRow "SELECT * FROM logistics_vehicle_type WHERE name = ?", vehicleTypeName

        sql.close()
        return result
    }

}
