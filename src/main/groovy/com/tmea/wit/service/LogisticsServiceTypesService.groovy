package com.tmea.wit.service

import com.tmea.wit.model.BaseApiResponse
import com.tmea.wit.model.GeneralApiListResponse
import com.tmea.wit.model.dto.FieldErrorDto
import com.tmea.wit.model.dto.request.ApiLogisticsServiceTypesRequestDto
import com.tmea.wit.model.dto.request.ApiStatusRequestDto
import com.tmea.wit.model.dto.response.ApiLogisticsServiceTypeResponseDto
import com.tmea.wit.util.ObjectUtils
import groovy.sql.Sql
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service

import javax.sql.DataSource

@Service
class LogisticsServiceTypesService {
    private DataSource dataSource

    @Autowired
    LogisticsServiceTypesService(DataSource dataSource) {
        this.dataSource = dataSource
    }

    @Autowired
    UserService userService

    BaseApiResponse addLogisticsServiceType(ApiLogisticsServiceTypesRequestDto apiLogisticsServiceTypesRequestDto, String username) {
        Sql sql = Sql.newInstance(dataSource)

        BaseApiResponse response = new BaseApiResponse()
        Map sqlParams = ObjectUtils.asMap(apiLogisticsServiceTypesRequestDto)
        List<FieldErrorDto> errors = []
        Long userId = userService.getUserIdFromUsername(username)

        if (userId) {
            sqlParams.put("isActive", true)
            sqlParams.put("addedBy", userId)

            //check if logistics service type with same name exist
            def serviceNameExist = sql.firstRow("""SELECT *
                                    FROM logistics_service_type
                                    WHERE name ILIKE ?.name
                                    """, sqlParams)

            if (serviceNameExist) {
                response.data = sqlParams
                response.status = HttpStatus.BAD_REQUEST.value()
                response.message = 'Logistics service type (' + sqlParams.name + ') already exist!'
                return response
            } else {
                sql.withTransaction {
                    def query = sql.executeInsert("""
                            INSERT INTO logistics_service_type(name, description, added_by, date_added, is_active)
                            VALUES (?.name, ?.description, ?.addedBy, current_timestamp, ?.isActive)
                            """, sqlParams)

                    if (query) {

                        Long serviceTypeId = query.get(0).get(0)
                        Map queryParams = [:]
                        queryParams.put("serviceTypeId", serviceTypeId)

                        def serviceType = sql.firstRow("""SELECT *
                                        FROM logistics_service_type
                                            WHERE id = ?.serviceTypeId
                                        """, queryParams)

                        def data = ObjectUtils.snakeCaseMapToPojo(serviceType, new ApiLogisticsServiceTypeResponseDto())
                        response.data = data
                        response.message = "Success"
                        response.status = HttpStatus.OK.value()
                    } else {
                        response.data = sqlParams
                        response.message = "Failed to add logistic service type"
                        response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
                    }
                }
            }

        } else {
            errors.add(new FieldErrorDto("userId", "Failed to fetch user details of the user adding logistics service type"))
            response.data = sqlParams
            response.message = "Failed to add logistic service type"
            response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
            response.errors = errors
        }

        sql.close()
        return response

    }


    BaseApiResponse editLogisticServiceType(ApiLogisticsServiceTypesRequestDto apiLogisticsServiceTypesRequestDto, Long id, String username) {
        Sql sql = Sql.newInstance(dataSource)

        BaseApiResponse response = new BaseApiResponse()
        Map sqlParams = ObjectUtils.asMap(apiLogisticsServiceTypesRequestDto);
        Long userId = userService.getUserIdFromUsername(username);
        List<FieldErrorDto> errors = []

        sqlParams.put("id", id);
        sqlParams.put("userId", userId)

        //check if record exist
        if (userId) {
            def recordExist = getServiceTypeById(id)

            if (recordExist) {
                Map updateParams = ObjectUtils.rowAsMap(recordExist);

                ObjectUtils.copyMapProperties(sqlParams, updateParams);


                   def editRecord = sql.executeUpdate(
                            """UPDATE logistics_service_type
                                    SET name = ?.name,
                                        description = ?.description
                                    WHERE id = ?.id
                                      """, updateParams)


                if (editRecord == 1) {
                    def serviceType = getServiceTypeById(id)
                    def data = ObjectUtils.snakeCaseMapToPojo(serviceType, new ApiLogisticsServiceTypeResponseDto())
                    response.data = data
                    response.message = editRecord + " record updated successfully"
                    response.status = HttpStatus.OK.value()
                } else {
                    response.data = sqlParams
                    response.message = "Failed to edit logistic service type"
                    response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
                }
            } else {
                response.status = HttpStatus.BAD_REQUEST.value()
                response.message = "Record not found!"
            }

        } else {
            errors.add(new FieldErrorDto("userId", "Failed to fetch user details of the user editing logistics service type"))
            response.data = null
            response.message = "Failed to edit logistic service type"
            response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
            response.errors = errors
        }

        sql.close()
        return response
    }

    GeneralApiListResponse getAllLogisticsServiceTypes(Map parameterMap) {
        Sql sql = Sql.newInstance(dataSource)
        GeneralApiListResponse response = new GeneralApiListResponse()
        List<ApiLogisticsServiceTypeResponseDto> logisticsServiceTypesList = new ArrayList<>()
        List<FieldErrorDto> errors = []

        def params = ObjectUtils.flattenListParam(parameterMap)
        def status = params.status
        def sqlParams = [start: 0, limit: 25]

        def query = "SELECT * FROM logistics_service_type"
        def queryFilter = ""
        def limitFilter

        //Pagination parameters & logic
        def page = params.page?.toInteger() ? params.page?.toInteger() : 0
        def size = params.size?.toInteger() ? params.size?.toInteger() : 25
        if (page == 0) {
            sqlParams.limit = 1 * size
            sqlParams.start = sqlParams.limit - size
        } else {
            sqlParams.limit = page * size
            sqlParams.start = sqlParams.limit - size
        }

        limitFilter = " LIMIT ?.limit OFFSET ?.start"
        query += limitFilter

        if (status) {
            if (params.status == 'active' || params.status == 'ACTIVE') {
                sqlParams.status = true
            } else if (params.status == 'inactive' || params.status == 'INACTIVE') {
                sqlParams.status = false
            } else {
                response.status = HttpStatus.BAD_REQUEST.value()
                response.message = "Invalid status value"
                sql.close();
                return response;
            }

            queryFilter = " WHERE is_active = ?.status"
            query += queryFilter
        }


        def data = sql.rows(query, sqlParams)
        String countQuery = "SELECT count(*) FROM logistics_service_type" + queryFilter

        def total
        if (queryFilter) {
            total = sql.firstRow(countQuery, sqlParams)?.get('count')
        } else {
            total = sql.firstRow(countQuery)?.get('count')
        }

        !total ? total = 0 : total

        data.each {
            def apiLogisticsResponse = ObjectUtils.snakeCaseMapToPojo(it, new ApiLogisticsServiceTypeResponseDto()) as ApiLogisticsServiceTypeResponseDto
            logisticsServiceTypesList.add(apiLogisticsResponse)
        }
        if (data) {
            response = new GeneralApiListResponse(logisticsServiceTypesList, total as Long, HttpStatus.OK.value(), "Success")
        } else {
            response = new GeneralApiListResponse(null, total as Long, HttpStatus.OK.value(), "No records found")

        }

        sql.close()
        return response
    }

    BaseApiResponse updateLogisticsServiceTypeStatus(Long logisticsServiceTypeId, ApiStatusRequestDto body, String username) {
        Sql sql = Sql.newInstance(dataSource)

        BaseApiResponse response = new BaseApiResponse(null, 200, "")
        Map params = ObjectUtils.asMap(body);
        Long userId = userService.getUserIdFromUsername(username);

        params.put("serviceTypeId", logisticsServiceTypeId);
        params.put("userId", userId)
        def newStatus = (params.status == 'ACTIVE' || params.status == 'active')
        params.status = newStatus

        if (userId) {
            def recordExist = getServiceTypeById(logisticsServiceTypeId);
            if (recordExist) {
                def update
                sql.withTransaction {
                    update = sql.executeUpdate(
                            """UPDATE logistics_service_type
                        SET is_active = ?.status
                        WHERE id = ?.serviceTypeId
                        """, params)
                }

                def statusMessage = newStatus ? 'activated' : 'deactivated'
                if (update == 1) {
                    def logisticServiceType = getServiceTypeById(logisticsServiceTypeId)
                    def message = "Logistics service type (" + logisticServiceType.name + ") " + statusMessage
                    def logisticServiceTypeResponse = ObjectUtils.snakeCaseMapToPojo(logisticServiceType, new ApiLogisticsServiceTypeResponseDto())
                    response.data = logisticServiceTypeResponse
                    response.status = HttpStatus.OK.value()
                    response.message = message
                } else {
                    response.status = HttpStatus.BAD_REQUEST.value()
                    response.message = "Update failed!!"
                }

            } else {
                response.status = HttpStatus.BAD_REQUEST.value()
                response.message = "Record not found"
            }

        } else {
            response.data = null
            response.message = "Failed to edit logistic service type status"
            response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
        }


        sql.close()
        return response
    }

    Map getServiceTypeById(Long id) {
        Sql sql = Sql.newInstance(dataSource)
        def result = sql.firstRow "SELECT * FROM logistics_service_type WHERE id = ?", id
        sql.close()
        return result
    }


}