package com.tmea.wit.service

import com.tmea.wit.model.BaseApiResponse
import com.tmea.wit.model.GeneralApiListResponse
import com.tmea.wit.model.dto.request.ApiTradeVolumeStatisticPostDto
import com.tmea.wit.model.dto.request.ApiTradeVolumeStatisticUpdate
import com.tmea.wit.model.dto.response.ApiPagedDataResponse
import com.tmea.wit.model.dto.response.ApiTradeVolumeStatisticFetchDto
import com.tmea.wit.util.ObjectUtils
import groovy.sql.Sql
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service

import javax.sql.DataSource

@Service
class TradeVolumeStatisticService {
    private DataSource dataSource
    UserService userService

    @Autowired
    TradeVolumeStatisticService(DataSource dataSource, UserService userService) {
        this.dataSource = dataSource
        this.userService = userService
    }


    //Add new trade volume statistic
    BaseApiResponse addNewTradeVolumeStatistic(ApiTradeVolumeStatisticPostDto tradeVolumeStatisticPostDto, String username) {
        Sql sql = Sql.newInstance(dataSource)

        BaseApiResponse response = new BaseApiResponse()
        Map sqlParams = ObjectUtils.asMap(tradeVolumeStatisticPostDto)
        Long userId = userService.getUserIdFromUsername(username)
        sqlParams.put("userId", userId)
        sqlParams.replace('year', Integer.parseInt(sqlParams.get("year") as String))


        //check if trade statistics record exists
        def tradeVolumeRecord = sql.firstRow("""SELECT *
                                FROM trade_volume_statistic 
                                WHERE 
                                      commodity_id = ?.commodityId
                                      AND trade_operation_id = ?.tradeOperationTypeId
                                      AND from_country = ?.originCountryId
                                      AND to_country = ?.destinationCountryId
                                      AND volume_month = ?.month
                                      AND volume_year = ?.year
                                """, sqlParams)
        if (tradeVolumeRecord) {
            response.status = HttpStatus.BAD_REQUEST.value()
            response.message = "Similar record already exist. Do you wish to update?"
            response.data = ObjectUtils.mapToSnakeCaseMap(tradeVolumeRecord)
        } else {
            sqlParams.put("isActive", true)
            sqlParams.put("addedBy", userId)
            sqlParams.put("measurementUnit", getCommodityTradeStatisticsMeasurementUnit(tradeVolumeStatisticPostDto.commodityId))

            sql.withTransaction {
                def insertRecord = sql.executeInsert("""
                        INSERT INTO trade_volume_statistic(trade_operation_id, commodity_id, from_country, to_country, quantity, is_active, added_by, date_added, volume_month, volume_year,measurement_unit_id) 
                        VALUES(?.tradeOperationTypeId,?.commodityId,?.originCountryId,?.destinationCountryId,?.quantity,?.isActive,?.addedBy,current_timestamp, ?.month,?.year,?.measurementUnit)""", sqlParams)
                if (insertRecord) {
                    Long id = insertRecord.get(0).get(0)

                    response.status = HttpStatus.OK.value()
                    response.data = sqlParams
                    response.message = "Trade volume record added successfully"
                } else {
                    response.status = HttpStatus.BAD_REQUEST.value()
                    response.message = "Failed to insert new reocord"
                }

            }
        }

        sql.close()
        return response

    }

    public BaseApiResponse updateTradeStatisticRecord(ApiTradeVolumeStatisticUpdate tradeVolumeStatisticUpdateDto, Long recordId, String name) {
        Sql sql = Sql.newInstance(dataSource);

        BaseApiResponse response = new BaseApiResponse()
        Map sqlParams = ObjectUtils.asMap(tradeVolumeStatisticUpdateDto);
        Long userId = userService.getUserIdFromUsername(name);
        sqlParams.put("id", recordId);
        sqlParams.put("userId", userId)

        //check if trade volume statistic exist
        //If existing update with new quantity else return record not found

        def tradeVolumeRecord = sql.firstRow("""
                                SELECT * FROM trade_volume_statistic
                                WHERE id = ?.id
                                AND is_active
                                """, sqlParams)

        if (!tradeVolumeRecord) {
            response.status = HttpStatus.NO_CONTENT.value()
            response.message = "Record not found"
        } else {

            Boolean newUpdate = Long.valueOf(tradeVolumeRecord.get("quantity")) != Long.valueOf(sqlParams.get("quantity"));

            if (newUpdate) {
                sql.withTransaction {
                    def deactivateOldRecord = sql.executeUpdate(
                            """UPDATE trade_volume_statistic
                                   SET is_active = false
                                   WHERE id = ?.id""", sqlParams
                    )

                    if (deactivateOldRecord) {
                        tradeVolumeRecord.replace("quantity", sqlParams.get("quantity"))
                        tradeVolumeRecord.replace("added_by", userId)
                        def updateRecord = sql.executeInsert(
                                """INSERT INTO trade_volume_statistic(trade_operation_id, commodity_id, from_country, to_country, quantity, is_active, added_by, date_added, volume_month, volume_year,measurement_unit_id) 
                                        VALUES(?.trade_operation_id, ?.commodity_id, ?.from_country, ?.to_country, ?.quantity, true, ?.added_by, current_timestamp, ?.volume_month,?.volume_year,?.measurement_unit_id)""", tradeVolumeRecord)


                        if (updateRecord) {

                            response.status = HttpStatus.OK.value()
                            response.message = "Record updated successfully"
                        }
                    }

                }

            } else {
                response.status = HttpStatus.BAD_REQUEST.value()
                response.message = "Cannot update record with same quantity value"
            }

        }


        sql.close()
        return response

    }

    public BaseApiResponse fetchTradeVolumeStatistics(Long commodity, Long tradeOperation, Long countryFrom, Long countryTo, Long category, Long subCategory, Integer monthFrom, Integer monthTo, Integer yearFrom, Integer yearTo, String query, String aggregator, Integer page, Integer pageSize) {
        Sql sql = Sql.newInstance(dataSource)

        ApiPagedDataResponse response = new ApiPagedDataResponse(400, "No records found")

        def requestParams = [:]

        def commodityFilter = ""
        if (commodity) {

            commodityFilter = " AND trade_volume_statistic.commodity_id  = ?.commodity "
            requestParams.put("commodity", commodity);

        }


        def tradeOperationFilter = "";
        if (tradeOperation) {
            tradeOperationFilter = " AND trade_volume_statistic.trade_operation_id  = ?.tradeOperation ";
            requestParams.put("tradeOperation", tradeOperation);
        }

        def countryFromFilter = "";
        if (countryFrom) {
            countryFromFilter = " AND trade_volume_statistic.from_country = ?.countryFrom ";
            requestParams.put("countryFrom", countryFrom);
        }

        def countryToFilter = "";
        if (countryTo) {
            countryToFilter = ' AND trade_volume_statistic.to_country = ?.countryTo ';
            requestParams.put("countryTo", countryTo);
        }

        def categoryFilter = "";
        if (category) {
            categoryFilter = " AND commodity_category.id = ?.category ";
            requestParams.put("category", category);
        }

        def subCategoryFilter = "";
        if (subCategory) {
            subCategoryFilter = " AND commodity_sub_category.id = ?.subCategory ";
            requestParams.put("subCategory", subCategory);
        }


        def queryFilter = "";
        if (query) {
            commodityFilter = "";
            categoryFilter = "";
            subCategoryFilter = "";

            String formattedQuery = "%$query%";
            queryFilter = """ AND (
                       commodity.name ILIKE ?.query OR
                       commodity_sub_category.name ILIKE ?.query OR
                       commodity_category.name ILIKE ?.query
                     )
             """;
            requestParams.put("query", formattedQuery);

        }

        def timeRangeFilter = "";

        if (yearFrom && yearTo) {
            if (yearTo >= yearFrom) {
                timeRangeFilter = ' AND trade_volume_statistic."volume_year" BETWEEN ?.yearFrom AND ?.yearTo ';
                requestParams.put("yearFrom", yearFrom);
                requestParams.put("yearTo", yearTo);
            }
        }

        if (monthFrom && monthTo && yearFrom && yearTo) {
            int monthYearFrom = ((yearFrom * 100) * monthFrom);
            int monthYearTo = ((yearTo * 100) * monthTo);

            if (monthYearTo >= monthYearFrom) {
                timeRangeFilter = """ AND (
                ((trade_volume_statistic.volume_year * 100) + trade_volume_statistic.volume_month)
                BETWEEN
                  ?.monthYearFrom
                  AND
                  ?.monthYearTo
                 )""";
            }

            requestParams.put("monthYearFrom", monthYearFrom);
            requestParams.put("monthYearTo", monthYearTo);
        }


        def paginationFilter = ""
        if (page && pageSize) {
            int offset = (page - 1) * pageSize
            int limit = pageSize
            paginationFilter = " LIMIT ?.limit OFFSET ?.offset "
            requestParams.put("limit", limit);
            requestParams.put("offset", offset);
        }

        def aggregatorFilter = ""
        def groupByFilter = ""

        def annualAggregator = ""
        def annualGroupBy = ""
        def commodityAggregator = ""
        def commodityGroupBy = ""
        def quarterAggregator = ""
        def quarterGroupBy = ""
        def tradeOperationAggregator = ""
        def tradeOperationGroupBy = ""
        def categoryAggregator = ""
        def categoryGroupBy = ""
        def subCategoryAggregator = ""
        def subCategoryGroupBy = ""


        if (aggregator) {
            String[] myAggregator = aggregator.split(",");
            Map aggregatorsMap = [:];
            for (int i = 0; i <= myAggregator.size() - 1; i++) {
                aggregatorsMap.put(myAggregator.getAt(i), myAggregator.getAt(i));

            }


            if (aggregatorsMap.get("annual")) {
                annualAggregator = "stats.year,"
                annualGroupBy = "year,"
            }

            if (aggregatorsMap.get("commodity")) {
                commodityAggregator = "stats.commodity,"
                commodityGroupBy = "commodity,"

            }

            if (aggregatorsMap.get("quarter")) {
                quarterAggregator = "stats.quarter,"
                quarterGroupBy = "quarter,"
                annualAggregator = "stats.year,"
                annualGroupBy = "year,"
            }

            if (aggregatorsMap.get("category")) {
                categoryAggregator = "stats.category_name,"
                categoryGroupBy = "category_name,"

            }

            if (aggregatorsMap.get("subCategory")) {
                subCategoryAggregator = "stats.sub_category_name,"
                subCategoryGroupBy = "sub_category_name,"

            }

            if (aggregatorsMap.get("tradeOperation")) {
                tradeOperationAggregator = "stats.trade_operation,"
                tradeOperationGroupBy = "trade_operation,"

            }

            aggregatorFilter = annualAggregator + commodityAggregator + quarterAggregator + categoryAggregator + subCategoryAggregator + tradeOperationAggregator;
            def commaGroupByFilter = annualGroupBy + commodityGroupBy + quarterGroupBy + categoryGroupBy + subCategoryGroupBy + tradeOperationGroupBy;
            groupByFilter = commaGroupByFilter.substring(0, commaGroupByFilter.length() - 1);
        }


        def filterQueryStr = commodityFilter + tradeOperationFilter + countryFromFilter + countryToFilter + subCategoryFilter + categoryFilter + timeRangeFilter + queryFilter + paginationFilter


        def count = sql.firstRow(""" SELECT COUNT(1)
                              
                            FROM commodity,
                                 trade_volume_statistic,
                                 commodity_category,
                                 commodity_sub_category,
                                 country AS from_country,
                                 country AS to_country,
                                 trade_operation
                            WHERE trade_volume_statistic.commodity_id = commodity.id
                                    AND commodity.commodity_sub_category_id = commodity_sub_category.id
                                    AND commodity_sub_category.commodity_category_id = commodity_category.id
                                    AND trade_volume_statistic.from_country = from_country."id"
                                    AND trade_volume_statistic.to_country = to_country."id"
                                    AND trade_volume_statistic.trade_operation_id = trade_operation."id"
                                    AND trade_volume_statistic.is_active
                    """ + filterQueryStr, requestParams);

        int totalRecords = count.get("count");


        def aggregatorQueryStr = """
                            SELECT
                                 commodity.name                    AS commodity,
                                 trade_operation.name                AS trade_operation,
                                 commodity_category.name             As category_name,
                                 commodity_sub_category.name         As sub_category_name,
                                 from_country.name                   AS from_country,
                                 to_country.name                     AS to_country,
                                 trim(TO_CHAR(TO_DATE(volume_month::text, 'MM'), 'Month')) AS "month",
                                 trade_volume_statistic.volume_year  AS "year",
                                 trade_volume_statistic.quantity,
                                 'Quarter.' || extract('quarter' from date(volume_year || '-' || volume_month || '-' || '01'))  AS "quarter"
                            FROM commodity,
                                 trade_volume_statistic,
                                 commodity_category,
                                 commodity_sub_category,
                                 country AS from_country,
                                 country AS to_country,
                                 trade_operation
                            WHERE trade_volume_statistic.commodity_id = commodity.id
                                    AND commodity.commodity_sub_category_id = commodity_sub_category.id
                                    AND commodity_sub_category.commodity_category_id = commodity_category.id
                                    AND trade_volume_statistic.from_country = from_country."id"
                                    AND trade_volume_statistic.to_country = to_country."id"
                                    AND trade_volume_statistic.trade_operation_id = trade_operation."id"
                                    AND trade_volume_statistic.is_active
                                """ + filterQueryStr


        if (aggregator) {

            def tradeVolumeStats = sql.rows("""
                                SELECT COUNT(1),
                                  ${aggregatorFilter}
                                  SUM(stats."quantity"),
                                  COUNT(stats."quantity"),
                                  ROUND(cast(AVG(stats."quantity") as numeric),2) AS AVG
                                  
                                FROM(${aggregatorQueryStr}) AS stats
                                GROUP BY ${groupByFilter}
                                """, requestParams)

            if (tradeVolumeStats) {
                response.status = HttpStatus.OK.value()
                response.message = "success"
                response.total = totalRecords
                response.data = ObjectUtils.mapToSnakeCaseArrayMap(tradeVolumeStats);
            } else {
                response.status = HttpStatus.NO_CONTENT.value()
                response.message = "No records found"
            }

        } else {

            def volumeStats = sql.rows(aggregatorQueryStr, requestParams);


            List<ApiTradeVolumeStatisticFetchDto> tradeVolumeStatDto = ObjectUtils.snakeCaseArrayMapToListPojos(volumeStats, ApiTradeVolumeStatisticFetchDto);


            if (volumeStats) {
                response.status = HttpStatus.OK.value()
                response.message = "success"
                response.data = tradeVolumeStatDto;
                response.total = totalRecords
            } else {
                response.status = HttpStatus.NO_CONTENT.value()
                response.message = "No records found"
            }
        }


        sql.close()
        return response
    }

    public Long getCommodityTradeStatisticsMeasurementUnit(Long commodityId) {
        Sql sql = Sql.newInstance(dataSource);
        Long measurementUnitId = sql.firstRow("SELECT trade_statistics_measurement_unit_id FROM commodity WHERE id = ?", commodityId).get("trade_statistics_measurement_unit_id");
        sql.close();
        return measurementUnitId;
    }

}
