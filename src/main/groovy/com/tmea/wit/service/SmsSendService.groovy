package com.tmea.wit.service

import com.africastalking.AfricasTalking
import com.africastalking.SmsService
import com.africastalking.sms.Recipient
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service


@Service
class SmsSendService {

    @Value('${wit.africastalking.username}')
    private String username;
    @Value('${wit.africastalking.apiKey}')
    private String apiKey;


    public boolean sendSingleSms(String recipient,String message){
        boolean smsStatus = false;



        AfricasTalking.initialize(username, apiKey);
        SmsService sms = AfricasTalking.getService(AfricasTalking.SERVICE_SMS);
        recipient = (recipient.substring(0,1) == '0')?'+254'+recipient.substring(1):recipient;
        String[] recipientsArray = [recipient];
        List<Recipient> response = sms.send(message, recipientsArray, true);
        Recipient rescipientResponse = response.get(0);



        if (rescipientResponse){
            smsStatus = rescipientResponse.status.equalsIgnoreCase("Success");
        }
        return smsStatus;

    }

    public List<Recipient> sendMultipleSms(String recipients,String message){
        AfricasTalking.initialize(username, apiKey);
        SmsService sms = AfricasTalking.getService(AfricasTalking.SERVICE_SMS);
        String[] recipientsArray = recipients.split(",");
        List<Recipient> response = sms.send(message, recipientsArray, true);
        return response;

    }


}
