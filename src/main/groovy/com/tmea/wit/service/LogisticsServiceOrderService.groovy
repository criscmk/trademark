package com.tmea.wit.service

import com.tmea.wit.model.BaseApiResponse
import com.tmea.wit.model.GeneralApiListResponse
import com.tmea.wit.model.dto.FieldErrorDto
import com.tmea.wit.model.dto.response.ApiFetchLogisticsServiceOrderResponseDto
import com.tmea.wit.model.dto.response.ApiLocationResponseDto
import com.tmea.wit.util.CommonDBFunctions
import com.tmea.wit.util.ObjectUtils
import groovy.sql.Sql
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service

import javax.sql.DataSource

@Service
class LogisticsServiceOrderService {
    @Autowired
    DataSource dataSource
    @Autowired
    UserService userService
    @Autowired
    GlobalConfigService globalConfigService
    @Autowired
    CommonDBFunctions commonDBFunctions


    public GeneralApiListResponse getLogisticsServiceOrder(Map parameterMap, String currentUser) {
        Sql sql = new Sql(dataSource)
        GeneralApiListResponse response = new GeneralApiListResponse();
        List<ApiLocationResponseDto> locationList = new ArrayList<>()
        List<FieldErrorDto> errors = []
        def params = ObjectUtils.flattenListParam(parameterMap)
        def status = params.status
        def queryParams = params
        def statusFilterQuery = ""
        def query = params.query
        def page = params.get("page")
        def size = params.get("size")
        def counterStatusQuery = ""
        def providerFilter = ""
        def queryFilter = ""
        def searchFilter = ""
        def orderedByFilter = ""
        def logisticsServiceFilter = ""
        def currentUserFilter = ""

        //filter by current loggedIn in user
        if (currentUser) {
            Long userId = userService.getUserIdFromUsername(currentUser)
            queryParams.put("userId", userId)
            currentUserFilter = " AND logistics_service.provider_user_id = ?.userId"
        }
        //Admin filter by specific provider
        if (!currentUser && params.providerId) {
            providerFilter = " AND logistics_service.provider_user_id = ?.providerId::BigInt"
        }
        // filter orderedBy
        if (params.orderedById) {
            orderedByFilter = " AND logistics_service_order_request.ordered_by = ?.orderedById::BigInt"
        }
        //filter by logistics service
        if (params.logisticsServiceId) {
            logisticsServiceFilter = " AND logistics_service_order_request.logistics_service_id = ?.logisticsServiceId::Bigint"
        }
        //search by name / description
        if (query) {
            String querySearch = "%$query%"
            queryParams.put("query", querySearch)
            searchFilter = """ AND (
                                    logistics_service.name ILIKE ?.query OR
                                    logistics_service.description ILIKE ?.query 
                                   
                                  )
                          """
        }
        def logisticsServiceOrderRequest=""
        if(params.logisticsServiceOrderRequestId)
        {
            logisticsServiceOrderRequest=" AND logistics_service_order.logistics_service_order_request_id =?.logisticsServiceOrderRequestId::BigInt "
        }
        def paymentFilter=""
        if(params.isPaymentComplete)
        {
            paymentFilter = " AND logistics_service_order.is_payment_complete =?.isPaymentComplete::Boolean"
        }

        //filter by status
        if (status) {
            def statusParam = params.status.toLowerCase()
            if (statusParam == 'pending' || statusParam == 'dispatched' || statusParam == 'cancelled') {
                def filterStatus = getLogisticsServiceStatusByName(status).get("id")
                if (filterStatus) {
                    queryParams.put("statusId", filterStatus)
                    statusFilterQuery = " AND logistics_service_order.logistics_service_order_status_id = ?.statusId "

                }
            } else {
                response.setMessage("Invalid status value")
                response.setData(null)
                response.setStatus(HttpStatus.BAD_REQUEST.value())
                sql.close()
                return response
            }

        }


        //pagination
        Map paginateParams = [start: 0, limit: 25]
        def paginate = commonDBFunctions.paginationSqlParams(paginateParams, params)
        def paginationFilter = " LIMIT ?.limit OFFSET ?.start"
        if (page && size) {
            queryParams = queryParams + paginate
        }
        queryFilter = statusFilterQuery + providerFilter + paymentFilter + orderedByFilter + providerFilter + searchFilter + currentUserFilter +logisticsServiceFilter + logisticsServiceOrderRequest

        def queryLogisticsOrdersQuery = getLogisticsOrder() + queryFilter + " ORDER BY id DESC" + paginationFilter

        String countQuery = """SELECT count(*) 
                                       FROM
                                       logistics_service_order,
                                        logistics_service,
                                        logistics_service_order_status,
                                        logistics_service_order_request,
                                         user_individual_detail,
                                         "user"
                                         WHERE 
                                        logistics_service_order.logistics_service_order_request_id = logistics_service_order_request.id
                                        AND
                                        logistics_service_order_request.logistics_service_id = logistics_service.id
                                        AND
                                        logistics_service_order.logistics_service_order_status_id = logistics_service_order_status.id
                                        AND
                                        logistics_service_order.logistics_service_order_request_id = logistics_service_order_request.id
                                         AND
                                         logistics_service_order_request.ordered_by = "user".id
                                         AND 
                                         user_individual_detail.user_id = "user".id
                                    
                                        """
        def total
        if (queryFilter) {
            total = sql.firstRow(countQuery + queryFilter, queryParams)?.get('count')
        } else {
            total = sql.firstRow(countQuery)?.get('count')
        }
        response.setTotal(total)
        def allLogisticsServiceOrder = sql.rows(queryLogisticsOrdersQuery, queryParams)


        if (allLogisticsServiceOrder) {

            response.setData(ObjectUtils.snakeCaseArrayMapToListPojos(allLogisticsServiceOrder, ApiFetchLogisticsServiceOrderResponseDto))
            response.setStatus(HttpStatus.OK.value())
            response.setMessage("Records found")
        } else {
            response.setData(null)
            response.setStatus(HttpStatus.NO_CONTENT.value())
            response.setMessage("No records found")
        }
        sql.close()
        response

    }

    public BaseApiResponse getLogisticsServiceOrderRequestById(Long requestId, String username) {
        Sql sql = new Sql(dataSource)
        BaseApiResponse response = new BaseApiResponse(null, 400, "")
        List<FieldErrorDto> errors = []
        Long userId = userService.getUserIdFromUsername(username)
        def queryParams = [:]
        queryParams.put("userId", userId)
        queryParams.put("orderId", requestId)

        if (requestId) {
            def filterQuery = " AND logistics_service_order.id = ?.orderId "
            def getLogisticOrderbyid = getLogisticsOrder() + filterQuery
            def allLogisticsOrderService = sql.firstRow(getLogisticOrderbyid, queryParams)
            if (allLogisticsOrderService) {
                def requestResponse = ObjectUtils.snakeCaseMapToPojo(allLogisticsOrderService, new ApiFetchLogisticsServiceOrderResponseDto())
                response.setStatus(HttpStatus.OK.value())
                response.setData(requestResponse)
                response.setMessage("Record found")
            } else {
                response.setStatus(HttpStatus.NO_CONTENT.value())
                response.setData(null)
                response.setMessage("No Record found")
            }

        } else {
            response.setMessage("Request Id is required")
            response.setStatus(HttpStatus.BAD_REQUEST.value())

        }
        sql.close()
        response

    }

    public BaseApiResponse updateLogisticsServiceOrderStatus(def body, Long logisticsServiceOrderId, String userName) {
        BaseApiResponse response = new BaseApiResponse(null, 200, "")
        Sql sql = new Sql(dataSource)
        def params = ObjectUtils.asMap(body)
        Long userId = userService.getUserIdFromUsername(userName)
        def queryParams = [:]
        def status = params.status
        status = status.toLowerCase()
        def updatedStatus = getLogisticsServiceStatusByName(status).get("id")
        def orderExists = getLogisticsServiceOrderById(logisticsServiceOrderId)
        queryParams.put("updatedStatus", updatedStatus)
        queryParams.put("orderId", logisticsServiceOrderId)
        //check if order exists
        if (orderExists) {
            //check if status exists
            if (updatedStatus) {
                def updateStatusQuery = sql.executeUpdate("""UPDATE logistics_service_order SET logistics_service_order_status_id = ?.updatedStatus WHERE id=?.orderId""", queryParams)
                if (updateStatusQuery) {
                    def updatedRecord = getLogisticsServiceOrderById(logisticsServiceOrderId)
                    response.status = HttpStatus.OK.value()
                    response.setData(updatedRecord)
                    response.setMessage("Order have been " + status + " successfully")
                } else {
                    response.status = HttpStatus.INTERNAL_SERVER_ERROR.value()
                    response.setData(null);
                    response.setMessage("Record not updated")
                }
            } else {
                response.status = HttpStatus.BAD_REQUEST.value()
                response.setData(null);
                response.setMessage("Status" + status + "does not exists")
                return response
            }

        } else {
            response.status = HttpStatus.BAD_REQUEST.value()
            response.setData(null);
            response.setMessage("Order number does not exists")
            return response
        }


        response

    }

    public BaseApiResponse cancelLogisticsServiceOrder(def body, Long logisticsServiceOrderId, String userName) {
        BaseApiResponse response = new BaseApiResponse()
        Sql sql = new Sql(dataSource)
        def params = ObjectUtils.asMap(body)
        Long userId = userService.getUserIdFromUsername(userName)
        def queryParams = [:]
        def status = "cancelled"
        queryParams.put("reason", params.reason)
        queryParams.put("userId", userId)
        queryParams.put("serviceId", logisticsServiceOrderId)
        response.setData(queryParams)
        def updatedStatus = getLogisticsServiceStatusByName(status).get("id")
        def orderExists = getLogisticsServiceOrderById(logisticsServiceOrderId)
        def orderIsCancelled = getLogisticsServiceCancelledOrderById(logisticsServiceOrderId)
        queryParams.put("updatedStatus", updatedStatus)
        queryParams.put("orderId", logisticsServiceOrderId)
        if (orderExists) {
            //check if order has been paid
            if (!orderExists.get("is_payment_complete")) {

            //check if the order is cancelled
            if (!orderIsCancelled) {
                sql.withTransaction {
                    if (updatedStatus) {
                        //update the order status to cancelled
                        def updateStatusQuery = sql.executeUpdate("""UPDATE logistics_service_order SET logistics_service_order_status_id = ?.updatedStatus WHERE id=?.orderId""", queryParams)
                        if (updateStatusQuery) {
                            //deActivate order
                            //sql.executeUpdate("""UPDATE logistics_service_order SET is_active = FALSE WHERE id=?.orderId""", queryParams)

                            //insert to order to cancelled orders
                            def cancelledOrder = sql.executeInsert("""INSERT INTO logistics_service_order_cancellation 
                                                                  (logistics_service_order_id, cancelled_by, reason_for_cancellation, date_cancelled)
                                                                  VALUES (?.serviceId, ?.userId, ?.reason ,CURRENT_TIMESTAMP)""", queryParams)
                            if (cancelledOrder) {

                                def cancelledOrderData = getLogisticsServiceOrderById(logisticsServiceOrderId)
                                response.setData(cancelledOrderData)
                                response.status = HttpStatus.OK.value()
                                response.setMessage("Order cancelled successfully")
                            } else {
                                response.setMessage("Order could not be cancelled")
                            }

                        } else {
                            response.setMessage("Order could not be cancelled")
                        }

                    } else {
                        response.status = HttpStatus.BAD_REQUEST.value()
                        response.setData(null);
                        response.setMessage("Status" + status + "does not exists")
                        sql.close()
                        return response
                    }
                }
            } else {
                response.setMessage("Order have already been cancelled")
                response.status=HttpStatus.BAD_REQUEST.value()
                return response
            }
        }else{
                response.status = HttpStatus.BAD_REQUEST.value()
                response.setData(orderExists);
                response.setMessage("Order could not be cancelled because payment is completed ")
                return response
            }


        } else {
            response.status = HttpStatus.BAD_REQUEST.value()
            response.setData(null);
            response.setMessage("Order number " + logisticsServiceOrderId + " order does not exists")
            return response
        }
        sql.close()
        response


    }

    public def getLogisticsServiceStatusByName(String statusName) {

        Sql sql = new Sql(dataSource)
        def vehicle = sql.firstRow "SELECT * FROM logistics_service_order_status WHERE is_active=TRUE AND name = ?", statusName
        sql.close()
        vehicle
    }

    public def getLogisticsServiceStatusById(Long statusId) {
        Sql sql = new Sql(dataSource)
        def vehicle = sql.firstRow "SELECT * FROM logistics_service_order_status WHERE is_active=TRUE AND id = ?", statusId
        sql.close()
        vehicle
    }

    public def getLogisticsServiceOrderById(Long orderRequestId) {
        Sql sql = new Sql(dataSource)
        def orderRequest = sql.firstRow "SELECT * FROM logistics_service_order WHERE is_active=TRUE AND   id = ?", orderRequestId
        sql.close()
        orderRequest
    }

    public def getLogisticsServiceCancelledOrderById(Long orderRequestId) {
        Sql sql = new Sql(dataSource)
        def orderRequest = sql.firstRow "SELECT * FROM logistics_service_order_cancellation WHERE   logistics_service_order_id = ?", orderRequestId
        sql.close()
        orderRequest
    }

    public String getLogisticsOrder() {
        def getAllLogisticsServiceOrdersRequest = """
                                         SELECT
                                         logistics_service_order.id,
                                         logistics_service_order.is_active,
                                         logistics_service_order.is_payment_complete,
                                         logistics_service_order.order_number,
                                         logistics_service.name AS ordered_service,
                                         logistics_service.id AS logistics_service_id,
                                         logistics_service.provider_user_id AS logistics_service_provider_id,
                                         logistics_service_order.price,
                                         logistics_service.id as logistics_service_id,
                                         logistics_service_order.date_added as date_approved,
                                         logistics_service_order_status.name AS order_status,
                                         "user".phone_number AS requested_by_phone,
                                         "user".email AS requested_by_email,
                                        CASE
                                        WHEN "user".user_type_id = 1 THEN (SELECT user_individual_detail.first_name||' '||user_individual_detail.middle_name||' '||user_individual_detail.last_name  FROM user_individual_detail WHERE "user".id = user_individual_detail.user_id)
                                        WHEN "user".user_type_id = 2 THEN (SELECT user_organization_detail.organization_name FROM user_organization_detail WHERE "user".id = user_organization_detail.user_id)
                                        END AS ordered_by_name
                                        FROM 
                                        logistics_service_order,
                                        logistics_service,
                                        logistics_service_order_status,
                                        logistics_service_order_request,
                                        "user"
                                         WHERE 
                                        logistics_service_order.logistics_service_order_request_id = logistics_service_order_request.id
                                        AND
                                        logistics_service_order_request.logistics_service_id = logistics_service.id
                                        AND
                                        logistics_service_order.logistics_service_order_status_id = logistics_service_order_status.id
                                      
                                        AND 
                                         logistics_service_order_request.ordered_by = "user".id
                                        
                                      
                    """
    }
}
