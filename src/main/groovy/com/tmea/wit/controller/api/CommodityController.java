package com.tmea.wit.controller.api;

import com.tmea.wit.model.BaseApiResponse;
import com.tmea.wit.model.GeneralApiListResponse;
import com.tmea.wit.model.dto.request.ApiCommodityRequestDto;
import com.tmea.wit.model.dto.request.ApiImageUploadRequestDto;
import com.tmea.wit.model.dto.request.ApiStatusRequestDto;
import com.tmea.wit.service.CommodityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.security.Principal;

@RestController
@RequestMapping("api/commodity")
@Validated
public class CommodityController {

    CommodityService commodityService;


    @Autowired
    public CommodityController(CommodityService commodityService) {
        this.commodityService = commodityService;
    }

    @GetMapping
    public GeneralApiListResponse getAllCommodities(WebRequest request,
                                                    @Min(0) Long page,
                                                    @Min(2) Long size){
        return commodityService.getCommodities(request.getParameterMap(),false);
    }
   @GetMapping("admin")
    public GeneralApiListResponse getAdminCommodities(WebRequest request,
                                                    @Min(0) Long page,
                                                    @Min(2) Long size){
        return commodityService.getCommodities(request.getParameterMap(),true);
    }

    @PostMapping
    public BaseApiResponse addCommodity(@RequestBody @Valid ApiCommodityRequestDto body,
                                        Principal principal){
        return commodityService.addCommodity(body, principal.getName());
    }

    @PutMapping("{commodityId}")
    public BaseApiResponse updateCommodity(@RequestBody @Valid ApiCommodityRequestDto body,
                                           @PathVariable @Min(1) Long commodityId, Principal principal){
        return commodityService.updateCommodity(commodityId, body, principal.getName());
    }

    @PutMapping("status/{commodityId}")
    public BaseApiResponse updateCommodityStatus(@RequestBody @Valid ApiStatusRequestDto body,
                                                 @PathVariable @Min(1) Long commodityId){
        return commodityService.updateCommodityStatus(commodityId, body);
    }

    @PutMapping("image/{commodityId}")
    public BaseApiResponse updateCommodityImage(@RequestBody @Valid ApiImageUploadRequestDto body,
                                                @PathVariable @Min(1) Long commodityId,
                                                Principal principal){
        return commodityService.updateCommodityImage(commodityId, body, principal.getName());
    }
}
