package com.tmea.wit.controller.api;

import com.tmea.wit.model.BaseApiResponse;
import com.tmea.wit.model.GeneralApiListResponse;
import com.tmea.wit.model.dto.request.ApiLogisticsServiceTypesRequestDto;
import com.tmea.wit.model.dto.request.ApiStatusRequestDto;
import com.tmea.wit.service.LogisticsServiceTypesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.security.Principal;

@RestController
@RequestMapping("/api/logistics-service-types")
@Validated
public class LogisticsServiceTypesController {

    @Autowired
    LogisticsServiceTypesService logisticsServiceTypesService;

    //Add new service type
    @PostMapping("")
    public BaseApiResponse addServiceType(@Valid @RequestBody ApiLogisticsServiceTypesRequestDto apiLogisticsServiceTypesRequestDto, Principal principal) {
        return logisticsServiceTypesService.addLogisticsServiceType(apiLogisticsServiceTypesRequestDto, principal.getName());
    }

    //Update logistics service type
    @PutMapping("/{logisticsServiceTypeId}")
    public BaseApiResponse updateTradeVolumeRecord(@Valid @RequestBody ApiLogisticsServiceTypesRequestDto apiLogisticsServiceTypesRequestDto,
                                                   @PathVariable("logisticsServiceTypeId")
                                                   @Valid @NotNull(message = "Logistics service type id cannot be null")
                                                   @Max(Long.MAX_VALUE)
                                                   @Min(1L) Long id, Principal principal) {
        return logisticsServiceTypesService.editLogisticServiceType(apiLogisticsServiceTypesRequestDto, id, principal.getName());
    }

    //Activate/Deactivate logistics service type
    @PutMapping("/status/{logisticsServiceTypeId}")
    public BaseApiResponse updateCommodityStatus(@RequestBody @Valid ApiStatusRequestDto body,
                                                 @PathVariable @Min(1) Long logisticsServiceTypeId,
                                                 Principal principal) {
        return logisticsServiceTypesService.updateLogisticsServiceTypeStatus(logisticsServiceTypeId, body, principal.getName());
    }

    //Fetch all logistics service types
    @GetMapping("")
    public GeneralApiListResponse getAllLogisticsServiceTypes(WebRequest request) {
        return logisticsServiceTypesService.getAllLogisticsServiceTypes(request.getParameterMap());
    }
}
