package com.tmea.wit.controller.api;

import com.tmea.wit.model.BaseApiResponse;
import com.tmea.wit.model.GeneralApiListResponse;
import com.tmea.wit.model.dto.request.ApiAddChargingModeRequestDto;
import com.tmea.wit.model.dto.request.ApiAddTradeOperationRequestDto;
import com.tmea.wit.service.ChargingModeService;
import com.tmea.wit.service.TradeOperationService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import javax.validation.Valid;
import javax.validation.constraints.Min;

@RestController
@RequestMapping("/api/charging-mode")
@Validated
public class ChargingModeController {
    ChargingModeService chargingModeService;
    public ChargingModeController(ChargingModeService chargingModeService){
        this.chargingModeService = chargingModeService;
    }
    @GetMapping("")
    public GeneralApiListResponse fetchAll(WebRequest request,
                                           @Min(0) Long page,
                                           @Min(2) Long size){
        return chargingModeService.getChargingMode(request.getParameterMap());
    }
    @PostMapping
    public BaseApiResponse addTradeOperation(@RequestBody @Valid ApiAddChargingModeRequestDto body){
        return chargingModeService.addChargingMode(body);
    }

}
