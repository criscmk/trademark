package com.tmea.wit.controller.api;

import com.tmea.wit.model.BaseApiResponse;
import com.tmea.wit.model.GeneralApiListResponse;
import com.tmea.wit.model.dto.request.*;
import com.tmea.wit.service.LogisticsServiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.security.Principal;

@RestController
@RequestMapping("/api/logistic-service")
@Validated
public class LogisticsServiceController {
    @Autowired
    LogisticsServiceService logisticsServiceService;

    //add new service
    @PostMapping("")
    public BaseApiResponse addNewService(@Valid @RequestBody ApiLogisticServicePostDto body, Principal principal) {
        return logisticsServiceService.addNewService(body, principal.getName());
    }

    @GetMapping("")
    public GeneralApiListResponse getAllLogisticsServices(WebRequest request) {
        return logisticsServiceService.getAllLogisticsServices(request.getParameterMap(),false);
    }

    @GetMapping("/details/{id}")
    public BaseApiResponse getServiceDetails(@PathVariable("id")
                                             @Valid @NotNull(message = "Service id cannot be null")
                                             @Min(1) Long id) {
        return logisticsServiceService.getServiceDetails(id);
    }

    @PutMapping("/update/{serviceId}")
    public BaseApiResponse editService(@Valid @RequestBody ApiLogisticsServiceUpdatePostDto body,
                                       @PathVariable("serviceId")
                                       @Valid @NotNull(message = "Service id cannot be null")
                                       @Min(1) Long id,
                                       Principal principal) {
        return logisticsServiceService.editService(body, id, principal.getName());
    }

    @PutMapping("/update-service-tariff/{serviceId}")
    public BaseApiResponse editServiceTariff(@Valid @RequestBody ApiLogisticServiceTariffPostDto body,
                                       @PathVariable("serviceId")
                                       @Valid @NotNull(message = "Service id cannot be null")
                                       @Min(1) Long id,
                                       Principal principal) {
        return logisticsServiceService.editTariff(body, id, principal.getName());
    }


    @GetMapping("/current-user")
    public GeneralApiListResponse getAllCurrentUserLogisticsServices(WebRequest request) {
        return logisticsServiceService.getAllLogisticsServices(request.getParameterMap(),true);
    }

    @GetMapping("/service-cost")
    public BaseApiResponse calculateServiceCost(WebRequest request) {
        return logisticsServiceService.calculateEstimateServiceCost(request.getParameterMap());
    }

    @GetMapping("/added-charges/{id}")
    public GeneralApiListResponse getServiceAddedCharges(@PathVariable("id")
                                                         @Valid @NotNull(message = "Service id cannot be null")
                                                         @Min(1) Long id) {
        return logisticsServiceService.fetchServiceAddedCharges(id);
    }

    @GetMapping("/service-tariff/{id}")
    public BaseApiResponse getServiceTariff(@PathVariable("id")
                                            @Valid @NotNull(message = "Service id cannot be null")
                                            @Min(1) Long id) {
        return logisticsServiceService.fetchServiceTariff(id);
    }

    @PutMapping("/remove-added-charges/{id}")
    public BaseApiResponse removeServiceAddedCharges(@PathVariable("id")
                                                     @Valid @NotNull(message = "Added charge id cannot be null")
                                                     @Min(1) Long id) {
        return logisticsServiceService.removeAddedCharges(id);
    }

    @PutMapping("/add-additional-charges/{serviceId}")
    public BaseApiResponse addServiceAdditionalCharges(@Valid @RequestBody ApiLogisticServiceAddedChargesPostDto body,
                                                       @PathVariable("serviceId")
                                                       @Valid @NotNull(message = "Service id cannot be null")
                                                       @Min(1) Long id, Principal principal) {
        return logisticsServiceService.addAdditionalCharge(body, id, principal.getName());
    }

    @PutMapping("/status/{serviceId}")
    public BaseApiResponse updateVehicleStatus(@RequestBody @Valid ApiStatusRequestDto body,
                                               @PathVariable
                                               @Valid @NotNull(message = "Service id cannot be null")
                                               @Min(1) Long serviceId,
                                               Principal principal) {
        return logisticsServiceService.updateServiceStatus(body, serviceId, principal.getName());
    }
}
