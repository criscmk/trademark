package com.tmea.wit.controller.api;

import com.tmea.wit.model.BaseApiResponse;
import com.tmea.wit.model.dto.request.ApiAddRegulationProcedureRequestDto;
import com.tmea.wit.model.dto.request.ApiRegulationProcedureRequirementRequestDto;
import com.tmea.wit.model.dto.request.ApiStatusRequestDto;
import com.tmea.wit.model.dto.request.ApiUpdateRegulationProcedureRequirementRequestDto;
import com.tmea.wit.service.RegulationProcedureRequirementService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.security.Principal;

@RestController
@RequestMapping("api/regulation-procedure-requirement")
@Validated
public class RegulationProcedureRequirementController {
    RegulationProcedureRequirementService regulationProcedureRequirementService;
    RegulationProcedureRequirementController(RegulationProcedureRequirementService regulationProcedureRequirementService)
    {
        this.regulationProcedureRequirementService = regulationProcedureRequirementService;
    }
    @PostMapping
    public BaseApiResponse addRegulatoryProcedure(@RequestBody @Valid ApiRegulationProcedureRequirementRequestDto body,
                                                  Principal principal){
        return regulationProcedureRequirementService.addRegulationProcedureRequirement(body, principal.getName());
    }
    @PutMapping("{id}")
    public BaseApiResponse editRegulationProcedureRequirement(@Valid @RequestBody ApiRegulationProcedureRequirementRequestDto body,
                                                   @PathVariable("id")
                                                   @Valid @NotNull(message = "regulation procedure requirement id cannot be null")
                                                   @Min(1) Long id,
                                                   Principal principal) {
        return regulationProcedureRequirementService.editRegulationProcedureRequirement(body, id, principal.getName());
    }
    @PutMapping("status/{id}")
    public BaseApiResponse updateRegulationProcedureRequirementStatus(@RequestBody @Valid ApiStatusRequestDto body,
                                                           @PathVariable @Min(1)
                                                           @Valid @NotNull(message = "regulation procedure requirement id cannot be null") Long id) {
        return regulationProcedureRequirementService.updateRegulationProcedureRequirementStatus(id, body);
    }

}
