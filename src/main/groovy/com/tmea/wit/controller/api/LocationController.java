package com.tmea.wit.controller.api;

import com.tmea.wit.model.BaseApiResponse;
import com.tmea.wit.model.GeneralApiListResponse;
import com.tmea.wit.model.dto.request.ApiLocationRequestDto;
import com.tmea.wit.model.dto.request.ApiStatusRequestDto;
import com.tmea.wit.service.LocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import javax.validation.Valid;
import javax.validation.constraints.Min;

@RestController
@RequestMapping("api/location")
@Validated
public class LocationController {

    LocationService locationService;

    @Autowired
    public LocationController(LocationService locationService) {
        this.locationService = locationService;
    }

    @GetMapping("")
    public GeneralApiListResponse getLocations(WebRequest request,
                                               @Min(0) Long page,
                                               @Min(2) Long size,
                                               @Min(1) Long levelId){
        return locationService.getLocations(request.getParameterMap(),false);
    }
    @GetMapping("admin")
    public GeneralApiListResponse adminGetLocations(WebRequest request,
                                               @Min(0) Long page,
                                               @Min(2) Long size,
                                               @Min(1) Long levelId){
        return locationService.getLocations(request.getParameterMap(),true);
    }

    @PostMapping
    public BaseApiResponse addLocation(@RequestBody @Valid ApiLocationRequestDto body){
        return locationService.addLocation(body);
    }

    @PutMapping("{locationId}")
    public BaseApiResponse updateLocation(@RequestBody @Valid ApiLocationRequestDto body,
                                                @PathVariable @Min(1) Long locationId){
        return locationService.updateLocation(locationId, body);
    }

    @PutMapping("/status/{locationId}")
    public BaseApiResponse updateLocationStatus(@PathVariable Long locationId,
                                                @RequestBody @Valid ApiStatusRequestDto body ){
        return locationService.updateLocationStatus(locationId, body);
    }
}
