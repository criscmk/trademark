package com.tmea.wit.controller.api;

import com.tmea.wit.model.BaseApiResponse;
import com.tmea.wit.model.GeneralApiListResponse;
import com.tmea.wit.model.dto.request.ApiEditDocumentInfoPostDto;
import com.tmea.wit.model.dto.request.ApiStatusRequestDto;
import com.tmea.wit.model.dto.request.ApiTradeDocumentInfoPostDto;
import com.tmea.wit.service.TradeDocumentInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.security.Principal;

@RestController
@RequestMapping("api/trade-document-info/")
@Validated
public class TradeDocumentInformationController {

    @Autowired
    TradeDocumentInfoService tradeDocumentInfoService;

    @PostMapping("")
    public BaseApiResponse addTradeDocumentInfo(@Valid @RequestBody ApiTradeDocumentInfoPostDto body, Principal principal) {
        return tradeDocumentInfoService.addDocumentInfo(body, principal.getName());
    }

    @PutMapping("{id}")
    public BaseApiResponse editTradeDocumentInfo(@Valid @RequestBody ApiEditDocumentInfoPostDto body,
                                                 @PathVariable("id")
                                                 @Valid @NotNull(message = "Trade document info id cannot be null")
                                                 @Min(1) Long id,
                                                 Principal principal) {
        return tradeDocumentInfoService.editDocumentInfo(body, id, principal.getName());
    }

    @GetMapping("admin")
    public GeneralApiListResponse adminFetchAllDocumentsInfo(WebRequest webRequest) {
        return tradeDocumentInfoService.getAllTradeDocumentInfo(webRequest.getParameterMap(), true);
    }

    @GetMapping("")
    public GeneralApiListResponse fetchAllDocumentsInfo(WebRequest webRequest) {
        return tradeDocumentInfoService.getAllTradeDocumentInfo(webRequest.getParameterMap(), false);
    }

    @GetMapping("admin/{id}")
    public GeneralApiListResponse adminFetchDocumentInfoSampleFiles(@PathVariable("id")
                                                                    @Valid @NotNull(message = "Trade document info id cannot be null")
                                                                    @Min(1) Long id,
                                                                    WebRequest webRequest) {
        return tradeDocumentInfoService.getAllTradeDocInfoSampleDocs(id, webRequest.getParameterMap(), true);
    }

    @GetMapping("{id}")
    public GeneralApiListResponse fetchDocumentInfoSampleFiles(@PathVariable("id")
                                                               @Valid @NotNull(message = "Trade document info id cannot be null")
                                                               @Min(1) Long id,
                                                               WebRequest webRequest) {
        return tradeDocumentInfoService.getAllTradeDocInfoSampleDocs(id, webRequest.getParameterMap(), false);
    }

    @PutMapping("update-doc-info-status/{id}")
    public BaseApiResponse updateDocInfoStatus(@Valid @RequestBody ApiStatusRequestDto body,
                                               @PathVariable("id")
                                               @Valid @NotNull(message = "Trade document info id cannot be null")
                                               @Min(1) Long id,
                                               Principal principal) {
        return tradeDocumentInfoService.activateDeactivateRecord(id, body, principal.getName(), false);
    }

    @PutMapping("update-sample-doc-status/{id}")
    public BaseApiResponse updateSampleFilesStatus(@Valid @RequestBody ApiStatusRequestDto body,
                                               @PathVariable("id")
                                               @Valid @NotNull(message = "Sample document id cannot be null")
                                               @Min(1) Long id,
                                               Principal principal) {
        return tradeDocumentInfoService.activateDeactivateRecord(id, body, principal.getName(), true);
    }
}
