package com.tmea.wit.controller.api;

import com.tmea.wit.model.BaseApiResponse;
import com.tmea.wit.model.dto.request.ApiPasswordResetDto;
import com.tmea.wit.model.dto.request.ApiPasswordResetRequestDto;
import com.tmea.wit.service.PasswordResetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/password")
public class PasswordController {

    @Autowired
    PasswordResetService passwordResetService;

    @PostMapping("/request/reset")
    public BaseApiResponse requestPasswordReset(@RequestBody @Valid ApiPasswordResetRequestDto apiPasswordResetRequestDto){
        return passwordResetService.requestPhonePasswordReset(apiPasswordResetRequestDto);
    }

    @PostMapping("/reset")
    public BaseApiResponse resetPassword(@RequestBody @Valid ApiPasswordResetDto apiPasswordResetDto){

        return passwordResetService.resetPhonePassword(apiPasswordResetDto);
    }
}
