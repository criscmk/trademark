package com.tmea.wit.controller.api;

import com.tmea.wit.model.BaseApiResponse;
import com.tmea.wit.model.GeneralApiListResponse;
import com.tmea.wit.model.dto.request.ApiStatusRequestDto;
import com.tmea.wit.model.dto.request.ApiTaxTypeRequestDto;
import com.tmea.wit.service.TaxTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import javax.validation.Valid;
import javax.validation.constraints.Min;

@RestController
@RequestMapping("api/tax-type")
@Validated
public class TaxTypeController {

    TaxTypeService taxTypeService;

    @Autowired
    public TaxTypeController(TaxTypeService taxTypeService) {
        this.taxTypeService = taxTypeService;
    }

    @GetMapping
    public GeneralApiListResponse getTaxTypes(WebRequest request,
                                                   @Min(0) Long page,
                                                   @Min(2) Long size){
        return taxTypeService.getTaxTypes(request.getParameterMap());
    }

    @PostMapping
    public BaseApiResponse addTaxType(@RequestBody @Valid ApiTaxTypeRequestDto body){
        return taxTypeService.addTaxType(body);
    }

    @PutMapping("{taxTypeId}")
    public BaseApiResponse updateTaxType(@RequestBody @Valid ApiTaxTypeRequestDto body,
                                          @PathVariable @Min(1) Long taxTypeId){
        return taxTypeService.updateTaxType(taxTypeId, body);
    }

    @PutMapping("status/{taxTypeId}")
    public BaseApiResponse updateTaxTypeStatus(@RequestBody @Valid ApiStatusRequestDto body,
                                                @PathVariable @Min(1) Long taxTypeId){
        return taxTypeService.updateTaxTypeStatus(taxTypeId, body);
    }
}
