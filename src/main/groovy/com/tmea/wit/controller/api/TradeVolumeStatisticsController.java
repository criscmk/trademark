package com.tmea.wit.controller.api;

import com.tmea.wit.model.BaseApiResponse;
import com.tmea.wit.model.dto.request.ApiTradeVolumeStatisticPostDto;
import com.tmea.wit.model.dto.request.ApiTradeVolumeStatisticUpdate;
import com.tmea.wit.service.TradeVolumeStatisticService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.security.Principal;

@RestController
@RequestMapping("/api/trade-volume-statistic")
@Validated
public class TradeVolumeStatisticsController {

    @Autowired
    TradeVolumeStatisticService tradeVolumeStatisticService;

    //Create new trade volume statistic
    @PostMapping("")
    public BaseApiResponse createNewStatisticRecord(@Valid @RequestBody ApiTradeVolumeStatisticPostDto apiTradeVolumeStatisticPostDto, Principal principal) {
        return tradeVolumeStatisticService.addNewTradeVolumeStatistic(apiTradeVolumeStatisticPostDto, principal.getName());
    }

    //Update trade volume statistic record
    @PutMapping("/{id}")
    public BaseApiResponse updateTradeVolumeRecord(@Valid @RequestBody ApiTradeVolumeStatisticUpdate apiTradeVolumeStatisticUpdate, @PathVariable("id") @Valid @NotNull(message = "Trade statistic id cannot be null")
    @Max(Long.MAX_VALUE)
    @Min(1L) Long id, Principal principal) {
        return tradeVolumeStatisticService.updateTradeStatisticRecord(apiTradeVolumeStatisticUpdate, id, principal.getName());
    }

    @GetMapping("")
    public BaseApiResponse getTradeStatistic(@RequestParam(required = false) @Max(Long.MAX_VALUE) Long commodity,
                                             @RequestParam(required = false) @Max(Long.MAX_VALUE) Long tradeOperation,
                                             @RequestParam(required = false) @Max(Long.MAX_VALUE) Long countryFrom,
                                             @RequestParam(required = false) @Max(Long.MAX_VALUE) Long countryTo,
                                             @RequestParam(required = false) @Max(Long.MAX_VALUE) Long category,
                                             @RequestParam(required = false) @Max(Long.MAX_VALUE) Long subCategory,
                                             @RequestParam(required = false) @Min(value = 1) @Max(value = 12) Integer monthFrom,
                                             @RequestParam(required = false) @Min(value = 1) @Max(value = 12) Integer monthTo,
                                             @RequestParam(required = false) @Min(value = 1900) @Max(value = 3000) Integer yearFrom,
                                             @RequestParam(required = false) @Min(value = 1900) @Max(value = 3000) Integer yearTo,
                                             @RequestParam(required = false) String query,
                                             @RequestParam(required = false) String aggregator,
                                             @RequestParam(required = false) Integer page,
                                             @RequestParam(required = false) Integer size) {
        return tradeVolumeStatisticService.fetchTradeVolumeStatistics(commodity, tradeOperation, countryFrom, countryTo, category, subCategory, monthFrom, monthTo, yearFrom, yearTo, query, aggregator, page, size);
    }

}
