package com.tmea.wit.controller.api;

import com.tmea.wit.model.BaseApiResponse;
import com.tmea.wit.model.GeneralApiListResponse;
import com.tmea.wit.model.dto.request.ApiCommodityCategoryRequestDto;
import com.tmea.wit.model.dto.request.ApiImageUploadRequestDto;
import com.tmea.wit.model.dto.request.ApiStatusRequestDto;
import com.tmea.wit.service.CommodityCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;


import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.security.Principal;

@RestController
@RequestMapping("api/commodity-category")
@Validated
public class CommodityCategoryController {

    CommodityCategoryService categoryService;
    @Autowired
    public CommodityCategoryController(CommodityCategoryService categoryService) {
        this.categoryService = categoryService;
    }
    @GetMapping
    public GeneralApiListResponse getAllCategories(WebRequest request,
                                                     @Min(0) Long page,
                                                     @Min(2) Long size){
        return categoryService.getCommodityCategories(request.getParameterMap(),false);
    }
    @GetMapping("admin")
    public GeneralApiListResponse getAdminCategories(WebRequest request,
                                                   @Min(0) Long page,
                                                   @Min(2) Long size){
        return categoryService.getCommodityCategories(request.getParameterMap(),true);
    }

    @PostMapping
    public BaseApiResponse addCategory(@RequestBody @Valid ApiCommodityCategoryRequestDto body,
                                       Principal principal){
        return categoryService.addCommodityCategory(body, principal.getName());
    }

    @PutMapping("{categoryId}")
    public BaseApiResponse updateCategory(@RequestBody @Valid ApiCommodityCategoryRequestDto body,
                                          @PathVariable @Min(1) Long categoryId,
                                          Principal principal){
        return categoryService.updateCommodityCategory(categoryId, body, principal.getName());
    }

    @PutMapping("status/{categoryId}")
    public BaseApiResponse updateCategoryStatus(@RequestBody @Valid ApiStatusRequestDto body,
                                                @PathVariable @Min(1) Long categoryId){
        return categoryService.updateCommodityCategoryStatus(categoryId, body);
    }

    @PutMapping("image/{categoryId}")
    public BaseApiResponse updateCategoryImage(@RequestBody @Valid ApiImageUploadRequestDto body,
                                               @PathVariable @Min(1) Long categoryId,
                                               Principal principal){
        return categoryService.updateCommodityCategoryImage(categoryId, body, principal.getName());
    }
}
