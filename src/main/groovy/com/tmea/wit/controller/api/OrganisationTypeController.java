package com.tmea.wit.controller.api;

import com.tmea.wit.model.BaseApiResponse;
import com.tmea.wit.model.GeneralApiListResponse;
import com.tmea.wit.model.dto.request.ApiStatusRequestDto;
import com.tmea.wit.service.OrganisationTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import javax.validation.Valid;
import javax.validation.constraints.Min;

@RestController
@RequestMapping("api/organisation-type")
@Validated
public class OrganisationTypeController {
    OrganisationTypeService organisationTypeService;

    @Autowired
    public OrganisationTypeController(OrganisationTypeService organisationTypeService) {
        this.organisationTypeService = organisationTypeService;
    }

    @GetMapping("")
    public GeneralApiListResponse fetchAll(WebRequest request,
                                           @Min(0) Long page,
                                           @Min(2) Long size){
        return organisationTypeService.getOrganisationTypes(request.getParameterMap());
    }

    @PutMapping("/status/{organisationTypeId}")
    public BaseApiResponse updateUserTypeStatus(@PathVariable @Min(1) Long organisationTypeId,
                                                @RequestBody @Valid ApiStatusRequestDto body){
        return organisationTypeService.updateOrganisationTypeStatus(organisationTypeId, body);
    }
}
