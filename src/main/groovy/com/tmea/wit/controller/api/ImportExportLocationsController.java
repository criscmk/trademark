package com.tmea.wit.controller.api;

import com.tmea.wit.model.BaseApiResponse;
import com.tmea.wit.model.GeneralApiListResponse;
import com.tmea.wit.model.dto.request.ApiEditImportExportPostDto;
import com.tmea.wit.model.dto.request.ApiImportExportLocationPostDto;
import com.tmea.wit.model.dto.request.ApiStatusRequestDto;
import com.tmea.wit.service.ImportExportLocationsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.security.Principal;

@RestController
@RequestMapping("api/import-export-location/")
@Validated
public class ImportExportLocationsController {
    private final ImportExportLocationsService importExportLocationsService;

    @Autowired

    public ImportExportLocationsController(ImportExportLocationsService importExportLocationsService) {
        this.importExportLocationsService = importExportLocationsService;
    }

    @PostMapping("")
    public BaseApiResponse addLocation(@Valid @RequestBody ApiImportExportLocationPostDto body,
                                       Principal principal) {
        return importExportLocationsService.addLocation(body, principal.getName());
    }

    @PutMapping("{id}")
    public BaseApiResponse editLocation(@Valid @RequestBody ApiEditImportExportPostDto body,
                                        @PathVariable("id")
                                        @Valid @NotNull(message = "Location id cannot be null")
                                        @Max(Long.MAX_VALUE)
                                        @Min(1L) Long id,
                                        Principal principal) {
        return importExportLocationsService.editImportExportLocation(id, body, principal.getName());
    }

    @PutMapping("update-status/{id}")
    public BaseApiResponse updateLocationStatus(@Valid @RequestBody ApiStatusRequestDto body,
                                                @PathVariable("id")
                                                @Valid @NotNull(message = "Location id cannot be null")
                                                @Max(Long.MAX_VALUE)
                                                @Min(1L) Long id,
                                                Principal principal) {
        return importExportLocationsService.activateDeactivateLocation(body, id, principal.getName());
    }

    @GetMapping("")
    public GeneralApiListResponse getAllImportExportLocations(WebRequest webRequest){
        return importExportLocationsService.fetchAllImportExportLocations(webRequest.getParameterMap(), false);
    }

    @GetMapping("admin")
    public GeneralApiListResponse getAllAdminImportExportLocations(WebRequest webRequest){
        return importExportLocationsService.fetchAllImportExportLocations(webRequest.getParameterMap(), true);
    }
}
