package com.tmea.wit.controller.api;

import com.tmea.wit.model.BaseApiResponse;
import com.tmea.wit.model.GeneralApiListResponse;
import com.tmea.wit.model.dto.request.ApiEditTradeDocumentTypePostDto;
import com.tmea.wit.model.dto.request.ApiStatusRequestDto;
import com.tmea.wit.model.dto.request.ApiTradeDocumentTypePostDto;
import com.tmea.wit.service.TradeDocumentsTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.security.Principal;

@RestController
@RequestMapping("api/trade-documents-types/")
@Validated
public class TradeDocumentsTypeController {

    @Autowired
    TradeDocumentsTypeService tradeDocumentsTypeService;

    @PostMapping("")
    public BaseApiResponse addTradeDocumentType(@Valid @RequestBody ApiTradeDocumentTypePostDto body, Principal principal) {
        return tradeDocumentsTypeService.addTradeDocumentType(body, principal.getName());
    }

    @PutMapping("edit/{id}")
    public BaseApiResponse editTradeDocumentType(@Valid @RequestBody ApiEditTradeDocumentTypePostDto body,
                                                 @PathVariable("id")
                                                 @Valid @NotNull(message = "Trade document id cannot be null")
                                                 @Min(1) Long id,
                                                 Principal principal) {
        return tradeDocumentsTypeService.editDocumentType(body, id, principal.getName());
    }

    @GetMapping("")
    public GeneralApiListResponse fetchAllTradeDocumentTypes(WebRequest webRequest) {
        return tradeDocumentsTypeService.fetchAllDocumentType(webRequest.getParameterMap(), false);
    }

    @GetMapping("admin")
    public GeneralApiListResponse fetchAllAdminTradeDocumentTypes(WebRequest webRequest) {
        return tradeDocumentsTypeService.fetchAllDocumentType(webRequest.getParameterMap(), true);
    }

    @PutMapping("{id}")
    public BaseApiResponse updateTradeDocumentTypeStatus(@Valid @RequestBody ApiStatusRequestDto body,
                                                         @PathVariable("id")
                                                         @Valid @NotNull(message = "Trade document id cannot be null")
                                                         @Min(1) Long id,
                                                         Principal principal){
        return tradeDocumentsTypeService.updateDocumentTypeStatus(body, id, principal.getName());
    }
}
