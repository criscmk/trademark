package com.tmea.wit.controller.api;

import com.tmea.wit.model.BaseApiResponse;
import com.tmea.wit.model.GeneralApiListResponse;
import com.tmea.wit.model.dto.request.ApiStatusRequestDto;
import com.tmea.wit.service.CountryService;
import com.tmea.wit.service.CurrencyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import javax.validation.Valid;
import javax.validation.constraints.Min;

@RestController
@RequestMapping(value = "api/currency")
@Validated
public class CurrencyController {

    CurrencyService currencyService;

    @Autowired
    public CurrencyController(CurrencyService currencyService){
        this.currencyService = currencyService;
    }

    @GetMapping("")
    public GeneralApiListResponse fetchAll(WebRequest request, @Min(0) Long page, @Min(2) Long size){
        return currencyService.getCurrencies(request.getParameterMap());
    }

    @PutMapping ("/status/{currencyId}")
    public BaseApiResponse updateCountryStatus(@RequestBody @Valid ApiStatusRequestDto body,
                                               @PathVariable @Min(1) Long currencyId){
        return currencyService.updateCurrencyStatus(currencyId, body);
    }
}
