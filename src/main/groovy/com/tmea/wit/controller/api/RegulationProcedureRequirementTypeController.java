package com.tmea.wit.controller.api;

import com.tmea.wit.model.BaseApiResponse;
import com.tmea.wit.model.dto.request.ApiAddRegulationProcedureRequestDto;
import com.tmea.wit.model.dto.request.ApiRegulationProcedureRequirementTypeRequestDto;
import com.tmea.wit.model.dto.request.ApiStatusRequestDto;
import com.tmea.wit.service.RegulationProcedureRequirementTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.security.Principal;
@RestController
@RequestMapping("api/regulation-procedure-requirement-type")
@Validated
public class RegulationProcedureRequirementTypeController {
    RegulationProcedureRequirementTypeService regulationProcedureRequirementTypeService;
    @Autowired
    public RegulationProcedureRequirementTypeController(RegulationProcedureRequirementTypeService regulationProcedureRequirementTypeService){
        this.regulationProcedureRequirementTypeService = regulationProcedureRequirementTypeService;
    }
    @PostMapping
    public BaseApiResponse addRegulationProcedureRequirementType(@RequestBody @Valid ApiRegulationProcedureRequirementTypeRequestDto body,
                                                  Principal principal){
        return regulationProcedureRequirementTypeService.addRegulationProcedureRequirementType(body, principal.getName());
    }
    @PutMapping("{id}")
    public BaseApiResponse editRegulationProcedure(@Valid @RequestBody ApiRegulationProcedureRequirementTypeRequestDto body,
                                                   @PathVariable("id")
                                                   @Valid @NotNull(message = "regulation procedure requirement type id cannot be null")
                                                   @Min(1) Long id,
                                                   Principal principal) {
        return regulationProcedureRequirementTypeService.editRegulationProcedureRequirementType(body, id, principal.getName());
    }
    @PutMapping("status/{id}")
    public BaseApiResponse updateRegulationProcedureRequirementTypeStatus(@Valid @RequestBody ApiStatusRequestDto body,
                                                   @PathVariable("id")
                                                   @Valid @NotNull(message = "regulation procedure requirement type id cannot be null")
                                                   @Min(1) Long id,
                                                   Principal principal) {
        return regulationProcedureRequirementTypeService.updateRegulationProcedureRequirementTypeStatus(id, body, principal.getName());
    }
}
