package com.tmea.wit.controller.api;

import com.tmea.wit.model.BaseApiResponse;
import com.tmea.wit.model.dto.request.ApiEditTaxInfoPostDto;
import com.tmea.wit.model.dto.request.ApiStatusRequestDto;
import com.tmea.wit.model.dto.request.ApiTaxInfoPostDto;
import com.tmea.wit.service.TaxInformationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.security.Principal;

@RestController
@RequestMapping("api/tax-information/")
@Validated
public class TaxInformationController {
    private final TaxInformationService taxInformationService;

    @Autowired
    public TaxInformationController(TaxInformationService taxInformationService) {
        this.taxInformationService = taxInformationService;
    }

    @PostMapping("")
    public BaseApiResponse addTaxInfo(@Valid @RequestBody ApiTaxInfoPostDto body,
                                      Principal principal) {
        return taxInformationService.addTaxInfo(body, principal.getName());
    }

    @PutMapping("{taxInfoId}")
    public BaseApiResponse editTaxInfo(@Valid @RequestBody ApiEditTaxInfoPostDto body,
                                       @PathVariable("taxInfoId")
                                       @Valid @NotNull(message = "Tax information id cannot be null")
                                       @Max(Long.MAX_VALUE)
                                       @Min(1L) Long id, Principal principal) {
        return taxInformationService.editTaxInfo(id, body, principal.getName());
    }

    @PutMapping("update-status/{id}")
    public BaseApiResponse activateDeactivateTaxInfo(@Valid @RequestBody ApiStatusRequestDto body,
                                                     @PathVariable("id")
                                                     @Valid @NotNull(message = "Tax information id cannot be null")
                                                     @Max(Long.MAX_VALUE)
                                                     @Min(1L) Long id, Principal principal) {
        return taxInformationService.activateDeactivateTaxInfo(body, id, principal.getName());
    }

    @GetMapping("{taxId}")
    public BaseApiResponse getTaxInformation(@PathVariable("taxId")
                                             @Valid @NotNull(message = "Tax id cannot be null")
                                             @Max(Long.MAX_VALUE) Long id) {
        return taxInformationService.getTaxInfo(id, false);
    }

    @GetMapping("admin/{taxId}")
    public BaseApiResponse getAdminTaxInformation(@PathVariable("taxId")
                                                  @Valid @NotNull(message = "Tax id cannot be null")
                                                  @Max(Long.MAX_VALUE) Long id) {
        return taxInformationService.getTaxInfo(id, true);
    }
}
