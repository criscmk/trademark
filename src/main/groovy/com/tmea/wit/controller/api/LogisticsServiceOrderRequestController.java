package com.tmea.wit.controller.api;

import com.tmea.wit.model.BaseApiResponse;
import com.tmea.wit.model.GeneralApiListResponse;
import com.tmea.wit.model.dto.request.ApiLogisticsOrderServiceApprovalDto;
import com.tmea.wit.model.dto.request.ApiLogisticsServiceOrderRequestDto;
import com.tmea.wit.model.dto.request.ApiUpdateLogisticsServiceOrderPriceRequestDto;
import com.tmea.wit.service.LogisticsServiceOrderRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.security.Principal;

@RestController
@RequestMapping("api/logistics-service-order-request")
@Validated
public class LogisticsServiceOrderRequestController {

    LogisticsServiceOrderRequestService logisticsServiceOrderRequestService;

    @Autowired
    public LogisticsServiceOrderRequestController(LogisticsServiceOrderRequestService logisticsServiceOrderRequestService) {
        this.logisticsServiceOrderRequestService = logisticsServiceOrderRequestService;
    }

    @PostMapping
    public BaseApiResponse addLogisticsServiceOrderRequest(@RequestBody @Valid ApiLogisticsServiceOrderRequestDto body, Principal principal){
        return logisticsServiceOrderRequestService.addLogisticsServiceOrderRequest(body,principal.getName());
    }
    @GetMapping("")
    public GeneralApiListResponse getLogisticsServiceOrderRequest(WebRequest request,
                                              @Min(0) Long page,
                                              @Min(2) Long size,
                                              Principal principal){
        return logisticsServiceOrderRequestService.getLogisticsServiceOrderRequest(request.getParameterMap(),null);
    }
    @GetMapping("current-user")
    public GeneralApiListResponse getProviderLogisticsServiceOrderRequest(WebRequest request,
                                              @Min(0) Long page,
                                              @Min(2) Long size,
                                              Principal principal){
        return logisticsServiceOrderRequestService.getLogisticsServiceOrderRequest(request.getParameterMap(),principal.getName());
    }
    @GetMapping("/{Id}")
    public BaseApiResponse getLogisticsServiceOrderRequestById(@PathVariable @Min(0) Long Id,
                                              Principal principal){
        return logisticsServiceOrderRequestService.getLogisticsServiceOrderRequestById(Id,principal.getName());
    }
    @PutMapping("/approve/{logisticsServiceOrderId}")
    public BaseApiResponse approveLogisticsServiceOrderRequest( @RequestBody @Valid ApiLogisticsOrderServiceApprovalDto body,
                                                @PathVariable @Min(0) Long logisticsServiceOrderId,
                                                Principal principal)
    {
        return logisticsServiceOrderRequestService.approveLogisticsServiceOrderRequest(body,logisticsServiceOrderId,principal.getName());
    }
    @PutMapping("/update/price/{logisticsServiceOrderId}")
    public BaseApiResponse updateLogisticsOrderRequestPrice( @RequestBody @Valid ApiUpdateLogisticsServiceOrderPriceRequestDto body,
                                                             @PathVariable Long logisticsServiceOrderId,
                                                             Principal principal)
    {
        return logisticsServiceOrderRequestService.updateLogisticsServiceOrderPrice(body,logisticsServiceOrderId,principal.getName());
    }


}
