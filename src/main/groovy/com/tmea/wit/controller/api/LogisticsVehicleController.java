package com.tmea.wit.controller.api;

import com.tmea.wit.model.BaseApiResponse;
import com.tmea.wit.model.GeneralApiListResponse;
import com.tmea.wit.model.dto.request.ApiAddLogisticsVehicleRequestDto;
import com.tmea.wit.model.dto.request.ApiStatusRequestDto;
import com.tmea.wit.service.LogisticsVehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import javax.validation.Valid;
import javax.validation.constraints.Min;

@RestController
@RequestMapping("api/logistics-vehicle")
@Validated

public class LogisticsVehicleController {
    LogisticsVehicleService logisticsVehicleService;

    @Autowired
    public LogisticsVehicleController(LogisticsVehicleService logisticsVehicleService) {
        this.logisticsVehicleService = logisticsVehicleService;
    }
    @PostMapping
    public BaseApiResponse addLogisticsVehicle(@RequestBody @Valid ApiAddLogisticsVehicleRequestDto body){
        return logisticsVehicleService.addVehicle(body);
    }
    @GetMapping("")
    public GeneralApiListResponse getAllVehicles(WebRequest request)
                                               {
        return logisticsVehicleService.getVehicles(request.getParameterMap(),false);
    }
    @GetMapping("/current-user")
    public GeneralApiListResponse getProviderVehicles(WebRequest request){

        return logisticsVehicleService.getVehicles(request.getParameterMap(),true);
    }

    @PutMapping("{vehicleId}")
    public BaseApiResponse updateVehicle(@RequestBody @Valid ApiAddLogisticsVehicleRequestDto body,
                                          @PathVariable @Min(1) Long vehicleId){
        return logisticsVehicleService.updateVehicle(vehicleId, body);
    }
    @PutMapping("/status/{vehicleId}")
    public BaseApiResponse updateVehicleStatus( @RequestBody @Valid ApiStatusRequestDto body,
                                                 @PathVariable Long vehicleId
                                               ){
        return logisticsVehicleService.updateVehicleStatus(body,vehicleId);
    }




}
