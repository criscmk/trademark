package com.tmea.wit.controller.api;

import com.tmea.wit.model.BaseApiResponse;
import com.tmea.wit.model.GeneralApiListResponse;
import com.tmea.wit.model.dto.request.ApiLogisticsVehicleTypePostDto;
import com.tmea.wit.model.dto.request.ApiStatusRequestDto;
import com.tmea.wit.service.LogisticsVehicleTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.security.Principal;

@RestController
@RequestMapping("/api/logistics-vehicle-type")
@Validated
public class LogisticsVehicleTypeController {

    @Autowired
    LogisticsVehicleTypeService logisticsVehicleTypeService;

    //Add new vehicle type
    @PostMapping("")
    public BaseApiResponse addNewVehicleType(@Valid @RequestBody ApiLogisticsVehicleTypePostDto apiLogisticsVehicleTypePostDto, Principal principal){
        return logisticsVehicleTypeService.addNewVehicle(apiLogisticsVehicleTypePostDto, principal.getName());
    }

    //Edit existing record
    @PutMapping("/{vehicleTypeId}")
    public BaseApiResponse editVehicleType(@Valid @RequestBody ApiLogisticsVehicleTypePostDto apiLogisticsVehicleTypePostDto,
                                           @PathVariable("vehicleTypeId")
                                           @Valid @NotNull(message = "Logistics vehicle type id cannot be null")
                                           @Max(Long.MAX_VALUE)
                                           @Min(1L) Long id, Principal principal){
        return logisticsVehicleTypeService.editLogisticsVehicle(apiLogisticsVehicleTypePostDto, id, principal.getName());
    }

    //Activate/Deactivate logistics vehicle type
    @PutMapping("/status/{vehicleTypeId}")
    public BaseApiResponse updateCommodityStatus(@RequestBody @Valid ApiStatusRequestDto body,
                                                 @PathVariable("vehicleTypeId")
                                                 @Valid @NotNull(message = "Logistics vehicle type id cannot be null")
                                                 @Min(1) Long id,
                                                 Principal principal) {
        return logisticsVehicleTypeService.updateLogisticsVehicleStatus(id, body, principal.getName());
    }

    //Fetch all logistics vehicle types
    @GetMapping
    public GeneralApiListResponse getAllLogisticsServiceTypes(WebRequest request) {
        return logisticsVehicleTypeService.fetchAllLogisticsVehicles(request.getParameterMap());
    }

}
