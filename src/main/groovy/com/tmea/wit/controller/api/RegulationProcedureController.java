package com.tmea.wit.controller.api;

import com.tmea.wit.model.BaseApiResponse;
import com.tmea.wit.model.dto.request.*;
import com.tmea.wit.service.RegulationCategoryService;
import com.tmea.wit.service.RegulationProcedureService;
import com.tmea.wit.service.RegulationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.security.Principal;

@RestController
@RequestMapping("api/regulation-procedure")
@Validated
public class RegulationProcedureController {
    RegulationProcedureService regulationProcedureService;

    @Autowired
    public RegulationProcedureController(RegulationProcedureService regulationProcedureService) {
        this.regulationProcedureService = regulationProcedureService;
    }

    @PostMapping
    public BaseApiResponse addRegulatoryProcedure(@RequestBody @Valid ApiAddRegulationProcedureRequestDto body,
                                                  Principal principal) {
        return regulationProcedureService.addRegulationProcedure(body, principal.getName());
    }

    @PutMapping("{id}")
    public BaseApiResponse editRegulationProcedure(@Valid @RequestBody ApiAddRegulationProcedureRequestDto body,
                                                   @PathVariable("id")
                                                   @Valid @NotNull(message = "regulation procedure id cannot be null")
                                                   @Min(1) Long id,
                                                   Principal principal) {
        return regulationProcedureService.editRegulationProcedure(body, id, principal.getName());
    }

    @PutMapping("status/{id}")
    public BaseApiResponse updateRegulationProcedureStatus(@RequestBody @Valid ApiStatusRequestDto body,
                                                           @PathVariable @Min(1)
                                                           @Valid @NotNull(message = "regulation procedure id cannot be null") Long id) {
        return regulationProcedureService.updateRegulationProcedureStatus(id, body);
    }

    @PutMapping("requirement/{id}")
    public BaseApiResponse linkProcedureRequirement(@RequestBody @Valid ApiRegulationProcedureRequirementRequestDto body,
                                                    @PathVariable @Min(1)
                                                    @Valid @NotNull(message = "regulation procedure id cannot be null") Long id,
                                                    Principal principal) {
        return regulationProcedureService.linkRegulationProcedureRequirement(id, body, principal.getName());
    }
}
