package com.tmea.wit.controller.api;

import com.tmea.wit.model.BaseApiResponse;
import com.tmea.wit.model.GeneralApiListResponse;
import com.tmea.wit.model.dto.request.ApiCancelMarketOrderDto;

import com.tmea.wit.model.dto.request.ApiMarketOrderRating;
import com.tmea.wit.model.dto.request.ApiUpdateMarketOrderStatusRequestDto;
import com.tmea.wit.service.MarketOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;

@RestController
@RequestMapping("api/market-order")
@Validated
public class MarketOrderController {
    MarketOrderService marketOrderService;
    @Autowired
    public MarketOrderController(MarketOrderService marketOrderService) {
        this.marketOrderService = marketOrderService;
    }
    @PostMapping("/cancel/{id}")
    public BaseApiResponse cancelMarketOrder(@RequestBody @Valid ApiCancelMarketOrderDto body,
                                                            @PathVariable @Min(1) Long id)
    {
        return marketOrderService.cancelMarketOrder(body,id);
    }
    @PutMapping("/status/{id}")
    public BaseApiResponse updateMarketOrderStatus(@RequestBody @Valid ApiUpdateMarketOrderStatusRequestDto body,
                                                            @PathVariable @Min(1) Long id)
    {
        return marketOrderService.updateMarketOrderStatus(body,id);
    }
    @GetMapping("")
    public GeneralApiListResponse getAllMarketOrderRequests(WebRequest request,
                                                            @RequestParam(required = false) @Pattern(regexp ="\\d{4}\\-(0?[1-9]|1[012])\\-(0?[1-9]|[12]\\d|3[01])",message = "Invalid date format . valid date format is YYYY-MM-DD") String date,
                                                            @RequestParam(required = false) @Pattern(regexp ="\\d{4}\\-(0?[1-9]|1[012])\\-(0?[1-9]|[12]\\d|3[01])",message = "Invalid date format . valid date format is YYYY-MM-DD") String dateTo,
                                                            @RequestParam(required = false) @Pattern(regexp ="\\d{4}\\-(0?[1-9]|1[012])\\-(0?[1-9]|[12]\\d|3[01])",message = "Invalid date format . valid date format is YYYY-MM-DD") String dateFrom)
    {
        return marketOrderService.getAllMarketOrder(request.getParameterMap(),0);
    }
    @GetMapping("seller")
    public GeneralApiListResponse getSellerMarketOrderRequest(WebRequest request,
                                                              @RequestParam(required = false) @Pattern(regexp ="\\d{4}\\-(0?[1-9]|1[012])\\-(0?[1-9]|[12]\\d|3[01])",message = "Invalid date format . valid date format is YYYY-MM-DD") String date,
                                                              @RequestParam(required = false) @Pattern(regexp ="\\d{4}\\-(0?[1-9]|1[012])\\-(0?[1-9]|[12]\\d|3[01])",message = "Invalid date format . valid date format is YYYY-MM-DD") String dateTo,
                                                              @RequestParam(required = false) @Pattern(regexp ="\\d{4}\\-(0?[1-9]|1[012])\\-(0?[1-9]|[12]\\d|3[01])",message = "Invalid date format . valid date format is YYYY-MM-DD") String dateFrom)
    {
        return marketOrderService.getAllMarketOrder(request.getParameterMap(), 1);
    }
    @GetMapping("buyer")
    public GeneralApiListResponse getBuyerMarketOrderRequest(WebRequest request,
                                                             @RequestParam(required = false) @Pattern(regexp ="\\d{4}\\-(0?[1-9]|1[012])\\-(0?[1-9]|[12]\\d|3[01])",message = "Invalid date format . valid date format is YYYY-MM-DD") String date,
                                                             @RequestParam(required = false) @Pattern(regexp ="\\d{4}\\-(0?[1-9]|1[012])\\-(0?[1-9]|[12]\\d|3[01])",message = "Invalid date format . valid date format is YYYY-MM-DD") String dateTo,
                                                             @RequestParam(required = false) @Pattern(regexp ="\\d{4}\\-(0?[1-9]|1[012])\\-(0?[1-9]|[12]\\d|3[01])",message = "Invalid date format . valid date format is YYYY-MM-DD") String dateFrom)
    {
        return marketOrderService.getAllMarketOrder(request.getParameterMap(), 2);
    }
    @GetMapping("/{id}")
    public BaseApiResponse getAllMarketOrderRequestsById(@PathVariable @Min(1)  Long id)
    {
        return marketOrderService.getMarketOrderById(id);
    }
    @PostMapping("/rating/{id}")
    public BaseApiResponse MarketOrderRating(@RequestBody @Valid ApiMarketOrderRating body,
                                             @PathVariable  @Min(1) Long id)
    {
        return marketOrderService.MarketOrderRating(body,id);
    }

}
