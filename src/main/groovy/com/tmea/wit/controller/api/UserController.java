package com.tmea.wit.controller.api;


import com.tmea.wit.model.BaseApiResponse;
import com.tmea.wit.model.dto.request.ApiUserIndividualRegDto;
import com.tmea.wit.model.dto.request.ApiUserOrganizationRegDto;
import com.tmea.wit.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import javax.validation.Valid;
import java.security.Principal;


@RestController
@RequestMapping("/api/user")
@Validated
public class UserController {


    @Autowired
    UserService userService;


    @PostMapping("/register/individual")
    public BaseApiResponse registerUserIndividual(@Valid @RequestBody ApiUserIndividualRegDto body){
        return userService.registerUser(body,1);
    }

    @PostMapping("/register/organization")
    public BaseApiResponse registerUserOrganization(@Valid @RequestBody ApiUserOrganizationRegDto body){
        return userService.registerUser(body,2);
    }

    @GetMapping("/mydetails")
    public BaseApiResponse getUser(WebRequest request,Principal principal){
        BaseApiResponse baseApiResponse = new BaseApiResponse();
        Long userId  = userService.getUserIdFromUsername(principal.getName());

        if(userId == null){
            baseApiResponse = new BaseApiResponse(null, HttpStatus.UNAUTHORIZED.value(),"Session does not belong to a user",null);
        }else{
            baseApiResponse = userService.getUserDetails(userId);
        }

        return baseApiResponse;
    }

    @GetMapping("/details/{userId}")
    public BaseApiResponse getUserDetails(@PathVariable Long userId){
        return userService.getUserDetails(userId);
    }


}
