package com.tmea.wit.controller.api;

import com.tmea.wit.model.BaseApiResponse;
import com.tmea.wit.model.GeneralApiListResponse;
import com.tmea.wit.model.dto.request.ApiCommodityCategoryRequestDto;
import com.tmea.wit.model.dto.request.ApiPaymentOptionRequestDto;
import com.tmea.wit.model.dto.request.ApiStatusRequestDto;
import com.tmea.wit.service.PaymentOptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import javax.validation.Valid;
import javax.validation.constraints.Min;

@RestController
@RequestMapping("api/payment-option")
@Validated
public class PaymentOptionController {

    PaymentOptionService paymentOptionService;

    @Autowired
    public PaymentOptionController(PaymentOptionService paymentOptionService) {
        this.paymentOptionService = paymentOptionService;
    }

    @GetMapping
    public GeneralApiListResponse getPaymentOptions(WebRequest request,
                                                   @Min(0) Long page,
                                                   @Min(2) Long size){
        return paymentOptionService.getPaymentOptions(request.getParameterMap());
    }

    @PostMapping
    public BaseApiResponse addPaymentOption(@RequestBody @Valid ApiPaymentOptionRequestDto body){
        return paymentOptionService.addPaymentOption(body);
    }

    @PutMapping("{paymentOptionId}")
    public BaseApiResponse updatePaymentOption(@RequestBody @Valid ApiCommodityCategoryRequestDto body,
                                          @PathVariable @Min(1) Long paymentOptionId){
        return paymentOptionService.updatePaymentOption(paymentOptionId, body);
    }

    @PutMapping("status/{paymentOptionId}")
    public BaseApiResponse updatePaymentOptionStatus(@RequestBody @Valid ApiStatusRequestDto body,
                                                @PathVariable @Min(1) Long paymentOptionId){
        return paymentOptionService.updatePaymentOptionStatus(paymentOptionId, body);
    }
}
