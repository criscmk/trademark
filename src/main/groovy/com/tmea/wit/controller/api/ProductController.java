package com.tmea.wit.controller.api;

import com.tmea.wit.constants.ApiStatusOptions;
import com.tmea.wit.model.BaseApiResponse;
import com.tmea.wit.model.GeneralApiListResponse;
import com.tmea.wit.model.dto.request.*;
import com.tmea.wit.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.security.Principal;

@RestController
@RequestMapping("api/product")
@Validated
public class ProductController {

    ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @PostMapping("")
    public BaseApiResponse addProduct(@Valid @RequestBody ApiProductPostDto body, Principal principal) {
        return productService.addProduct(body, principal.getName());
    }

    @PutMapping("/edit/{productId}")
    public BaseApiResponse editProduct(@Valid @RequestBody ApiProductUpdateRequestDto body,
                                       @PathVariable("productId")
                                       @Valid @NotNull(message = "Product id cannot be null")
                                       @Min(1) Long id,
                                       Principal principal) {
        return productService.editProduct(body, id, principal.getName());
    }

    @GetMapping("")
    public GeneralApiListResponse fetchAllProducts(WebRequest request) {
        return productService.getAllProducts(request.getParameterMap(), false, false);
    }

    @GetMapping("/current-user")
    public GeneralApiListResponse fetchAllCurrentUserProducts(WebRequest request) {
        return productService.getAllProducts(request.getParameterMap(), true, false);
    }

    @GetMapping("/admin")
    public GeneralApiListResponse fetchAllAdminProducts(WebRequest request) {
        return productService.getAllProducts(request.getParameterMap(), true, true);
    }

    @GetMapping("/product-details/{productId}")
    public BaseApiResponse productDetails(@PathVariable("productId")
                                          @Valid @NotNull(message = "Product id cannot be null")
                                          @Min(1) Long id) {
        return productService.getProductDetails(id);
    }


    @PutMapping("/update-status/{status}/{productId}")
    public BaseApiResponse updateProductStatus(WebRequest request,
                                                  @PathVariable("productId")
                                                  @Valid @NotNull(message = "Product id cannot be null")
                                                  @Min(1) Long id,
                                                  @PathVariable("status")
                                                  String status,
                                                  @RequestBody ApiProductStatusUpdateRequestDto apiProductStatusUpdateRequestDto,
                                                  Principal principal) {
        return productService.updateProductStatus(apiProductStatusUpdateRequestDto, id, principal.getName(),status);
    }




    @PutMapping("/image/add/{productId}")
    public BaseApiResponse addProductImage(@Valid @RequestBody ApiAddProductImagePostDto body,
                                           @PathVariable("productId")
                                           @Valid @NotNull(message = "Product id cannot be null")
                                           @Min(1) Long id,
                                           Principal principal) {
        return productService.addProductImage(body, id, principal.getName());
    }

    @PutMapping("/image/remove/{imageId}")
    public BaseApiResponse deleteProductImage(@PathVariable("imageId")
                                              @Valid @NotNull(message = "Image id cannot be null")
                                              @Min(1) Long id,
                                              Principal principal) {
        return productService.removeProductImage(id, principal.getName());
    }

    @PutMapping("/image/set-primary/{imageId}")
    public BaseApiResponse setImagePrimaryImage(@PathVariable("imageId")
                                                @Valid @NotNull(message = "Image id cannot be null")
                                                @Min(1) Long id,
                                                Principal principal) {
        return productService.setProductPrimaryImage(id, principal.getName());
    }

    @GetMapping("product-trader-details/{traderId}")
    public BaseApiResponse getProductTraderDetails(@PathVariable("traderId")
                                                   @Valid @NotNull(message = "Trader id cannot be null")
                                                   @Min(1) Long id) {
        return productService.getTraderDetails(id);
    }
}
