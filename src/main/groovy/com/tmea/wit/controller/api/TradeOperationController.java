package com.tmea.wit.controller.api;

import com.tmea.wit.model.BaseApiResponse;
import com.tmea.wit.model.GeneralApiListResponse;
import com.tmea.wit.model.dto.request.ApiAddTradeOperationRequestDto;
import com.tmea.wit.model.dto.request.ApiStatusRequestDto;
import com.tmea.wit.service.TradeOperationService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import javax.validation.Valid;
import javax.validation.constraints.Min;

@RestController
@RequestMapping("/api/trade-operation")
@Validated
public class TradeOperationController {
    TradeOperationService tradeOperationService;
    public TradeOperationController(TradeOperationService tradeOperationService){
        this.tradeOperationService = tradeOperationService;
    }
    @GetMapping("")
    public GeneralApiListResponse fetchAll(WebRequest request,
                                           @Min(0) Long page,
                                           @Min(2) Long size){
        return tradeOperationService.getTradeOperation(request.getParameterMap());
    }
    @PutMapping("/status/{id}")
    public BaseApiResponse updateUserTypeStatus(@PathVariable @Min(1) Long id,
                                                @RequestBody @Valid ApiStatusRequestDto body){
        return tradeOperationService.updateTradeOperationStatus(id, body);
    }
    @PostMapping
    public BaseApiResponse addTradeOperation(@RequestBody @Valid ApiAddTradeOperationRequestDto body){
        return tradeOperationService.addTradeOperation(body);
    }

}
