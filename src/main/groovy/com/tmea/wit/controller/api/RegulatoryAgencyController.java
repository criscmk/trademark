package com.tmea.wit.controller.api;

import com.tmea.wit.model.BaseApiResponse;
import com.tmea.wit.model.GeneralApiListResponse;
import com.tmea.wit.model.dto.request.ApiAddRegulatoryAgencyPostDto;
import com.tmea.wit.model.dto.request.ApiEditRegulatoryAgencyPostDto;
import com.tmea.wit.model.dto.request.ApiStatusRequestDto;
import com.tmea.wit.service.RegulatoryAgencyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.security.Principal;

@RestController
@RequestMapping("api/regulatory-agency")
@Validated
public class RegulatoryAgencyController {

    @Autowired
    RegulatoryAgencyService regulatoryAgencyService;

    @PostMapping("")
    public BaseApiResponse addNewRegulatoryAgency(@Valid @RequestBody ApiAddRegulatoryAgencyPostDto apiAddRegulatoryAgencyPostDto, Principal principal) {
        return regulatoryAgencyService.addNewRegulatoryAgency(apiAddRegulatoryAgencyPostDto, principal.getName());
    }

    @PutMapping("/{agencyId}")
    public BaseApiResponse updateRegulatoryAgency(@Valid @RequestBody ApiEditRegulatoryAgencyPostDto body,
                                                  @PathVariable("agencyId")
                                                  @Valid @NotNull(message = "Agency id cannot be null")
                                                  @Min(1) Long id,
                                                  Principal principal){
        return regulatoryAgencyService.editAgency(id, body, principal.getName());
    }

    @GetMapping("")
    public GeneralApiListResponse fetchAllRegulatoryAgency(WebRequest webRequest){
        return regulatoryAgencyService.fetchAllRegulatoryAgency(webRequest.getParameterMap(), false);
    }

    @GetMapping("/admin")
    public GeneralApiListResponse fetchAllAdminRegulatoryAgency(WebRequest webRequest){
        return regulatoryAgencyService.fetchAllRegulatoryAgency(webRequest.getParameterMap(), true);
    }

    @PutMapping("/edit-status/{agencyId}")
    public BaseApiResponse updateAgencyStatus(@Valid @RequestBody ApiStatusRequestDto body,
                                              @PathVariable("agencyId")
                                              @Valid @NotNull(message = "Agency id cannot be null")
                                              @Min(1) Long id,
                                              Principal principal){
        return regulatoryAgencyService.updateAgencyStatus(id, body, principal.getName());
    }
}
