package com.tmea.wit.controller.api;

import com.tmea.wit.model.BaseApiResponse;
import com.tmea.wit.model.dto.request.ApiUnapprovedProductPostDto;
import com.tmea.wit.service.ProductApprovalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("api/product-approval/")
@Validated
public class ProductApprovalController {
    private ProductApprovalService productApprovalService;

    @Autowired
    public ProductApprovalController(ProductApprovalService productApprovalService) {
        this.productApprovalService = productApprovalService;
    }

    @GetMapping("product-details/{productId}")
    public BaseApiResponse getProductDetails(@PathVariable("productId")
                                             @Valid @NotNull(message = "Product id cannot be null")
                                             @Min(1) Long id) {
        return productApprovalService.getProductDetails(id);
    }

    @PostMapping("approve-product")
    public BaseApiResponse approveProduct(@Valid @RequestBody List<ApiUnapprovedProductPostDto> body, Principal principal) {
        return productApprovalService.approveProduct(body, principal.getName());
    }

}
