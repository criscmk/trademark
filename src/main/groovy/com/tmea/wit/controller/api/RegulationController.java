package com.tmea.wit.controller.api;

import com.tmea.wit.model.BaseApiResponse;
import com.tmea.wit.model.GeneralApiListResponse;
import com.tmea.wit.model.dto.request.ApiAddRegulationCategoryRequestDto;
import com.tmea.wit.model.dto.request.ApiAddRegulationRequestDto;
import com.tmea.wit.model.dto.request.ApiStatusRequestDto;
import com.tmea.wit.model.dto.request.ApiUpdateRegulationRequestDto;
import com.tmea.wit.service.RegulationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.security.Principal;

@RestController
@RequestMapping("api/regulation")
@Validated
public class RegulationController {
    RegulationService regulationService;

    @Autowired
    public RegulationController(RegulationService regulationService) {
        this.regulationService = regulationService;
    }

    @PostMapping
    public BaseApiResponse addRegulation(@RequestBody @Valid ApiAddRegulationRequestDto body,
                                         Principal principal) {
        return regulationService.addRegulation(body, principal.getName());
    }

    @PutMapping("{id}")
    public BaseApiResponse editRegulationCategory(@Valid @RequestBody ApiUpdateRegulationRequestDto body,
                                                  @PathVariable("id")
                                                  @Valid @NotNull(message = "regulation category id cannot be null")
                                                  @Min(1) Long id,
                                                  Principal principal) {
        return regulationService.editRegulation(body, id, principal.getName());
    }

    @PutMapping("status/{id}")
    public BaseApiResponse updateRegulationStatus(@RequestBody @Valid ApiStatusRequestDto body,
                                                  @PathVariable @Min(1)
                                                  @Valid @NotNull(message = "regulation  id cannot be null") Long id) {
        return regulationService.updateRegulationStatus(id, body);
    }

    @GetMapping
    public GeneralApiListResponse getRegulation(WebRequest request,
                                                @Min(0) Long page,
                                                @Min(2) Long size) {
        return regulationService.getRegulation(request.getParameterMap(), false);
    }

    @GetMapping("admin")
    public GeneralApiListResponse getAdminRegulation(WebRequest request,
                                                     @Min(0) Long page,
                                                     @Min(2) Long size) {
        return regulationService.getRegulation(request.getParameterMap(), true);
    }

    @GetMapping("{id}")
    public BaseApiResponse getRegulationDetails(@PathVariable @Min(1)
                                               @Valid @NotNull(message = "regulation  id cannot be null") Long id)

    {
        return regulationService.getRegulationDetail(id,false);
    }
    @GetMapping("admin/{id}")
    public BaseApiResponse getRegulationDetailsAdmin(@PathVariable @Min(1)
                                                     @Valid @NotNull(message = "regulation  id cannot be null") Long id)

    {
        return regulationService.getRegulationDetail(id,true);
    }
}
