package com.tmea.wit.controller.api;

import com.tmea.wit.model.BaseApiResponse;
import com.tmea.wit.model.GeneralApiListResponse;
import com.tmea.wit.model.dto.request.ApiMeasurementUnitRequestDto;
import com.tmea.wit.model.dto.request.ApiStatusRequestDto;
import com.tmea.wit.service.MeasurementUnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import javax.validation.Valid;
import javax.validation.constraints.Min;

@RestController
@RequestMapping("api/measurement-unit")
@Validated
public class MeasurementUnitController {

   MeasurementUnitService unitService;

    @Autowired
    public MeasurementUnitController(MeasurementUnitService unitService) {
        this.unitService = unitService;
    }

    @GetMapping
    public GeneralApiListResponse getAllMeasurementUnits(WebRequest request,
                                                         @Min(0) Long page,
                                                         @Min(2) Long size){
        return unitService.getMeasurementUnits(request.getParameterMap());
    }

    @PostMapping
    public BaseApiResponse addMeasurementUnit(@RequestBody  @Valid ApiMeasurementUnitRequestDto body){
        return unitService.addMeasurementUnit(body);
    }

    @PutMapping("{unitId}")
    public BaseApiResponse updateMeasurementUnit(@RequestBody @Valid ApiMeasurementUnitRequestDto body,
                                                @PathVariable @Min(1) Long unitId){
        return unitService.updateMeasurementUnit(unitId, body);
    }

    @PutMapping("status/{commodityId}")
    public BaseApiResponse updateMeasurementUnitStatus(@RequestBody @Valid ApiStatusRequestDto body,
                                                       @PathVariable @Min(1) Long commodityId){
        return unitService.updateMeasurementUnitStatus(commodityId, body);
    }
}
