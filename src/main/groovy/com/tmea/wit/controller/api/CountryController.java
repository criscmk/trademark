package com.tmea.wit.controller.api;

import com.tmea.wit.constants.ApiStatusOptions;
import com.tmea.wit.model.BaseApiResponse;
import com.tmea.wit.model.GeneralApiListResponse;
import com.tmea.wit.model.dto.request.ApiStatusRequestDto;
import com.tmea.wit.service.CountryService;
import com.tmea.wit.validators.EnumValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import javax.validation.Valid;
import javax.validation.constraints.Min;

@RestController
@RequestMapping(value = "api/country")
@Validated
public class CountryController {

    CountryService countryService;

    @Autowired
    public CountryController(CountryService countryService){
        this.countryService = countryService;
    }

    @GetMapping("")
    public GeneralApiListResponse fetchAll(WebRequest request,
                                           @Min(0) Long page,
                                           @Min(2) Long size){
        return countryService.getCountries(request.getParameterMap());
    }

    @PutMapping ("/status/{countryId}")
    public BaseApiResponse updateCountryStatus(@RequestBody @Valid ApiStatusRequestDto body,
                                               @PathVariable @Min(1) Long countryId){
        return countryService.updateStatus(countryId, body);
    }
}
