package com.tmea.wit.controller.api;

import com.tmea.wit.model.BaseApiResponse;
import com.tmea.wit.model.dto.request.*;
import com.tmea.wit.service.StandardSpecificationService;
import com.tmea.wit.service.StandardSpecificationTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("api/standard-specification")
@Validated
public class StandardSpecificationController {
    StandardSpecificationService standardSpecificationService;
    @Autowired
    public StandardSpecificationController(StandardSpecificationService standardSpecificationService) {
        this.standardSpecificationService = standardSpecificationService;
    }
    @PostMapping
    public BaseApiResponse addStandardSpecification(@RequestBody @Valid ApiAddStandardSpecificationRequestDto body){
        return standardSpecificationService.addStandardSpecification(body);
    }
    @PutMapping("status/{id}")
    public BaseApiResponse updateStandardSpecificationStatus(@RequestBody @Valid ApiStatusRequestDto body,
                                                                 @PathVariable @Min(1)
                                                                 @Valid @NotNull(message = "standardSpecificationType  id cannot be null") Long id) {
        return standardSpecificationService.updateStandardSpecificationStatus(id, body);
    }
    @PutMapping("{id}")
    public BaseApiResponse updateStandardSpecificationDetails(@RequestBody @Valid ApiAddStandardSpecificationRequestDto body,
                                                                  @PathVariable @Min(1)
                                                                  @Valid @NotNull(message = "standard Specification  id cannot be null") Long id) {

        return standardSpecificationService.updateStandardSpecificationDetails(id, body);
    }
}
