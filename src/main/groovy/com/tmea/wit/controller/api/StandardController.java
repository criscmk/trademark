package com.tmea.wit.controller.api;

import com.tmea.wit.model.BaseApiResponse;
import com.tmea.wit.model.GeneralApiListResponse;
import com.tmea.wit.model.dto.request.ApiAddStandardRequestDto;
import com.tmea.wit.model.dto.request.ApiAddStandardSpecificationTypeRequestDto;
import com.tmea.wit.model.dto.request.ApiEditStandardSpecificationTypeRequestDto;
import com.tmea.wit.model.dto.request.ApiStatusRequestDto;
import com.tmea.wit.service.StandardService;
import com.tmea.wit.service.StandardSpecificationTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("api/standard")
@Validated
public class StandardController {
    StandardService standardService;

    @Autowired
    public StandardController(StandardService standardService) {
        this.standardService = standardService;
    }
    @PostMapping
    public BaseApiResponse addStandardSpecification(@RequestBody @Valid ApiAddStandardRequestDto body){
        return standardService.addStandard(body);
    }
    @PutMapping("status/{id}")
    public BaseApiResponse updateStandardSpecificationTypeStatus(@RequestBody @Valid ApiStatusRequestDto body,
                                                                 @PathVariable @Min(1)
                                                                 @Valid @NotNull(message = "standardSpecificationType  id cannot be null") Long id) {
        return standardService.updateStandardStatus(id, body);
    }
    @PutMapping("{id}")
    public BaseApiResponse updateStandardDetails(@RequestBody @Valid ApiAddStandardRequestDto body,
                                                                  @PathVariable @Min(1)
                                                                  @Valid @NotNull(message = "standardSpecificationType  id cannot be null") Long id) {

        return standardService.updateStandardDetails(id, body);
    }
    @GetMapping
    public GeneralApiListResponse getStandard(WebRequest request,
                                                @Min(0) Long page,
                                                @Min(2) Long size) {
        return standardService.getStandard(request.getParameterMap(), false);
    }

    @GetMapping("admin")
    public GeneralApiListResponse getAdminStandard(WebRequest request,
                                                     @Min(0) Long page,
                                                     @Min(2) Long size) {
        return standardService.getStandard(request.getParameterMap(), true);
    }

    @GetMapping("admin/{id}")
    public BaseApiResponse getStandardDetailsAdmin(@PathVariable @Min(1)
                                                        @Valid @NotNull(message = "standard  id cannot be null") Long id){
        return standardService.getStandardDetails(id, true);
    }
    @GetMapping("{id}")
    public BaseApiResponse getStandardDetails(@PathVariable @Min(1)
                                                        @Valid @NotNull(message = "standard  id cannot be null") Long id){
        return standardService.getStandardDetails(id, false);
    }
}
