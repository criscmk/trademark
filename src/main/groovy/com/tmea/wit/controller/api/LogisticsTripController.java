package com.tmea.wit.controller.api;

import com.tmea.wit.model.BaseApiResponse;
import com.tmea.wit.model.GeneralApiListResponse;
import com.tmea.wit.model.dto.request.ApiCancelLogisticsTripDtoRequest;
import com.tmea.wit.model.dto.request.ApiCreateLogisticsTripRequestDto;
import com.tmea.wit.service.LogisticsTripService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "api/logistics-trip")
@Validated
public class LogisticsTripController {
    LogisticsTripService logisticsTripService;

    @Autowired
    public LogisticsTripController(LogisticsTripService logisticsTripService) {
        this.logisticsTripService = logisticsTripService;
    }

    @PostMapping
    public BaseApiResponse createLogisticsTrip(@RequestBody @Valid ApiCreateLogisticsTripRequestDto body){
        return logisticsTripService.createTrip(body);
    }
    @GetMapping
    public GeneralApiListResponse getAllLogisticsTrip(WebRequest request
                                                   ){
        return logisticsTripService.getLogisticsTrip(request.getParameterMap(),false);
    }
    @GetMapping("/current-user")
    public GeneralApiListResponse getProviderTripLogisticsTrip(WebRequest request
                                                   ){
        return logisticsTripService.getLogisticsTrip(request.getParameterMap(),true);
    }
    @GetMapping("/{id}")
    public BaseApiResponse getLogisticsTripById(@PathVariable Long id
    ){
        return logisticsTripService.getLogisticsTripById(id);
    }
    @PostMapping("/cancel")
    public BaseApiResponse cancelLogisticsTrip(@RequestBody @Valid ApiCancelLogisticsTripDtoRequest body
    ){
        return logisticsTripService.cancelLogisticsTrip(body);
    }
    @PutMapping("/dispatch/{id}")
    public BaseApiResponse dispatchLogisticsTrip(@PathVariable Long id)
    {
        return logisticsTripService.dispatchLogisticsTrip(id);
    }
    @PutMapping("/complete/{id}")
    public BaseApiResponse completeLogisticsTrip(@PathVariable Long id)
    {
        return logisticsTripService.completeLogisticsTrip(id);
    }
}
