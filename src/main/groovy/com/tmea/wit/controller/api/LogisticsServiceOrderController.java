package com.tmea.wit.controller.api;

import com.tmea.wit.model.BaseApiResponse;
import com.tmea.wit.model.GeneralApiListResponse;
import com.tmea.wit.model.dto.request.ApiCancelLogisticsServiceOrderDto;
import com.tmea.wit.model.dto.request.ApiLogisticsServiceOrderStatusDto;
import com.tmea.wit.service.LogisticsServiceOrderService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.security.Principal;

@RestController
@RequestMapping("api/logistics-service-order")
@Validated
public class LogisticsServiceOrderController {
    LogisticsServiceOrderService logisticsServiceOrderService;
    public LogisticsServiceOrderController(LogisticsServiceOrderService logisticsServiceOrderService) {
        this.logisticsServiceOrderService = logisticsServiceOrderService;
    }

    @GetMapping("")
    public GeneralApiListResponse getLogisticsServiceOrderRequest(WebRequest request,

                                                                  Principal principal){
        return logisticsServiceOrderService.getLogisticsServiceOrder(request.getParameterMap(),null);
    }
    @GetMapping("current-user")
    public GeneralApiListResponse getProviderLogisticsServiceOrderRequest(WebRequest request,

                                                                  Principal principal){
        return logisticsServiceOrderService.getLogisticsServiceOrder(request.getParameterMap(),principal.getName());
    }
    @GetMapping("/{Id}")
    public BaseApiResponse getLogisticsServiceOrderById(@PathVariable @Min(1) Long Id,
                                                               Principal principal){
        return logisticsServiceOrderService.getLogisticsServiceOrderRequestById(Id,principal.getName());
    }
    @PutMapping("/status/{logisticsServiceOrderId}")
    public BaseApiResponse updateLogisticsServiceOrderStatus(@RequestBody @Valid ApiLogisticsServiceOrderStatusDto body,
                                                             @PathVariable @Min(1) Long logisticsServiceOrderId,
                                                             Principal principal)
    {
        return logisticsServiceOrderService.updateLogisticsServiceOrderStatus(body,logisticsServiceOrderId,principal.getName());
    }
    @PutMapping("/cancel/{logisticsServiceOrderId}")
    public BaseApiResponse cancelLogisticsServiceOrderRequest( @RequestBody @Valid ApiCancelLogisticsServiceOrderDto body,
                                                        @PathVariable @Min(1) Long logisticsServiceOrderId,
                                                        Principal principal)
    {
        return logisticsServiceOrderService.cancelLogisticsServiceOrder(body,logisticsServiceOrderId,principal.getName());
    }



}
