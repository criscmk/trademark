package com.tmea.wit.controller.api;

import com.tmea.wit.model.BaseApiResponse;
import com.tmea.wit.model.dto.request.ApiRegulationProcedureRequirementTypeRequestDto;
import com.tmea.wit.model.dto.request.ApiRegulationRegulatoryAgencyRequestDto;
import com.tmea.wit.service.RegulationProcedureRequirementTypeService;
import com.tmea.wit.service.RegulationRegulatoryAgencyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.security.Principal;

    @RestController
    @RequestMapping("api/regulation-regulatory-agency")
    @Validated
    public class RegulationRegulatoryAgencyRequestController {
        RegulationRegulatoryAgencyService regulationRegulatoryAgencyService;
        @Autowired
        public RegulationRegulatoryAgencyRequestController(RegulationRegulatoryAgencyService regulationRegulatoryAgencyService){
            this.regulationRegulatoryAgencyService = regulationRegulatoryAgencyService;
        }
        @PostMapping
        public BaseApiResponse addRegulationProcedureRequirementType(@RequestBody @Valid ApiRegulationRegulatoryAgencyRequestDto body,
                                                                     Principal principal){
            return regulationRegulatoryAgencyService.addRegulationRegulatoryAgency(body, principal.getName());
        }
}
