package com.tmea.wit.controller.api;

import com.tmea.wit.model.BaseApiResponse;
import com.tmea.wit.model.GeneralApiListResponse;
import com.tmea.wit.model.dto.request.ApiAddRegulationCategoryRequestDto;
import com.tmea.wit.model.dto.request.ApiProductUpdateRequestDto;
import com.tmea.wit.model.dto.request.ApiStatusRequestDto;
import com.tmea.wit.model.dto.request.ApiUpdateRegulationCategoryRequestDto;
import com.tmea.wit.service.RegulationCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.security.Principal;

@RestController
@RequestMapping("api/regulation-category")
@Validated
public class RegulationCategoryController {
    RegulationCategoryService regulationCategoryService;
    @Autowired
    public RegulationCategoryController(RegulationCategoryService regulationCategoryService){
        this.regulationCategoryService = regulationCategoryService;
    }
    @PostMapping
    public BaseApiResponse addRegulatoryCategory(@RequestBody @Valid ApiAddRegulationCategoryRequestDto body,
                                        Principal principal){
        return regulationCategoryService.addRegulatoryCategory(body, principal.getName());
    }
    @PutMapping("{id}")
    public BaseApiResponse editRegulationCategory(@Valid @RequestBody ApiAddRegulationCategoryRequestDto body,
                                       @PathVariable("id")
                                       @Valid @NotNull(message = "regulation category id cannot be null")
                                       @Min(1) Long id,
                                       Principal principal) {
        return regulationCategoryService.editRegulationCategory(body, id, principal.getName());
    }
    @PutMapping("status/{categoryId}")
    public BaseApiResponse updateRegulationCategoryStatus(@RequestBody @Valid ApiStatusRequestDto body,
                                                 @PathVariable @Min(1)
                                                 @Valid @NotNull(message = "regulation category id cannot be null") Long categoryId){
        return regulationCategoryService.updateRegulationCategoryStatus(categoryId, body);
    }
    @GetMapping
    public GeneralApiListResponse fetchRegulatoryCategory(WebRequest request,
                                                        @Min(0) Long page,
                                                        @Min(2) Long size){
        return regulationCategoryService.getRegulationCategory(request.getParameterMap(),false);
    }

    @GetMapping("admin")
    public GeneralApiListResponse fetchAdminRegulatoryCategory(WebRequest request,
                                                        @Min(0) Long page,
                                                        @Min(2) Long size){
        return regulationCategoryService.getRegulationCategory(request.getParameterMap(),true);
    }

}
