package com.tmea.wit.controller.api;

import com.tmea.wit.model.BaseApiResponse;
import com.tmea.wit.model.GeneralApiListResponse;
import com.tmea.wit.model.dto.request.ApiCommoditySubCategoryRequestDto;
import com.tmea.wit.model.dto.request.ApiImageUploadRequestDto;
import com.tmea.wit.model.dto.request.ApiStatusRequestDto;
import com.tmea.wit.service.CommoditySubCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.security.Principal;

@RestController
@RequestMapping("api/commodity-sub-category")
@Validated
public class CommoditySubCategoryController {

    CommoditySubCategoryService subCategoryService;

    @Autowired
    public CommoditySubCategoryController(CommoditySubCategoryService subCategoryService) {
        this.subCategoryService = subCategoryService;
    }

    @GetMapping
    public GeneralApiListResponse getAllSubCategories(WebRequest request,
                                                      @Min(0) Long page,
                                                      @Min(2) Long size,
                                                      @Min(1) Long categoryId){
        return subCategoryService.getCommoditySubCategories(request.getParameterMap(),false);
    }
    @GetMapping("admin")
    public GeneralApiListResponse getAdminSubCategories(WebRequest request,
                                                      @Min(0) Long page,
                                                      @Min(2) Long size,
                                                      @Min(1) Long categoryId){
        return subCategoryService.getCommoditySubCategories(request.getParameterMap(),true);
    }

    @PostMapping
    public BaseApiResponse addSubCategory(@RequestBody @Valid ApiCommoditySubCategoryRequestDto body,
                                          Principal principal){
        return subCategoryService.addCommoditySubCategory(body, principal.getName());
    }

    @PutMapping("{subCategoryId}")
    public BaseApiResponse updateSubCategory(@RequestBody @Valid ApiCommoditySubCategoryRequestDto body,
                                             @PathVariable @Min(1) Long subCategoryId,
                                             Principal principal){
        return subCategoryService.updateCommoditySubCategory(subCategoryId, body, principal.getName());
    }

    @PutMapping("status/{subCategoryId}")
    public BaseApiResponse updateSubCategoryStatus(@RequestBody @Valid ApiStatusRequestDto body,
                                             @PathVariable @Min(1) Long subCategoryId){
        return subCategoryService.updateCommoditySubCategoryStatus(subCategoryId, body);
    }

    @PutMapping("image/{subCategoryId}")
    public BaseApiResponse updateSubCategoryImage(@RequestBody @Valid ApiImageUploadRequestDto body,
                                                @PathVariable @Min(1) Long subCategoryId,
                                                Principal principal){
        return subCategoryService.updateCommoditySubCategoryImage(subCategoryId, body, principal.getName());
    }
}
