package com.tmea.wit.controller.api;

import com.tmea.wit.model.BaseApiResponse;
import com.tmea.wit.model.GeneralApiListResponse;
import com.tmea.wit.model.dto.request.ApiStatusRequestDto;
import com.tmea.wit.service.UserTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import javax.validation.Valid;
import javax.validation.constraints.Min;

@RestController
@RequestMapping("api/user-type")
@Validated
public class UserTypeController {

    UserTypeService userTypeService;

    @Autowired
    public UserTypeController(UserTypeService userTypeService) {
        this.userTypeService = userTypeService;
    }

    @GetMapping("")
    public GeneralApiListResponse fetchAll(WebRequest request,
                                           @Min(0) Long page,
                                           @Min(2) Long size){
        return userTypeService.getUserTypes(request.getParameterMap());
    }
    @PutMapping("/status/{userTypeId}")
    public BaseApiResponse updateUserTypeStatus(@PathVariable @Min(1) Long userTypeId,
                                                @RequestBody @Valid ApiStatusRequestDto body ){
        return userTypeService.updateUserTypeStatus(userTypeId, body);
    }

}
