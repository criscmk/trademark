package com.tmea.wit.controller.api;

import com.tmea.wit.model.BaseApiResponse;
import com.tmea.wit.model.GeneralApiListResponse;
import com.tmea.wit.model.dto.request.ApiMeasurementMetricRequestDto;
import com.tmea.wit.model.dto.request.ApiStatusRequestDto;
import com.tmea.wit.service.MeasurementMetricService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import javax.validation.Valid;
import javax.validation.constraints.Min;

@RestController
@RequestMapping("api/measurement-metric")
@Validated
public class MeasurementMetricController {

    MeasurementMetricService metricService;

    @Autowired
    public MeasurementMetricController(MeasurementMetricService metricService) {
        this.metricService = metricService;
    }

    @GetMapping
    public GeneralApiListResponse getAllMeasurementMetric(WebRequest request,
                                                          @Min(0) Long page,
                                                          @Min(2) Long size){
        return metricService.getMeasurementMetrics(request.getParameterMap());
    }

    @PostMapping
    public BaseApiResponse addMeasurementMetric(@RequestBody @Valid ApiMeasurementMetricRequestDto body){
        return metricService.addMeasurementMetric(body);
    }

    @PutMapping("{metricId}")
    public BaseApiResponse updateMeasurementMetric(@RequestBody @Valid ApiMeasurementMetricRequestDto body,
                                                    @PathVariable @Min(1) Long metricId){
        return metricService.updateMeasurementMetric(metricId, body);
    }

    @PutMapping("status/{metricId}")
    public BaseApiResponse updateMeasurementMetricStatus(@RequestBody @Valid ApiStatusRequestDto body,
                                                         @PathVariable @Min(1) Long metricId){
        return metricService.updateMeasurementMetricStatus(metricId, body);
    }
}
