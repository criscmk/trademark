package com.tmea.wit.controller.api;

import com.tmea.wit.model.BaseApiResponse;
import com.tmea.wit.model.GeneralApiListResponse;
import com.tmea.wit.model.dto.request.ApiAddMarketOrderRequestDto;
import com.tmea.wit.model.dto.request.ApiApproveMarketOrderRequestDto;
import com.tmea.wit.model.dto.request.ApiUpdateMarketOrderQuantityRequestDto;
import com.tmea.wit.model.dto.request.ApiUpdateMarketOrderRequestPriceRequest;
import com.tmea.wit.service.MarketOrderRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;

@RestController
@RequestMapping("api/market-order-request")
@Validated
public class MarketOrderRequestController {
    MarketOrderRequestService marketOrderRequestService;
    @Autowired
    public MarketOrderRequestController(MarketOrderRequestService marketOrderRequestService) {
        this.marketOrderRequestService = marketOrderRequestService;
    }
    @PostMapping
    public BaseApiResponse addLocation(@RequestBody @Valid ApiAddMarketOrderRequestDto body){
        return marketOrderRequestService.addMarketOrderRequest(body);
    }
    @PostMapping("/approve/{id}")
    public BaseApiResponse approveLogisticsOrderRequestPrice(@RequestBody @Valid ApiApproveMarketOrderRequestDto body,
                                                             @PathVariable  @Min(1) Long id)
    {
        return marketOrderRequestService.approveMarketOrderRequest(body,id);
    }
    @PutMapping("/price/{id}")
    public BaseApiResponse updateLogisticsOrderRequestPrice( @RequestBody @Valid ApiUpdateMarketOrderRequestPriceRequest body,
                                                             @PathVariable  @Min(1) Long id)
    {

        return marketOrderRequestService.updateMarketOrderRequestPrice(body,id);
    }
    @PutMapping("/quantity/{id}")
    public BaseApiResponse updateLogisticsOrderRequestQuantity( @RequestBody @Valid ApiUpdateMarketOrderQuantityRequestDto body,
                                                             @PathVariable  @Min(1) Long id)
    {
        return marketOrderRequestService.updateMarketOrderRequestQuantity(body,id);
    }
    @GetMapping("")
    public GeneralApiListResponse getAllMarketOrderRequests(WebRequest request,
                                                           @RequestParam(required = false) @Pattern(regexp ="\\d{4}\\-(0?[1-9]|1[012])\\-(0?[1-9]|[12]\\d|3[01])",message = "Invalid date format . valid date format is YYYY-MM-DD") String date,
                                                           @RequestParam(required = false) @Pattern(regexp ="\\d{4}\\-(0?[1-9]|1[012])\\-(0?[1-9]|[12]\\d|3[01])",message = "Invalid date format . valid date format is YYYY-MM-DD") String dateTo,
                                                           @RequestParam(required = false) @Pattern(regexp ="\\d{4}\\-(0?[1-9]|1[012])\\-(0?[1-9]|[12]\\d|3[01])",message = "Invalid date format . valid date format is YYYY-MM-DD") String dateFrom
                                                    )
    {
        return marketOrderRequestService.getAllMarketOrderRequest(request.getParameterMap(),0);
    }

    @GetMapping("seller")
    public GeneralApiListResponse getSellerMarketOrderRequest(WebRequest request,
                                                              @RequestParam(required = false) @Pattern(regexp ="\\d{4}\\-(0?[1-9]|1[012])\\-(0?[1-9]|[12]\\d|3[01])",message = "Invalid date format . valid date format is YYYY-MM-DD") String date,
                                                              @RequestParam(required = false) @Pattern(regexp ="\\d{4}\\-(0?[1-9]|1[012])\\-(0?[1-9]|[12]\\d|3[01])",message = "Invalid date format . valid date format is YYYY-MM-DD") String dateTo,
                                                              @RequestParam(required = false) @Pattern(regexp ="\\d{4}\\-(0?[1-9]|1[012])\\-(0?[1-9]|[12]\\d|3[01])",message = "Invalid date format . valid date format is YYYY-MM-DD") String dateFrom
                                                              )
    {
        return marketOrderRequestService.getAllMarketOrderRequest(request.getParameterMap(), 1);
    }
    @GetMapping("buyer")
    public GeneralApiListResponse getBuyerMarketOrderRequest(WebRequest request,
                                                             @RequestParam(required = false) @Pattern(regexp ="\\d{4}\\-(0?[1-9]|1[012])\\-(0?[1-9]|[12]\\d|3[01])",message = "Invalid date format . valid date format is YYYY-MM-DD") String date,
                                                             @RequestParam(required = false) @Pattern(regexp ="\\d{4}\\-(0?[1-9]|1[012])\\-(0?[1-9]|[12]\\d|3[01])",message = "Invalid date format . valid date format is YYYY-MM-DD") String dateTo,
                                                             @RequestParam(required = false) @Pattern(regexp ="\\d{4}\\-(0?[1-9]|1[012])\\-(0?[1-9]|[12]\\d|3[01])",message = "Invalid date format . valid date format is YYYY-MM-DD") String dateFrom)
    {
        return marketOrderRequestService.getAllMarketOrderRequest(request.getParameterMap(), 2);
    }
    @GetMapping("/{id}")
    public BaseApiResponse getAllMarketOrderRequestsById(@PathVariable  @Min(1) Long id)
    {
        return marketOrderRequestService.getAllMarketOrderRequestById(id);
    }
}
