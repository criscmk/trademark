package com.tmea.wit.controller.api;

import com.tmea.wit.model.BaseApiResponse;
import com.tmea.wit.model.GeneralApiListResponse;
import com.tmea.wit.model.dto.request.ApiTradeDocumentCategoryPostDto;
import com.tmea.wit.service.TradeDocumentCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.security.Principal;

@RestController
@RequestMapping("api/trade-document-category/")
@Validated
public class TradeDocumentCategoryController {

    @Autowired
    TradeDocumentCategoryService tradeDocumentCategoryService;

    @PostMapping("")
    public BaseApiResponse addCategory(@Valid @RequestBody ApiTradeDocumentCategoryPostDto body, Principal principal){
        return tradeDocumentCategoryService.addCategory(body, principal.getName());
    }

    @PutMapping("edit/{id}")
    public BaseApiResponse editCategory(@Valid @RequestBody ApiTradeDocumentCategoryPostDto body,
                                        @PathVariable("id")
                                        @Valid @NotNull(message = "Category id cannot be null")
                                        @Min(1) Long id,
                                        Principal principal){
        return tradeDocumentCategoryService.editCategory(body, id, principal.getName());
    }

    @GetMapping("")
    public GeneralApiListResponse fetchAll(WebRequest webRequest){
        return tradeDocumentCategoryService.fetchAllCategories(webRequest.getParameterMap());
    }
}
