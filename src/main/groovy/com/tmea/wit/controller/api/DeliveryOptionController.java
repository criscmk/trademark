package com.tmea.wit.controller.api;

import com.tmea.wit.model.BaseApiResponse;
import com.tmea.wit.model.GeneralApiListResponse;
import com.tmea.wit.model.dto.request.ApiDeliveryOptionRequestDto;
import com.tmea.wit.model.dto.request.ApiStatusRequestDto;
import com.tmea.wit.service.DeliveryOptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import javax.validation.Valid;
import javax.validation.constraints.Min;

@RestController
@RequestMapping("api/delivery-option")
@Validated
public class DeliveryOptionController {

    DeliveryOptionService deliveryOptionService;

    @Autowired
    public DeliveryOptionController(DeliveryOptionService deliveryOptionService) {
        this.deliveryOptionService = deliveryOptionService;
    }

    @GetMapping
    public GeneralApiListResponse getDeliveryOptions(WebRequest request,
                                                   @Min(0) Long page,
                                                   @Min(2) Long size){
        return deliveryOptionService.getDeliveryOptions(request.getParameterMap());
    }

    @PostMapping
    public BaseApiResponse addDeliveryOption(@RequestBody @Valid ApiDeliveryOptionRequestDto body){
        return deliveryOptionService.addDeliveryOption(body);
    }

    @PutMapping("{deliveryOptionId}")
    public BaseApiResponse updateDeliveryOption(@RequestBody @Valid ApiDeliveryOptionRequestDto body,
                                          @PathVariable @Min(1) Long deliveryOptionId){
        return deliveryOptionService.updateDeliveryOption(deliveryOptionId, body);
    }

    @PutMapping("status/{deliveryOptionId}")
    public BaseApiResponse updateDeliveryOptionStatus(@RequestBody @Valid ApiStatusRequestDto body,
                                                @PathVariable @Min(1) Long deliveryOptionId){
        return deliveryOptionService.updateDeliveryOptionStatus(deliveryOptionId, body);
    }
}
