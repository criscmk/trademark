package com.tmea.wit.controller.api;

import com.tmea.wit.model.BaseApiResponse;
import com.tmea.wit.model.GeneralApiListResponse;
import com.tmea.wit.model.dto.request.ApiLocationLevelRequestDto;
import com.tmea.wit.model.dto.request.ApiStatusRequestDto;
import com.tmea.wit.service.LocationLevelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import javax.validation.Valid;
import javax.validation.constraints.Min;

@RestController
@RequestMapping("api/location-level")
@Validated
public class LocationLevelController {

    LocationLevelService locationLevelService;

    @Autowired
    public LocationLevelController(LocationLevelService locationLevelService) {
        this.locationLevelService = locationLevelService;
    }

    @GetMapping
    public GeneralApiListResponse fetchAll(WebRequest request,
                                           @Min(0) Long page,
                                           @Min(2) Long size){
        return locationLevelService.getLocationLevels(request.getParameterMap());
    }

    @PostMapping
    public BaseApiResponse addLocationLevel(@RequestBody  @Valid ApiLocationLevelRequestDto body){
        return locationLevelService.addLocationLevel(body);
    }

    @PutMapping("{levelId}")
    public BaseApiResponse updateLocationLevel(@RequestBody @Valid ApiLocationLevelRequestDto body,
                                                @PathVariable @Min(1) Long levelId){
        return locationLevelService.updateLocationLevel(levelId, body);
    }

    @PutMapping("status/{levelId}")
    public BaseApiResponse updateLocationLevelStatus(@RequestBody @Valid ApiStatusRequestDto body,
                                                     @PathVariable @Min(1) Long levelId){
        return locationLevelService.updateLocationLevelStatus(levelId, body);
    }
}
