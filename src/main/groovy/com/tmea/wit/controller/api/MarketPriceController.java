package com.tmea.wit.controller.api;

import com.tmea.wit.model.BaseApiResponse;
import com.tmea.wit.model.GeneralApiListResponse;
import com.tmea.wit.model.dto.request.ApiMarketPricePostDto;
import com.tmea.wit.model.dto.request.ApiMarketPriceUpdateDto;
import com.tmea.wit.service.MarketPriceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;
import java.security.Principal;

@RestController
@RequestMapping("/api/market-price")
@Validated
public class MarketPriceController {
    @Autowired
    MarketPriceService marketPriceService;

    @PostMapping("")
    public BaseApiResponse postMarketPrice(@Valid @RequestBody ApiMarketPricePostDto body,Principal principal){
        return marketPriceService.postMarketPrice(body,principal.getName());
    }
    @PutMapping("/{id}")
    public BaseApiResponse updateMarketPrice(@Valid @RequestBody ApiMarketPriceUpdateDto body, Principal principal,@PathVariable @Min(1) Long  id){
        return marketPriceService.updateMarketPrice(body,id,principal.getName());
    }
    @GetMapping("")
    public GeneralApiListResponse getMarketPriceInquiry(@RequestParam(required = false)        @Min(1) Long commodity,
                                                        @RequestParam(required = false)        @Min(1) Long location,
                                                        @RequestParam(required = false)        @Min(1) Long category,
                                                        @RequestParam(required = false)        @Min(1) Long subCategory,
                                                        @RequestParam(required = false)        @Pattern(regexp ="\\d{4}\\-(0?[1-9]|1[012])\\-(0?[1-9]|[12]\\d|3[01])",message = "Invalid date format . valid date format is YYYY-MM-DD") String date,
                                                        @RequestParam(required = false)        @Pattern(regexp ="\\d{4}\\-(0?[1-9]|1[012])\\-(0?[1-9]|[12]\\d|3[01])",message = "Invalid date format . valid date format is YYYY-MM-DD") String dateTo,
                                                        @RequestParam(required = false)        @Pattern(regexp ="\\d{4}\\-(0?[1-9]|1[012])\\-(0?[1-9]|[12]\\d|3[01])",message = "Invalid date format . valid date format is YYYY-MM-DD") String dateFrom,
                                                        @RequestParam(required = false)        String query,
                                                        @RequestParam(required = false)        String aggregator,
                                                        @RequestParam(required = false)        @Min(0)  Integer page,
                                                        @RequestParam(required = false)        @Min(2) Integer  size)
    {
        return marketPriceService.getCommodityMarketPrice(commodity, location, date,category,subCategory,dateFrom,dateTo,query,aggregator,page,size);
    }



}

