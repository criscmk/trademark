package com.tmea.wit.controller.api;

import com.tmea.wit.model.BaseApiResponse;
import com.tmea.wit.model.dto.request.ApiAddStandardSpecificationTypeRequestDto;
import com.tmea.wit.model.dto.request.ApiEditStandardSpecificationTypeRequestDto;
import com.tmea.wit.model.dto.request.ApiStatusRequestDto;
import com.tmea.wit.model.dto.request.ApiTaxTypeRequestDto;
import com.tmea.wit.service.StandardSpecificationTypeService;
import com.tmea.wit.service.TaxTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("api/standard-specification-type")
@Validated
public class StandardSpecificationTypeController {
    StandardSpecificationTypeService standardSpecificationTypeService;

    @Autowired
    public StandardSpecificationTypeController(StandardSpecificationTypeService standardSpecificationTypeService) {
        this.standardSpecificationTypeService = standardSpecificationTypeService;
    }
    @PostMapping
    public BaseApiResponse addStandardSpecificationTypeService(@RequestBody @Valid ApiAddStandardSpecificationTypeRequestDto body){
        return standardSpecificationTypeService.addStandardSpecificationType(body);
    }
    @PutMapping("status/{id}")
    public BaseApiResponse updateStandardSpecificationTypeStatus(@RequestBody @Valid ApiStatusRequestDto body,
                                                  @PathVariable @Min(1)
                                                  @Valid @NotNull(message = "standardSpecificationType  id cannot be null") Long id) {
        return standardSpecificationTypeService.updateStandardSpecificationTypeStatus(id, body);
    }
    @PutMapping("{id}")
    public BaseApiResponse updateStandardSpecificationTypeDetails(@RequestBody @Valid ApiEditStandardSpecificationTypeRequestDto body,
                                                  @PathVariable @Min(1)
                                                  @Valid @NotNull(message = "standardSpecificationType  id cannot be null") Long id) {

        return standardSpecificationTypeService.updateStandardSpecificationTypeDetails(id, body);
    }
    @GetMapping()
    public BaseApiResponse fetchStandardSpecificationTypeDetails(WebRequest request,
                                                                 @Min(0) Long page,
                                                                 @Min(2) Long size) {

        return standardSpecificationTypeService.fetchStandardSpecificationTypeDetails(request.getParameterMap(),false);
    }
    @GetMapping("admin")
    public BaseApiResponse fetchStandardSpecificationTypeAdminDetails(WebRequest request,
                                                                 @Min(0) Long page,
                                                                 @Min(2) Long size) {

        return standardSpecificationTypeService.fetchStandardSpecificationTypeDetails(request.getParameterMap(),true);
    }
    @GetMapping("{id}")
    public BaseApiResponse fetchStandardSpecificationTypeDetailsById( @PathVariable @Min(1)
                                                                      @Valid @NotNull(message = "standardSpecificationType  id cannot be null") Long id) {

        return standardSpecificationTypeService.fetchStandardSpecificationTypeDetailsById(id);
    }

}
