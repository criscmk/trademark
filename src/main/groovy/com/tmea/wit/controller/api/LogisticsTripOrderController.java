package com.tmea.wit.controller.api;

import com.tmea.wit.model.BaseApiResponse;
import com.tmea.wit.model.GeneralApiListResponse;
import com.tmea.wit.model.dto.request.ApiAssignOrderToVehicleRequestDto;
import com.tmea.wit.service.LogisticsTripOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping(value = "api/logistics-trip-order")
@Validated
public class LogisticsTripOrderController {
    LogisticsTripOrderService logisticsOrderTripService;
    @Autowired
    public LogisticsTripOrderController(LogisticsTripOrderService logisticsOrderTripService) {
        this.logisticsOrderTripService = logisticsOrderTripService;
    }
    @PostMapping
    public BaseApiResponse assignOrderToVehicle(@RequestBody @Valid ApiAssignOrderToVehicleRequestDto body){
        return logisticsOrderTripService.assignOrderToVehicle(body);
    }
    @GetMapping("/{id}")
    public GeneralApiListResponse getServiceDetails(@PathVariable("id")
                                             @Valid @NotNull(message = "Trip id cannot be null")
                                             @Min(1) Long id) {
        return logisticsOrderTripService.getLogisticsTripOrder(id);
    }
    @PostMapping("/cancel/{id}")
    public BaseApiResponse cancelTripOrder(@PathVariable("id")
                                             @Valid @NotNull(message = "Trip id cannot be null")
                                             @Min(1) Long id) {
        return logisticsOrderTripService.cancelTripOrder(id);
    }
}
