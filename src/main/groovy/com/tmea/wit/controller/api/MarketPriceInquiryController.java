package com.tmea.wit.controller.api;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tmea.wit.model.BaseApiResponse;
import com.tmea.wit.model.GeneralApiListResponse;
import com.tmea.wit.model.dto.request.ApiAnswerMarketPriceInquiryDto;
import com.tmea.wit.model.dto.request.ApiMarketPriceInquiryDto;
import com.tmea.wit.service.MarketPriceInquiryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.security.Principal;

@RestController
@RequestMapping("/api/market-price-inquiry")
@Validated
public class MarketPriceInquiryController {
    @Autowired
    MarketPriceInquiryService marketPriceInquiryService;



    @PostMapping("")
    public BaseApiResponse postMarketPriceInquiry(@Valid @RequestBody ApiMarketPriceInquiryDto body, Principal principal){
        return marketPriceInquiryService.postCommodityMarketPriceInquiry(body,principal.getName());
    }

    @PostMapping("/answer")
    public BaseApiResponse AnswerMarketPriceInquiry(@Valid @RequestBody ApiAnswerMarketPriceInquiryDto body, Principal principal) {
        return marketPriceInquiryService.answerMarketPriceInquiry(body, principal.getName());
    }
    @GetMapping("")
    public GeneralApiListResponse getAllMarketPriceInquiry(@RequestParam(required = false)  @Max(Long.MAX_VALUE) Integer page,
                                                           @RequestParam(required = false)  @Max(Long.MAX_VALUE) Integer size,
                                                           @RequestParam(required = false)  @Pattern(regexp ="\\d{4}\\-(0?[1-9]|1[012])\\-(0?[1-9]|[12]\\d|3[01])",message = "Invalid date format . valid date format is YYYY-MM-DD") String date,
                                                           @RequestParam(required = false)  @Pattern(regexp ="\\d{4}\\-(0?[1-9]|1[012])\\-(0?[1-9]|[12]\\d|3[01])",message = "Invalid date format . valid date format is YYYY-MM-DD") String dateTo,
                                                           @RequestParam(required = false)  @Pattern(regexp ="\\d{4}\\-(0?[1-9]|1[012])\\-(0?[1-9]|[12]\\d|3[01])",message = "Invalid date format . valid date format is YYYY-MM-DD") String dateFrom,
                                                           @RequestParam(required = false)  String  isAnswered)
    {
        return marketPriceInquiryService.getAllMarketPriceInquiries(isAnswered,date,dateFrom,dateTo,page,size);
    }
    @GetMapping("/find")
    public BaseApiResponse getMarketPriceInquiry(@RequestParam("commodity") @NotNull @Max(Long.MAX_VALUE) Long commodity,
                                                 @RequestParam("location") @NotNull @Max(Long.MAX_VALUE) Long location,
                                                 @RequestParam(required = false)  @Pattern(regexp ="\\d{4}\\-(0?[1-9]|1[012])\\-(0?[1-9]|[12]\\d|3[01])",message = "Invalid date format . valid date format is YYYY-MM-DD")String date,

                                                 Principal principal) {
        return marketPriceInquiryService.getCommodityMarketInquiry(commodity, location, date, principal.getName());
    }


}
