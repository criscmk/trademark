package com.tmea.wit.controller.api;

import com.tmea.wit.model.BaseApiResponse;
import com.tmea.wit.model.GeneralApiListResponse;
import com.tmea.wit.model.dto.request.ApiStatusRequestDto;
import com.tmea.wit.service.OrganisationSizeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import javax.validation.Valid;
import javax.validation.constraints.Min;

@RestController
@RequestMapping("api/organisation-size")
@Validated
public class OrganisationSizeController {
    OrganisationSizeService organisationSizeService;

    @Autowired
    public OrganisationSizeController(OrganisationSizeService organisationSizeService) {
        this.organisationSizeService = organisationSizeService;
    }

    @GetMapping
    public GeneralApiListResponse fetchAll(WebRequest request,
                                           @Min(0) Long page,
                                           @Min(2) Long size){
        return organisationSizeService.getOrganisationSize(request.getParameterMap());
    }

    @PutMapping("/status/{organisationSizeId}")
    public BaseApiResponse updateUserTypeStatus(@PathVariable @Min(1) Long organisationSizeId,
                                                @RequestBody @Valid ApiStatusRequestDto body ){
        return organisationSizeService.updateOrganisationSizeStatus(organisationSizeId, body);
    }
}
