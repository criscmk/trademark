package com.tmea.wit.controller.api;

import com.tmea.wit.model.BaseApiResponse;
import com.tmea.wit.model.GeneralApiListResponse;
import com.tmea.wit.model.dto.request.ApiStatusRequestDto;
import com.tmea.wit.service.LanguageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import javax.validation.Valid;
import javax.validation.constraints.Min;

@RestController
@RequestMapping("api/language")
public class LanguageController {
    LanguageService languageService;

    @Autowired
    public LanguageController(LanguageService languageService) {
        this.languageService = languageService;
    }

    @GetMapping("/list")
    public GeneralApiListResponse fetchAll(WebRequest request,
                                           @Min(0) Long page,
                                           @Min(2) Long size){
        return languageService.getLanguages(request.getParameterMap());
    }

    @PutMapping("/status/{languageId}")
    public BaseApiResponse updateUserTypeStatus(@PathVariable @Min(1) Long languageId,
                                                @RequestBody @Valid ApiStatusRequestDto body ){
        return languageService.updateLanguageStatus(languageId, body);
    }
}
