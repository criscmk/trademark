package com.tmea.wit.controller.api;

import com.tmea.wit.model.BaseApiResponse;
import com.tmea.wit.model.GeneralApiListResponse;
import com.tmea.wit.model.dto.request.ApiStatusRequestDto;
import com.tmea.wit.service.GenderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import javax.validation.Valid;
import javax.validation.constraints.Min;

@RestController
@RequestMapping("api/gender")
@Validated
public class GenderController {
    GenderService genderService;

    @Autowired
    public GenderController(GenderService genderService) {
        this.genderService = genderService;
    }

    @GetMapping
    public GeneralApiListResponse fetchAll(WebRequest request,
                                           @Min(0) Long page,
                                           @Min(2) Long size){
        return genderService.getGenders(request.getParameterMap());
    }

    @PutMapping("/status/{genderId}")
    public BaseApiResponse updateUserTypeStatus(@PathVariable @Min(1) Long genderId,
                                                @RequestBody @Valid ApiStatusRequestDto body){
        return genderService.updateGenderStatus(genderId, body);
    }
}
