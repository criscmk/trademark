package com.tmea.wit.controller.api;

import com.tmea.wit.model.BaseApiResponse;
import com.tmea.wit.model.dto.request.ApiResendVerificationCodeDto;
import com.tmea.wit.model.dto.request.ApiValidateCodeDto;
import com.tmea.wit.service.SmsVerificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/phoneverification")
@Validated
public class UserPhoneVerificationController {

    @Autowired
    SmsVerificationService smsVerificationService;



    @PostMapping("/resend")
    public BaseApiResponse resendVerificationCode(@Valid @RequestBody ApiResendVerificationCodeDto apiResendVerificationCodeDto){
        return smsVerificationService.resendVerificationCode(apiResendVerificationCodeDto);
    }

    @PostMapping("/validate")
    public BaseApiResponse validateVerificationCode(@Valid @RequestBody ApiValidateCodeDto apiValidateCodeDto){
        return smsVerificationService.validateCode(apiValidateCodeDto);
    }


}
