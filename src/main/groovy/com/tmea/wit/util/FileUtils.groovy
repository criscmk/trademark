package com.tmea.wit.util

import com.tmea.wit.core.storage.PolicyTypes
import com.tmea.wit.core.storage.StorageApi
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component
import org.springframework.web.multipart.MultipartFile

import java.text.SimpleDateFormat

@Component
class FileUtils {

    @Value('${services.storage.publicUrl}')
    public final String IMAGE_SERVER_URL

    String encodeFileToBase64String(MultipartFile file){
        String encodedFileString = "data:"+file.getContentType()+";base64,"+Base64
                .getEncoder()
                .encodeToString(file.getBytes())

        return encodedFileString
    }
    static String getExtensionFromBase64String(String fileContent){
        return fileContent.split(";")[0].split("/")[1];
    }

     String saveFile(String userName, long itemId, String bucketName, String image, StorageApi storageApi){
        String fileName = userName+"_"+new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date())
        def imagePath = "${itemId}/$fileName"
        storageApi.saveObjectData(bucketName,imagePath,image, PolicyTypes.READ_AND_WRITE)
        //Logic to update URL
        String imageURL = IMAGE_SERVER_URL+"/"+bucketName+"/"+imagePath+"."+getExtensionFromBase64String(image)
        return imageURL
    }
}
