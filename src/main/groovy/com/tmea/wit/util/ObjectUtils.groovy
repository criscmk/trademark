package com.tmea.wit.util

import org.springframework.util.StringUtils

class ObjectUtils {

    public static Map asMap(def object) {
        def jsonSlurper = new groovy.json.JsonSlurperClassic()
        Map map = jsonSlurper.parseText(this.asJson(object));
        return map
    }

    public static String asJson(def object){
        def jsonOutput = new groovy.json.JsonOutput()
        String json = jsonOutput.toJson(object)
        return json
    }

    public static Object snakeCaseMapToPojo(def mapToConvert,def object){
        mapToConvert.each {
            String key = com.tmea.wit.util.StringUtils.toCamelCase(it.key);
            object[key] = it.value;
        }
        return object;
    }

    public static Map flattenListParam(Map map){
        [:].putAll(map.collect{ k, v ->
            def outPut = (String[]) v;
            new MapEntry(k, outPut[0])});
    }


    public static List<Object> snakeCaseArrayMapToListPojos(def mapsToConvert,Class dto){
        List<Object> pojosList = [];
        mapsToConvert.each{
            def pojo = dto.newInstance();
            def mapToConvert = it;
            mapToConvert.each {
                String key = com.tmea.wit.util.StringUtils.toCamelCase(it.key);
                pojo[key] = it.value;
            }
            pojosList.add(pojo);
        }

        return pojosList;
    }

    public static Object mapToSnakeCaseMap(def mapToConvert){
        Map convertedMap = [:]
        mapToConvert.each {
            String key = com.tmea.wit.util.StringUtils.toCamelCase(it.key);
            convertedMap.put(key,it.value);
        }
        return convertedMap;
    }

    public static List<Object> mapToSnakeCaseArrayMap(def mapsToConvert){
        List responseList = [];
        mapsToConvert.each{
            Map responseItem = [:]
            def mapToConvert = it;
            mapToConvert.each {
                String key = com.tmea.wit.util.StringUtils.toCamelCase(it.key);
                responseItem.put(key,it.value)
            }
            responseList.add(responseItem);
        }

        return responseList;
    }

    public static Map rowAsMap(Map row) {
        def rowMap = [:]
        row.keySet().each {column ->
            rowMap[column] = row[column]
        }
        return rowMap
    }

    public static copyMapProperties(Map source, Map target) {

        source.each { key, value ->
            if (target.containsKey(key)){
                if(value){
                    target[key] = value
                }
            }

        }
    }


}
