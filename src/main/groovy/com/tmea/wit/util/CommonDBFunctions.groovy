package com.tmea.wit.util

import groovy.sql.Sql
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

import javax.sql.DataSource

@Component
class CommonDBFunctions {
    DataSource dataSource

    @Autowired
    CommonDBFunctions(DataSource dataSource) {
        this.dataSource = dataSource
    }

    public long getUserIdFromEmail(String email){
        Sql sql = new Sql(dataSource)
        def emailParam = [email: email]
        long userId = sql.firstRow("SELECT id FROM user WHERE email = ?.email", emailParam).get('id')
        sql.close()
        return userId
    }

    public static Map paginationSqlParams(Map sqlParams, Map params){
        def page =  params.page?.toInteger() ? params.page?.toInteger() : 0
        def size = params.size?.toInteger() ? params.size?.toInteger() : 25
        if(page == 0){
            sqlParams.limit = 1 * size
            sqlParams.start = sqlParams.limit - size
        }else{
            sqlParams.limit = page * size
            sqlParams.start = sqlParams.limit - size
        }
        sqlParams
    }

    public static String getLimitClause(){
        return " LIMIT ?.limit OFFSET ?.start"
    }

    public static Map addQueryFilters(def status, def sqlQuery, def countQuery){
        Map queryMap = [:]
        def limitFilter
        def queryFilter = ""
        if (status){
            queryFilter = " WHERE is_active = ?.status"
            sqlQuery += queryFilter
        }
        limitFilter = " LIMIT ?.limit OFFSET ?.start"
        sqlQuery += limitFilter
        countQuery += queryFilter
        queryMap.put("selectQuery", sqlQuery)
        queryMap.put("countQuery", countQuery)
        queryMap.put("queryFilter", queryFilter)
        return queryMap
    }

    public long getTotalCountBasedOnFilters(String queryFilter, String countQuery, Map sqlParams){
        Sql sql = new Sql(dataSource)
        def total
        if(queryFilter){
            total = sql.firstRow(countQuery, sqlParams)?.get('count')
        }else{
            total = sql.firstRow(countQuery)?.get('count')
        }
        sql.close()
        !total ? total = 0 : total
    }

    public def getDBRecordByTableColumn(String value, String tableName, String column){
        Sql sql = new Sql(dataSource)
        def record = sql.firstRow  "SELECT * FROM ${tableName} WHERE ${column} ILIKE ?", value
        sql.close()
        record
    }

    public static def getStatusAsBoolean(def status){
        if(status.toLowerCase() == 'active'){
            return true
        }else if (status.toLowerCase() == 'inactive'){
            return false
        }else{
            return null
        }
    }
    public def getDBRecordByName(String value, String tableName, String column){
        Sql sql = new Sql(dataSource)
        def record = sql.firstRow  "SELECT * FROM ${tableName} WHERE ${column} ILIKE ?", value
        sql.close()
        record
    }
    public def getDBRecordById(Long value, String tableName, String column){
        Sql sql = new Sql(dataSource)
        def record = sql.firstRow  "SELECT * FROM ${tableName} WHERE ${column} = ?", value
        sql.close()
        record
    }

    public def getDBRecordByNameAndStatus(String value, String tableName, String column){
        Sql sql = new Sql(dataSource)
        def record = sql.firstRow  "SELECT * FROM ${tableName} WHERE is_active AND  ${column} ILIKE ?", value
        sql.close()
        record
    }

    public def getDBRecordByIdAndStatus(Long value, String tableName, String column){
        Sql sql = new Sql(dataSource)
        def record = sql.firstRow  "SELECT * FROM ${tableName} WHERE is_active AND  ${column} = ?", value
        sql.close()
        record
    }

}
