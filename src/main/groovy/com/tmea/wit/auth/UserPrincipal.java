package com.tmea.wit.auth;

import com.tmea.wit.entity.UserEntity;
import com.tmea.wit.entity.PermissionEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import java.util.*;

public class UserPrincipal implements UserDetails {

    private UserEntity user;
    private List<PermissionEntity> userRolePermissions;

    public UserPrincipal(UserEntity user, List<PermissionEntity> userRolePermissions) {
        this.user = user;
        this.userRolePermissions = userRolePermissions;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        if(null==userRolePermissions){
            return Collections.emptySet();
        }
        Set<SimpleGrantedAuthority> grantedAuthorities = new HashSet<>();

        userRolePermissions.forEach(group->{
            grantedAuthorities.add(new SimpleGrantedAuthority(group.getName()));
        });
        return grantedAuthorities;
    }

    @Override
    public String getPassword() {
        return this.user.getPassword();
    }

    @Override
    public String getUsername() {
        return this.user.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return !this.user.getAccountExpired();
    }

    @Override
    public boolean isAccountNonLocked() {
        return !this.user.getAccountLocked();
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return !this.user.getCredentialsExpired();
    }

    @Override
    public boolean isEnabled() {
        return this.user.getActive();
    }
}
