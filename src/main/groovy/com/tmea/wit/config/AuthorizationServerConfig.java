package com.tmea.wit.config;


import com.tmea.wit.service.AuthUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.code.AuthorizationCodeServices;
import org.springframework.security.oauth2.provider.code.JdbcAuthorizationCodeServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;

import javax.sql.DataSource;

@EnableAuthorizationServer
@Configuration
public class AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {

    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private AuthUserDetailsService userDetailsService;

    @Autowired
    @Qualifier("dataSource")
    DataSource dataSource;
    @Autowired
    private TokenStore tokenStore;


    @Override
    public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
        security.passwordEncoder(new BCryptPasswordEncoder(12))
                //.allowFormAuthenticationForClients()
                .allowFormAuthenticationForClients()
                .checkTokenAccess("permitAll()")
                .tokenKeyAccess("permitAll()");
    }

    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        clients.jdbc(dataSource);
                /*.withClient("wit_web_app")
                .scopes("REGISTER_USER")
                .secret("$2y$12$xzgzUqOdUhBMba3JtqT9COvi99flh5lh3DWnrO2LhAj/vSgfIo/Y.")
                .autoApprove(true)
                .authorities("ROLE_WIT_AUTHORIZED_CLIENT")
                .authorizedGrantTypes("client_credentials","password","refresh_token")
                .accessTokenValiditySeconds(3600)
                .refreshTokenValiditySeconds(-1)
                .resourceIds("oauth2-resource")
                .and().build();*/
        /*.withClient("wit_android_app")
                .scopes("REGISTER_USER")
                .secret("$2y$12$4SLZ8/vAreBB9RB7VnbKFOMMdm6Dxu9g7THjHu1cpTa9p2vHl.s.a")
                .autoApprove(true)
                .authorities("ROLE_WIT_AUTHORIZED_CLIENT")
                .authorizedGrantTypes("client_credentials","password","refresh_token")
                .accessTokenValiditySeconds(3600)
                .refreshTokenValiditySeconds(-1)
                .resourceIds("oauth2-resource")
                .and().build();*/
    }

    @Bean
    public TokenStore tokenStore() {
        return new JdbcTokenStore(dataSource);
    }


    @Bean
    protected AuthorizationCodeServices authorizationCodeServices() {
        return new JdbcAuthorizationCodeServices(dataSource);
    }


    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        endpoints
                .authorizationCodeServices(authorizationCodeServices())
                .authenticationManager(authenticationManager)
                .userDetailsService(userDetailsService)
                .tokenStore(tokenStore).approvalStoreDisabled();
    }
}
