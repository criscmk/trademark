package com.tmea.wit.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "user", schema = "public", catalog = "wit")
public class UserEntity implements Serializable {
    private long id;
    private String email;
    private String phoneNumber;
    private String profileImage;
    private String password;
    private Timestamp dateJoined;
    private Integer userTypeId;
    private Long country;
    private Integer languagePreference;
    private Integer locationId;
    private Integer approvalStatus;
    private Boolean isActive;
    private Boolean accountExpired;
    private Boolean accountLocked;
    private Boolean credentialsExpired;
    private String username;

    @Id
    @Column(name = "id")
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "email")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Basic
    @Column(name = "phone_number")
    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Basic
    @Column(name = "profile_image")
    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    @Basic
    @Column(name = "password")
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Basic
    @Column(name = "date_joined")
    public Timestamp getDateJoined() {
        return dateJoined;
    }

    public void setDateJoined(Timestamp dateJoined) {
        this.dateJoined = dateJoined;
    }

    @Basic
    @Column(name = "user_type_id")
    public Integer getUserTypeId() {
        return userTypeId;
    }

    public void setUserTypeId(Integer userTypeId) {
        this.userTypeId = userTypeId;
    }

    @Basic
    @Column(name = "country_id")
    public Long getCountry() {
        return country;
    }

    public void setCountry(Long country) {
        this.country = country;
    }

    @Basic
    @Column(name = "language_preference")
    public Integer getLanguagePreference() {
        return languagePreference;
    }

    public void setLanguagePreference(Integer languagePreference) {
        this.languagePreference = languagePreference;
    }

    @Basic
    @Column(name = "location_id")
    public Integer getLocationId() {
        return locationId;
    }

    public void setLocationId(Integer locationId) {
        this.locationId = locationId;
    }

    @Basic
    @Column(name = "approval_status_id")
    public Integer getApprovalStatus() {
        return approvalStatus;
    }

    public void setApprovalStatus(Integer approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    @Basic
    @Column(name = "is_active")
    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    @Basic
    @Column(name = "account_expired")
    public Boolean getAccountExpired() {
        return accountExpired;
    }

    public void setAccountExpired(Boolean accountExpired) {
        this.accountExpired = accountExpired;
    }

    @Basic
    @Column(name = "account_locked")
    public Boolean getAccountLocked() {
        return accountLocked;
    }

    public void setAccountLocked(Boolean accountLocked) {
        this.accountLocked = accountLocked;
    }

    @Basic
    @Column(name = "credentials_expired")
    public Boolean getCredentialsExpired() {
        return credentialsExpired;
    }

    public void setCredentialsExpired(Boolean credentialsExpired) {
        this.credentialsExpired = credentialsExpired;
    }

    @Basic
    @Column(name = "username")
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserEntity that = (UserEntity) o;
        return id == that.id &&
                Objects.equals(email, that.email) &&
                Objects.equals(phoneNumber, that.phoneNumber) &&
                Objects.equals(profileImage, that.profileImage) &&
                Objects.equals(password, that.password) &&
                Objects.equals(dateJoined, that.dateJoined) &&
                Objects.equals(userTypeId, that.userTypeId) &&
                Objects.equals(country, that.country) &&
                Objects.equals(languagePreference, that.languagePreference) &&
                Objects.equals(locationId, that.locationId) &&
                Objects.equals(approvalStatus, that.approvalStatus) &&
                Objects.equals(isActive, that.isActive) &&
                Objects.equals(accountExpired, that.accountExpired) &&
                Objects.equals(accountLocked, that.accountLocked) &&
                Objects.equals(credentialsExpired, that.credentialsExpired) &&
                Objects.equals(username, that.username);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, email, phoneNumber, profileImage, password, dateJoined, userTypeId, country, languagePreference, locationId, approvalStatus, isActive, accountExpired, accountLocked, credentialsExpired, username);
    }
}
