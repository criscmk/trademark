package com.tmea.wit.model.dto.request

import javax.validation.constraints.Max
import javax.validation.constraints.Min
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

class ApiAddStandardRequestDto {
    @NotBlank(message = "name cannot be null")
    @Size(min = 3, max = 100)
    String title
    @NotBlank(message = "name cannot be null")
    String description
    @NotNull(message = "trade operation id cannot be null")
    @Max(Long.MAX_VALUE)
    @Min(1L)
    Long tradeOperation
    @NotNull(message = "commodity id cannot be null")
    @Max(Long.MAX_VALUE)
    @Min(1L)
    Long commodity
    @NotNull(message = "import location id is required")
    @Max(Long.MAX_VALUE)
    @Min(1L)
    Long importLocation
    @NotNull(message = "import location id is required")
    @Max(Long.MAX_VALUE)
    @Min(1L)
    Long exportLocation
}
