package com.tmea.wit.model.dto.response

class ApiLogisticsServiceAddedChargesResponseDto {
    Long id
    Long logisticsServiceId
    String serviceType
    Double amount

    ApiLogisticsServiceAddedChargesResponseDto() {
    }
}
