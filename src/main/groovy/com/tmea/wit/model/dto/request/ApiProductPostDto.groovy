package com.tmea.wit.model.dto.request

import javax.validation.constraints.DecimalMin
import javax.validation.constraints.Max
import javax.validation.constraints.Min
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull
import javax.validation.constraints.Pattern
import javax.validation.constraints.PositiveOrZero
import javax.validation.constraints.Size

class ApiProductPostDto {
    @NotNull(message = "Commodity id is required")
    @Min(1L)
    @Max(Long.MAX_VALUE)
    Long commodityId
    @NotBlank(message = "Product name is required")
    @Size(min = 3, max = 100)
    String name
    @Size(min = 3, max = 200)
    String description
    @NotNull(message = "Price is required")
    @DecimalMin(value = "0.0", inclusive = false, message = "Price cannot be zero")
    Double price

    @NotNull(message = "Minimum order quantity cannot be null")
    @PositiveOrZero(message = "Minimum order quantity cannot be of negative value")
    Integer minimumOrderQuantity
    @NotNull(message = "Measurement unit id is required")
    @Min(1L)
    @Max(Long.MAX_VALUE)
    Long measurementUnitId
    @NotNull
    @Pattern(regexp = "data:image\\/([a-zA-Z]*);base64,(.*)",message = "Must be a base64 encoded image")
    String primaryImage
    List<ApiImageUploadRequestDto> images
    @NotNull(message = "Language id is required")
    @Min(1L)
    @Max(Long.MAX_VALUE)
    Long languageId
    @NotNull(message = "isInStock value is required")
    Boolean isInStock
    @NotNull(message = "Publish value is required")
    Boolean isPublished
}
