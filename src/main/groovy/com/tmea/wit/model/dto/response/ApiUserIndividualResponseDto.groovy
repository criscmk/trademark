package com.tmea.wit.model.dto.response

class ApiUserIndividualResponseDto extends ApiUserBaseResponseDto{
    String firstName;
    String middleName;
    String lastName;
    String genderName;
}
