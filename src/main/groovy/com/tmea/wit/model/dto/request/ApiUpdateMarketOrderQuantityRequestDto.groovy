package com.tmea.wit.model.dto.request

import javax.validation.constraints.NotNull

class ApiUpdateMarketOrderQuantityRequestDto {
    @NotNull(message ="New Market Order Request quantity is required")
    Double quantity;
}
