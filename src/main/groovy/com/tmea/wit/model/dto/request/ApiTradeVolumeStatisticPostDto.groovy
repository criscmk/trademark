package com.tmea.wit.model.dto.request

import javax.validation.constraints.*

class ApiTradeVolumeStatisticPostDto {

    @NotNull(message = "Commodity cannot be null")
    @Max(Long.MAX_VALUE)
    @Min(1L)
    Long commodityId;

    @NotNull(message = "Origin country cannot be null")
    @Max(Long.MAX_VALUE)
    @Min(1L)
    Long originCountryId;

    @NotNull(message = "Trade operation type cannot be null")
    @Max(Long.MAX_VALUE)
    @Min(1L)
    Long tradeOperationTypeId

    @NotNull(message = "Destination country cannot be null")
    @Max(Long.MAX_VALUE)
    @Min(1L)
    Long destinationCountryId;

    @NotNull(message = "Month cannot be null")
    @Min(value = 1L, message = "Month cannot be less than 1")
    @Max(value = 12L, message = "Month cannot be greater than 12")
    Integer month

    @NotNull(message = "Year cannot be null")
    @Pattern(regexp = /\d{4}/, message = "Wrong year format")
    String year

    @NotNull(message = "Quantity cannot be null")
    @PositiveOrZero(message = "Quantity cannot be of negative value")
    Long quantity

}
