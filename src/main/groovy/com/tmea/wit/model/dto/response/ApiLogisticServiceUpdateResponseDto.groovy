package com.tmea.wit.model.dto.response

class ApiLogisticServiceUpdateResponseDto {
    Long id
    String name
    String description
}
