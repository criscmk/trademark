package com.tmea.wit.model.dto.response

class ApiGenderResponseDto {
    Long id
    String name
    Boolean isActive
}
