package com.tmea.wit.model.dto.request

import javax.validation.constraints.NotBlank
import javax.validation.constraints.Size

class ApiAddTradeOperationRequestDto {
    @NotBlank
    @Size(min = 3, max = 200)
    String name;
    @NotBlank
    @Size(min = 3, max = 200)
    String description;
}
