package com.tmea.wit.model.dto.request


import javax.validation.constraints.NotNull

class ApiUpdateMarketOrderRequestPriceRequest {
    @NotNull(message ="New Market Order Request price is required")

    BigDecimal price;
}
