package com.tmea.wit.model.dto.response

class ApiLogisticsServiceResponseDto {
    Long id
    String name
    String description
    String locationFrom
    String locationTo
    String providerName
    boolean isActive
    ApiLogisticsServiceTariffResponseDto tariff
    List<ApiLogisticsServiceAddedChargesResponseDto> addedCharges
}
