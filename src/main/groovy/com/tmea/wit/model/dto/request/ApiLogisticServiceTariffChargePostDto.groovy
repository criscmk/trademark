package com.tmea.wit.model.dto.request


import javax.validation.constraints.NotNull

class ApiLogisticServiceTariffChargePostDto {

    @NotNull(message = "Unit from is mandatory")
    Integer fromUnit

    @NotNull(message = "Unit to is mandatory")
    Integer toUnit

    @NotNull(message = "Charge is mandatory")
    Double charge
}
