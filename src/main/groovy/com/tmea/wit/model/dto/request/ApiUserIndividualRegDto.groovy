package com.tmea.wit.model.dto.request;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class ApiUserIndividualRegDto extends ApiUserBaseRegDto{
    @NotBlank
    @Size(min = 3,max = 100)
    String  firstName;
    @NotBlank
    @Size(min = 3,max = 100)
    String middleName;
    @NotBlank
    @Size(min = 3,max = 100)
    String lastName;
    @NotNull
    @Min(value = 1l)
    Long gender;
}
