package com.tmea.wit.model.dto.response

class ApiFetchMarketOrderRequestResponseDto {
    Long id
    String product
    Long commodityId
    String commodityName
    String description
    Double unitPrice
    Integer quantityOrdered
    Double totalPrice
    String customerComments
    String approvalStatus
    String measurementUnit
    String soldByPhone
    String soldByEmail
    String soldByName
    String requestedByPhone
    String requestedByEmail
    String requestedByName
    String dateRequested

}
