package com.tmea.wit.model.dto.response

class ApiUserBaseResponseDto {
    Long id;
    String email;
    String phoneNumber;
    String profileImage;
    String countryName;
    String languageName;
    Boolean isActive;
    String username;
    String locationName;
    Long userTypeId;
    String userTypeName;
    BigDecimal rating
}
