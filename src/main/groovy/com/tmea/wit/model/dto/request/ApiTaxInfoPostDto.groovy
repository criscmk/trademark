package com.tmea.wit.model.dto.request

import javax.validation.constraints.Max
import javax.validation.constraints.Min
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

class ApiTaxInfoPostDto {
    @NotNull(message = "Tax cannot be blank")
    @Max(Long.MAX_VALUE)
    @Min(value = 1L, message = "Only positive integers allowed")
    Long taxId
    @NotBlank(message = "Description cannot be blank")
    @Size(min = 3)
    String description
    @NotBlank(message = "Basis of charge cannot be blank")
    @Size(min = 3)
    String basisOfCharge
    @NotBlank(message = "Taxable good cannot be blank")
    @Size(min = 3)
    String taxableGood
    @NotNull(message = "Rate cannot be blank")
    Double rate
    @NotBlank(message = "Exemption cannot be blank")
    @Size(min = 3)
    String exemption
    @Size(min = 3)
    String additionalInformation
    @NotNull(message = "Regulatory agency cannot be blank")
    @Max(Long.MAX_VALUE)
    @Min(value = 1L, message = "Only positive integers allowed")
    Long regulatoryAgency
}
