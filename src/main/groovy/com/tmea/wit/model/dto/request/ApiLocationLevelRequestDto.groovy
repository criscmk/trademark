package com.tmea.wit.model.dto.request


import javax.validation.constraints.Min
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

class ApiLocationLevelRequestDto {
    @NotBlank(message = "location level name cannot be blank")
    @Size(min = 3, max = 50)
    String name
    @Min(value = 1l)
    Long parentId
}
