package com.tmea.wit.model.dto.response;

import com.tmea.wit.model.BaseApiResponse;

public class ApiPagedDataResponse extends BaseApiResponse {

    private Integer total;

    public ApiPagedDataResponse(int status, String message) {
        super(status, message);
        this.total = total;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }
}
