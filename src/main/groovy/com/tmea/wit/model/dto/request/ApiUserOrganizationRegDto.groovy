package com.tmea.wit.model.dto.request;

import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class ApiUserOrganizationRegDto extends ApiUserIndividualRegDto{
    @NotBlank
    @Size(min = 3,max = 200)
    String organizationName;
    @NotNull
    @Min(value = 1l)
    Integer organizationType;
    @NotNull
    @Min(value = 1l)
    Integer organizationSize;
    @Size(min = 3,max = 50)
    String postalAddress;
    @Size(min = 3,max = 50)
    String physicalAddress;
    @URL
    String website;
}
