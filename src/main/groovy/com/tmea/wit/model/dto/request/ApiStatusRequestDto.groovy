package com.tmea.wit.model.dto.request

import com.tmea.wit.constants.ApiStatusOptions
import com.tmea.wit.validators.EnumValidator

import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

class ApiStatusRequestDto {
    @NotNull
    @Size(min = 6, max = 8)
    @EnumValidator(message = "Status needs to be ACTIVE or INACTIVE", enumClazz = ApiStatusOptions.class)
    String status
}
