package com.tmea.wit.model.dto.request

import javax.validation.constraints.*

class ApiLogisticsServiceOrderRequestDto {

    @NotBlank(message = "description cannot be blank")
    @Size(min = 3, max = 100)
    String description
    @NotNull(message ="order Price is required")
    Double price;
    @NotNull(message ="order quantity is required")
    Integer quantity;
    @NotNull
    @Max(Long.MAX_VALUE)
    @Min(1L)
    Long logisticsService;
    @Max(Long.MAX_VALUE)
    @Min(1L)
    Long marketOrderId;

}
