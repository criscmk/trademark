package com.tmea.wit.model.dto.response

public class APiFetchAllMarketPriceInquiryResponseDto {
    Long id
    String commodity
    String location
    String marketPriceDate
    String DateInquired
    Boolean IsAnswered
    String commodityCategory
    String commoditySubCategory
    String inquiredByName
    String inquiredByEmail
    String inquiredByPhone
    String comment

    APiFetchAllMarketPriceInquiryResponseDto() {
    }
}
