package com.tmea.wit.model.dto.response

class ApiPaymentOptionResponseDto {
    Long id
    String name
    String description
    Boolean isActive
    Long addedBy
    Date dateAdded
}
