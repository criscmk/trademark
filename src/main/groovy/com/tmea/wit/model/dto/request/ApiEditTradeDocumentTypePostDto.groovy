package com.tmea.wit.model.dto.request

import javax.validation.constraints.Max
import javax.validation.constraints.Min
import javax.validation.constraints.Size

class ApiEditTradeDocumentTypePostDto {
    @Size(min = 3 , max = 50)
    String name
    @Max(Long.MAX_VALUE)
    @Min(value = 1L, message = "Only positive integers allowed")
    Long tradeDocumentCategoryId
}
