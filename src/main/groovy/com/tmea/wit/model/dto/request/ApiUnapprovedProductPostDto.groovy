package com.tmea.wit.model.dto.request

import javax.validation.constraints.Max
import javax.validation.constraints.Min
import javax.validation.constraints.NotNull

class ApiUnapprovedProductPostDto {
    @NotNull(message = "Product id cannot be null")
    @Min(1L)
    @Max(Long.MAX_VALUE)
    Long id
}
