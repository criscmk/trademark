package com.tmea.wit.model.dto.response

class ApiCurrencyResponseDto {
    Long id
    String name
    String code
    String currencySymbol
    Long display
    Long countryId
    Boolean isActive
}
