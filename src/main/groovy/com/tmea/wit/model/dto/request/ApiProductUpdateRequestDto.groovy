package com.tmea.wit.model.dto.request

import javax.validation.constraints.DecimalMin
import javax.validation.constraints.Max
import javax.validation.constraints.Min
import javax.validation.constraints.Size

class ApiProductUpdateRequestDto {
    @Size(min = 3, max = 100)
    String name
    @Size(min = 3, max = 200)
    String description
    @DecimalMin(value = "0.0", inclusive = false, message = "Price cannot be zero")
    Double price
    Integer minimum_order_quantity
    @Min(1L)
    @Max(Long.MAX_VALUE)
    Long measurement_unit_id
}
