package com.tmea.wit.model.dto.response

class ApiTaxResponseDto {
    Long id
    String name
    Integer value
    Long chargeModeId
    Boolean isActive
    Long addedBy
    Date dateAdded
}
