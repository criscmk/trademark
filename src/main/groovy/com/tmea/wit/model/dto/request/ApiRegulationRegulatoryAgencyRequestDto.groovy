package com.tmea.wit.model.dto.request

import javax.validation.constraints.Max
import javax.validation.constraints.Min
import javax.validation.constraints.NotNull

class ApiRegulationRegulatoryAgencyRequestDto {
    @NotNull
    @Min(1L)
    @Max(Long.MAX_VALUE)
    Long regulatoryAgency
    @NotNull
    @Min(1L)
    @Max(Long.MAX_VALUE)
    Long regulation


}
