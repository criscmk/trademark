package com.tmea.wit.model.dto.response

class ApiMeasurementUnitResponseDto {
    Long id
    String name
    String description
    String code
    Long measurementMetricId
    Boolean isActive
    Long addedBy
    Date dateAdded
}
