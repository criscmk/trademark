package com.tmea.wit.model.dto.response

class ApiFetchAllProductsResponseDto {
    Long id
    String name
    String description
    BigDecimal price
    Integer minimumOrderQuantity
    String commodityName
    String measurementUnit
    String categoryName
    String subCategoryName
    String language
    String traderName
    Long traderId
    Boolean isPostToInventory
    Boolean isUpdateSales
    Boolean isApproved
    String dateAdded
    Boolean isInStock
    Boolean isPublished
    Boolean isFeatured
    Boolean isActive
}
