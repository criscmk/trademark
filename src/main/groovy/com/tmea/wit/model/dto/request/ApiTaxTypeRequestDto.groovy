package com.tmea.wit.model.dto.request

import javax.validation.constraints.Min
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

class ApiTaxTypeRequestDto {

    @NotBlank(message = "tax name cannot be blank")
    @Size(min = 3, max = 100)
    String name
    @NotNull
    @Min(value = 1l)
    Integer value
    @NotNull
    @Min(value = 1l)
    Long chargeModeId
}
