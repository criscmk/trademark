package com.tmea.wit.model.dto.request

import javax.validation.constraints.Min
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

class ApiCancelLogisticsTripDtoRequest {
    @NotNull
    @Min(value = 1l)
    Long tripId
    @Size(min = 3, max = 300)
    String comment
}
