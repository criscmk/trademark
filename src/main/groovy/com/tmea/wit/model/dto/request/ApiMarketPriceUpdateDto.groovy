package com.tmea.wit.model.dto.request


import javax.validation.constraints.NotNull

class ApiMarketPriceUpdateDto {

    @NotNull
    Long price;
}
