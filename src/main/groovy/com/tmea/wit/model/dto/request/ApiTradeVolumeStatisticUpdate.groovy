package com.tmea.wit.model.dto.request


import javax.validation.constraints.NotNull
import javax.validation.constraints.PositiveOrZero

class ApiTradeVolumeStatisticUpdate {


    @NotNull(message = "Quantity cannot be null")
    @PositiveOrZero(message = "Quantity cannot be of negative value")
    Long quantity
}
