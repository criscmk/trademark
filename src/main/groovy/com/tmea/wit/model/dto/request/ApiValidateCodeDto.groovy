package com.tmea.wit.model.dto.request

import javax.validation.constraints.*

class ApiValidateCodeDto {

    @NotNull
    @Max(Long.MAX_VALUE)
    @Min(1L)
    Long userId;

    @NotBlank
    @Size(max = 10,min=3)
    String code;

}
