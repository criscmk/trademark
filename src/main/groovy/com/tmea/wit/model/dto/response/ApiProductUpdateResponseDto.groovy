package com.tmea.wit.model.dto.response

class ApiProductUpdateResponseDto {
    Long id
    String name
    String description
    Double price
    Integer minimumOrderQuantity
    String measurementUnit
}
