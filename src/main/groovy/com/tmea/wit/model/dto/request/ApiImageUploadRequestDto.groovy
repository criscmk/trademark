package com.tmea.wit.model.dto.request

import javax.validation.constraints.NotNull
import javax.validation.constraints.Pattern

class ApiImageUploadRequestDto {
    @NotNull
    @Pattern(regexp = "data:image\\/([a-zA-Z]*);base64,(.*)",message = "Must be a base64 encoded image")
    String image
}
