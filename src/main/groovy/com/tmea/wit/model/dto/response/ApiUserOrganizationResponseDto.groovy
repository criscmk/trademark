package com.tmea.wit.model.dto.response

class ApiUserOrganizationResponseDto extends ApiUserBaseResponseDto {

    String organizationName;
    String postalAddress;
    String physicalAddress;
    String website;
    String organizationType;
    String organizationSize;

}
