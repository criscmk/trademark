package com.tmea.wit.model.dto.request

import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

class ApiAddChargingModeRequestDto {
    @NotBlank
    @Size(min = 3, max = 200)
    String name;

}
