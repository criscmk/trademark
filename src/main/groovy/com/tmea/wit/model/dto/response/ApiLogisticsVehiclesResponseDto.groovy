package com.tmea.wit.model.dto.response

class ApiLogisticsVehiclesResponseDto {
    Long id
    String plateNumber
    String description
    Long logisticsVehicleTypeId
    String logisticsVehicleType
    Boolean  isActive
    String providerName
    String providerPhone
    String providerEmail
    Long providerUserId

}
