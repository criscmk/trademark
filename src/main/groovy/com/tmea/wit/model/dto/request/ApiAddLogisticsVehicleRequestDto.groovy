package com.tmea.wit.model.dto.request

import javax.validation.constraints.*

class ApiAddLogisticsVehicleRequestDto {
    @NotBlank(message = "plate number cannot be blank")
    @Size(min = 3, max = 50)
    String plateNumber
    @NotBlank(message = "description cannot be blank")
    @Size(min = 3, max = 200)
    String description
    @NotNull
    @Max(Long.MAX_VALUE)
    @Min(1L)
    Long logisticsVehicleType;

}
