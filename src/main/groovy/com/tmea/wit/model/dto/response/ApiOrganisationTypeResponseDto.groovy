package com.tmea.wit.model.dto.response

class ApiOrganisationTypeResponseDto {
    Long id
    String name
    Boolean isActive
}
