package com.tmea.wit.model.dto.request


import javax.validation.constraints.*

class ApiMarketOrderRating {
    @NotNull(message = "rating cannot be blank")
    @Min(value = 1l,message = "rating cannot be less than 1")
    @Max(value = 5l,message = "rating cannot be more than 5")
    Long rating
    @NotBlank(message = "user type must not be blank")
    @Size(min = 3, max = 100)
    String ratingUserType

}
