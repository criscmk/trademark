package com.tmea.wit.model.dto.response

class ApiLogisticsServiceTariffResponseDto {
    Long id
    Long logisticsServiceId
    String chargingMode
    String currency
    String measurementUnit
    String minimumOrder
    String maximumOrder
    List<ApiLogisticsServiceTariffChargesResponseDto> tariffCharges;

}
