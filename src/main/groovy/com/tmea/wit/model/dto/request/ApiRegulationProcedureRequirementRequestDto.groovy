package com.tmea.wit.model.dto.request

import javax.validation.constraints.Max
import javax.validation.constraints.Min
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

class ApiRegulationProcedureRequirementRequestDto {
    @NotNull
    @Min(1L)
    @Max(Long.MAX_VALUE)
    Long regulationProcedureRequirementType

    @NotNull
    @Min(1L)
    @Max(Long.MAX_VALUE)
    Long regulationProcedure

    @NotBlank
    String requirement
}
