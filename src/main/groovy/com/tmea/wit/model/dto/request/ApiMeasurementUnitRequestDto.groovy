package com.tmea.wit.model.dto.request


import javax.validation.constraints.Min
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

class ApiMeasurementUnitRequestDto {
    @NotBlank(message = "measurement unit name cannot be blank")
    @Size(min = 3, max = 50)
    String name
    @Size(min = 3, max = 200)
    String description
    @Size(min = 1, max = 20)
    String code
    @NotNull
    @Min(value = 1l)
    Long measurementMetricId
}
