package com.tmea.wit.model.dto.response

class ApiRegulatoryAgencyResponseDto {
    Long id
    String code
    String name
    String description
    String logoUrl
    String country
    String emailAddress
    String phoneNumber
    String websiteUrl
    String postalAddress
    String physicalAddress
    String isActive

    ApiRegulatoryAgencyResponseDto(){

    }
}
