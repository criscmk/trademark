package com.tmea.wit.model.dto.response

class APIFetchMarketPriceInquiryResponseDto {
    Long id
    String commodity
    String location
    Date date
    Long price
    String commodityCategory
    String commoditySubCategory
    String currency
    String currencySymbol
    String measurementUnit
}
