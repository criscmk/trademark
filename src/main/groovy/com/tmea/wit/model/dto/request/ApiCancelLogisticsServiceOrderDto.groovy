package com.tmea.wit.model.dto.request


import javax.validation.constraints.NotBlank
import javax.validation.constraints.Size

class ApiCancelLogisticsServiceOrderDto {
    @NotBlank(message = "Reason for Logistics Service Order Cancellation")
    @Size(min = 3, max = 200)
    String reason

}
