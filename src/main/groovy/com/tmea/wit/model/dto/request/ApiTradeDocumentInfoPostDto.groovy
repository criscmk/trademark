package com.tmea.wit.model.dto.request

import javax.validation.constraints.Max
import javax.validation.constraints.Min
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

class ApiTradeDocumentInfoPostDto {
    @NotBlank(message = "Name cannot be blank")
    @Size(min = 3, max = 100)
    String name
    @NotBlank(message = "Description cannot be blank")
    @Size(min = 3, max = 500)
    String description
    @NotNull(message = "Trade document type cannot be null")
    @Max(Long.MAX_VALUE)
    @Min(value = 1L, message = "Only positive integers allowed")
    Long documentTypeId
    @NotNull(message = "Trade operation cannot be null")
    @Max(Long.MAX_VALUE)
    @Min(value = 1L, message = "Only positive integers allowed")
    Long tradeOperationId
    @NotNull(message = "CountryFrom cannot be null")
    @Max(Long.MAX_VALUE)
    @Min(value = 1L, message = "Only positive integers allowed")
    Long countryFromId
    @NotNull(message = "CountryTo cannot be null")
    @Max(Long.MAX_VALUE)
    @Min(value = 1L, message = "Only positive integers allowed")
    Long countryToId
    @NotNull(message = "Commodity cannot be null")
    @Max(Long.MAX_VALUE)
    @Min(value = 1L, message = "Only positive integers allowed")
    Long commodityId
    List<ApiTradeDocumentSamplePostDto> documentSamples
}
