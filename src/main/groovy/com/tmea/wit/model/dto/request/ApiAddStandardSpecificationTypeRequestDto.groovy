package com.tmea.wit.model.dto.request

import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

class ApiAddStandardSpecificationTypeRequestDto {
    @NotBlank(message = "name cannot be null")
    @Size(min = 3, max = 200)
    String name
    @NotBlank(message = "name cannot be null")
    String description


}
