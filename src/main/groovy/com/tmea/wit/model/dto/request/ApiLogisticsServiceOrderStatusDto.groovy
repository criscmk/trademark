package com.tmea.wit.model.dto.request


import com.tmea.wit.constants.ApiLogisticServiceOrderSStatusOptions
import com.tmea.wit.validators.EnumValidator

import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

class ApiLogisticsServiceOrderStatusDto {
    @NotNull
    @Size(min = 6, max = 10)
    @EnumValidator(message = "Approval needs to be  DELIVERED or DISPATCHED", enumClazz = ApiLogisticServiceOrderSStatusOptions.class)
    String status
}