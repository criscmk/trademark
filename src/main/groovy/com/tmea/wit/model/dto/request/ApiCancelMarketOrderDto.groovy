package com.tmea.wit.model.dto.request

import javax.validation.constraints.NotBlank
import javax.validation.constraints.Size

class ApiCancelMarketOrderDto {
    @NotBlank(message = "Cancellation Reason is required")
    @Size(min = 3, max = 200)
    String reason
}
