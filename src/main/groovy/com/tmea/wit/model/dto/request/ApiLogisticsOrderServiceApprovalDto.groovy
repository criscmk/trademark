package com.tmea.wit.model.dto.request

import com.tmea.wit.constants.ApiLogisticsOrderServiceApprovalOptions
import com.tmea.wit.validators.EnumValidator

import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

class ApiLogisticsOrderServiceApprovalDto {
    @NotNull
    @Size(min = 6, max = 10)
    @EnumValidator(message = "Approval needs to be APPROVED or REJECTED", enumClazz = ApiLogisticsOrderServiceApprovalOptions.class)
    String approval
    @NotBlank(message = "approval description  cannot be blank")
    @Size(min = 3, max = 300)
    String comment
}
