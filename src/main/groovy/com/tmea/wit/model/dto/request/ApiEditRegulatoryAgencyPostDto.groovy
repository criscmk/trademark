package com.tmea.wit.model.dto.request

import org.hibernate.validator.constraints.URL

import javax.validation.constraints.Email
import javax.validation.constraints.Size

class ApiEditRegulatoryAgencyPostDto {
    @Size(min = 2, max = 10)
    String code
    @Size(min = 3, max = 100)
    String name
    @Size(min = 3, max = 500)
    String description
    @Size(min = 10)
    String logoUrl
    @Email
    @Size(max = 50,message = "Email field cannot be longer that 50 characters")
    String emailAddress
    @Size(max = 20,message = "Phone number field cannot be longer that 20 characters")
    String phoneNumber
    @URL
    String websiteUrl
    @Size(min = 3,max = 50)
    String postalAddress
    @Size(min = 3,max = 50)
    String physicalAddress
}
