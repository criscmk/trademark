package com.tmea.wit.model.dto.request

import javax.validation.constraints.Max
import javax.validation.constraints.Min
import javax.validation.constraints.NotNull

class ApiUpdateRegulationRequestDto {

    @Max(Long.MAX_VALUE)
    @Min(1L)
    Long tradeOperation;

    @Max(Long.MAX_VALUE)
    @Min(1L)
    Long subCategory;

    @Max(Long.MAX_VALUE)
    @Min(1L)
    Long countryFrom;

    @Max(Long.MAX_VALUE)
    @Min(1L)
    Long countryTo;
}
