package com.tmea.wit.model.dto.request


import javax.validation.constraints.Size

class ApiLogisticsServiceUpdatePostDto {

    @Size(min = 0, max = 100)
    String name

    @Size(min = 0, max = 200)
    String description
}
