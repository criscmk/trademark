package com.tmea.wit.model.dto.response

class ApiCommodityResponseDto {
    Long id
    String name
    String description
    Long commoditySubCategoryId
    String commoditySubCategoryName
    Long commodityCategoryId
    String commodityCategoryName
    Boolean isActive
    Long addedBy
    Date dateAdded
    String imageUrl
    Long marketPriceMeasurementUnitId
    String marketPriceMeasurementUnit
    Long tradeStatisticsMeasurementUnitId
}
