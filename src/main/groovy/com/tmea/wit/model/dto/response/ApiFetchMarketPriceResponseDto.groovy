package com.tmea.wit.model.dto.response

class ApiFetchMarketPriceResponseDto {
    Long id
    String commodity
    String location
    Long price
    String date
    String commodityCategory
    String commoditySubCategory
    String currency
    String currencySymbol
    String measurementUnit
}
