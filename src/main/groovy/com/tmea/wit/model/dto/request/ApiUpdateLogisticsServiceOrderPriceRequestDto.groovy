package com.tmea.wit.model.dto.request

import javax.validation.constraints.NotNull

class ApiUpdateLogisticsServiceOrderPriceRequestDto {
    @NotNull(message ="Logistics service price is required")
    Double price;

}
