package com.tmea.wit.model.dto.response

class ApiLocationResponseDto {
    Long id
    String name
    String codeName
    Long levelId
    Boolean  isActive
    Long addedBy
    Date dateAdded
}
