package com.tmea.wit.model.dto.request

import javax.validation.constraints.Max
import javax.validation.constraints.Min
import javax.validation.constraints.NotNull

class ApiAnswerMarketPriceInquiryDto {
    @NotNull
    @Max(Long.MAX_VALUE)
    @Min(1L)
    Long inquiry;
    @NotNull(message ="Commodity Price is required")
    Double price;



}
