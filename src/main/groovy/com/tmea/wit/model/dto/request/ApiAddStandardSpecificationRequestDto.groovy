package com.tmea.wit.model.dto.request

import javax.validation.constraints.Max
import javax.validation.constraints.Min
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

class ApiAddStandardSpecificationRequestDto {
    @NotBlank(message = "name cannot be null")
    @Size(min = 3, max = 200)
    String title
    @NotBlank(message = "name cannot be null")
    String description
    @NotNull(message = "standard id is required")
    @Max(Long.MAX_VALUE)
    @Min(1L)
    Long standard
    @NotNull(message = "specification type id is required")
    @Max(Long.MAX_VALUE)
    @Min(1L)
    Long standardSpecificationType

}
