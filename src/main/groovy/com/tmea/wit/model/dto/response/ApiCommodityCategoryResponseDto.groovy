package com.tmea.wit.model.dto.response

class ApiCommodityCategoryResponseDto {
    Long id
    String name
    String description
    Boolean isActive
    Long addedBy
    Date dateAdded
    String imageUrl
}
