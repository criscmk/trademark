package com.tmea.wit.model.dto.request

import javax.validation.constraints.NotBlank
import javax.validation.constraints.Size

class ApiImportExportLocationPostDto {
    @NotBlank(message = "Location name cannot be blank")
    @Size(min = 3, max = 100)
    String name
    @NotBlank(message = "Location code cannot be blank")
    @Size(min = 2, max = 20)
    String code
    @NotBlank(message = "Location description cannot be blank")
    @Size(min = 3, max = 200)
    String description

}
