package com.tmea.wit.model.dto.request

class ApiAddProductImagePostDto {
    List<ApiImageUploadRequestDto> images
}
