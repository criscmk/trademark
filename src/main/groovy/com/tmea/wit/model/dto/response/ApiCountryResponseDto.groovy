package com.tmea.wit.model.dto.response

class ApiCountryResponseDto {
    Long id
    String name
    String codeName
    String defaultLanguage
    Boolean isActive
}
