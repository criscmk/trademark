package com.tmea.wit.model.dto.request

import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

class ApiRegulationProcedureRequirementTypeRequestDto {
    @NotBlank(message = " name cannot be blank")
    @Size(min = 3, max = 100)
    String name
    @NotBlank(message = "description cannot be null")
    @Size(min = 3, max = 200)
    String description
}
