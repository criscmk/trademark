package com.tmea.wit.model.dto.request

import javax.validation.constraints.NotBlank
import javax.validation.constraints.Size

class ApiUpdateRegulationCategoryRequestDto {

    @Size(min = 3, max = 100)
    String title

    @Size(min = 3, max = 200)
    String description
}
