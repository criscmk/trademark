package com.tmea.wit.model.dto.request

import javax.validation.constraints.Max
import javax.validation.constraints.Min
import javax.validation.constraints.NotNull

class ApiAssignOrderToVehicleRequestDto {
    @NotNull
    @Max(Long.MAX_VALUE)
    @Min(1L)
    Long tripId;
    @NotNull
    @Max(Long.MAX_VALUE)
    @Min(1L)
    Long orderId;
}
