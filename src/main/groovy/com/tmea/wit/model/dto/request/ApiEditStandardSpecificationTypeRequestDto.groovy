package com.tmea.wit.model.dto.request

import javax.validation.constraints.NotBlank
import javax.validation.constraints.Size

class ApiEditStandardSpecificationTypeRequestDto {

    String name
    String description
}