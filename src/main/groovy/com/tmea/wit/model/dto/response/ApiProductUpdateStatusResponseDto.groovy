package com.tmea.wit.model.dto.response

class ApiProductUpdateStatusResponseDto {
    Long id
    String name
    String description
    Boolean isActive
    Boolean isFeatured
    Boolean isInStock
    Boolean isPublished
    Boolean isApproved
}
