package com.tmea.wit.model.dto.response

class ApiLanguageResponseDto {
    Long id
    String name
    String codeName
    Boolean isActive
}
