package com.tmea.wit.model.dto.request

import javax.validation.constraints.NotNull

class ApiProductStatusUpdateRequestDto {

    @NotNull
    Boolean status
}
