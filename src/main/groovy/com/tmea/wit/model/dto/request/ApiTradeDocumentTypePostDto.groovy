package com.tmea.wit.model.dto.request

import javax.validation.constraints.Max
import javax.validation.constraints.Min
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

class ApiTradeDocumentTypePostDto {
    @NotBlank(message = "Trade document type name cannot be blank")
    @Size(min = 3, max = 50)
    String name
    @NotNull(message = "Trade document category cannot be null")
    @Max(Long.MAX_VALUE)
    @Min(value = 1L, message = "Only positive integers allowed")
    Long documentCategoryId
}
