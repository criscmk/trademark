package com.tmea.wit.model.dto.request

import javax.validation.constraints.NotBlank
import javax.validation.constraints.Size

class ApiDeliveryOptionRequestDto {

    @NotBlank(message = "delivery option name cannot be blank")
    @Size(min = 3, max = 100)
    String name
    @Size(min = 3, max = 200)
    String description
}
