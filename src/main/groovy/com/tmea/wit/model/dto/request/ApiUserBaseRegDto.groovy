package com.tmea.wit.model.dto.request;

import com.tmea.wit.constants.AccountCommunicationModes;
import com.tmea.wit.validators.EnumValidator;

import javax.validation.constraints.*;

public class ApiUserBaseRegDto {

    @NotBlank(message ="Email cannot be null or blank" )
    @Email
    @Size(max = 50,message = "Email field cannot be longer that 50 characters")
    String email;
    @NotBlank(message = "Phone number cannot be null  or blank")
    @Size(max = 20,message = "Phone number field cannot be longer that 20 characters")
    String phoneNumber;
    @NotBlank(message ="Password cannot be null  or blank")
    String password;
    @NotBlank(message ="Confirm password cannot be null  or blank")
    String confirmPassword;
    @NotNull
    @Min(value = 1l,message = "Only positive integers allowed for country field")
    Long country;
    @NotNull
    @Min(value = 1l,message = "Only positive integers allowed for country field")
    Long languagePreference;
    @NotNull
    @Min(value = 1l,message = "Only positive integers allowed for country field")
    Long location;
    @NotNull(message = "Value cannot be null")
    @EnumValidator(message = "Verification mode needs to be EMAIL or SMS",enumClazz = AccountCommunicationModes.class)
    String verificationMode;
    @NotNull(message = "Role must have a value")
    @Min(value = 1l,message = "Only positive integers allowed for role field")
    Long role;


}
