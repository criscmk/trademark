package com.tmea.wit.model.dto.response

class ApiFetchMarketOrderResponseDto {
    Long id
    String product
    String commodity
    String description
    Double unitPrice
    Integer quantityOrdered
    Double totalPrice
    String customerComments
    Double marketOrderRating
    String marketOrderStatus
    String measurementUnit
    String soldByPhone
    String soldByEmail
    String soldByName
    String requestedByPhone
    String requestedByEmail
    String requestedByName
    String dateRequested
}
