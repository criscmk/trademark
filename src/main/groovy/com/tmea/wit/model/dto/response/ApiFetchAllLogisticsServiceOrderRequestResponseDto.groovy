package com.tmea.wit.model.dto.response

class ApiFetchAllLogisticsServiceOrderRequestResponseDto {
    Long id
    String logisticsServiceName
    Long quantity
    Long price
    Long marketOrderId
    String status

    String requestedByName
    String requestedByPhone
    String requestedByEmail
    Date dateRequested
}
