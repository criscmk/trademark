package com.tmea.wit.model.dto.response

class ApiFetchLogisticsTripResponseDto {
    Long id
    String status
    Boolean isActive
    String vehiclePlateNumber
}
