package com.tmea.wit.model.dto.request

import javax.validation.constraints.Max
import javax.validation.constraints.Min
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

class ApiAddMarketOrderRequestDto {
    @NotNull
    @Max(Long.MAX_VALUE)
    @Min(1L)
    Long product;
    @NotNull
    Double quantity;
    @Size(min = 3, max = 200)
    String comment


}
