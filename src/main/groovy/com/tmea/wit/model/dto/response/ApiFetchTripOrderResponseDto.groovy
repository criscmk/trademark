package com.tmea.wit.model.dto.response

class ApiFetchTripOrderResponseDto {
    Long id
    String orderNumber
    String assignedVehicle
    String tripStatus

}
