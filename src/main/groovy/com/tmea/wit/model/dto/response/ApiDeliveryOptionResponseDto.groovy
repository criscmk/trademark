package com.tmea.wit.model.dto.response

class ApiDeliveryOptionResponseDto {
    Long id
    String name
    String description
    Boolean isActive
    Long addedBy
    Date dateAdded
}
