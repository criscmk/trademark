package com.tmea.wit.model.dto.request

import javax.validation.constraints.NotBlank


class ApiPasswordResetRequestDto {

    @NotBlank
    String phoneNumber;

}
