package com.tmea.wit.model.dto.request

import javax.validation.constraints.*

class ApiLogisticServicePostDto {

    @NotBlank(message = "Service name cannot be blank")
    @Size(min = 3, max = 100)
    String serviceName

    @NotBlank(message = "Service description cannot be blank")
    @Size(min = 3, max = 200)
    String description

    @NotNull(message = "Location from cannot be null")
    @Max(Long.MAX_VALUE)
    @Min(1L)
    Long locationFromId

    @NotNull(message = "Location to cannot be null")
    @Max(Long.MAX_VALUE)
    @Min(1L)
    Long locationToId

    @NotNull
    ApiLogisticServiceTariffPostDto serviceTariff

    List<ApiLogisticServiceAddedChargesPostDto> addedCharges
}
