package com.tmea.wit.model.dto.response

class ApiMeasurementMetricResponseDto {
    Long id
    String name
    String description
    String code
    Boolean isActive
    Long addedBy
    Date dateAdded
}
