package com.tmea.wit.model.dto.request


import javax.validation.constraints.Min
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

class ApiLocationRequestDto {
    @NotBlank(message = "location name cannot be blank")
    @Size(min = 3, max = 100)
    String name
    @Size(min = 3, max = 200)
    String codeName
    @NotNull
    @Min(value = 1l)
    Long levelId
}
