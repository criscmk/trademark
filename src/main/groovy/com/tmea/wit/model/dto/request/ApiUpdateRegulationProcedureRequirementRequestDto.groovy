package com.tmea.wit.model.dto.request

import javax.validation.constraints.Max
import javax.validation.constraints.Min
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull

class ApiUpdateRegulationProcedureRequirementRequestDto {
    @Min(1L)
    @Max(Long.MAX_VALUE)
    Long regulationProcedureRequirementType


    @Min(1L)
    @Max(Long.MAX_VALUE)
    Long regulationProcedure

    @NotBlank
    String requirement
}
