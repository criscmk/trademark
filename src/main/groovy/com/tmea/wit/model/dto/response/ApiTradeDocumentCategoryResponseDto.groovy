package com.tmea.wit.model.dto.response

class ApiTradeDocumentCategoryResponseDto {
    String id
    String name
    String dateAdded
}
