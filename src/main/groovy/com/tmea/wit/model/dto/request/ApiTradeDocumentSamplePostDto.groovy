package com.tmea.wit.model.dto.request

import javax.validation.constraints.NotBlank
import javax.validation.constraints.Size

class ApiTradeDocumentSamplePostDto {
    @NotBlank(message = "Name cannot be blank")
    @Size(min = 3, max = 100)
    String name
    @NotBlank
    String path
}
