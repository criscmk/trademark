package com.tmea.wit.model.dto.response

class ApiLocationLevelResponseDto {
    Long id
    String name
    Long parentId
    Boolean isActive
    Long addedBy
    Date dateAdded
}
