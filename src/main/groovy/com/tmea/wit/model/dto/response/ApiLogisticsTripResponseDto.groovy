package com.tmea.wit.model.dto.response

class ApiLogisticsTripResponseDto {
    Long id
    String status
    Boolean isActive
    String vehiclePlateNumber
}
