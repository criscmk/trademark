package com.tmea.wit.model.dto.response;

class ApiOrganisationSizeResponseDto {
    Long id
    String name
    Boolean isActive
}
