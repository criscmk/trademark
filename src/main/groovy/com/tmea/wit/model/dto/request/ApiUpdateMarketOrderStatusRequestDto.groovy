package com.tmea.wit.model.dto.request


import javax.validation.constraints.NotBlank
import javax.validation.constraints.Size

class ApiUpdateMarketOrderStatusRequestDto {
    @NotBlank(message = "new status is required")
    String status

    @Size(min = 3, max = 200)
    String comment
}
