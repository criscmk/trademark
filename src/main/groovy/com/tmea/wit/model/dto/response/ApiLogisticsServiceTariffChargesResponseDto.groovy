package com.tmea.wit.model.dto.response

class ApiLogisticsServiceTariffChargesResponseDto {

    Long id
    Long logisticsServiceTariffId
    String fromUnit
    String toUnit
    Double charge

    ApiLogisticsServiceTariffChargesResponseDto() {
    }
}
