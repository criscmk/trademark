package com.tmea.wit.model.dto.response

class ApiProductResponseDto {
    Long id
    String name
    String description
    BigDecimal price
    Integer minimumOrderQuantity
    String primaryImage
}
