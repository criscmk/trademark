package com.tmea.wit.model.dto.response

class ApiCommoditySubCategoryResponseDto {
    Long id
    String name
    String description
    Long commodityCategoryId
    String commodityCategoryName
    Boolean isActive
    Long addedBy
    Date dateAdded
    String imageUrl
}
