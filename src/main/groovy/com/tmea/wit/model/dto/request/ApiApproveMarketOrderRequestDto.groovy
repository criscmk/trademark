package com.tmea.wit.model.dto.request

import com.tmea.wit.constants.ApiLogisticsOrderServiceApprovalOptions
import com.tmea.wit.validators.EnumValidator

import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

class ApiApproveMarketOrderRequestDto {
    @NotNull
    @Size(min = 6, max = 10)
    @EnumValidator(message = "Approval needs to be APPROVED or REJECTED", enumClazz = ApiLogisticsOrderServiceApprovalOptions.class)
    String approval

}
