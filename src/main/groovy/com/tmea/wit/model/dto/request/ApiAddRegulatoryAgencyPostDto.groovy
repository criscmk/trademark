package com.tmea.wit.model.dto.request

import org.hibernate.validator.constraints.URL

import javax.validation.constraints.Email
import javax.validation.constraints.Max
import javax.validation.constraints.Min
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

class ApiAddRegulatoryAgencyPostDto {
    @Size(min = 2, max = 10)
    String code
    @NotBlank(message = "Regulatory agency name cannot be blank")
    @Size(min = 3, max = 100)
    String agencyName
    @NotBlank(message = "Description cannot be blank")
    @Size(min = 3, max = 500)
    String description
    @NotBlank(message = "Logo url cannot be blank")
    String logoUrl
    @NotNull(message = "Regulatory agency country cannot be blank")
    @Max(Long.MAX_VALUE)
    @Min(value = 1L, message = "Only positive integers allowed")
    Long countryId
    @NotBlank(message ="Email cannot be null or blank" )
    @Email
    @Size(max = 50,message = "Email field cannot be longer that 50 characters")
    String email
    @NotBlank(message = "Phone number cannot be null  or blank")
    @Size(max = 20,message = "Phone number field cannot be longer that 20 characters")
    String phoneNumber
    @URL
    String websiteUrl
    @Size(min = 3,max = 50)
    String postalAddress
    @Size(min = 3,max = 50)
    String physicalAddress

}
