package com.tmea.wit.model.dto.request

import com.fasterxml.jackson.annotation.JsonFormat

import javax.validation.constraints.Max
import javax.validation.constraints.Min
import javax.validation.constraints.NotNull
import javax.validation.constraints.Pattern

class ApiMarketPricePostDto {
    @NotNull
    @Max(Long.MAX_VALUE)
    @Min(1L)
    Long commodity;
    @NotNull
    @Max(Long.MAX_VALUE)
    @Min(1L)
    Long location;
    @NotNull(message ="Commodity Price is required")
    Double price;
    @NotNull()
    @Pattern(regexp ="\\d{4}\\-(0?[1-9]|1[012])\\-(0?[1-9]|[12]\\d|3[01])",message = "Invalid date format . valid date format is YYYY-MM-DD")
    @JsonFormat(pattern = "yyyy-mm-dd")
    String date;




}
