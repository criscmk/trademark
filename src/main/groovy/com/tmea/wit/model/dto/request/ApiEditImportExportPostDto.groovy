package com.tmea.wit.model.dto.request


import javax.validation.constraints.Size

class ApiEditImportExportPostDto {

    @Size(min = 3, max = 100)
    String name

    @Size(min = 2, max = 20)
    String code

    @Size(min = 3, max = 200)
    String description
}
