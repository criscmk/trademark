package com.tmea.wit.model.dto.request

import javax.validation.constraints.Min
import javax.validation.constraints.NotNull

class ApiCreateLogisticsTripRequestDto {
    @NotNull
    @Min(value = 1l)
    Long vehicleId

}
