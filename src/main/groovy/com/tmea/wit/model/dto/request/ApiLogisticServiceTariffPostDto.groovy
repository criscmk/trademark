package com.tmea.wit.model.dto.request

import javax.validation.constraints.Max
import javax.validation.constraints.Min
import javax.validation.constraints.NotNull

class ApiLogisticServiceTariffPostDto {

    @NotNull(message = "Charging mode is mandatory")
    @Max(Long.MAX_VALUE)
    @Min(1L)
    Long chargingModeId

    @NotNull(message = "Currency is mandatory")
    @Max(Long.MAX_VALUE)
    @Min(1L)
    Long currencyId

    @NotNull(message = "Measurement unit is mandatory")
    @Max(Long.MAX_VALUE)
    @Min(1L)
    Long measurementUnitId

    @NotNull(message = "Minimum order required")
    Integer minimumOrder

    @NotNull(message = "Maximum order required")
    Integer maximumOrder

    List<ApiLogisticServiceTariffChargePostDto> serviceTariffCharges
}
