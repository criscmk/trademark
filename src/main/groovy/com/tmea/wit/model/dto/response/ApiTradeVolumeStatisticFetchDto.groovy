package com.tmea.wit.model.dto.response

class ApiTradeVolumeStatisticFetchDto {
    String commodity
    String tradeOperation
    String categoryName
    String subCategoryName
    String fromCountry
    String toCountry
    String quarter
    Long quantity
    String month
    Integer year


    ApiTradeVolumeStatisticFetchDto() {

    }
}
