package com.tmea.wit.model.dto.request

import javax.validation.constraints.Max
import javax.validation.constraints.Min
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

class ApiAddRegulationProcedureRequestDto {
    @NotBlank(message = "title option name cannot be blank")
    @Size(min = 3, max = 100)
    String title
    @NotBlank(message = "description option name cannot be blank")
    String description
    @NotNull
    @Max(Long.MAX_VALUE)
    @Min(1L)
    Long regulation;
    @Size(max = 255)
    String applicationUrl
    @NotNull
    @Max(Long.MAX_VALUE)
    @Min(1L)
    Long regulationCategory;

    @Max(Long.MAX_VALUE)
    @Min(1L)
    double cost

    @Size( max = 200)
    String timeFrame

    @Size( max = 250)
    String paymentProcess

    @NotNull
    @Max(Long.MAX_VALUE)
    @Min(1L)
    int priority

    @NotNull(message = "application type option name cannot be blank")
    Boolean applyOnline


}
