package com.tmea.wit.model.dto.response

class ApiLogisticsVehicleTypeResponseDto {
    Long id
    String name
    Long addedBy
    Date dateAdded
    Boolean isActive
}
