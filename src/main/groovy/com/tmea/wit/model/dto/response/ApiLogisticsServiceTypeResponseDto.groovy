package com.tmea.wit.model.dto.response

class ApiLogisticsServiceTypeResponseDto {
    Long id
    String name
    String description
    Long addedBy
    Date dateAdded
    Boolean isActive
}
