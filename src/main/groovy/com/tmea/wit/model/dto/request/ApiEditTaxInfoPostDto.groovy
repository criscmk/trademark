package com.tmea.wit.model.dto.request

import javax.validation.constraints.Max
import javax.validation.constraints.Min
import javax.validation.constraints.Size

class ApiEditTaxInfoPostDto {

    @Size(min = 3)
    String description
    @Size(min = 3)
    String basisOfCharge
    @Size(min = 3)
    String taxableGood
    @Size(min = 3)
    String rate
    @Size(min = 3)
    String exemption
    @Size(min = 3)
    String additionalInformation
    @Max(Long.MAX_VALUE)
    @Min(value = 1L, message = "Only positive integers allowed")
    Long regulatoryAgencyId
}
