package com.tmea.wit.model.dto.response

import com.tmea.wit.model.dto.request.ApiImageUploadRequestDto

class ApiProductDetailsResponseDto {
    Long id
    String name
    String description
    BigDecimal price
    Integer minimumOrderQuantity
    String commodityName
    String measurementUnit
    String language
    String traderName
    String primaryImage
    Long traderId
    Long categoryId
    Long commodityId
    Long subCategoryId
    String categoryName
    String subCategoryName
    Boolean isPostToInventory
    Boolean isUpdateSales
    Boolean isApproved
    String dateAdded
    Boolean isInStock
    Boolean isPublished
    Boolean isFeatured
    Boolean isActive
    List<ApiProductImageResponseDto> images
}
