package com.tmea.wit.model.dto.request

import javax.validation.constraints.Max
import javax.validation.constraints.Min
import javax.validation.constraints.NotNull

public class ApiResendVerificationCodeDto {

    @NotNull
    @Max(Long.MAX_VALUE)
    @Min(1L)
    Long userId;

}
