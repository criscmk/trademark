package com.tmea.wit.model.dto.response

class ApiDocumentTypeResponseDto {
    Long id
    String name
    String documentCategory
    String addedBy
    String dateAdded
    boolean isActive
}
