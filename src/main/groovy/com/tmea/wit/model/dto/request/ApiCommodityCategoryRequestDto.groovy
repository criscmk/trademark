package com.tmea.wit.model.dto.request

import javax.validation.constraints.NotBlank
import javax.validation.constraints.Pattern
import javax.validation.constraints.Size

class ApiCommodityCategoryRequestDto {

    @NotBlank(message = "Category name cannot be blank")
    @Size(min = 3, max = 100)
    String name
    @Size(min = 3, max = 200)
    String description
    @Pattern(regexp = "data:image\\/([a-zA-Z]*);base64,(.*)",message = "Must be a base64 encoded image")
    String image
}
