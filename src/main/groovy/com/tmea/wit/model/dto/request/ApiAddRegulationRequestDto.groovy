package com.tmea.wit.model.dto.request

import javax.validation.constraints.Max
import javax.validation.constraints.Min
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

class ApiAddRegulationRequestDto {
    @NotNull
    @Max(Long.MAX_VALUE)
    @Min(1L)
    Long tradeOperation;
    @NotNull
    @Max(Long.MAX_VALUE)
    @Min(1L)
    Long subCategory;
    @NotNull
    @Max(Long.MAX_VALUE)
    @Min(1L)
    Long countryFrom;
    @NotNull
    @Max(Long.MAX_VALUE)
    @Min(1L)
    Long countryTo;


}
