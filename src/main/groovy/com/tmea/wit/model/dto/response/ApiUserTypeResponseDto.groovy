package com.tmea.wit.model.dto.response;

class ApiUserTypeResponseDto {
    Long id;
    String name;
    Boolean isActive;
}
