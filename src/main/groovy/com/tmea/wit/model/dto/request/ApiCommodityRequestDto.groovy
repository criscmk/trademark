package com.tmea.wit.model.dto.request


import javax.validation.constraints.Min
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull
import javax.validation.constraints.Pattern
import javax.validation.constraints.Size

class ApiCommodityRequestDto {
    @NotBlank(message = "commodity name cannot be blank")
    @Size(min = 3, max = 100)
    String name
    @Size(min = 3, max = 200)
    String description
    @NotNull
    @Min(value = 1l)
    Long commoditySubCategoryId
    @Pattern(regexp = "data:image\\/([a-zA-Z]*);base64,(.*)",message = "Must be a base64 encoded image")
    String image
    @NotNull
    @Min(value = 1L)
    Long marketPriceMeasurementUnitId
    @NotNull
    @Min(value = 1L)
    Long tradeStatisticsMeasurementUnitId
}
