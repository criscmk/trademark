package com.tmea.wit.model.dto.request

import javax.validation.constraints.DecimalMin
import javax.validation.constraints.Max
import javax.validation.constraints.Min
import javax.validation.constraints.NotNull

class ApiLogisticServiceAddedChargesPostDto {

    @NotNull(message = "Logistics service type cannot be null")
    @Max(Long.MAX_VALUE)
    @Min(1L)
    Long serviceTypeId

    @DecimalMin(value = "0.0", inclusive = false, message = "Amount cannot be blank")
    Double amount
}
