package com.tmea.wit.model.dto.response

class ApiFetchLogisticsServiceOrderResponseDto {
    Long id
    String OrderNumber
    String OrderedService
    Boolean isPaymentComplete
    Boolean isActive
    Long logisticsServiceId
    String logisticsServiceProviderId
    String orderStatus
    Long price
    String orderedByName
    String requestedByPhone
    String requestedByEmail
    Date dateApproved

}
