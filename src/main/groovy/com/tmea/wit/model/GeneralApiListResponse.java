package com.tmea.wit.model;


import com.tmea.wit.model.dto.FieldErrorDto;

import java.util.List;

public class GeneralApiListResponse extends  BaseApiResponse {

    private Long total;
    private List<?> data;

    public GeneralApiListResponse() {
    }

    public GeneralApiListResponse(List<?> data, Long total, Integer status, String message) {
        this.data = data;
        this.total = total;
        this.status = status;
        this.message = message;
    }

    public GeneralApiListResponse(List<?> data, Long total, Integer status, String message, List<FieldErrorDto> errors) {
        this.data = data;
        this.total = total;
        this.status = status;
        this.message = message;
        this.errors = errors;
    }

    @Override
    public List<?> getData() {
        return data;
    }

    public void setData(List<?> data) {
        this.data = data;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }
}
