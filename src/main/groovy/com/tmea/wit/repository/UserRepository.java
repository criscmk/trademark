package com.tmea.wit.repository;

import com.tmea.wit.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Long>{

   @Query(value = "SELECT * FROM \"user\" WHERE email ILIKE ?1 OR phone_number ILIKE ?1 OR username ILIKE ?1",nativeQuery = true)
    UserEntity findUserByDetails(String username);

    @Query(value = "SELECT id FROM \"user\" WHERE email ILIKE ?1 OR phone_number ILIKE ?1 OR username ILIKE ?1",nativeQuery = true)
    Long findUserIdByDetails(String username);

}
