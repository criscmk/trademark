package com.tmea.wit.repository;

import com.tmea.wit.entity.PermissionEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PermissionRepository extends JpaRepository<PermissionEntity,Long>{


    @Query(value = "SELECT  permission.* FROM permission,role_permission,user_role_allocation,\"user\" WHERE user_role_allocation.role_id = role_permission.role_id AND user_role_allocation.user_id = \"user\".id AND role_permission.permission_id = permission.id AND (\"user\".email ILIKE ?1 OR \"user\".phone_number ILIKE ?1 OR \"user\".username ILIKE ?1)",nativeQuery = true)
    List<PermissionEntity> findByRoleByUsername(String username);

}
