package com.tmea.wit.constants;

public enum ApiLogisticServiceOrderSStatusOptions {
    DELIVERED,
    DISPATCHED
}
