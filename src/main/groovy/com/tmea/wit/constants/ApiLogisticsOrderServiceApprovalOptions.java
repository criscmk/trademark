package com.tmea.wit.constants;

public enum ApiLogisticsOrderServiceApprovalOptions {
    APPROVED,
    REJECTED
}
