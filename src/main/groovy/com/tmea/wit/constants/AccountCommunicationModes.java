package com.tmea.wit.constants;

public enum AccountCommunicationModes {
    EMAIL,
    SMS
}
