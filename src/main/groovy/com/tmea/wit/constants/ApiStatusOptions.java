package com.tmea.wit.constants;

public enum ApiStatusOptions {
    ACTIVE,
    INACTIVE
}
