alter table tax_information
    add is_active boolean default true;

alter table tax_information
    add added_by bigint;

alter table tax_information
    add date_added timestamp default current_timestamp;
