CREATE TABLE IF NOT EXISTS sales
(
    id              int              NOT NULL,
    created_on      timestamp default current_timestamp,
    updated_on      timestamp default current_timestamp,
    identifier      varchar(255)     NOT NULL,
    sale_id         varchar(255)     NOT NULL,
    item            varchar(50)      NOT NULL,
    quantity        int              NOT NULL,
    sale_unit_price double precision NOT NULL,
    sale_amount     double precision NOT NULL,
    sale_date       timestamp        NOT NULL,
    constraint sales_pk
        primary key (id)
);

create unique index sales_identifier_uindex
    on sales (identifier);