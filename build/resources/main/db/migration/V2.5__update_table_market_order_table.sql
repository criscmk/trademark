alter table market_order drop column is_processed;

alter table market_order
    add is_payment_complete boolean default false;
