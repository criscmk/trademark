alter table regulatory_agency
    add is_active boolean;

alter table regulatory_agency
    add added_by bigint;

