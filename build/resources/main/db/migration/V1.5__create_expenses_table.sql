CREATE TABLE IF NOT EXISTS expenses
(
    id           int              NOT NULL,
    created_on   timestamp default current_timestamp,
    updated_on   timestamp default current_timestamp,
    identifier   varchar(255)     NOT NULL,
    expense_id   varchar(255)     NOT NULL,
    category     varchar(50)      NOT NULL,
    description  varchar(50)      NOT NULL,
    amount       double precision NOT NULL,
    expense_date timestamp        NOT NULL,
    constraint expenses_pk
        primary key (id)
);

create unique index expenses_identifier_uindex
    on expenses (identifier);