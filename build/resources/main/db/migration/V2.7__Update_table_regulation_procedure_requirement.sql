alter table regulatory_procedure rename to regulation_procedure;

alter table regulation_procedure_requirement
    add "regulation_procedure_id " BIGINT;

alter table regulation_procedure_requirement
    add constraint regulation_procedure_requirement_regulation_procedure__fk
        foreign key ("regulation_procedure_id ") references regulation_procedure(id)
            on update cascade on delete cascade;