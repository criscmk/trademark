alter table product
    add is_in_stock boolean;

alter table product
    add is_published boolean;

alter table product
    add language_id bigint;

alter table product
    add constraint product_language_id_fkey
        foreign key (language_id) references language(id);

alter table product
    add is_featured boolean;

alter table product
    add is_active boolean;