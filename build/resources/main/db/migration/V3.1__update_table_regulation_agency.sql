alter table regulatory_agency alter column name type varchar(100) using name::varchar(100);

alter table regulatory_agency alter column description type text using description::text;

alter table regulatory_agency alter column logo_url type text using logo_url::text;

