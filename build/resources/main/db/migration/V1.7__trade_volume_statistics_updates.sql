ALTER TABLE trade_volume_statistic
    DROP COLUMN from_date,
    DROP COLUMN to_date;

ALTER TABLE trade_volume_statistic
    ADD COLUMN volume_month int,
    ADD COLUMN volume_year int;

alter table commodity
    add trade_statistics_measurement_unit_id bigint;

alter table commodity
    add constraint commodity_trade_statistics_measurement_unit_id_fk
        foreign key (trade_statistics_measurement_unit_id) references measurement_unit
            on update restrict on delete restrict;

