create table market_order_request_price_update
(
    id bigserial not null,
    market_order_request_id int,
    updated_price double precision,
    updated_by bigint,
    is_active boolean,
    date_updated timestamp default CURRENT_TIMESTAMP,
    constraint market_order_request_price_update_market_order_request__fk
        foreign key (market_order_request_id) references market_order_request(id)
            on update cascade on delete cascade,
    constraint market_order_request_price_update_user__fk
        foreign key (updated_by) references "user"(id)
            on update cascade on delete cascade
);

create unique index market_order_request_price_update_id_uindex
    on market_order_request_price_update (id);

alter table market_order_request_price_update
    add constraint market_order_request_price_update_pk
        primary key (id);


alter table market_order
    add is_active boolean default True;


alter table market_order
    add market_order_status_id bigint;

alter table market_order
    add order_number varchar;

alter table market_order
    add market_order_request_id bigint;

alter table market_order
    add constraint market_order_market_order_request__fk
        foreign key (market_order_request_id) references market_order_request(id)
            on update cascade on delete cascade;

create table market_order_request_updated_quantity
(
    id serial not null,
    updated_quantity int,
    updated_by int,
    date_updated timestamp default CURRENT_TIMESTAMP,
    constraint market_order_request_updated_quantity_user__fk
        foreign key (updated_by) references "user"(id)
            on update cascade on delete cascade
);

create unique index market_order_request_updated_quantity_id_uindex
    on market_order_request_updated_quantity (id);

alter table market_order_request_updated_quantity
    add constraint market_order_request_updated_quantity_pk
        primary key (id);

alter table market_order_rating drop column date_rated;
alter table market_order_rating
    add date_rated timestamp default current_timestamp;

alter table market_order_rating
    add constraint market_order_rating_user_type_id_fkey
        foreign key (rating_user_type_id) references rating_user_type;